package com.zarholding.zar

import androidx.lifecycle.ViewModel
import com.zar.core.enums.EnumApiError
import com.zar.core.models.ErrorApiModel
import com.zarholding.zar.di.Providers
import com.zarholding.zar.di.ResourcesProvider
import com.zarholding.zar.model.data.response.GeneralResponse
import com.zarholding.zar.model.data.response.NetworkResult
import com.zarholding.zar.tools.manager.CheckApiRequestManager
import com.zarholding.zar.tools.SingleLiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.Main
import retrofit2.Response
import javax.inject.Inject

/**
 * Created by m-latifi on 10/8/2022.
 */

@HiltViewModel
open class ZarViewModel @Inject constructor() : ViewModel() {

    @Inject
    lateinit var resourcesProvider: ResourcesProvider

    @Inject
    lateinit var checkApiRequestManager: CheckApiRequestManager

    val errorLiveDate: SingleLiveEvent<ErrorApiModel> by lazy { SingleLiveEvent<ErrorApiModel>() }

    //---------------------------------------------------------------------------------------------- checkResponse
    fun <T: Any> checkResponse(
        networkResult: NetworkResult<T>,
        onReceiveData: (T) -> Unit
    ) {
        when (networkResult) {
            is NetworkResult.Failure -> {
                val url = Providers.url.substringAfterLast("/").substringBefore(":")
                if (networkResult.errorMessage.message.contains(url))
                    setMessage(resourcesProvider.getString(R.string.pleaseCheckYouConnection))
                else
                    setMessage(message = networkResult.errorMessage.message, type = networkResult.errorMessage.type)
            }
            is NetworkResult.Success -> { onReceiveData.invoke(networkResult.data) }
        }
    }
    //---------------------------------------------------------------------------------------------- checkResponse


    //---------------------------------------------------------------------------------------------- callApi
    fun <T : Any> callApi(
        request: Response<GeneralResponse<T?>>?,
        showMessageAfterSuccessResponse: Boolean = false,
        onReceiveData: (T) -> Unit
    ) {
        checkApiRequestManager.handelApi(
            request = request,
            showMessageAfterSuccessResponse = showMessageAfterSuccessResponse,
            onReceiveData = {
                onReceiveData(it)
            },
            onError = {
                errorLiveDate.postValue(it)
            }
        )
    }
    //---------------------------------------------------------------------------------------------- callApi


    //---------------------------------------------------------------------------------------------- setMessage
    fun setMessage(message: String, type: EnumApiError = EnumApiError.Warning) {
        errorLiveDate.postValue(ErrorApiModel(type, message))
    }
    //---------------------------------------------------------------------------------------------- setMessage


    //---------------------------------------------------------------------------------------------- exceptionHandler
    fun exceptionHandler() = CoroutineExceptionHandler { _, throwable ->
        CoroutineScope(Main).launch {
            throwable.localizedMessage?.let {
                val url = Providers.url.substringAfterLast("/").substringBefore(":")
                if (it.contains(url))
                    setMessage(resourcesProvider.getString(R.string.pleaseCheckYouConnection))
                else
                    setMessage(it)
            }
        }
    }
    //---------------------------------------------------------------------------------------------- exceptionHandler
}