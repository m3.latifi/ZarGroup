package com.zarholding.zar.model.data.response.survey

import com.zarholding.zar.model.data.enum.EnumSurveyStatus
import java.time.LocalDateTime

/**
 * Created by m-latifi on ۰۸/۰۲/۲۰۲۳.
 */
data class SurveyModel(
   val id : Long,
   val createDate : LocalDateTime,
   val title : String?,
   val description : String?,
   val startDate : LocalDateTime,
   val endDate : LocalDateTime,
   val needAuthenticate : Boolean,
   val status : EnumSurveyStatus,
   val companiesCode : String?,
   val unitCodes : String?,
   val jobKeyCodes : String?,
   val personnelNumbers : String?,
   val publishByUnit: String?,
   val isDelete : Boolean
)
