package com.zarholding.zar.model.data.response.insurance.history

/**
 * Created by m-latifi on 10/17/2023.
 */

data class SupplementalInsuranceHistoryModel(
    val id: Long,
    val title: String?,
    val startDate: String?,
    val endDate: String?,
    val fileName: String?,
    val levelTitle: String?,
    val amount: Long,
    val personnelFirstName: String?,
    val personnelLastName: String?,
    val personnelNationalCode: String?,
    val personnelMobile: String?,
    val accountNumber: String,
    val dependentPersons: List<SupplementalInsuranceDependentPersonsModel>?
)
