package com.zarholding.zar.model.data.request

import com.zarholding.zar.model.data.enum.EnumTripStatus

/**
 * Create by Mehrdad on 1/7/2023
 */
data class DriverChangeTripStatus(
    val Id : Int,
    val TripStatus : com.zarholding.zar.model.data.enum.EnumTripStatus,
    val TripLat : Double,
    val TripLong : Double
)
