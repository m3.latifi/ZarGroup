package com.zarholding.zar.model.data.response.insurance

/**
 * Created by m-latifi on 10/15/2023.
 */

data class SupplementalInsuranceLevelModel(
    val id: Long,
    val title: String?,
    val amount: Long
)
