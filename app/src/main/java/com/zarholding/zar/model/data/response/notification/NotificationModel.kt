package com.zarholding.zar.model.data.response.notification

import com.zarholding.zar.model.data.enum.NotificationSystemType
import java.time.LocalDateTime

data class NotificationModel(
    val id: Int,
    var isRead: Boolean,
    val subject: String?,
    val message: String?,
    val systemType: NotificationSystemType,
    val senderId: Int,
    val senderName: String?,
    val senderPersonnelNumber: String?,
    val receiverId: Int,
    val status: String?,
    val createDate: LocalDateTime?,
    var select: Boolean
)