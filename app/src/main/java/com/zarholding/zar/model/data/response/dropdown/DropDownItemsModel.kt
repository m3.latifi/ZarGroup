package com.zarholding.zar.model.data.response.dropdown

data class DropDownItemsModel(
    val items : List<DropDownModel>?
)
