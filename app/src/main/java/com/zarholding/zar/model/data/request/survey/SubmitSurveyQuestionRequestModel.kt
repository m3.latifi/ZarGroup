package com.zarholding.zar.model.data.request.survey

/**
 * Created by zar on 2/19/2023.
 */

data class SubmitSurveyQuestionRequestModel(
    val surveyId: Long,
    val questionId: Long,
    val objectIds: List<Long>?,
    val additionalText: String?,
    val text: String?,
    val numericAnswer: Int = 0,
    val selectedDate: String?,
    val selectedTime: String?
)
