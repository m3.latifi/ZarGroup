package com.zarholding.zar.model.data.response.update


/**
 * create by m-latifi on 4/25/2023
 */

data class AppVersionModel(
    val id: Int,
    val currentVersion: Long,
    val fileName: String?
)
