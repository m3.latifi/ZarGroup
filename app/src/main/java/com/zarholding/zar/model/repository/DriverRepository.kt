package com.zarholding.zar.model.repository

import com.zar.core.tools.api.apiCall
import com.zarholding.zar.model.data.enum.EnumDriverType
import javax.inject.Inject

/**
 * Created by m-latifi on ۰۸/۰۲/۲۰۲۳.
 */

class DriverRepository @Inject constructor() : ZarRepository() {

    //---------------------------------------------------------------------------------------------- requestGetDriver
    suspend fun requestGetDriver(type: EnumDriverType, companyCode: String?) =
        apiCall { api.requestGetDriver(type, companyCode, tokenRepository.getBearerToken()) }
    //---------------------------------------------------------------------------------------------- requestGetDriver

}