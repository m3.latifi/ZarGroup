package com.zarholding.zar.model.data.response.survey

data class SurveyOptionModel(
    val questionOptionId: Long,
    val optionTitle: String?,
    val score: Int,
    val hasAdditionalText: Boolean,
    val rangeFrom: Int,
    val rangeTo: Int
)
