package com.zarholding.zar.model.data.request

import com.zarholding.zar.model.data.enum.EnumStatus

data class TripRequestRegisterStatusModel(
    val id: Int,
    val Status: EnumStatus,
    val Reason: String?
)