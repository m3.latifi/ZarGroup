package com.zarholding.zar.model.data.enum

enum class EnumPersonnelType {
    Administrative,
    Personnel,
    Driver
}