package com.zarholding.zar.model.data.enum

enum class EnumTaxiRequestTypePro {
    Departure,
    Return
}