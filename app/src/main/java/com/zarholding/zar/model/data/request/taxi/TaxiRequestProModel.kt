package com.zarholding.zar.model.data.request.taxi

import com.zarholding.zar.model.data.enum.EnumTaxiRequestType
import com.zarholding.zar.model.data.response.taxi.TaxiRequestPointModel

data class TaxiRequestProModel(
    val type : EnumTaxiRequestType,
    val departureDate : String,
    val departureTime : String,
    val returnDate : String?,
    val returnTime : String?,
    val passengers : List<Int>,
    val travelReason : String,
    val siteId : Int?,
    val personnelJobKeyCode : String?,
    val items: List<TaxiRequestPointModel>
)
