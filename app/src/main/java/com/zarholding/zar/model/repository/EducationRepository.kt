package com.zarholding.zar.model.repository

import com.zar.core.tools.api.apiCall
import com.zarholding.zar.model.data.response.GeneralResponse
import com.zarholding.zar.model.data.response.NetworkResult
import com.zarholding.zar.model.data.response.education.EducationDetailModel
import com.zarholding.zar.model.data.response.PagingListModel
import com.zarholding.zar.model.data.response.education.EducationModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import retrofit2.Response
import javax.inject.Inject

/**
 * Created by m-latifi on 7/29/2023.
 */

class EducationRepository @Inject constructor() : ZarRepository() {


    private var pageNumber = 0
    private val pageSize = 20

    //---------------------------------------------------------------------------------------------- getVideoCategory
    fun getVideoCategory() = flow {
        val response =
            api.getVideoCategory(tokenRepository.getBearerToken())
        if (response.data.isNullOrEmpty())
            emit(emitError(response.message))
        else
            emit(NetworkResult.Success(response.data))
    }.catch { e -> emit(handleFlowException(e)) }
    //---------------------------------------------------------------------------------------------- getVideoCategory


    //---------------------------------------------------------------------------------------------- getVideoList
    suspend fun getVideoList(videoGroupId: Long):
            Response<GeneralResponse<PagingListModel<EducationModel>?>>? {
        pageNumber++
        return apiCall { api.getVideoList(
            pageNumber = pageNumber,
            pagesize = pageSize,
            videoGroupId = videoGroupId,
            token = tokenRepository.getBearerToken()
        ) }
    }
    //---------------------------------------------------------------------------------------------- getVideoList


    //---------------------------------------------------------------------------------------------- getVideoDetail
    suspend fun getVideoDetail(videoId: Long):
            Response<GeneralResponse<PagingListModel<EducationDetailModel>?>>? {
        pageNumber++
        return apiCall { api.getVideoDetail(
            videoId = videoId,
            pageNumber = pageNumber,
            pagesize = pageSize,
            token = tokenRepository.getBearerToken()
        ) }
    }
    //---------------------------------------------------------------------------------------------- getVideoDetail

}