package com.zarholding.zar.model.data.other.notification

import com.zarholding.zar.model.data.response.notification.NotificationModel
import java.time.LocalDateTime

/**
 * Created by m-latifi on 11/16/2022.
 */

class NotificationCategoryModel(
    val name: String,
    val date : LocalDateTime?,
    val notifications: MutableList<NotificationModel>
)