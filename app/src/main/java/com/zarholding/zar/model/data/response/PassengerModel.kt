package com.zarholding.zar.model.data.response

data class PassengerModel(
    val key : String?,
    val value : String?
)
