package com.zarholding.zar.model.repository

import androidx.lifecycle.viewModelScope
import com.zarholding.zar.R
import com.zarholding.zar.model.data.database.entity.UserInfoEntity
import com.zarholding.zar.model.data.response.NetworkResult
import com.zarholding.zar.model.data.response.ResponseResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by m-latifi on ۰۸/۰۲/۲۰۲۳.
 */

class PassengerRepository @Inject constructor(
    private val userRepository: UserRepository
) : ZarRepository() {

    private val passengers = mutableListOf<UserInfoEntity>()

    //---------------------------------------------------------------------------------------------- getPassengers
    fun getPassengers() = passengers
    //---------------------------------------------------------------------------------------------- getPassengers


    //---------------------------------------------------------------------------------------------- addCurrentUserToPassenger
    fun addCurrentUserToPassenger() = flow {
        userRepository.getUser()?.let {
            passengers.add(it)
        }
        emit(passengers)
    }
    //---------------------------------------------------------------------------------------------- addCurrentUserToPassenger


    //---------------------------------------------------------------------------------------------- deletePassenger
    fun deletePassenger(item: UserInfoEntity) = flow {
        passengers.remove(item)
        emit(passengers)
    }
    //---------------------------------------------------------------------------------------------- deletePassenger


    //---------------------------------------------------------------------------------------------- addPersonToPassengers
    fun addPersonToPassengers(item: UserInfoEntity) = flow {
        val exist = passengers
            .find { userInfoEntity -> userInfoEntity.userName == item.userName }
        if (exist == null) {
            passengers.add(item)
            emit(NetworkResult.Success(passengers))
        } else {
            emit(emitError(resourcesProvider.getString(R.string.duplicateInsert)))
        }
    }
    //---------------------------------------------------------------------------------------------- addPersonToPassengers


    //---------------------------------------------------------------------------------------------- getPassengersId
    fun getPassengersId() = passengers.map { userInfoEntity -> userInfoEntity.id }
    //---------------------------------------------------------------------------------------------- getPassengersId


}