package com.zarholding.zar.model.data.enum

enum class EnumArticleType {
    Article,
    SlideShow
}