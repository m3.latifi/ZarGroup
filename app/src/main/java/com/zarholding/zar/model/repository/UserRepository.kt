package com.zarholding.zar.model.repository

import com.google.firebase.messaging.FirebaseMessaging
import com.zar.core.enums.EnumApiError
import com.zar.core.models.ErrorApiModel
import com.zar.core.tools.api.apiCall
import com.zar.core.tools.extensions.persianNumberToEnglishNumber
import com.zarholding.zar.model.data.database.dao.RoleDao
import com.zarholding.zar.model.data.database.dao.UserInfoDao
import com.zarholding.zar.model.data.database.entity.RoleEntity
import com.zarholding.zar.model.data.database.entity.UserInfoEntity
import com.zarholding.zar.model.data.other.ShowImageModel
import com.zarholding.zar.model.data.request.FilterUserRequestModel
import com.zarholding.zar.model.data.request.LoginRequestModel
import com.zarholding.zar.model.data.request.UserInfoRequest
import com.zarholding.zar.model.data.response.GeneralResponse
import com.zarholding.zar.model.data.response.NetworkResult
import com.zarholding.zar.model.data.response.user.UserItemsModel
import com.zarholding.zar.tools.manager.SharedPreferencesManager
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import retrofit2.Response
import javax.inject.Inject

/**
 * Created by m-latifi on 11/26/2022.
 */

class UserRepository @Inject constructor(
    private val userInfoDao: UserInfoDao,
    private val roleDao: RoleDao,
    private val sharedPreferencesManager: SharedPreferencesManager
) : ZarRepository() {

    private var pageNumber = 0
    private val pageSize = 20
    val filterUser = FilterUserRequestModel(pageNumber, pageSize, "")

    //---------------------------------------------------------------------------------------------- getSavedUserName
    fun getSavedUserName() = sharedPreferencesManager.getUserName()
    //---------------------------------------------------------------------------------------------- getSavedUserName


    //---------------------------------------------------------------------------------------------- getSavedPassword
    fun getSavedPassword() = sharedPreferencesManager.getPassword()
    //---------------------------------------------------------------------------------------------- getSavedPassword


    //---------------------------------------------------------------------------------------------- isBiometricEnable
    fun isBiometricEnable() = sharedPreferencesManager.isBiometricEnable()
    //---------------------------------------------------------------------------------------------- isBiometricEnable


    //---------------------------------------------------------------------------------------------- login
    fun login(
        userName: String,
        password: String
    ) = flow {
        val loginModel = LoginRequestModel(
            userName = userName.persianNumberToEnglishNumber(),
            password = password.persianNumberToEnglishNumber()
        )
        val response = api.requestLogin(login = loginModel)
        if (response.data.isNullOrEmpty())
            emit(
                NetworkResult.Failure(
                    ErrorApiModel(
                        type = EnumApiError.Error,
                        message = response.message
                    )
                )
            )
        else {
            val oldUser = sharedPreferencesManager.getUserName()
            if (!oldUser.equals(loginModel.userName))
                sharedPreferencesManager.changeBiometricEnable(biometric = false)

            sharedPreferencesManager.saveUserLogin(
                token = response.data,
                userName = loginModel.userName,
                password = loginModel.password
            )
        }
        delay(3000)
        emit(NetworkResult.Success(response.message))
    }.catch { e ->
        emit(handleFlowException(e))
    }
    //---------------------------------------------------------------------------------------------- login


    //---------------------------------------------------------------------------------------------- requestUserPermission
    fun requestUserPermission() = flow {
        val response = api.requestUserPermission(
            token = tokenRepository.getBearerToken()
        )
        if (response.data.isNullOrEmpty())
            emit(
                NetworkResult.Failure(
                    ErrorApiModel(
                        type = EnumApiError.Error,
                        message = response.message
                    )
                )
            )
        else {
            val roles: List<RoleEntity> = response.data.map { RoleEntity(it) }
            insertUserRole(roles)
            delay(2000)
            emit(NetworkResult.Success(true))
        }
    }.catch { e ->
        emit(handleFlowException(e))
    }
    //---------------------------------------------------------------------------------------------- requestUserPermission


    //---------------------------------------------------------------------------------------------- isUserLogin
    fun isUserLogin() = flow {
        if (!isLogged())
            emit(NetworkResult.Success(false))
        else {
            fireBaseToken()
            delay(2000)
            val response = api.requestGetUserInfo(
                fireBaseToken = sharedPreferencesManager.getFirebaseToken(),
                token = tokenRepository.getBearerToken()
            )
            if (response.data == null)
                emit(
                    NetworkResult.Failure(
                        ErrorApiModel(
                            type = EnumApiError.Error,
                            message = response.message
                        )
                    )
                )
            else {
                insertUserInfo(response.data)
                emit(NetworkResult.Success(true))
            }
        }
    }.catch { e ->
        emit(handleFlowException(e))
    }
    //---------------------------------------------------------------------------------------------- isUserLogin


    //---------------------------------------------------------------------------------------------- fireBaseToken
    private fun fireBaseToken() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener { task ->
            val newToken = if (!task.isSuccessful)
                ""
            else
                task.result
            sharedPreferencesManager.setFirebaseToken(newToken)
        }.addOnFailureListener {
            sharedPreferencesManager.setFirebaseToken("")
        }
    }
    //---------------------------------------------------------------------------------------------- fireBaseToken


    //---------------------------------------------------------------------------------------------- requestGetUserInfo
    fun requestGetUserInfo(id: Int) = flow {
        val request = UserInfoRequest(id, null)
        val response =
            api.requestGetUserInfo(request, tokenRepository.getBearerToken())
        if (response.data == null)
            emit(emitError(response.message))
        else
            emit(NetworkResult.Success(response.data))
    }.catch { e -> emit(handleFlowException(e)) }
    //---------------------------------------------------------------------------------------------- requestGetUserInfo


    //---------------------------------------------------------------------------------------------- requestGetUser
    suspend fun requestGetUser(search: String, token: String):
            Response<GeneralResponse<UserItemsModel?>>? {
        filterUser.Search = search.persianNumberToEnglishNumber()
        filterUser.PageNumber++
        return apiCall { api.requestGetUser(filterUser, token) }
    }
    //---------------------------------------------------------------------------------------------- requestGetUser


    //---------------------------------------------------------------------------------------------- insertUserInfo
    fun insertUserInfo(user: UserInfoEntity) {
        userInfoDao.insertUserInfo(user)
    }
    //---------------------------------------------------------------------------------------------- insertUserInfo


    //---------------------------------------------------------------------------------------------- getUser
    fun getUser(): UserInfoEntity? {
        return userInfoDao.getUserInfo()
    }
    //---------------------------------------------------------------------------------------------- getUser


    //---------------------------------------------------------------------------------------------- getUserType
    fun getUserType() = getUser()?.userType?.let { enumValueOf(it) }
        ?: run { com.zarholding.zar.model.data.enum.EnumPersonnelType.Personnel }
    //---------------------------------------------------------------------------------------------- getUserType


    //---------------------------------------------------------------------------------------------- insertUserRole
    private fun insertUserRole(roles: List<RoleEntity>) {
        roleDao.deleteAllRecord()
        roleDao.insert(roles)
    }
    //---------------------------------------------------------------------------------------------- insertUserRole


    //---------------------------------------------------------------------------------------------- deleteUser
    fun deleteUser() {
        userInfoDao.deleteAll()
        roleDao.deleteAllRecord()
    }
    //---------------------------------------------------------------------------------------------- deleteUser


    //---------------------------------------------------------------------------------------------- isLogged
    private fun isLogged() = tokenRepository.getToken()?.let {
        true
    } ?: false
    //---------------------------------------------------------------------------------------------- isLogged


    //---------------------------------------------------------------------------------------------- getBearerToken
    fun getBearerToken() = tokenRepository.getBearerToken()
    //---------------------------------------------------------------------------------------------- getBearerToken


    //---------------------------------------------------------------------------------------------- getModelForShowImageProfile
    fun getModelForShowImageProfile(userInfoEntity: UserInfoEntity?): ShowImageModel {
        val model = ShowImageModel("", null, null)
        userInfoEntity?.userName?.let {
            model.imageName = it
        }
        model.token = tokenRepository.getBearerToken()
        return model
    }
    //---------------------------------------------------------------------------------------------- getModelForShowImageProfile

}