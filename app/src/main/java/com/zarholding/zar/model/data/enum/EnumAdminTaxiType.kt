package com.zarholding.zar.model.data.enum

enum class EnumAdminTaxiType {
    REQUEST,
    HISTORY,
    MY
}