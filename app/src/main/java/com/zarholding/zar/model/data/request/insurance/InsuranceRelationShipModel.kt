package com.zarholding.zar.model.data.request.insurance

/**
 * Created by m-latifi on 10/16/2023.
 */

data class InsuranceRelationShipModel(
    val firstName: String,
    val lastName: String,
    val nationalCode : String,
    val birthCertificateNumber: String,
    val birthdate: String,
    val genderCode: String,
    val fatherName: String,
    val relationShipId: Long
)
