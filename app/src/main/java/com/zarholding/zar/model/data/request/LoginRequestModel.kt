package com.zarholding.zar.model.data.request

/**
 * Created by m-latifi on 11/9/2022.
 */

data class LoginRequestModel(
    val userName : String,
    val password : String
)
