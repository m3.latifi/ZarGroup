package com.zarholding.zar.model.data.request.survey

/**
 * Created by zar on 2/19/2023.
 */

data class SurveyAnswerRequestModel(
    val QuestionId: Long,
    val QuestionOptionIds: List<Long>?,
    val AdditionalText: String?
)
