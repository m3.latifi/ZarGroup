package com.zarholding.zar.model.repository

import com.zar.core.enums.EnumApiError
import com.zar.core.models.ErrorApiModel
import com.zar.core.tools.api.toMap
import com.zarholding.zar.di.ResourcesProvider
import com.zarholding.zar.model.api.ApiSuperApp
import com.zarholding.zar.model.data.response.NetworkResult
import org.json.JSONObject
import retrofit2.HttpException
import javax.inject.Inject

/**
 * Created by m-latifi on 8/8/2023.
 */

open class ZarRepository @Inject constructor() {

    @Inject
    lateinit var api: ApiSuperApp

    @Inject
    lateinit var tokenRepository: TokenRepository

    @Inject
    lateinit var resourcesProvider: ResourcesProvider


    //---------------------------------------------------------------------------------------------- emitError
    fun <T> emitError(message: String): NetworkResult<T> {
        return NetworkResult.Failure(ErrorApiModel(type = EnumApiError.Error, message = message))
    }
    //---------------------------------------------------------------------------------------------- emitError


    //---------------------------------------------------------------------------------------------- handleFlowException
    fun <T> handleFlowException(e: Throwable): NetworkResult<T> {
        when (e) {
            is HttpException -> {
                val type =
                    if (e.code() == 401)
                        EnumApiError.UnAuthorization
                    else if (e.code() == 403 )
                        EnumApiError.UnAccess
                    else
                        EnumApiError.Error
                return NetworkResult.Failure(
                    ErrorApiModel(
                        type = type,
                        message = responseMessage(e.response()?.errorBody()?.string())
                    )
                )
            }

            else -> {
                return NetworkResult.Failure(
                    ErrorApiModel(
                        type = EnumApiError.Error,
                        message = e.message.toString()
                    )
                )
            }
        }
    }
    //---------------------------------------------------------------------------------------------- handleFlowException


    //-------------------------------------------------------------------------------------------------- responseMessage
    private fun responseMessage(response: String?): String {
        val error = if (response.isNullOrEmpty())
            return "متاسفانه خطایی رخ داده، چند دقیقه بعد دوباره تلاش کنید"
        else
            JSONObject(response)
        return if (!error.has("errors")) {
            error.getString("message")
        } else {
            val errors = error.getJSONObject("errors").toMap()
            val sb = StringBuilder()
            errors.forEach {
                run {
                    sb.append(it.value)
                    sb.append("\n")
                }
            }
            sb.toString()
        }
    }
    //-------------------------------------------------------------------------------------------------- responseMessage

}