package com.zarholding.zar.model.repository

import android.os.Bundle
import androidx.core.text.isDigitsOnly
import com.zarholding.zar.R
import com.zarholding.zar.model.data.response.NetworkResult
import com.zarholding.zar.tools.CompanionValues
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

/**
 * Created by m-latifi on 11/4/2023.
 */

class HseRepository @Inject constructor() : ZarRepository() {


    //---------------------------------------------------------------------------------------------- requestGetHsePeriods
    fun requestGetHsePeriods() = flow {
        val response =
            api.requestGetHsePeriods(token = tokenRepository.getBearerToken())
        if (response.data.isNullOrEmpty())
            emit(emitError(response.message))
        else
            emit(NetworkResult.Success(response.data))
    }.catch { e -> emit(handleFlowException(e)) }
    //---------------------------------------------------------------------------------------------- requestGetHsePeriods


    //---------------------------------------------------------------------------------------------- requestGetExaminations
    fun requestGetExaminations(arguments: Bundle?) = flow {
        if (arguments == null)
            emit(emitError(resourcesProvider.getString(R.string.failedToReadId)))
        else {
            val id = arguments.getString(CompanionValues.EXAMINATION_ID, null)
            val idCheck = id?.replace("-", "")
            if ((id != null) && idCheck?.isDigitsOnly() == true) {
                val response = api.requestGetExaminations(
                    id = id.toLong(),
                    token = tokenRepository.getBearerToken()
                )
                if (response.data.isNullOrEmpty())
                    emit(emitError(response.message))
                else
                    emit(NetworkResult.Success(response.data))
            } else
                emit(emitError(resourcesProvider.getString(R.string.failedToReadId)))
        }
    }.catch { e -> emit(handleFlowException(e)) }
    //---------------------------------------------------------------------------------------------- requestGetExaminations


}