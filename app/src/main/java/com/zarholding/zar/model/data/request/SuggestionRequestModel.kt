package com.zarholding.zar.model.data.request

/**
 * Created by m-latifi on 9/25/2023.
 */

data class SuggestionRequestModel(
    val search: String,
    val excludeIds: List<Long>?
)
