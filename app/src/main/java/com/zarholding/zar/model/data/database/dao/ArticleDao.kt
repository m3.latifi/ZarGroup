package com.zarholding.zar.model.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.zarholding.zar.model.data.database.entity.ArticleEntity

@Dao
interface ArticleDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertArticle(articleEntity: ArticleEntity)

    @Query("SELECT * FROM Article WHERE articleType = :articleType ORDER BY id ASC")
    fun getArticles(articleType: String): List<ArticleEntity>


    @Query("DELETE FROM Article")
    fun deleteArticles()

}