package com.zarholding.zar.model.data.other

import android.os.Bundle

/**
 * Created by m-latifi on 11/14/2022.
 */

data class AppModel(
    val icon : Int,
    val title : String,
    val link : Int,
    val bundle: Bundle? = null
)
