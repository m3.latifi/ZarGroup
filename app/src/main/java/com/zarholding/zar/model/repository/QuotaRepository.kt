package com.zarholding.zar.model.repository

import com.zar.core.tools.api.apiCall
import com.zarholding.zar.model.data.enum.EnumQuotaApprove
import com.zarholding.zar.model.data.response.GeneralResponse
import com.zarholding.zar.model.data.response.PagingListModel
import com.zarholding.zar.model.data.response.quota.QuotaHistoryModel
import retrofit2.Response
import javax.inject.Inject

/**
 * Created by m-latifi on 9/3/2023.
 */

class QuotaRepository @Inject constructor() : ZarRepository() {

    private var pageNumber = 0
    private val pageSize = 20


    //---------------------------------------------------------------------------------------------- getMyQuota
    suspend fun getMyQuota() =
        apiCall { api.getMyQuota(tokenRepository.getBearerToken()) }
    //---------------------------------------------------------------------------------------------- getMyQuota


    //---------------------------------------------------------------------------------------------- assignMyQuotaToAnother
    suspend fun assignMyQuotaToAnother(userId: Int, quotaId: Long) =
        apiCall { api.assignMyQuotaToAnother(userId, quotaId, tokenRepository.getBearerToken()) }
    //---------------------------------------------------------------------------------------------- assignMyQuotaToAnother


    //---------------------------------------------------------------------------------------------- approveQuota
    suspend fun approveQuota(status: EnumQuotaApprove?, quotaId: Long) =
        apiCall { api.approveQuota(status, quotaId, tokenRepository.getBearerToken()) }
    //---------------------------------------------------------------------------------------------- approveQuota


    //---------------------------------------------------------------------------------------------- getMyQuotaHistory
    suspend fun getMyQuotaHistory():
            Response<GeneralResponse<PagingListModel<QuotaHistoryModel>?>>? {
        pageNumber++
        return apiCall { api.getMyQuotaHistory(
            pageNumber = pageNumber,
            pagesize = pageSize,
            token = tokenRepository.getBearerToken()
        ) }
    }
    //---------------------------------------------------------------------------------------------- getMyQuotaHistory


}