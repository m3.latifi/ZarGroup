package com.zarholding.zar.model.data.response.quota

import com.zarholding.zar.model.data.enum.EnumQuotaApprove

/**
 * Created by m-latifi on 9/2/2023.
 */

data class QuotaModel(
    val id: Long,
    val quotaName: String?,
    val fromDate: String?,
    val toDate: String?,
    val quotasOwnerId: Long,
    val quotasOwnerName: String?,
    val receiverId: Long,
    val receiverName: String?,
    val status: EnumQuotaApprove
)
