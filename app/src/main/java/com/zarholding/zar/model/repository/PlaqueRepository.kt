package com.zarholding.zar.model.repository

import com.zar.core.tools.extensions.persianNumberToEnglishNumber
import com.zarholding.zar.R
import com.zarholding.zar.ext.toCarPlaque
import com.zarholding.zar.model.data.database.entity.UserInfoEntity
import com.zarholding.zar.model.data.request.CarPlaqueEditRequest
import com.zarholding.zar.model.data.request.UserInfoRequest
import com.zarholding.zar.model.data.response.NetworkResult
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

/**
 * Created by m-latifi on 11/21/2023.
 */

class PlaqueRepository @Inject constructor(
    private val userRepository: UserRepository
) : ZarRepository() {


    //---------------------------------------------------------------------------------------------- getUserInfoByPlaque
    fun getUserInfoByPlaque(
        plaqueNumber1: String?,
        plaqueAlphabet: String?,
        plaqueNumber2: String?,
        plaqueCity: String?
    ) = flow {
        val checkPlaque = checkPlaqueValidation(
            plaqueNumber1 = plaqueNumber1,
            plaqueAlphabet = plaqueAlphabet,
            plaqueNumber2 = plaqueNumber2,
            plaqueCity = plaqueCity
        )
        if (!checkPlaque)
            emit(emitError(resourcesProvider.getString(R.string.plaqueInformationIsEmpty)))
        else {
            val plaque = plaqueNumber1.persianNumberToEnglishNumber() +
                    plaqueAlphabet +
                    plaqueNumber2.persianNumberToEnglishNumber() +
                    plaqueCity.persianNumberToEnglishNumber()
            val request = UserInfoRequest(null, plaque)
            val response =
                api.requestGetUserInfo(request, tokenRepository.getBearerToken())
            if (response.data == null)
                emit(emitError(response.message))
            else
                emit(NetworkResult.Success(response.data))
        }
    }.catch { e -> emit(handleFlowException(e)) }
    //---------------------------------------------------------------------------------------------- getUserInfoByPlaque


    //---------------------------------------------------------------------------------------------- changeCarPlaque
    fun changeCarPlaque(
        carModel: String?,
        plaqueNumber1: String?,
        plaqueAlphabet: String?,
        plaqueNumber2: String?,
        plaqueCity: String?
    ) = flow {
        val checkPlaque = checkPlaqueValidation(
            plaqueNumber1 = plaqueNumber1,
            plaqueAlphabet = plaqueAlphabet,
            plaqueNumber2 = plaqueNumber2,
            plaqueCity = plaqueCity
        ) && !carModel.isNullOrEmpty()
        if (!checkPlaque)
            emit(emitError(resourcesProvider.getString(R.string.plaqueInformationIsEmpty)))
        else {
            val plaque = plaqueNumber1.persianNumberToEnglishNumber() +
                    plaqueAlphabet +
                    plaqueNumber2.persianNumberToEnglishNumber() +
                    plaqueCity.persianNumberToEnglishNumber()
            val model = CarPlaqueEditRequest(carModel, plaque)
            val response =
                api.requestChangeCarPlaque(model, tokenRepository.getBearerToken())
            if (response.data == null)
                emit(emitError(response.message))
            else {
                userRepository.getUser()?.let { user ->
                    user.pelak = plaque
                    user.carModel = carModel
                    userRepository.insertUserInfo(user)
                }
                emit(NetworkResult.Success(response.data))
            }
        }
    }
    //---------------------------------------------------------------------------------------------- changeCarPlaque


    //---------------------------------------------------------------------------------------------- checkPlaqueValidation
    private fun checkPlaqueValidation(
        plaqueNumber1: String?,
        plaqueAlphabet: String?,
        plaqueNumber2: String?,
        plaqueCity: String?
    ): Boolean =
        plaqueNumber1?.length == 2 && plaqueNumber2?.length == 3 &&
                plaqueCity?.length == 2 && !plaqueAlphabet.isNullOrEmpty()
    //---------------------------------------------------------------------------------------------- checkPlaqueValidation


    //---------------------------------------------------------------------------------------------- getCarModel
    fun getCarModel(userInfoEntity: UserInfoEntity?) = userInfoEntity?.carModel ?: ""
    //---------------------------------------------------------------------------------------------- getCarModel


    //---------------------------------------------------------------------------------------------- getPlaqueNumber1
    fun getPlaqueNumber1(userInfoEntity: UserInfoEntity?) =
        userInfoEntity?.pelak?.toCarPlaque("number1") ?: ""
    //---------------------------------------------------------------------------------------------- getPlaqueNumber1


    //---------------------------------------------------------------------------------------------- getPlaqueNumber2
    fun getPlaqueNumber2(userInfoEntity: UserInfoEntity?) =
        userInfoEntity?.pelak?.toCarPlaque("number2") ?: ""
    //---------------------------------------------------------------------------------------------- getPlaqueNumber2


    //---------------------------------------------------------------------------------------------- getPlaqueCity
    fun getPlaqueCity(userInfoEntity: UserInfoEntity?) =
        userInfoEntity?.pelak?.toCarPlaque("city") ?: ""
    //---------------------------------------------------------------------------------------------- getPlaqueCity


    //---------------------------------------------------------------------------------------------- getPlaqueAlphabet
    fun getPlaqueAlphabet(userInfoEntity: UserInfoEntity?) =
        userInfoEntity?.pelak?.toCarPlaque("alphabet") ?: ""
    //---------------------------------------------------------------------------------------------- getPlaqueAlphabet


    //---------------------------------------------------------------------------------------------- getAlphabet
    fun getAlphabet() = listOf(
        "آ",
        "ب",
        "پ",
        "ت",
        "ث",
        "ج",
        "چ",
        "ح",
        "خ",
        "د",
        "ذ",
        "ر",
        "ز",
        "ژ",
        "س",
        "ش",
        "ص",
        "ض",
        "ط",
        "ظ",
        "ع",
        "غ",
        "ف",
        "ق",
        "ک",
        "گ",
        "ل",
        "م",
        "ن",
        "و",
        "ه",
        "ی"
    )
    //---------------------------------------------------------------------------------------------- getAlphabet

}