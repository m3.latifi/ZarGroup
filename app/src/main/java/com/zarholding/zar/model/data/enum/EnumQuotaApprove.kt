package com.zarholding.zar.model.data.enum

/**
 * Created by m-latifi on 9/3/2023.
 */

enum class EnumQuotaApprove {
    Assign,
    Delivered,
    Finished
}