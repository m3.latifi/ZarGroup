package com.zarholding.zar.model.data.response.address

data class AddressModel(
    val country : String?,
    val county : String?,
    val city : String?,
    val town : String?,
    val neighbourhood : String?,
    val suburb : String?,
    val residential : String?,
    val road : String?,
    val district : String?,

    val state_district : String?,
    val state : String?,
    val building : String?,


) {
    var favId: Int? = null
    var addressTitle: String? = null
}
