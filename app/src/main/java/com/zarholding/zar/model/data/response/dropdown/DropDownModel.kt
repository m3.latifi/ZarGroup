package com.zarholding.zar.model.data.response.dropdown

data class DropDownModel(
    val text : String,
    val value : String
)
