package com.zarholding.zar.model.repository

import com.zar.core.enums.EnumApiError
import com.zar.core.models.ErrorApiModel
import com.zar.core.tools.api.apiCall
import com.zarholding.zar.model.data.request.NotificationUnreadCountRequestModel
import com.zarholding.zar.model.data.response.NetworkResult
import com.zarholding.zar.tools.manager.SharedPreferencesManager
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

/**
 * Created by m-latifi on ۰۸/۰۲/۲۰۲۳.
 */

class NotificationRepository @Inject constructor(
    private val sharedPreferencesManager: SharedPreferencesManager
) : ZarRepository() {

    //---------------------------------------------------------------------------------------------- requestGetNotificationUnreadCount
    fun requestGetNotificationUnreadCount() = flow {
        val request = NotificationUnreadCountRequestModel(
            0,
            "superapp",
            sharedPreferencesManager.getLastNotificationId()
        )
        val response = api.requestGetNotificationUnreadCount(
            request = request,
            token = tokenRepository.getBearerToken()
        )
        if (response.data == null)
            emit(NetworkResult.Failure(ErrorApiModel(type = EnumApiError.Error, message = response.message)))
        else {
            sharedPreferencesManager.setLastNotificationId(response.data.lastId)
            emit(NetworkResult.Success(response.data.unreadCount))
        }
    }.catch { e ->
        emit(handleFlowException(e))
    }
    //---------------------------------------------------------------------------------------------- requestGetNotificationUnreadCount



    //---------------------------------------------------------------------------------------------- requestGetNotification
    suspend fun requestGetNotification() =
        apiCall { api.requestGetNotification(tokenRepository.getBearerToken()) }
    //---------------------------------------------------------------------------------------------- requestGetNotification


    //---------------------------------------------------------------------------------------------- requestReadNotification
    suspend fun requestReadNotification(ids: List<Int>) =
        apiCall { api.requestReadNotification(ids, tokenRepository.getBearerToken()) }
    //---------------------------------------------------------------------------------------------- requestReadNotification

}