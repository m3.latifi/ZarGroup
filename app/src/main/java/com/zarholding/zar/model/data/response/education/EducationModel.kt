package com.zarholding.zar.model.data.response.education

/**
 * Created by m-latifi on 7/26/2023.
 */

data class EducationModel(
    val id: Long = 1,
    val groupDescription: String,
    val fromDate: String,
    val toDate: String,
    val description: String,
    val sessionCount: Int,
    val sessionsMin: Int,
    val isDeleted: Boolean
)
