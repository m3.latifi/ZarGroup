package com.zarholding.zar.model.repository

import com.zarholding.zar.model.data.request.*
import com.zarholding.zar.model.data.response.NetworkResult
import com.zarholding.zar.model.data.response.taxi.TaxiFavPlaceModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

/**
 * Created by m-latifi on ۰۸/۰۲/۲۰۲۳.
 */

class FavoriteRepository @Inject constructor() : ZarRepository() {


    private val favorites = mutableListOf<TaxiFavPlaceModel>()


    //---------------------------------------------------------------------------------------------- getFavorites
    fun getFavorites() = favorites
    //---------------------------------------------------------------------------------------------- getFavorites



    //---------------------------------------------------------------------------------------------- requestGetTaxiFavPlace
    fun requestGetTaxiFavPlace() = flow {
        val response =
            api.requestGetTaxiFavPlace(tokenRepository.getBearerToken())
        if (response.data.isNullOrEmpty())
            emit(emitError(response.message))
        else {
            favorites.addAll(response.data)
            emit(NetworkResult.Success(favorites))
        }
    }.catch { e -> emit(handleFlowException(e)) }
    //---------------------------------------------------------------------------------------------- requestGetTaxiFavPlace


    //---------------------------------------------------------------------------------------------- requestAddFavPlace
    fun requestAddFavPlace(request: TaxiAddFavPlaceRequest) = flow {
        val response =
            api.requestAddFavPlace(request, tokenRepository.getBearerToken())
        if (response.data == null)
            emit(emitError(response.message))
        else {
            favorites.add(response.data)
            emit(NetworkResult.Success(response.data))
        }
    }.catch { e -> emit(handleFlowException(e)) }
    //---------------------------------------------------------------------------------------------- requestAddFavPlace


    //---------------------------------------------------------------------------------------------- requestDeleteFavPlace
    fun requestDeleteFavPlace(id: Int) = flow {
        val response =
            api.requestDeleteFavPlace(id, tokenRepository.getBearerToken())
        if (response.data == null)
            emit(emitError(response.message))
        else {
            favorites.removeIf { it.id == id }
            emit(NetworkResult.Success(response.data))
        }
    }.catch { e -> emit(handleFlowException(e)) }
    //---------------------------------------------------------------------------------------------- requestDeleteFavPlace


    //---------------------------------------------------------------------------------------------- getFavoriteTitle
    fun getFavoriteTitle(favId: Int?): String {
        var address = ""
        if (favId != null && favorites.isNotEmpty()) {
            address = favorites.find {
                it.id == favId && it.isAll
            }?.locationName ?: ""
        }
        return address
    }
    //---------------------------------------------------------------------------------------------- getFavoriteTitle


}