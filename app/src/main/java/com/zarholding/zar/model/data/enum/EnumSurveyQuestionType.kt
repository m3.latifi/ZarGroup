package com.zarholding.zar.model.data.enum

/**
 * Created by m-latifi on ۰۸/۰۲/۲۰۲۳.
 */
enum class EnumSurveyQuestionType {
    singlelinettext,
    multilinetext,
    choicesingleselect_dropdown,
    choicesingleselect_radio,
    choicemultipleselect,
    yesno,
    numericrange,
    date,
    time,
    satisfaction,
    numeric
}