package com.zarholding.zar.model.data.enum

enum class EnumDriverType {
    Commute,
    Personnel,
    Agency
}