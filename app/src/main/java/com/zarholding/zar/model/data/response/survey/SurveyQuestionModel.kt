package com.zarholding.zar.model.data.response.survey

import com.zarholding.zar.model.data.enum.EnumSurveyQuestionType

/**
 * Created by m-latifi on ۰۸/۰۲/۲۰۲۳.
 */
data class SurveyQuestionModel(
    val rowNumber: Long,
    val questionId: Long,
    val questionTitle: String?,
    val isRequired: Boolean,
    val questionType: EnumSurveyQuestionType,
    val options: List<SurveyOptionModel>?
) {
    var answer: String? = null
    var selectedAnswerId: Long? = null
    var selectedAnswersId: MutableList<Long>? = null
}
