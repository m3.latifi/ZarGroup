package com.zarholding.zar.model.data.enum

enum class EnumTaxiRequestStatus {
    Pending,
    Reject,
    Confirm,
    Confirmed
}