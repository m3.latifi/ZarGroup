package com.zarholding.zar.model.repository

import com.zar.core.enums.EnumApiError
import com.zar.core.models.ErrorApiModel
import com.zarholding.zar.model.data.database.dao.ArticleDao
import com.zarholding.zar.model.data.database.entity.ArticleEntity
import com.zarholding.zar.model.data.enum.EnumArticleType
import com.zarholding.zar.model.data.request.ArticleRequestModel
import com.zarholding.zar.model.data.response.NetworkResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

/**
 * Created by m-latifi on ۰۸/۰۲/۲۰۲۳.
 */

class ArticleRepository @Inject constructor(
    private val articleDao: ArticleDao
) : ZarRepository() {

    //---------------------------------------------------------------------------------------------- requestGetArticles
    fun requestGetArticles() = flow {
        val model = ArticleRequestModel(
            PageNumber = 1,
            PageSize = 100,
            Search = "",
            isAdmin = false,
            ArticleType = "Article"
        )
        val response = api.requestGetArticles(
            articleRequestModel = model,
            token = tokenRepository.getBearerToken()
        )
        if (response.data == null || response.data.items.isEmpty())
            emit(NetworkResult.Failure(ErrorApiModel(type = EnumApiError.Error, message = response.message)))
        else {
            deleteArticles()
            delay(500)
            insertArticle(items = response.data.items)
            delay(500)
            emit(NetworkResult.Success(getArticles(EnumArticleType.Article)))
        }
    }.catch { e ->
        emit(handleFlowException(e))
    }.flowOn(Dispatchers.IO)
    //---------------------------------------------------------------------------------------------- requestGetArticles


    //---------------------------------------------------------------------------------------------- insertArticle
    private fun insertArticle(items: List<ArticleEntity?>) {
        for (item in items)
            item?.let { articleDao.insertArticle(it) }
    }
    //---------------------------------------------------------------------------------------------- insertArticle


    //---------------------------------------------------------------------------------------------- getArticles
    private fun getArticles(type: EnumArticleType): List<ArticleEntity> {
        return articleDao.getArticles(type.name)
    }
    //---------------------------------------------------------------------------------------------- getArticles


    //---------------------------------------------------------------------------------------------- deleteArticles
    private fun deleteArticles() {
        articleDao.deleteArticles()
    }
    //---------------------------------------------------------------------------------------------- deleteArticles

}