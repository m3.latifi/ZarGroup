package com.zarholding.zar.model.data.response.address

data class AddressSuggestionModel(
    val place_id : Long,
    val lat : Double,
    val lon : Double,
    val boundingbox : List<Double>,
    val address : AddressModel
)
