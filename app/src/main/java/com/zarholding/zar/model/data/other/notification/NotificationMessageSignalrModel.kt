package com.zarholding.zar.model.data.other.notification

data class NotificationMessageSignalrModel(
    val UserAction: String?,
    val ActionParam: String?,
    val Body: String?
)