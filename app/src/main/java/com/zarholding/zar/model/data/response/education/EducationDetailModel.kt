package com.zarholding.zar.model.data.response.education

/**
 * Created by m-latifi on 7/26/2023.
 */

data class EducationDetailModel(
    val id: Long,
    val title: String?,
    val videoFileName: String?,
    val sessionNumber: Int,
    val sessionMin: Int,
    val total: Int
)
