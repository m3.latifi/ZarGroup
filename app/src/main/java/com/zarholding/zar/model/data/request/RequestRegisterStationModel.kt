package com.zarholding.zar.model.data.request

/**
 * Created by m-latifi on 11/27/2022.
 */

data class RequestRegisterStationModel(
    val commuteTripId : Int,
    val stationTripId : Int
)
