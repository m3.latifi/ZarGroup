package com.zarholding.zar.model.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.zarholding.zar.model.data.database.converter.ListStringConverters
import com.zarholding.zar.model.data.database.converter.LocalDateTimeConverter
import com.zarholding.zar.model.data.database.dao.ArticleDao
import com.zarholding.zar.model.data.database.dao.RoleDao
import com.zarholding.zar.model.data.database.dao.UserInfoDao
import com.zarholding.zar.model.data.database.entity.ArticleEntity
import com.zarholding.zar.model.data.database.entity.RoleEntity
import com.zarholding.zar.model.data.database.entity.UserInfoEntity


@Database(entities = [ArticleEntity::class, UserInfoEntity::class, RoleEntity::class], version = 1,
    exportSchema = false)
@TypeConverters(ListStringConverters::class, LocalDateTimeConverter::class)
abstract class AppDatabase: RoomDatabase() {
    abstract fun articleDao() : ArticleDao
    abstract fun userInfoDao() : UserInfoDao
    abstract fun roleDao() : RoleDao
}