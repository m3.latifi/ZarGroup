package com.zarholding.zar.model.data.other.version

/**
 * Created by m-latifi on 9/30/2023.
 */

data class VersionFeatureModel(
    val version: String,
    val features: List<String>
)
