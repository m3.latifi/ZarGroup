package com.zarholding.zar.model.data.response.driver

import com.zarholding.zar.model.data.enum.EnumDriverType

data class DriverModel(
    val id : Int,
    val fullName : String?,
    val mobile : String?,
    val nationalCode : String?,
    val pelak : String?,
    val description : String?,
    val carImage : String?,
    val carImageName : String?,
    val driverImage : String?,
    val driverImageName : String?,
    val driverType : com.zarholding.zar.model.data.enum.EnumDriverType,
    val companyCode : String?,
    val companyName : String?
)
