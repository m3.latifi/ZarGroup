package com.zarholding.zar.model.data.enum

enum class EnumStatus {
    Pending,
    Confirmed,
    Reject
}