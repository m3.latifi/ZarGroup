package com.zarholding.zar.model.data.request

import com.zarholding.zar.model.data.enum.EnumPersonnelType

data class AdminTaxiListRequest(
    var PageNumber : Int,
    val PageSize : Int,
    var UserType : EnumPersonnelType?
)
