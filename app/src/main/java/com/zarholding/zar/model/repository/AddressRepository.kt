package com.zarholding.zar.model.repository

import com.zarholding.zar.model.data.request.SuggestionRequestModel
import com.zarholding.zar.model.data.response.NetworkResult
import com.zarholding.zar.model.data.response.address.AddressSuggestionModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import org.osmdroid.util.GeoPoint
import javax.inject.Inject

/**
 * Created by m-latifi on ۰۸/۰۲/۲۰۲۳.
 */

class AddressRepository @Inject constructor() : ZarRepository() {


    //---------------------------------------------------------------------------------------------- requestGetAddress
    suspend fun requestGetAddress(geoPoint: GeoPoint) = flow {
        val response =
            api.requestGetAddress(lat = geoPoint.latitude, lon = geoPoint.longitude)
        if (response.data == null)
            emit(emitError(message = response.message))
        else
            emit(NetworkResult.Success(response.data))
    }.catch { e ->
        emit(handleFlowException(e))
    }
    //---------------------------------------------------------------------------------------------- requestGetAddress


    //---------------------------------------------------------------------------------------------- requestGetSuggestionAddress
    suspend fun requestGetSuggestionAddress(
        address: String,
        suggestion: List<AddressSuggestionModel>?
    ) = flow {
        val split = address.split(" ")
        var search = ""
        for (item in split)
            search += "$item+"
        val excludePlaceIds = mutableListOf<Long>()
        suggestion?.let {
            for (item in suggestion)
                excludePlaceIds.add(item.place_id)
        }
        val request = SuggestionRequestModel(search = search, excludeIds = excludePlaceIds)
        val response = api.requestGetSuggestionAddress(request)
        if (response.data.isNullOrEmpty())
            emit(emitError(response.message))
        else
            emit(NetworkResult.Success(response.data))
    }.catch { e -> emit(handleFlowException(e)) }
    //---------------------------------------------------------------------------------------------- requestGetSuggestionAddress

}