package com.zarholding.zar.model.repository

import android.os.Environment
import com.zar.core.enums.EnumApiError
import com.zar.core.models.ErrorApiModel
import com.zar.core.tools.manager.DeviceManager
import com.zarholding.zar.model.data.enum.EnumSystemType
import com.zarholding.zar.model.data.response.NetworkResult
import com.zarholding.zar.tools.manager.SharedPreferencesManager
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import java.io.File
import javax.inject.Inject


/**
 * create by m-latifi on 4/25/2023
 */

class AppUpdateRepository @Inject constructor(
    private val deviceManager: DeviceManager,
    private val sharedPreferencesManager: SharedPreferencesManager
) : ZarRepository() {


    //---------------------------------------------------------------------------------------------- requestGetAppVersion
    fun requestGetAppVersion() = flow {
        val response =
            api.requestGetAppVersion(EnumSystemType.SuperApp.name)
        if (response.data == null)
            emit(
                NetworkResult.Failure(
                    ErrorApiModel(
                        type = EnumApiError.Error,
                        message = response.message
                    )
                )
            )
        else {
            val currentVersion = deviceManager.appVersionCode()
            if (currentVersion < response.data.currentVersion) {
                response.data.fileName?.let {
                    emit(NetworkResult.Success(it))
                } ?: run { emit(NetworkResult.Success("")) }
            } else
                emit(NetworkResult.Success(""))
        }
    }.catch { e ->
        emit(handleFlowException(e))
    }
    //---------------------------------------------------------------------------------------------- requestGetAppVersion


    //---------------------------------------------------------------------------------------------- checkVersionIsNew
    fun checkVersionIsNew() = flow {
        val downloadFolder =
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
        val destinationDir = File(downloadFolder.absolutePath, "Zar")
        if (destinationDir.exists())
            destinationDir.deleteRecursively()
        val saveVersion = sharedPreferencesManager.getVersion()
        val currentVersion = deviceManager.appVersionCode()
        if (saveVersion < currentVersion) {
            sharedPreferencesManager.setVersion(currentVersion)
            emit(true)
        } else
            emit(false)
    }
    //---------------------------------------------------------------------------------------------- checkVersionIsNew


}