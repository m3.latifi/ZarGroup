package com.zarholding.zar.model.data.response.insurance

/**
 * Created by m-latifi on 10/14/2023.
 */

data class SupplementalInsuranceModel(
    val id: Long,
    val title: String?,
    val startDate: String?,
    val endDate: String?,
    val startShowDate: String?,
    val endShowDate: String?,
    val insurerCompany: String?,
    val fileName: String?,
    val points: List<SupplementalInsurancePointModel>?
)
