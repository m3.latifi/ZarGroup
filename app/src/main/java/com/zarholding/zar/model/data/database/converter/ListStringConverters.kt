package com.zarholding.zar.model.data.database.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

class ListStringConverters {


    //---------------------------------------------------------------------------------------------- fromString
    @TypeConverter
    fun fromString(value: String?): List<String?>? {
        val listType: Type = object : TypeToken<List<String?>?>() {}.type
        return Gson().fromJson(value, listType)
    }
    //---------------------------------------------------------------------------------------------- fromString


    //---------------------------------------------------------------------------------------------- fromList
    @TypeConverter
    fun fromList(list: List<String?>?): String? {
        val gson = Gson()
        return gson.toJson(list)
    }
    //---------------------------------------------------------------------------------------------- fromList
}