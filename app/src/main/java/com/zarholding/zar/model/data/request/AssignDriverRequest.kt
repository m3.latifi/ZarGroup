package com.zarholding.zar.model.data.request

data class AssignDriverRequest(
    val Id : String,
    val DriverId : String
)
