package com.zarholding.zar.model.repository

import com.zar.core.tools.api.apiCall
import com.zarholding.zar.model.data.request.survey.SubmitSurveyQuestionRequestModel
import com.zarholding.zar.model.data.response.GeneralResponse
import com.zarholding.zar.model.data.response.survey.SurveyItemModel
import retrofit2.Response
import javax.inject.Inject

/**
 * Created by m-latifi on ۰۸/۰۲/۲۰۲۳.
 */
class SurveyRepository @Inject constructor(
    private val userRepository: UserRepository
) : ZarRepository() {

    //---------------------------------------------------------------------------------------------- requestGetUserSurveys
    suspend fun requestGetUserSurveys(): Response<GeneralResponse<SurveyItemModel?>>? {
        val user = userRepository.getUser()
        return apiCall {
            api.requestGetUserSurveys(
                user?.personnelJobKeyCode,
                user?.companyCode,
                tokenRepository.getBearerToken()
            )
        }
    }
    //---------------------------------------------------------------------------------------------- requestGetUserSurveys


    //---------------------------------------------------------------------------------------------- requestGetSurveyDetail
    suspend fun requestGetSurveyDetail(surveyId: Long) =
        apiCall { api.requestGetSurveyDetail(surveyId, tokenRepository.getBearerToken()) }
    //---------------------------------------------------------------------------------------------- requestGetSurveyDetail


    //---------------------------------------------------------------------------------------------- requestSubmitSurvey
    suspend fun requestSubmitSurvey(request: List<SubmitSurveyQuestionRequestModel>) =
        apiCall { api.requestSubmitSurvey(request, tokenRepository.getBearerToken()) }
    //---------------------------------------------------------------------------------------------- requestSubmitSurvey

}