package com.zarholding.zar.model.data.response.education

/**
 * Created by m-latifi on 7/26/2023.
 */

data class EducationCategoryModel(
    val id: Long = 1,
    val value: String?
)
