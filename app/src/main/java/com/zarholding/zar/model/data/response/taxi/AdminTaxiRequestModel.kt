package com.zarholding.zar.model.data.response.taxi

import com.zarholding.zar.model.data.enum.EnumTaxiRequestStatus
import com.zarholding.zar.model.data.enum.EnumTaxiRequestType
import com.zarholding.zar.model.data.enum.EnumTripStatus
import com.zarholding.zar.model.data.response.PassengerModel


data class AdminTaxiRequestModel(
    val id : Int,
    val userId : Int,
    val userName: String?,
    val requesterName : String?,
    val status : EnumTaxiRequestStatus,
    val type : EnumTaxiRequestType,
    val departureDate : String?,
    val departureTime : String?,
    val returnDate : String?,
    val returnTime : String?,
    val passengers : List<Int>?,
    val listPassengers : List<PassengerModel>?,
    val strPassengers : String?,
    val travelReason : String?,
    val rejectReason : String?,
    val approverId : Int,
    val personnelJobKeyCode : String?,
    val personnelJobKeyText : String?,
    val organizationUnitText : String?,
    val fromCompany : String?,
    val companyName  : String?,
    val approverName : String?,
    val waitingTime : Int,
    var tripStatus : EnumTripStatus,
    val locations: List<TaxiRequestPointModel>?
)
