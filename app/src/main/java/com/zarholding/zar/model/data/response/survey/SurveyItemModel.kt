package com.zarholding.zar.model.data.response.survey

/**
 * Created by m-latifi on ۰۸/۰۲/۲۰۲۳.
 */
data class SurveyItemModel(
    val items : List<SurveyModel>?,
    val totalCount : Int
)
