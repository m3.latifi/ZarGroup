package com.zarholding.zar.model.data.request.hse

/**
 * Created by m-latifi on 11/4/2023.
 */

data class HseExaminationModel(
    val id: Long,
    val periodId: Long,
    val examinationTypeId: Long,
    val examinationTypeText: String?,
    val fileName: String?
)
