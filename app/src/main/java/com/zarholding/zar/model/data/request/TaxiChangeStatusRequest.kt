package com.zarholding.zar.model.data.request

import com.zarholding.zar.model.data.enum.EnumTaxiRequestStatus

data class TaxiChangeStatusRequest(
    val Id : Int,
    val Status : EnumTaxiRequestStatus,
    val Reason : String?,
    val PersonnelJobKeyCode : String?,
    val fromCompany : String?
)
