package com.zarholding.zar.model.data.enum

/**
 * Created by m-latifi on ۰۸/۰۲/۲۰۲۳.
 */
enum class EnumSurveyStatus {
    Created,
    Answering,
    Finished
}