package com.zarholding.zar.model.data.response.insurance.history

/**
 * Created by m-latifi on 10/17/2023.
 */

data class SupplementalInsuranceDependentPersonsModel(
    val firstName: String?,
    val lastName: String?,
    val nationalCode: String?,
    val relationShipText: String?
)
