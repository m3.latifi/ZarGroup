package com.zarholding.zar.model.data.request.insurance

/**
 * Created by m-latifi on 10/16/2023.
 */

data class RegisterSupplementalInsuranceModel(
    val accountNumber: String,
    val supplementalInsuranceLevelId: Long,
    val dependentList: List<InsuranceRelationShipModel>?
)
