package com.zarholding.zar.model.data.enum

enum class EnumTaxiRequestType {
    OneWay,
    Return
}