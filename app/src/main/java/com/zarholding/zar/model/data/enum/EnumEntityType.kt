package com.zarholding.zar.model.data.enum


/**
 * create by m-latifi on 4/29/2023
 */

enum class EnumEntityType {
    APK,
    articles,
    drivers,
    cars,
    insurance,
    HSE
}