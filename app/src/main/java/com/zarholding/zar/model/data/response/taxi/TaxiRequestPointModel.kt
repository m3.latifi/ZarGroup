package com.zarholding.zar.model.data.response.taxi

import com.zarholding.zar.model.data.enum.EnumTaxiRequestTypePro
import com.zarholding.zar.view.fragment.user.taxi.pro.component.TaxiMapView

/**
 * Created by m-latifi on 10/28/2023.
 */

data class TaxiRequestPointModel(
    val type : TaxiMapView.TaxiMapType,
    val requestType: EnumTaxiRequestTypePro,
    val lat: Double,
    val long: Double,
    val address: String
)
