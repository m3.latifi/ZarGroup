package com.zarholding.zar.model.repository

import com.zar.core.tools.hilt.ProgressResponseBody
import com.zarholding.zar.di.Providers
import com.zarholding.zar.model.api.ApiSuperApp
import com.zarholding.zar.model.data.enum.EnumEntityType
import com.zarholding.zar.model.data.enum.EnumSystemType
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit
import javax.inject.Inject


/**
 * create by m-latifi on 4/25/2023
 */

class DownloadFileRepository @Inject constructor() : ZarRepository() {

    //---------------------------------------------------------------------------------------------- downloadContentFile
    suspend fun downloadContentFile(fileName: String, type: EnumEntityType) =
        retrofit()
            .create(ApiSuperApp::class.java)
            .downloadContentFile(type, fileName)
    //---------------------------------------------------------------------------------------------- downloadContentFile


    //---------------------------------------------------------------------------------------------- downloadApkFile
    suspend fun downloadApkFile(fileName: String, type: EnumEntityType) =
        retrofit()
            .create(ApiSuperApp::class.java)
            .downloadApkFile(EnumSystemType.SuperApp.name, type, fileName)
    //---------------------------------------------------------------------------------------------- downloadApkFile


    //---------------------------------------------------------------------------------------------- retrofit
    private fun retrofit(): Retrofit = Retrofit.Builder()
        .baseUrl(Providers.url)
        .client(httpClient())
        .build()
    //---------------------------------------------------------------------------------------------- retrofit


    //---------------------------------------------------------------------------------------------- httpClient
    private fun httpClient() = OkHttpClient()
        .newBuilder()
        .connectTimeout(60, TimeUnit.SECONDS)
        .addInterceptor(interceptor())
        .build()
    //---------------------------------------------------------------------------------------------- httpClient


    //---------------------------------------------------------------------------------------------- interceptor
    private fun interceptor() = Interceptor { chain ->
        val originalResponse = chain.proceed(chain.request())
        originalResponse.newBuilder()
            .body(ProgressResponseBody(originalResponse.body!!) { _, _, _ -> })
            .build()
    }
    //---------------------------------------------------------------------------------------------- interceptor

}