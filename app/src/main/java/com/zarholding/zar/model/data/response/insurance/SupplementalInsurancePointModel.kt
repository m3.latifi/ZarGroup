package com.zarholding.zar.model.data.response.insurance

/**
 * Created by m-latifi on 10/14/2023.
 */

data class SupplementalInsurancePointModel(
    val id: Long,
    val description: String?
)
