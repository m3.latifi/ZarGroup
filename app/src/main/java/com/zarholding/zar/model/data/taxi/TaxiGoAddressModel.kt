package com.zarholding.zar.model.data.taxi

import com.zarholding.zar.model.data.other.AddressWithPointModel

/**
 * Created by m-latifi on 10/24/2023.
 */

data class TaxiGoAddressModel(
    val origins: MutableList<AddressWithPointModel> = mutableListOf(),
    var destinations: MutableList<AddressWithPointModel> = mutableListOf()
)