package com.zarholding.zar.model.data.response.address

data class AddressResponseModel(
   val lat : String?,
   val lon : String?,
   val address : AddressModel?
)