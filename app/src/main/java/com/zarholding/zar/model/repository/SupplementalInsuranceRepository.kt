package com.zarholding.zar.model.repository

import com.zar.core.tools.api.apiCall
import com.zarholding.zar.model.data.request.insurance.RegisterSupplementalInsuranceModel
import javax.inject.Inject

/**
 * Created by m-latifi on 10/14/2023.
 */

class SupplementalInsuranceRepository @Inject constructor() : ZarRepository() {


    //---------------------------------------------------------------------------------------------- requestGetListOfSupplementalInsurance
    suspend fun requestGetListOfSupplementalInsurance() =
        apiCall {
            api.requestGetListOfSupplementalInsurance(
                forSignUp = true,
                token = tokenRepository.getBearerToken()
            )
        }
    //---------------------------------------------------------------------------------------------- requestGetListOfSupplementalInsurance


    //---------------------------------------------------------------------------------------------- requestGetSupplementalLevels
    suspend fun requestGetSupplementalLevels(insuranceId: Long) =
        apiCall {
            api.requestGetSupplementalLevels(
                insuranceId = insuranceId,
                token = tokenRepository.getBearerToken()
            )
        }
    //---------------------------------------------------------------------------------------------- requestGetSupplementalLevels


    //---------------------------------------------------------------------------------------------- requestRegisterSupplementalInsurance
    suspend fun requestRegisterSupplementalInsurance(request: RegisterSupplementalInsuranceModel) =
        apiCall {
            api.requestRegisterSupplementalInsurance(
                request = request,
                token = tokenRepository.getBearerToken()
            )
        }
    //---------------------------------------------------------------------------------------------- requestRegisterSupplementalInsurance


    //---------------------------------------------------------------------------------------------- requestSupplementalHistory
    suspend fun requestSupplementalHistory() =
        apiCall {
            api.requestSupplementalHistory(
                token = tokenRepository.getBearerToken()
            )
        }
    //---------------------------------------------------------------------------------------------- requestSupplementalHistory

}