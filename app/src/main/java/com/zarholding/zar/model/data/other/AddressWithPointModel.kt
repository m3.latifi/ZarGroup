package com.zarholding.zar.model.data.other

import com.zarholding.zar.model.data.response.address.AddressModel
import org.osmdroid.util.GeoPoint

/**
 * Created by m-latifi on 10/23/2023.
 */

data class AddressWithPointModel(
    val address: AddressModel,
    val geoPoint: GeoPoint
)