package com.zarholding.zar.model.data.response

/**
 * Created by m-latifi on 8/24/2023.
 */

data class PagingListModel<T>(
    val items: List<T>?,
    val totalCount: Int
)
