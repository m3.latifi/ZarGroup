package com.zarholding.zar.model.repository

import com.zar.core.enums.EnumApiError
import com.zar.core.models.ErrorApiModel
import com.zar.core.tools.api.apiCall
import com.zarholding.zar.model.data.response.NetworkResult
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

/**
 * Created by m-latifi on ۰۸/۰۲/۲۰۲۳.
 */

class DropdownRepository @Inject constructor() : ZarRepository() {

    //---------------------------------------------------------------------------------------------- requestGetCompanies
    suspend fun requestGetCompanies() = flow {
        val response =
            api.requestGetCompanies(tokenRepository.getBearerToken())
        if (response.data == null || response.data.items.isNullOrEmpty())
            emit(NetworkResult.Failure(ErrorApiModel(type = EnumApiError.Error, message = response.message)))
        else
            emit(NetworkResult.Success(response.data.items))
    }.catch { e ->
        emit(handleFlowException(e))
    }
    //---------------------------------------------------------------------------------------------- requestGetCompanies


    //---------------------------------------------------------------------------------------------- requestGetSites
    suspend fun requestGetSites() = flow {
        val response =
            api.requestGetSites(tokenRepository.getBearerToken())
        if (response.data == null || response.data.items.isNullOrEmpty())
            emit(NetworkResult.Failure(ErrorApiModel(type = EnumApiError.Error, message = response.message)))
        else
            emit(NetworkResult.Success(response.data.items))
    }.catch { e ->
        emit(handleFlowException(e))
    }
    //---------------------------------------------------------------------------------------------- requestGetSites



    //---------------------------------------------------------------------------------------------- requestGetRelationShip
    suspend fun requestGetRelationShip() =
        apiCall { api.requestGetRelationShip(tokenRepository.getBearerToken()) }
    //---------------------------------------------------------------------------------------------- requestGetRelationShip

}