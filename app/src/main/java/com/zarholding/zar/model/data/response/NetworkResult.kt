package com.zarholding.zar.model.data.response

import com.zar.core.models.ErrorApiModel

/**
 * Created by m-latifi on 11/7/2023.
 */

sealed class NetworkResult<T> {
    data class Success<T>(val data: T) : NetworkResult<T>()
    data class Failure<T>(val errorMessage: ErrorApiModel) : NetworkResult<T>()
}
