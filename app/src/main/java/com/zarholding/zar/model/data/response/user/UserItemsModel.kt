package com.zarholding.zar.model.data.response.user

import com.zarholding.zar.model.data.database.entity.UserInfoEntity

data class UserItemsModel(
    val items : List<UserInfoEntity>?
)