package com.zarholding.zar.model.data.response

/**
 * Created by m-latifi on 11/7/2023.
 */

sealed class ResponseResult<T>{
    data class Loading<T>(val isLoading: Boolean): ResponseResult<T>()
    data class Success<T>(val data: T) : ResponseResult<T>()
}
