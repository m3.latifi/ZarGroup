package com.zarholding.zar.model.data.response.quota

import com.zarholding.zar.model.data.enum.EnumQuotaApprove

/**
 * Created by m-latifi on 9/2/2023.
 */

data class QuotaHistoryModel(
    val id: Long,
    val quotaName: String?,
    val personnelNumber: String?,
    val personnelName: String?,
    val companyText: String?,
    val organizationPositionText: String?,
    val status: EnumQuotaApprove,
    val receiveDate: String?,
    val receiverName: String?,
    val deliverName: String?,
    val exitDate: String?,
    val extruderName: String?
)
