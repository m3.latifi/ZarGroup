package com.zarholding.zar.model.data.request

data class NotificationUnreadCountRequestModel(
    val receiverId : Int,
    val systemType : String,
    val lastId : Int
)