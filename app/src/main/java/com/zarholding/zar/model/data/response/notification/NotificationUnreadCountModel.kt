package com.zarholding.zar.model.data.response.notification

data class NotificationUnreadCountModel(
    val unreadCount : Int,
    val lastId : Int
)
