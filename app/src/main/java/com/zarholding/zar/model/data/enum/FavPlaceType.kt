package com.zarholding.zar.model.data.enum

enum class FavPlaceType {
    NONE,
    ORIGIN,
    DESTINATION
}