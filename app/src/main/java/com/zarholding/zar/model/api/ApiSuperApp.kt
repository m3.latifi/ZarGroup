package com.zarholding.zar.model.api

import com.zarholding.zar.model.data.database.entity.UserInfoEntity
import com.zarholding.zar.model.data.enum.EnumEntityType
import com.zarholding.zar.model.data.enum.EnumQuotaApprove
import com.zarholding.zar.model.data.request.*
import com.zarholding.zar.model.data.request.hse.HseExaminationModel
import com.zarholding.zar.model.data.request.insurance.RegisterSupplementalInsuranceModel
import com.zarholding.zar.model.data.request.survey.SubmitSurveyQuestionRequestModel
import com.zarholding.zar.model.data.request.taxi.TaxiRequestProModel
import com.zarholding.zar.model.data.response.GeneralResponse
import com.zarholding.zar.model.data.response.address.AddressResponseModel
import com.zarholding.zar.model.data.response.address.AddressSuggestionModel
import com.zarholding.zar.model.data.response.article.ArticleItemsModel
import com.zarholding.zar.model.data.response.dropdown.DropDownItemsModel
import com.zarholding.zar.model.data.response.driver.DriverModel
import com.zarholding.zar.model.data.response.education.EducationCategoryModel
import com.zarholding.zar.model.data.response.education.EducationDetailModel
import com.zarholding.zar.model.data.response.PagingListModel
import com.zarholding.zar.model.data.response.dropdown.DropDownModel
import com.zarholding.zar.model.data.response.education.EducationModel
import com.zarholding.zar.model.data.response.insurance.history.SupplementalInsuranceHistoryModel
import com.zarholding.zar.model.data.response.insurance.SupplementalInsuranceLevelModel
import com.zarholding.zar.model.data.response.insurance.SupplementalInsuranceModel
import com.zarholding.zar.model.data.response.notification.NotificationModel
import com.zarholding.zar.model.data.response.notification.NotificationUnreadCountModel
import com.zarholding.zar.model.data.response.quota.QuotaHistoryModel
import com.zarholding.zar.model.data.response.quota.QuotaModel
import com.zarholding.zar.model.data.response.survey.SurveyDetailModel
import com.zarholding.zar.model.data.response.survey.SurveyItemModel
import com.zarholding.zar.model.data.response.taxi.*
import com.zarholding.zar.model.data.response.trip.*
import com.zarholding.zar.model.data.response.update.AppVersionModel
import com.zarholding.zar.model.data.response.user.UserItemsModel
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*


/**
 * Created by m-latifi on 11/26/2022.
 */

interface ApiSuperApp {

    companion object {
        const val api = "/Api"
        const val v1 = "$api/V1"
        const val logIn = "$v1/LogIn"
        const val personnelRegisteredStation = "$v1/PersonnelsRegisteredStation"
        const val carRequest = "$v1/CarRequest"
        const val notification = "$v1/Notification"
        const val survey = "$v1/Survey"
        const val files = "$v1/Files"
        const val video = "$v1/Video"
        const val personnelQuota = "$v1/PersonnelQuota"
        const val openStreetMap = "$v1/OpenStreetMap"
        const val supplementalInsurance = "$v1/SupplementalInsurance"
        const val dropDown = "$v1/DropDown"
        const val content = "$v1/content"
        const val hse = "$v1/hse"
        const val article = "$v1/Article"
    }

    //---------------------------------------------------------------------------------------------- logIn
    @POST("$logIn/login-users")
    suspend fun requestLogin(
        @Body login: LoginRequestModel
    ): GeneralResponse<String?>


    @GET("$logIn/get-persmissions")
    suspend fun requestUserPermission(
        @Header("Authorization") token: String
    ): GeneralResponse<List<String>?>


    @GET("$logIn/login-userInfo")
    suspend fun requestGetUserInfo(
        @Query("UserToken") fireBaseToken: String?,
        @Header("Authorization") token: String
    ): GeneralResponse<UserInfoEntity?>

    @POST("$logIn/login-userInfoByInfo")
    suspend fun requestGetUserInfo(
        @Body request: UserInfoRequest,
        @Header("Authorization") token: String
    ): GeneralResponse<UserInfoEntity?>


    @POST("$logIn/get-filter-users")
    suspend fun requestGetUser(
        @Body request: FilterUserRequestModel,
        @Header("Authorization") token: String
    ): Response<GeneralResponse<UserItemsModel?>>
    //---------------------------------------------------------------------------------------------- logIn


    //---------------------------------------------------------------------------------------------- article
    @POST("$article/list-articles")
    suspend fun requestGetArticles(
        @Body articleRequestModel: ArticleRequestModel,
        @Header("Authorization") token: String
    ): GeneralResponse<ArticleItemsModel?>
    //---------------------------------------------------------------------------------------------- article


    //---------------------------------------------------------------------------------------------- personnelRegisteredStation
    @GET("$personnelRegisteredStation/list-registered-trip")
    suspend fun requestGetAllTrips(
        @Header("Authorization") token: String
    ): GeneralResponse<List<TripModel>?>


    @POST("$personnelRegisteredStation/register-station")
    suspend fun requestRegisterStation(
        @Body registerStationModel: RequestRegisterStationModel,
        @Header("Authorization") token: String
    ): Response<GeneralResponse<RegisterStationModel?>>


    @GET("$personnelRegisteredStation/delete-register-station/{id}")
    suspend fun requestDeleteRegisteredStation(
        @Path("id") id: Int,
        @Header("Authorization") token: String
    ): Response<GeneralResponse<Int?>>

    @GET("$personnelRegisteredStation/list-request-registered-trip")
    suspend fun requestGetTripForRegister(
        @Header("Authorization") token: String
    ): Response<GeneralResponse<List<TripRequestRegisterModel>?>>


    @POST("$personnelRegisteredStation/response-request-registered-trip")
    suspend fun requestChangeStatusTripRegister(
        @Body request: List<TripRequestRegisterStatusModel>,
        @Header("Authorization") token: String
    ): Response<GeneralResponse<Int?>>
    //---------------------------------------------------------------------------------------------- personnelRegisteredStation


    //---------------------------------------------------------------------------------------------- carRequest
    @GET("$carRequest/get-favoritelocations")
    suspend fun requestGetTaxiFavPlace(
        @Header("Authorization") token: String
    ): GeneralResponse<List<TaxiFavPlaceModel>?>


    @POST("$carRequest/Add-favoritelocations")
    suspend fun requestAddFavPlace(
        @Body request: TaxiAddFavPlaceRequest,
        @Header("Authorization") token: String
    ): GeneralResponse<TaxiFavPlaceModel?>


    @GET("$carRequest/delete-favoritelocations/{id}")
    suspend fun requestDeleteFavPlace(
        @Path("id") id: Int,
        @Header("Authorization") token: String
    ): GeneralResponse<Boolean?>

    @POST("$carRequest/Add-carrequest")
    suspend fun requestTaxi(
        @Body request: TaxiRequestProModel,
        @Header("Authorization") token: String
    ): GeneralResponse<Int?>


    @POST("$carRequest/get-mycarrequestlist")
    suspend fun requestMyTaxiRequestList(
        @Body request: AdminTaxiListRequest,
        @Header("Authorization") token: String
    ): Response<GeneralResponse<List<AdminTaxiRequestModel>?>>


    @POST("$carRequest/get-CarRequestlist")
    suspend fun requestTaxiList(
        @Body request: AdminTaxiListRequest,
        @Header("Authorization") token: String
    ): Response<GeneralResponse<List<AdminTaxiRequestModel>?>>


    @POST("$carRequest/responeCarRequest")
    suspend fun requestChangeStatusOfTaxiRequests(
        @Body request: TaxiChangeStatusRequest,
        @Header("Authorization") token: String
    ): Response<GeneralResponse<Boolean?>>


    @POST("$carRequest/assign-Driver-ToRrquest")
    suspend fun requestAssignDriverToRequest(
        @Body request: AssignDriverRequest,
        @Header("Authorization") token: String
    ): Response<GeneralResponse<Boolean?>>

    @GET("$carRequest/get-Drivers")
    suspend fun requestGetDriver(
        @Query("driverType") type: com.zarholding.zar.model.data.enum.EnumDriverType,
        @Query("companyCode") companyCode: String?,
        @Header("Authorization") token: String
    ): Response<GeneralResponse<List<DriverModel>?>>

    @POST("$carRequest/driverResponeCarRequest")
    suspend fun requestDriverChangeTripStatus(
        @Body request: DriverChangeTripStatus,
        @Header("Authorization") token: String
    ): Response<GeneralResponse<Boolean?>>
    //---------------------------------------------------------------------------------------------- carRequest


    //---------------------------------------------------------------------------------------------- notification
    @POST("$notification/unread-count-by-user")
    suspend fun requestGetNotificationUnreadCount(
        @Body request: NotificationUnreadCountRequestModel,
        @Header("Authorization") token: String
    ): GeneralResponse<NotificationUnreadCountModel?>


    @GET("$notification/get-messages")
    suspend fun requestGetNotification(
        @Header("Authorization") token: String
    ): Response<GeneralResponse<List<NotificationModel>?>>

    @POST("$notification/set-read")
    suspend fun requestReadNotification(
        @Body ids: List<Int>,
        @Header("Authorization") token: String
    ): Response<GeneralResponse<Boolean?>>
    //---------------------------------------------------------------------------------------------- notification


    //---------------------------------------------------------------------------------------------- survey
    @GET("$survey/user-surveys")
    suspend fun requestGetUserSurveys(
        @Query("jobKeyCode") jobKeyCode: String?,
        @Query("companyCode") companyCode: String?,
        @Header("Authorization") token: String
    ): Response<GeneralResponse<SurveyItemModel?>>

    @GET("$survey/Get-Survey-Form")
    suspend fun requestGetSurveyDetail(
        @Query("SurveyId") surveyId: Long,
        @Header("Authorization") token: String
    ): Response<GeneralResponse<SurveyDetailModel?>>


    @POST("$survey/Survey-Form")
    suspend fun requestSubmitSurvey(
        @Body request: List<SubmitSurveyQuestionRequestModel>,
        @Header("Authorization") token: String
    ): Response<GeneralResponse<Boolean?>>
    //---------------------------------------------------------------------------------------------- survey


    //---------------------------------------------------------------------------------------------- requestGetAppVersion
    @GET("$files/files-get-appVersion")
    suspend fun requestGetAppVersion(
        @Query("app") app: String
    ): GeneralResponse<AppVersionModel?>

    @Streaming
    @GET("$files/files-get-apk")
    suspend fun downloadApkFile(
        @Query("SystemType") systemType: String,
        @Query("EntityType") entityType: EnumEntityType,
        @Query("FileName") fileName: String
    ): Response<ResponseBody>
    //---------------------------------------------------------------------------------------------- requestGetAppVersion



    //---------------------------------------------------------------------------------------------- video
    @GET("$video/get-VideoGroups")
    suspend fun getVideoCategory(
        @Header("Authorization") token: String
    ): GeneralResponse<List<EducationCategoryModel>?>


    @GET("$video/get-Videolist/{videoGroupId}")
    suspend fun getVideoList(
        @Path("videoGroupId") videoGroupId: Long,
        @Query("PageNumber") pageNumber: Int,
        @Query("Pagesize") pagesize: Int,
        @Header("Authorization") token: String
    ): Response<GeneralResponse<PagingListModel<EducationModel>?>>

    @GET("$video/get-VideoDetaillist")
    suspend fun getVideoDetail(
        @Query("VideoId") videoId: Long,
        @Query("PageNumber") pageNumber: Int,
        @Query("Pagesize") pagesize: Int,
        @Header("Authorization") token: String
    ): Response<GeneralResponse<PagingListModel<EducationDetailModel>?>>
    //---------------------------------------------------------------------------------------------- video


    //---------------------------------------------------------------------------------------------- personnelQuota
    @GET("$personnelQuota/get-MyQuotas")
    suspend fun getMyQuota(
        @Header("Authorization") token: String
    ): Response<GeneralResponse<List<QuotaModel>?>>

    @FormUrlEncoded
    @POST("$personnelQuota/assign-MyQuotaToAnOther")
    suspend fun assignMyQuotaToAnother(
        @Field("UserId") userId: Int,
        @Field("QuotaId") quotaId: Long,
        @Header("Authorization") token: String
    ): Response<GeneralResponse<Boolean?>>

    @FormUrlEncoded
    @POST("$personnelQuota/approve-Quota")
    suspend fun approveQuota(
        @Field("status") status: EnumQuotaApprove?,
        @Field("QuotaId") quotaId: Long,
        @Header("Authorization") token: String
    ): Response<GeneralResponse<Boolean?>>

    @GET("$personnelQuota/Get-historyOfMyQuotas")
    suspend fun getMyQuotaHistory(
        @Query("PageNumber") pageNumber: Int,
        @Query("Pagesize") pagesize: Int,
        @Header("Authorization") token: String
    ): Response<GeneralResponse<PagingListModel<QuotaHistoryModel>?>>
    //---------------------------------------------------------------------------------------------- personnelQuota


    //---------------------------------------------------------------------------------------------- openStreetMap
    @GET("$openStreetMap/get-Address")
    suspend fun requestGetAddress(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double
    ): GeneralResponse<AddressResponseModel?>

    @POST("$openStreetMap/get-SuggestionAddress")
    suspend fun requestGetSuggestionAddress(
        @Body request: SuggestionRequestModel
    ): GeneralResponse<List<AddressSuggestionModel>?>

    //---------------------------------------------------------------------------------------------- openStreetMap



    //---------------------------------------------------------------------------------------------- supplementalInsurance

    @GET("$supplementalInsurance/list-supplemental")
    suspend fun requestGetListOfSupplementalInsurance(
        @Query("forSignUp") forSignUp: Boolean,
        @Header("Authorization") token: String
    ): Response<GeneralResponse<PagingListModel<SupplementalInsuranceModel>?>>

    @GET("$supplementalInsurance/list-supplementalLevels")
    suspend fun requestGetSupplementalLevels(
        @Query("InsuranceId") insuranceId: Long,
        @Header("Authorization") token: String
    ): Response<GeneralResponse<List<SupplementalInsuranceLevelModel>?>>

    @POST("$supplementalInsurance/Register-supplementalInsurance")
    suspend fun requestRegisterSupplementalInsurance(
        @Body request: RegisterSupplementalInsuranceModel,
        @Header("Authorization") token: String
    ): Response<GeneralResponse<Boolean?>>

    @GET("$supplementalInsurance/list-supplementalHistory")
    suspend fun requestSupplementalHistory(
        @Header("Authorization") token: String
    ): Response<GeneralResponse<PagingListModel<SupplementalInsuranceHistoryModel>?>>

    //---------------------------------------------------------------------------------------------- supplementalInsurance


    //---------------------------------------------------------------------------------------------- requestGetCompanies
    @GET("$dropDown/get-companies")
    suspend fun requestGetCompanies(
        @Header("Authorization") token: String
    ): GeneralResponse<DropDownItemsModel?>

    @GET("$dropDown/get-sites")
    suspend fun requestGetSites(
        @Header("Authorization") token: String
    ): GeneralResponse<DropDownItemsModel?>

    @GET("$dropDown/get-relationShip")
    suspend fun requestGetRelationShip(
        @Header("Authorization") token: String
    ): Response<GeneralResponse<DropDownItemsModel?>>
    //---------------------------------------------------------------------------------------------- requestGetCompanies



    //---------------------------------------------------------------------------------------------- requestGetCompanies
    @GET("$hse/get-listOfPeriods")
    suspend fun requestGetHsePeriods(
        @Header("Authorization") token: String
    ): GeneralResponse<List<DropDownModel>?>

    @GET("$hse/get-ListOfExaminations/{id}")
    suspend fun requestGetExaminations(
        @Path("id") id: Long,
        @Header("Authorization") token: String
    ): GeneralResponse<List<HseExaminationModel>?>
    //---------------------------------------------------------------------------------------------- requestGetCompanies






    @Streaming
    @GET("$content/file")
    suspend fun downloadContentFile(
        @Query("entityType") entityType: EnumEntityType,
        @Query("fileName") fileName: String
    ): Response<ResponseBody>

    @POST("$v1/User/Edit-Myinfo")
    suspend fun requestChangeCarPlaque(
        @Body request: CarPlaqueEditRequest,
        @Header("Authorization") token: String
    ): GeneralResponse<Boolean?>










}