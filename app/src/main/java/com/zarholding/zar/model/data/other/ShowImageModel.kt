package com.zarholding.zar.model.data.other

data class ShowImageModel(
    var imageName : String,
    var entityType : String?,
    var token : String?
)
