package com.zarholding.zar.model.repository

import com.zar.core.tools.api.apiCall
import com.zarholding.zar.model.data.enum.EnumPersonnelType
import com.zarholding.zar.model.data.enum.EnumTaxiRequestType
import com.zarholding.zar.model.data.request.*
import com.zarholding.zar.model.data.request.taxi.TaxiRequestProModel
import com.zarholding.zar.model.data.response.GeneralResponse
import com.zarholding.zar.model.data.response.NetworkResult
import com.zarholding.zar.model.data.response.taxi.AdminTaxiRequestModel
import com.zarholding.zar.model.data.response.taxi.TaxiRequestPointModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import retrofit2.Response
import javax.inject.Inject

/**
 * Created by m-latifi on ۰۸/۰۲/۲۰۲۳.
 */

class TaxiRepository @Inject constructor(
    private val userRepository: UserRepository
) : ZarRepository() {

    private var pageNumber = 0
    private val pageSize = 100
    val request = AdminTaxiListRequest(pageNumber, pageSize, null)



    //---------------------------------------------------------------------------------------------- requestTaxi
    fun requestTaxi(
        type: EnumTaxiRequestType,
        departureDate: String?,
        departureTime: String?,
        returnDate: String?,
        returnTime: String?,
        passengers: List<Int>,
        travelReason: String,
        siteId: Int?,
        items: List<TaxiRequestPointModel>
    ) = flow {
        val request = TaxiRequestProModel(
            type = type,
            departureDate = departureDate.toString().replace("/", ""),
            departureTime = departureTime.toString(),
            returnDate = returnDate?.replace("/", ""),
            returnTime = returnTime,
            passengers = passengers,
            travelReason = travelReason,
            siteId = siteId,
            personnelJobKeyCode = userRepository.getUser()?.personnelJobKeyCode,
            items = items
        )
        val response = api.requestTaxi(
            request = request,
            token = tokenRepository.getBearerToken()
        )
        if (response.data == null)
            emit(emitError(response.message))
        else
            emit(NetworkResult.Success(response.data))
    }.catch { e -> emit(handleFlowException(e)) }
    //---------------------------------------------------------------------------------------------- requestTaxi


    //---------------------------------------------------------------------------------------------- requestTaxiList
    suspend fun requestTaxiList(enumPersonnelType: EnumPersonnelType):
            Response<GeneralResponse<List<AdminTaxiRequestModel>?>>? {
        request.PageNumber++
        request.UserType = enumPersonnelType
        return apiCall { api.requestTaxiList(request, tokenRepository.getBearerToken()) }
    }
    //---------------------------------------------------------------------------------------------- requestTaxiList


    //---------------------------------------------------------------------------------------------- requestMyTaxiRequestList
    suspend fun requestMyTaxiRequestList():
            Response<GeneralResponse<List<AdminTaxiRequestModel>?>>? {
        request.PageNumber++
        return apiCall { api.requestMyTaxiRequestList(request, tokenRepository.getBearerToken()) }
    }
    //---------------------------------------------------------------------------------------------- requestMyTaxiRequestList


    //---------------------------------------------------------------------------------------------- requestChangeStatusOfTaxiRequests
    suspend fun requestChangeStatusOfTaxiRequests(request: TaxiChangeStatusRequest):
            Response<GeneralResponse<Boolean?>>? {
        this.request.PageNumber = 0
        return apiCall {
            api.requestChangeStatusOfTaxiRequests(
                request,
                tokenRepository.getBearerToken()
            )
        }
    }
    //---------------------------------------------------------------------------------------------- requestChangeStatusOfTaxiRequests


    //---------------------------------------------------------------------------------------------- requestAssignDriverToRequest
    suspend fun requestAssignDriverToRequest(request: AssignDriverRequest):
            Response<GeneralResponse<Boolean?>>? {
        this.request.PageNumber = 0
        return apiCall {
            api.requestAssignDriverToRequest(
                request,
                tokenRepository.getBearerToken()
            )
        }
    }
    //---------------------------------------------------------------------------------------------- requestAssignDriverToRequest


    //---------------------------------------------------------------------------------------------- requestDriverChangeTripStatus
    suspend fun requestDriverChangeTripStatus(request: DriverChangeTripStatus) =
        apiCall { api.requestDriverChangeTripStatus(request, tokenRepository.getBearerToken()) }
    //---------------------------------------------------------------------------------------------- requestDriverChangeTripStatus

}