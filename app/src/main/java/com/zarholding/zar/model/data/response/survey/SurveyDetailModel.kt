package com.zarholding.zar.model.data.response.survey

/**
 * Created by m-latifi on ۰۸/۰۲/۲۰۲۳.
 */
data class SurveyDetailModel(
    val surveyId: Int,
    val title: String?,
    val description: String?,
    val questions: List<SurveyQuestionModel>?
)
