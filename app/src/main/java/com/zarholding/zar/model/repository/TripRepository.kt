package com.zarholding.zar.model.repository

import com.zar.core.enums.EnumApiError
import com.zar.core.models.ErrorApiModel
import com.zar.core.tools.api.apiCall
import com.zarholding.zar.model.data.request.RequestRegisterStationModel
import com.zarholding.zar.model.data.request.TripRequestRegisterStatusModel
import com.zarholding.zar.model.data.response.NetworkResult
import com.zarholding.zar.model.data.response.trip.TripModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

/**
 * Created by m-latifi on 11/26/2022.
 */

class TripRepository @Inject constructor() : ZarRepository() {

    private var tripList: List<TripModel> = emptyList()


    //---------------------------------------------------------------------------------------------- getTrip
    fun getTrip(
        refreshForce: Boolean,
        isMyTrip: Boolean,
        siteId: Int,
        searchText: String
    ): Flow<NetworkResult<List<TripModel>>> {
        return if (refreshForce || tripList.isEmpty())
            requestGetAllTrips(
                isMyTrip = isMyTrip,
                siteId = siteId,
                searchText = searchText
            )
        else {
            if (isMyTrip)
                getMyTripFilter()
            else
                getAllTripFilter(siteId = siteId, searchText = searchText)
        }
    }
    //---------------------------------------------------------------------------------------------- getTrip


    //---------------------------------------------------------------------------------------------- requestGetAllTrips
    private fun requestGetAllTrips(
        isMyTrip: Boolean,
        siteId: Int,
        searchText: String
    ) = flow {
        val response =
            api.requestGetAllTrips(tokenRepository.getBearerToken())
        if (response.data.isNullOrEmpty())
            emit(
                NetworkResult.Failure(
                    ErrorApiModel(
                        type = EnumApiError.Error,
                        message = response.message
                    )
                )
            )
        else {
            tripList = response.data
            if (isMyTrip)
                getMyTripFilter().collect {
                    emit(NetworkResult.Success(it.data))
                }
            else
                getAllTripFilter(siteId = siteId, searchText = searchText).collect {
                    emit(NetworkResult.Success(it.data))
                }
        }
    }.catch { e ->
        emit(handleFlowException(e))
    }
    //---------------------------------------------------------------------------------------------- requestGetAllTrips


    //---------------------------------------------------------------------------------------------- getAllTripFilter
    private fun getAllTripFilter(siteId: Int, searchText: String) = flow {
        val list = if (siteId >= 0)
            tripList.filter {
                it.myStationTripId == 0 && (it.siteId == siteId) &&
                        (if (searchText.isEmpty()) true else {
                            (it.originName?.contains(searchText) == true) ||
                                    (it.destinationName?.contains(searchText) == true) ||
                                    (it.commuteDriverName?.contains(searchText) == true) ||
                                    (it.stations?.any { item ->
                                        item.stationName?.contains(
                                            searchText
                                        ) == true
                                    } == true)
                        })
            }
        else
            tripList.filter { it.myStationTripId == 0 }
        emit(NetworkResult.Success(list))
    }
    //---------------------------------------------------------------------------------------------- getAllTripFilter


    //---------------------------------------------------------------------------------------------- getAllTripFilter
    private fun getMyTripFilter() = flow {
        val list = tripList.filter { it.myStationTripId != 0 }
        emit(NetworkResult.Success(list))
    }
    //---------------------------------------------------------------------------------------------- getAllTripFilter


    //---------------------------------------------------------------------------------------------- requestRegisterStation
    suspend fun requestRegisterStation(request: RequestRegisterStationModel) =
        apiCall { api.requestRegisterStation(request, tokenRepository.getBearerToken()) }
    //---------------------------------------------------------------------------------------------- requestRegisterStation


    //---------------------------------------------------------------------------------------------- requestDeleteRegisteredStation
    suspend fun requestDeleteRegisteredStation(id: Int) =
        apiCall { api.requestDeleteRegisteredStation(id, tokenRepository.getBearerToken()) }
    //---------------------------------------------------------------------------------------------- requestDeleteRegisteredStation


    //---------------------------------------------------------------------------------------------- requestGetTripForRegister
    suspend fun requestGetTripForRegister() =
        apiCall { api.requestGetTripForRegister(tokenRepository.getBearerToken()) }
    //---------------------------------------------------------------------------------------------- requestGetTripForRegister


    //---------------------------------------------------------------------------------------------- requestChangeStatusTripRegister
    suspend fun requestChangeStatusTripRegister(
        request: List<TripRequestRegisterStatusModel>
    ) = apiCall { api.requestChangeStatusTripRegister(request, tokenRepository.getBearerToken()) }
    //---------------------------------------------------------------------------------------------- requestChangeStatusTripRegister

}