package com.zarholding.zar.model.data.response.article

import com.zarholding.zar.model.data.database.entity.ArticleEntity

data class ArticleItemsModel(
    val items : List<ArticleEntity?>
)
