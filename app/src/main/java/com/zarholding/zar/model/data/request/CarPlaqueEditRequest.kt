package com.zarholding.zar.model.data.request

/**
 * Create by Mehrdad on 1/8/2023
 */
data class CarPlaqueEditRequest(
    val carModel : String?,
    val pelak : String
)
