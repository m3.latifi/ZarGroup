package com.zarholding.zar

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.zar.core.enums.EnumApiError
import com.zarholding.zar.view.activity.MainActivity

/**
 * Created by m-latifi on 10/8/2022.
 */

abstract class ZarFragment<DB : ViewDataBinding> : Fragment() {

    abstract var layout : Int
    protected lateinit var binding : DB

    //---------------------------------------------------------------------------------------------- onCreateView
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, layout, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }
    //---------------------------------------------------------------------------------------------- onCreateView


    //---------------------------------------------------------------------------------------------- showMessage
    fun showMessage(message: String, type: EnumApiError = EnumApiError.Warning, duration: Int = 3) {
        activity?.let {
            (it as MainActivity).showMessage(
                message = message,
                type = type,
                duration = duration)
        }
    }
    //---------------------------------------------------------------------------------------------- showMessage


    //---------------------------------------------------------------------------------------------- gotoFragment
    protected fun gotoFragment(fragment: Int, bundle: Bundle? = null) {
        try {
            findNavController().navigate(fragment, bundle)
        } catch (e: java.lang.Exception){
            findNavController().navigate(R.id.action_goto_SplashFragment)
        }
    }
    //---------------------------------------------------------------------------------------------- gotoFragment


}