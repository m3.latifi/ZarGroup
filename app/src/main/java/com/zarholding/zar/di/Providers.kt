package com.zarholding.zar.di

import android.content.Context
import com.zarholding.zar.model.api.ApiSuperApp
import com.zarholding.zar.tools.manager.LoggerManager
import com.zarholding.zar.tools.manager.NotificationManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by m-latifi on 11/8/2022.
 */

@Module
@InstallIn(SingletonComponent::class)
class Providers {

    companion object {
//        const val url = "http://5.160.125.98:5081"
//        const val url = "http://192.168.50.153:9091"
//        const val url = "http://192.168.50.113:5555"
//        const val url = "http://192.168.50.110:5081"
//        const val url = "https://varanegar-prd.zarholding.com:4433"//supperApp test 110
        const val url = "https://app.zarholding.com:5081"//supperApp
    }

    //---------------------------------------------------------------------------------------------- provideBPMSUrl
    @Provides
    @Singleton
    fun provideBPMSUrl() = url
    //---------------------------------------------------------------------------------------------- provideBPMSUrl



    //---------------------------------------------------------------------------------------------- provideApiBPMS
    @Provides
    @Singleton
    fun provideApiBPMS(retrofit: Retrofit) : ApiSuperApp =
        retrofit.create(ApiSuperApp::class.java)
    //---------------------------------------------------------------------------------------------- provideApiBPMS


    //---------------------------------------------------------------------------------------------- providerLoggerManager
    @Provides
    @Singleton
    fun providerLoggerManager(@ApplicationContext context: Context) = LoggerManager(context)
    //---------------------------------------------------------------------------------------------- providerLoggerManager


    //---------------------------------------------------------------------------------------------- provideNotificationManager
    @Provides
    @Singleton
    fun provideNotificationManager(@ApplicationContext context: Context) =
        NotificationManager(context)
    //---------------------------------------------------------------------------------------------- provideNotificationManager

}