package com.zarholding.zar.view.adapter.holder.hse

import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemHseExaminationBinding
import com.zarholding.zar.model.data.request.hse.HseExaminationModel

/**
 * Created by m-latifi on 11/4/2023.
 */

class HseExaminationHolder(
    private val binding: ItemHseExaminationBinding,
    private val onClick: (String) -> Unit
) : RecyclerView.ViewHolder(binding.root) {


    //---------------------------------------------------------------------------------------------- bind
    fun bind(item: HseExaminationModel) {
        binding.textViewTitle.text = item.examinationTypeText
        binding.root.setOnClickListener {
            item.fileName?.let { onClick.invoke(item.fileName) }
        }
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind

}