package com.zarholding.zar.view.fragment.user.bus

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Size
import android.view.*
import android.widget.ImageView
import android.widget.Spinner
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.button.MaterialButton
import com.skydoves.powerspinner.IconSpinnerAdapter
import com.skydoves.powerspinner.IconSpinnerItem
import com.zar.core.enums.EnumApiError
import com.zar.core.tools.manager.DialogManager
import com.zarholding.zar.R
import com.zarholding.zar.model.data.response.trip.TripModel
import com.zarholding.zar.tools.manager.NotificationManager
import com.zarholding.zar.tools.manager.OsmManager
import com.zarholding.zar.view.activity.MainActivity
import com.zarholding.zar.view.dialog.ShowImageDialog
import com.zarholding.zar.view.adapter.recycler.MyServiceAdapter
import com.zarholding.zar.view.adapter.recycler.ServiceAdapter
import com.zarholding.zar.view.adapter.SpinnerStringAdapter
import com.zarholding.zar.view.dialog.ConfirmDialog
import com.zarholding.zar.ZarFragment
import com.zarholding.zar.databinding.FragmentServiceBinding
import com.zarholding.zar.ext.config
import com.zarholding.zar.ext.startLoading
import com.zarholding.zar.ext.stopLoading
import com.zarholding.zar.model.data.enum.EnumStatus
import com.zarholding.zar.model.data.response.ResponseResult
import com.zarholding.zar.tools.manager.getShimmerBuild
import com.zarholding.zar.tools.signalr.RemoteSignalREmitter
import com.zarholding.zar.tools.signalr.SignalRListenerBus
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.overlay.Marker
import javax.inject.Inject


/**
 * Created by m-latifi on 11/19/2022.
 */

@AndroidEntryPoint
class BusServiceFragment(override var layout: Int = R.layout.fragment_service) :
    ZarFragment<FragmentServiceBinding>() {

    @Inject
    lateinit var notificationManager: NotificationManager

    private val viewModel: BusServiceViewModel by viewModels()

    private lateinit var osmManager: OsmManager
    private var markerCar: Marker? = null
    private var job: Job? = null
    private var jobResetMoveMap: Job? = null
    private var signalRListener: SignalRListenerBus? = null
    private var moveMap = false
    private var tripId = 0
    private var stationId = 0
    private var selectedTripType = TripSelect.ALL
    private var searchJob: Job? = null

    //---------------------------------------------------------------------------------------------- ShowTrip
    private enum class TripSelect {
        ALL,
        MY
    }
    //---------------------------------------------------------------------------------------------- ShowTrip


    //---------------------------------------------------------------------------------------------- onViewCreated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }
    //---------------------------------------------------------------------------------------------- onViewCreated


    //---------------------------------------------------------------------------------------------- initView
    private fun initView() {
        binding.shimmerViewContainer.config(getShimmerBuild())
        osmManager = OsmManager(binding.mapView)
        osmManager.mapInitialize()
        setListener()
        selectedTripType = TripSelect.ALL
        observeLiveDate()
        getTripList(isMyTrip = false)
    }
    //---------------------------------------------------------------------------------------------- initView


    //---------------------------------------------------------------------------------------------- setListener
    @SuppressLint("ClickableViewAccessibility")
    private fun setListener() {
        binding.textViewMyService.setOnClickListener { getTripList(isMyTrip = true) }
        binding.textViewListService.setOnClickListener { selectListAllServices() }
        binding.mapView.setOnTouchListener { _, _ ->
            moveMap = false
            resetMoveMap()
            false
        }

        binding.textInputEditTextSearch.addTextChangedListener {
            searchJob?.cancel()
            createJobForSearch(it.toString())
        }

        binding.textViewCloseMap.setOnClickListener { binding.expandableMap.collapse() }
    }
    //---------------------------------------------------------------------------------------------- setListener


    //---------------------------------------------------------------------------------------------- resetMoveMap
    private fun resetMoveMap() {
        jobResetMoveMap?.cancel()
        jobResetMoveMap = CoroutineScope(IO).launch {
            delay(5000)
            moveMap = true
        }
    }
    //---------------------------------------------------------------------------------------------- resetMoveMap


    //---------------------------------------------------------------------------------------------- observeLiveDate
    private fun observeLiveDate() {
        viewModel.errorLiveDate.observe(viewLifecycleOwner) {
            binding.shimmerViewContainer.stopLoading()
            showMessage(it.message, it.type)
            when (it.type) {
                EnumApiError.UnAuthorization -> (activity as MainActivity?)?.gotoFirstFragmentWithDeleteUser()
                else -> {}
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {

                launch {
                    viewModel.allTrip.collect {
                        binding.shimmerViewContainer.stopLoading()
                        when(it) {
                            is ResponseResult.Loading -> {}
                            is ResponseResult.Success -> { setServiceAdapter(it.data) }
                        }
                    }
                }

                launch {
                    viewModel.myTrip.collect {
                        binding.shimmerViewContainer.stopLoading()
                        when(it) {
                            is ResponseResult.Loading -> {}
                            is ResponseResult.Success -> { checkMyServiceList(it.data) }
                        }
                    }
                }

                launch {
                    viewModel.getCompanies.collect {
                        when(it) {
                            is ResponseResult.Loading -> {}
                            is ResponseResult.Success -> { initCompanySpinner() }
                        }
                    }
                }

            }
        }

    }
    //---------------------------------------------------------------------------------------------- observeLiveDate


    //---------------------------------------------------------------------------------------------- getTripList
    private fun getTripList(isMyTrip: Boolean) {
        osmManager.clearOverlays()
        binding.shimmerViewContainer.startLoading()
        viewModel.getTripList(isMyTrip = isMyTrip)
    }
    //---------------------------------------------------------------------------------------------- getTripList


    //---------------------------------------------------------------------------------------------- openCall
    private fun openCall(number: String?) {
        number?.let {
            val tel = number.replace("[^0-9]".toRegex(), "")
            if (tel.isEmpty())
                return
            val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", tel, null))
            startActivity(intent)
        } ?: run {
            showMessage(getString(R.string.driversMobileIsEmpty))
        }
    }
    //---------------------------------------------------------------------------------------------- openCall


    //---------------------------------------------------------------------------------------------- selectListAllServices
    private fun selectListAllServices() {
        getTripList(isMyTrip = false)
        binding.textInputEditTextSearch.setText(viewModel.searchText)
        if (viewModel.companyCode != -1) {
            val index = viewModel.companiesList.indexOfFirst { it.value.toInt() == viewModel.companyCode }
            binding.powerSpinnerCompany.selectItemByIndex(index)
            binding.textInputLayoutSearch.visibility = View.VISIBLE
            binding.powerSpinnerCompany.showArrow = false
            binding.powerSpinnerCompany.setBackgroundResource(R.drawable.a_drawable_primary_curve)
        } else {
            binding.powerSpinnerCompany.text = getString(R.string.pleaseSelectCompany)
            binding.powerSpinnerCompany.clearSelectedItem()
            binding.powerSpinnerCompany.showArrow = true
            binding.powerSpinnerCompany.setBackgroundResource(R.drawable.a_drawable_spinner)
            binding.textInputLayoutSearch.visibility = View.GONE
        }
        binding.powerSpinnerCompany.visibility = View.VISIBLE
    }
    //---------------------------------------------------------------------------------------------- selectListAllServices


    //---------------------------------------------------------------------------------------------- setServiceAdapter
    private fun setServiceAdapter(tripList: List<TripModel>) {
        binding.textViewListService.setBackgroundResource(R.drawable.a_drawable_trip_select_button)
        binding.textViewMyService.setBackgroundResource(R.drawable.a_drawable_trip_unselect_button)
        if (context == null)
            return
        val adapter = ServiceAdapter(
            tripList = tripList,
            onClick = { drawRoadOnMap(it, TripSelect.ALL) },
            onRegisterStation = { showDialogRegisterStation(it) },
            onClickImage = {
                ShowImageDialog(
                    requireContext(),
                    it,
                    onShowDialog = { (activity as MainActivity?)?.enableBlurView() },
                    onDismissDialog = { (activity as MainActivity?)?.disableBlurView() }
                ).show()
            }
        )
        val linearLayoutManager = LinearLayoutManager(
            requireContext(),
            LinearLayoutManager.VERTICAL,
            false
        )
//        linearLayoutManager.reverseLayout = true
        binding.recyclerViewService.layoutManager = linearLayoutManager
        binding.recyclerViewService.adapter = adapter
    }
    //---------------------------------------------------------------------------------------------- setServiceAdapter



    //---------------------------------------------------------------------------------------------- checkMyServiceList
    private fun checkMyServiceList(items: List<TripModel>) {
        binding.powerSpinnerCompany.visibility = View.GONE
        binding.textInputLayoutSearch.visibility = View.GONE
        binding.textViewMyService.setBackgroundResource(R.drawable.a_drawable_trip_select_button)
        binding.textViewListService.setBackgroundResource(R.drawable.a_drawable_trip_unselect_button)
        if (items.isNotEmpty())
            setMyServiceAdapter(items)
        else {
            binding.shimmerViewContainer.stopLoading()
            showMessage(getString(R.string.userServiceIsEmpty))
            selectListAllServices()
        }
    }
    //---------------------------------------------------------------------------------------------- checkMyServiceList


    //---------------------------------------------------------------------------------------------- setMyServiceAdapter
    private fun setMyServiceAdapter(tripList: List<TripModel>) {
        if (context == null)
            return
        val adapter = MyServiceAdapter(
            tripList = tripList,
            onClick = { drawRoadOnMap(it, TripSelect.MY) },
            onDeleteRegisterStation = { showDialogDeleteRegisterStation(it) },
            onClickImage = {
                ShowImageDialog(
                    requireContext(),
                    it,
                    onShowDialog = { (activity as MainActivity?)?.enableBlurView() },
                    onDismissDialog = { (activity as MainActivity?)?.disableBlurView() }).show()
            },
            onClickCall = {
                openCall(it)
            }
        )
        val linearLayoutManager = LinearLayoutManager(
            requireContext(),
            LinearLayoutManager.VERTICAL,
            false
        )
        binding.recyclerViewService.layoutManager = linearLayoutManager
        binding.recyclerViewService.adapter = adapter
    }
    //---------------------------------------------------------------------------------------------- setMyServiceAdapter



    //---------------------------------------------------------------------------------------------- showDialogRegisterStation
    private fun showDialogRegisterStation(item: TripModel) {
        if (context == null)
            return
        item.stations?.let {
            val adapter = SpinnerStringAdapter(it)
            val dialog = DialogManager().createDialogHeightWrapContent(
                requireContext(),
                R.layout.dialog_register_station,
                Gravity.BOTTOM,
                150
            )
            val spinner = dialog.findViewById<Spinner>(R.id.spinnerStations)
            spinner.adapter = adapter
            val imageClose = dialog.findViewById<ImageView>(R.id.imageViewClose)
            imageClose.setOnClickListener { dialog.dismiss() }
            val buttonRegister = dialog.findViewById<MaterialButton>(R.id.buttonRegister)
            buttonRegister.setOnClickListener {
                showDialogConfirmRegisterStation(
                    item.commuteTripName,
                    item.stations[spinner.selectedItemPosition].stationName,
                    item.id,
                    item.stations[spinner.selectedItemPosition].id
                )
                dialog.dismiss()
            }
            dialog.show()
        }
    }
    //---------------------------------------------------------------------------------------------- showDialogRegisterStation


    //---------------------------------------------------------------------------------------------- showDialogConfirmRegisterStation
    private fun showDialogConfirmRegisterStation(
        tripName: String?,
        stationName: String?,
        tripId: Int,
        stationId: Int
    ) {
        if (context == null)
            return
        ConfirmDialog(
            context = requireContext(),
            type = ConfirmDialog.ConfirmType.WARNING,
            title = getString(R.string.confirmForRegisterStation, stationName, tripName),
            onYesClick = { requestRegisterStation(tripId, stationId) },
            onShowDialog = { (activity as MainActivity?)?.enableBlurView() },
            onDismissDialog = { (activity as MainActivity?)?.disableBlurView() }
        ).show()
    }
    //---------------------------------------------------------------------------------------------- showDialogConfirmRegisterStation


    //---------------------------------------------------------------------------------------------- requestRegisterStation
    private fun requestRegisterStation(tripId: Int, stationId: Int) {
        binding.shimmerViewContainer.startLoading()
        binding.recyclerViewService.adapter = null
        selectedTripType = TripSelect.MY
        viewModel.requestRegisterStation(tripId, stationId)
    }
    //---------------------------------------------------------------------------------------------- requestRegisterStation


    //---------------------------------------------------------------------------------------------- showDialogDeleteRegisterStation
    private fun showDialogDeleteRegisterStation(item: TripModel) {
        if (context == null)
            return
        ConfirmDialog(
            context = requireContext(),
            type = ConfirmDialog.ConfirmType.DELETE,
            title = getString(R.string.confirmForDelete, item.myStationName),
            onYesClick = { requestDeleteRegisterStation(item.myStationTripId) },
            onShowDialog = { (activity as MainActivity?)?.enableBlurView() },
            onDismissDialog = { (activity as MainActivity?)?.disableBlurView() }
        ).show()
    }
    //---------------------------------------------------------------------------------------------- showDialogDeleteRegisterStation


    //---------------------------------------------------------------------------------------------- requestDeleteRegisterStation
    private fun requestDeleteRegisterStation(stationId: Int) {
        binding.shimmerViewContainer.startLoading()
        binding.recyclerViewService.adapter = null
        selectedTripType = TripSelect.MY
        viewModel.requestDeleteRegisteredStation(stationId)
    }
    //---------------------------------------------------------------------------------------------- requestDeleteRegisterStation


    //---------------------------------------------------------------------------------------------- drawRoadOnMap
    private fun drawRoadOnMap(item: TripModel, tripSelect: TripSelect) {
        if (binding.textViewLoading.visibility == View.VISIBLE)
            return
        if (!binding.expandableMap.isExpanded)
            binding.expandableMap.expand()
        binding.textViewLoading.visibility = View.VISIBLE
        tripId = item.id
        stationId = item.myStationTripId

        job = CoroutineScope(IO).launch {

            CoroutineScope(IO).launch {
                delay(300)
                if (!item.tripPoints.isNullOrEmpty())
                    osmManager.drawPolyLine(item.tripPoints)
            }.join()

            CoroutineScope(IO).launch {
                delay(300)
                if (!item.stations.isNullOrEmpty())
                    osmManager.addStationMarker(item.stations)
            }.join()

            CoroutineScope(Main).launch {
                delay(300)
                binding.textViewLoading.visibility = View.GONE
                if (tripSelect == TripSelect.MY && item.myStationTripStatus == EnumStatus.Confirmed)
                    startSignalR()
            }.join()
        }
    }
    //---------------------------------------------------------------------------------------------- drawRoadOnMap


    //---------------------------------------------------------------------------------------------- startSignalR
    private fun startSignalR() {
        signalRListener?.stopConnection()
        markerCar = null
        val remote = object : RemoteSignalREmitter {
            override fun onConnectToSignalR() {
                signalRListener?.registerToGroupForService(tripId, stationId)
            }

            override fun onGetPoint(lat: String, lng: String) {
                CoroutineScope(IO).launch {
                    withContext(Main) {
                        moveCarMarker(GeoPoint(lat.toDouble(), lng.toDouble()))
                    }
                }
            }

            override fun onPreviousStationReached(message: String) {
                showNotificationPreviousStationReached()
            }
        }

        CoroutineScope(IO).launch {
            delay(2000)
            signalRListener = SignalRListenerBus(remote, viewModel.getToken())
            signalRListener?.startConnection()
            moveMap = true
        }
    }
    //---------------------------------------------------------------------------------------------- startSignalR


    //---------------------------------------------------------------------------------------------- moveCarMarker
    private fun moveCarMarker(position: GeoPoint) {
        markerCar?.let {
            it.position = position
        } ?: run {
            val iconBus = osmManager
                .createMarkerIconDrawable(Size(70, 100), R.drawable.a_icon_bus_marker)
            markerCar = osmManager.addMarker(iconBus, position, null)
        }
        if (moveMap)
            osmManager.moveCamera(position)
        else
            binding.mapView.invalidate()
    }
    //---------------------------------------------------------------------------------------------- moveCarMarker


    //---------------------------------------------------------------------------------------------- showNotificationPreviousStationReached
    private fun showNotificationPreviousStationReached() {
        notificationManager.showNotification(
            0,
            getString(R.string.messagePreviousStationReached),
            null
        )
    }
    //---------------------------------------------------------------------------------------------- showNotificationPreviousStationReached


    //---------------------------------------------------------------------------------------------- initCompanySpinner
    private fun initCompanySpinner() {
        if (viewModel.companiesList.isNotEmpty()) {
            val items = viewModel.companiesList.map { company ->
                IconSpinnerItem(company.text)
            }
            binding.powerSpinnerCompany.apply {
                setSpinnerAdapter(IconSpinnerAdapter(this))
                setItems(items)
                getSpinnerRecyclerView().layoutManager =
                    LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                lifecycleOwner = viewLifecycleOwner
                setOnSpinnerOutsideTouchListener { _, _ ->  dismiss()}
                setOnSpinnerItemSelectedListener<IconSpinnerItem> { _, _, newIndex, _ ->
                    binding.textInputLayoutSearch.visibility = View.VISIBLE
                    binding.powerSpinnerCompany
                        .setBackgroundResource(R.drawable.a_drawable_primary_curve)
                    viewModel.companyCode = viewModel.companiesList[newIndex].value.toInt()
                    viewModel.searchText = binding.textInputEditTextSearch.text.toString()
                    viewModel.getTripList(
                        isMyTrip = false
                    )
                }
            }
            binding.powerSpinnerCompany.visibility = View.VISIBLE
        }
    }
    //---------------------------------------------------------------------------------------------- initCompanySpinner


    //---------------------------------------------------------------------------------------------- createJobForSearch
    private fun createJobForSearch(search: String) {
        searchJob = CoroutineScope(IO).launch {
            if (binding.powerSpinnerCompany.selectedIndex < 0)
                return@launch
            delay(500)
            withContext(Main) {
                if (viewModel.companiesList.isNotEmpty() && binding.powerSpinnerCompany.selectedIndex != -1) {
                    viewModel.companyCode = viewModel.companiesList[binding.powerSpinnerCompany.selectedIndex].value.toInt()
                    viewModel.searchText = search
                    viewModel.getTripList(
                        isMyTrip = false
                    )
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- createJobForSearch


    //---------------------------------------------------------------------------------------------- onDestroyView
    override fun onDestroyView() {
        super.onDestroyView()
        job?.cancel()
        searchJob?.cancel()
        signalRListener?.stopConnection()
        jobResetMoveMap?.cancel()
        binding.mapView.onPause()
    }
    //---------------------------------------------------------------------------------------------- onDestroyView


}