package com.zarholding.zar.view.fragment.user.hse

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.GridLayoutManager
import com.zarholding.zar.R
import com.zarholding.zar.ZarFragment
import com.zarholding.zar.databinding.FragmentHsePeriodBinding
import com.zarholding.zar.ext.config
import com.zarholding.zar.ext.startLoading
import com.zarholding.zar.ext.stopLoading
import com.zarholding.zar.model.data.response.ResponseResult
import com.zarholding.zar.model.data.response.dropdown.DropDownModel
import com.zarholding.zar.tools.CompanionValues
import com.zarholding.zar.tools.manager.getShimmerBuild
import com.zarholding.zar.view.adapter.recycler.hse.HsePeriodAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

/**
 * Created by m-latifi on 11/9/2022.
 */

@AndroidEntryPoint
class HsePeriodFragment(override var layout: Int = R.layout.fragment_hse_period) :
    ZarFragment<FragmentHsePeriodBinding>() {

    private val viewModel: HseViewModel by viewModels()


    //---------------------------------------------------------------------------------------------- onViewCreated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()

    }
    //---------------------------------------------------------------------------------------------- onViewCreated


    //---------------------------------------------------------------------------------------------- initView
    private fun initView() {
        binding.shimmerViewContainer.config(getShimmerBuild())
        observeLiveDate()
        getHsePeriods()
    }
    //---------------------------------------------------------------------------------------------- initView



    //---------------------------------------------------------------------------------------------- getHsePeriods
    private fun getHsePeriods() {
        binding.shimmerViewContainer.startLoading()
        viewModel.requestGetHsePeriods()
    }
    //---------------------------------------------------------------------------------------------- getHsePeriods


    //---------------------------------------------------------------------------------------------- observeLiveDate
    private fun observeLiveDate() {
        viewModel.errorLiveDate.observe(viewLifecycleOwner) {
            binding.shimmerViewContainer.stopLoading()
            showMessage(it.message, it.type)
        }

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.hsePeriods.collect {
                    when(it) {
                        is ResponseResult.Loading -> {}
                        is ResponseResult.Success -> { setPeriodAdapter(it.data) }
                    }
                }
            }
        }

    }
    //---------------------------------------------------------------------------------------------- observeLiveDate



    //---------------------------------------------------------------------------------------------- setPeriodAdapter
    private fun setPeriodAdapter(items: List<DropDownModel>) {
        binding.shimmerViewContainer.stopLoading()
        if (context == null)
            return
        val adapter = HsePeriodAdapter(items) {
            val bundle = Bundle()
            bundle.putString(CompanionValues.EXAMINATION_ID,it.value)
            gotoFragment(R.id.action_goto_hseExaminationFragment, bundle)
        }
        val gridLayoutManager = GridLayoutManager(
            requireContext(),
            3,
            GridLayoutManager.VERTICAL,
            false
        )
        binding.recyclerHse.layoutManager = gridLayoutManager
        binding.recyclerHse.layoutDirection = View.LAYOUT_DIRECTION_RTL
        binding.recyclerHse.adapter = adapter
    }
    //---------------------------------------------------------------------------------------------- setPeriodAdapter

}