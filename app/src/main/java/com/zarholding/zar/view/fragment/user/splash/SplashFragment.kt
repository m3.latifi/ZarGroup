package com.zarholding.zar.view.fragment.user.splash

import android.Manifest
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.zar.core.enums.EnumApiError
import com.zar.core.tools.manager.PermissionManager
import com.zarholding.zar.R
import com.zarholding.zar.view.activity.MainActivity
import com.zarholding.zar.ZarFragment
import com.zarholding.zar.databinding.FragmentSplashBinding
import com.zarholding.zar.model.data.enum.EnumEntityType
import com.zarholding.zar.model.data.response.ResponseResult
import com.zarholding.zar.tools.CompanionValues
import com.zarholding.zar.tools.manager.MemoryManager
import com.zarholding.zar.view.activity.MainViewModel
import com.zarholding.zar.view.dialog.ConfirmDialog
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import javax.inject.Inject

/**
 * Created by m-latifi on 11/8/2022.
 */


@AndroidEntryPoint
class SplashFragment(override var layout: Int = R.layout.fragment_splash) :
    ZarFragment<FragmentSplashBinding>() {

    private val splashViewModel: SplashViewModel by viewModels()

    @Inject
    lateinit var permissionManager: PermissionManager

    @Inject
    lateinit var memoryManager: MemoryManager

    private var job: Job? = null

    //---------------------------------------------------------------------------------------------- onViewCreated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }
    //---------------------------------------------------------------------------------------------- onViewCreated


    //---------------------------------------------------------------------------------------------- initView
    private fun initView() {
        observeLiveDate()
        binding.buttonReTry.setOnClickListener { notificationPermission() }
        notificationPermission()
    }
    //---------------------------------------------------------------------------------------------- initView


    //---------------------------------------------------------------------------------------------- notificationPermissionLauncher
    private val notificationPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions()
        ) { results ->
            permissionManager.checkPermissionResult(results) {
                if (it)
                    requestGetAppVersion()
            }
        }
    //---------------------------------------------------------------------------------------------- notificationPermissionLauncher



    //---------------------------------------------------------------------------------------------- notificationPermission
    private fun notificationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            val permission = listOf(Manifest.permission.POST_NOTIFICATIONS)
            val check = permissionManager.isPermissionGranted(
                permissions = permission,
                launcher = notificationPermissionLauncher
            )
            if (check) requestGetAppVersion()
        } else requestGetAppVersion()
    }
    //---------------------------------------------------------------------------------------------- notificationPermission


    //---------------------------------------------------------------------------------------------- requestGetAppVersion
    private fun requestGetAppVersion() {
        binding.buttonReTry.visibility = View.GONE
        splashViewModel.requestGetAppVersion()
    }
    //---------------------------------------------------------------------------------------------- requestGetAppVersion


    //---------------------------------------------------------------------------------------------- gotoFragmentLogin
    private fun gotoFragmentLogin() {
        job = CoroutineScope(IO).launch {
            delay(3000)
            withContext(Main) {
                gotoFragment(R.id.action_goto_loginFragment)
            }
        }
    }
    //---------------------------------------------------------------------------------------------- gotoFragmentLogin



    //---------------------------------------------------------------------------------------------- observeLiveDate
    private fun observeLiveDate() {
        splashViewModel.errorLiveDate.observe(viewLifecycleOwner) {
            binding.buttonReTry.visibility = View.VISIBLE
            showMessage(it.message, it.type)
            when (it.type) {
                EnumApiError.UnAuthorization -> (activity as MainActivity?)?.gotoFirstFragmentWithDeleteUser()
                else -> {}
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {

                launch {
                    splashViewModel.downloadLastVersion.collect {
                        when(it) {
                            is ResponseResult.Loading -> {}
                            is ResponseResult.Success -> {
                                storagePermission(fileName = splashViewModel.newVersionFileName)
                            }
                        }
                    }
                }

                launch {
                    splashViewModel.userNotificationUnRead.collect {
                        when(it) {
                            is ResponseResult.Loading -> {}
                            is ResponseResult.Success -> {
                                if (it.data == -1)
                                    gotoFragmentLogin()
                                else {
                                    (activity as MainActivity).setUserInfo()
                                    MainViewModel.notificationCount = it.data
                                    (activity as MainActivity).setNotificationCount(it.data)
                                    gotoFragment(R.id.action_goto_HomeFragment)
                                }
                            }
                        }
                    }
                }

            }
        }

    }
    //---------------------------------------------------------------------------------------------- observeLiveDate


    //---------------------------------------------------------------------------------------------- storagePermissionLauncher
    private val storagePermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions()
        ) { results ->
            permissionManager.checkPermissionResult(results) {
                if (it && splashViewModel.newVersionFileName.isNotEmpty())
                    showDialogUpdateAppVersion(splashViewModel.newVersionFileName)
            }
        }
    //---------------------------------------------------------------------------------------------- storagePermissionLauncher


    //---------------------------------------------------------------------------------------------- cameraPermission
    private fun storagePermission(fileName: String) {
        if (!memoryManager.checkFreeSpaceForDownloading()) {
            showMessage(getString(R.string.internalMemoryIsFull))
            return
        }
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.Q) {
            val permissions = listOf(
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            val check = permissionManager.isPermissionGranted(
                permissions = permissions,
                launcher = storagePermissionLauncher
            )
            if (check)
                showDialogUpdateAppVersion(fileName)
        } else
            showDialogUpdateAppVersion(fileName)
    }
    //---------------------------------------------------------------------------------------------- cameraPermission


    //---------------------------------------------------------------------------------------------- showDialogUpdateAppVersion
    private fun showDialogUpdateAppVersion(fileName: String) {
        if (context == null)
            return
        ConfirmDialog(
            context = requireContext(),
            type = ConfirmDialog.ConfirmType.WARNING,
            title = getString(R.string.doYouWantToUpdateApp),
            onYesClick = { gotoFragmentDownload(fileName) },
            onShowDialog = { (activity as MainActivity?)?.enableBlurView() },
            onDismissDialog = { (activity as MainActivity?)?.disableBlurView() },
            force = true
        ).show()
    }
    //---------------------------------------------------------------------------------------------- showDialogUpdateAppVersion


    //---------------------------------------------------------------------------------------------- gotoFragmentDownload
    private fun gotoFragmentDownload(fileName: String) {
        val bundle = Bundle()
        bundle.putString(CompanionValues.DOWNLOAD_URL, fileName)
        bundle.putString(CompanionValues.DOWNLOAD_TYPE, EnumEntityType.APK.name)
        gotoFragment(R.id.action_goto_DownloadUpdateFragment, bundle)
    }
    //---------------------------------------------------------------------------------------------- gotoFragmentDownload



    //---------------------------------------------------------------------------------------------- onDestroyView
    override fun onDestroyView() {
        super.onDestroyView()
        job?.cancel()
    }
    //---------------------------------------------------------------------------------------------- onDestroyView


}