package com.zarholding.zar.view.fragment.admin.qrcode

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.zar.core.enums.EnumApiError
import com.zarholding.zar.R
import com.zarholding.zar.ZarFragment
import com.zarholding.zar.databinding.FragmentQrReaderBinding
import com.zarholding.zar.ext.startLoading
import com.zarholding.zar.ext.stopLoading
import com.zarholding.zar.model.data.enum.EnumQuotaApprove
import com.zarholding.zar.tools.CompanionValues
import com.zarholding.zar.tools.manager.RoleManager
import com.zarholding.zar.tools.manager.getShimmerBuild
import com.zarholding.zar.view.activity.MainActivity
import dagger.hilt.android.AndroidEntryPoint
import io.github.g00fy2.quickie.QRResult
import io.github.g00fy2.quickie.ScanCustomCode
import io.github.g00fy2.quickie.config.ScannerConfig
import javax.inject.Inject

/**
 * Created by m-latifi on 9/3/2023.
 */

@AndroidEntryPoint
class QrCodeReaderFragment(
    override var layout: Int = R.layout.fragment_qr_reader
) : ZarFragment<FragmentQrReaderBinding>() {

    private val viewModel: QrCodeReaderViewModel by viewModels()

    @Inject
    lateinit var roleManager: RoleManager

    private val scanCustomCode =
        registerForActivityResult(ScanCustomCode()) { result -> handleResult(result) }


    //---------------------------------------------------------------------------------------------- onViewCreated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }
    //---------------------------------------------------------------------------------------------- onViewCreated


    //---------------------------------------------------------------------------------------------- initView
    private fun initView() {
        binding.shimmerViewContainer.setShimmer(getShimmerBuild())
        setListener()
        observeLiveDate()
        val type = arguments?.getString(CompanionValues.TYPE_OBJECT, null)
        if (type == null)
            activity?.onBackPressedDispatcher?.onBackPressed()
        else {
            viewModel.type = enumValueOf<EnumQuotaApprove>(type)
        }
    }
    //---------------------------------------------------------------------------------------------- initView


    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener() {
        binding.imageviewQrReader.setOnClickListener {
            startQRCodeReader()
        }
    }
    //---------------------------------------------------------------------------------------------- setListener


    //---------------------------------------------------------------------------------------------- observeLiveDate
    private fun observeLiveDate() {
        viewModel.errorLiveDate.observe(viewLifecycleOwner) {
            showMessage(
                message = it.message,
                type = it.type,
                duration = 5
            )
            binding.shimmerViewContainer.stopLoading()
            when (it.type) {
                EnumApiError.UnAuthorization -> (activity as MainActivity?)?.gotoFirstFragmentWithDeleteUser()
                else -> {}
            }
        }
    }
    //---------------------------------------------------------------------------------------------- observeLiveDate


    //---------------------------------------------------------------------------------------------- startQRCodeReader
    private fun startQRCodeReader() {
        scanCustomCode.launch(
            ScannerConfig.build {
                setOverlayStringRes(R.string.clearCameraAndInFrontOfQr) // string resource used for the scanner overlay
                setOverlayDrawableRes(R.drawable.a_ic_qr_code_scan) // drawable resource used for the scanner overlay
                setShowTorchToggle(true)
                setUseFrontCamera(false) // use the front camera
            }
        )
    }
    //---------------------------------------------------------------------------------------------- startQRCodeReader


    //---------------------------------------------------------------------------------------------- handleResult
    private fun handleResult(result: QRResult) {
        when (result) {
            is QRResult.QRSuccess -> {
                val id = result.content.rawValue
                if (id == null)
                    showMessage(getString(R.string.idIsWrong))
                else {
                    id.toLongOrNull()?.let {
                        binding.shimmerViewContainer.startLoading()
                        viewModel.approveQuota(id.toLong())
                    }
                }
            }

            else -> {

            }
        }
    }
    //---------------------------------------------------------------------------------------------- handleResult


}