package com.zarholding.zar.view.fragment.user.education.detail

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.zar.core.enums.EnumApiError
import com.zarholding.zar.R
import com.zarholding.zar.ZarFragment
import com.zarholding.zar.databinding.FragmentEducationDetailBinding
import com.zarholding.zar.ext.config
import com.zarholding.zar.ext.setEducationTime
import com.zarholding.zar.ext.startLoading
import com.zarholding.zar.ext.stopLoading
import com.zarholding.zar.model.data.response.education.EducationDetailModel
import com.zarholding.zar.tools.CompanionValues
import com.zarholding.zar.tools.manager.getShimmerBuild
import com.zarholding.zar.view.activity.MainActivity
import com.zarholding.zar.view.adapter.recycler.education.EducationDetailAdapter
import com.zarholding.zar.view.custom.EndlessScrollListener
import com.zarholding.zar.view.custom.getEndlessScrollListener
import dagger.hilt.android.AndroidEntryPoint

/**
 * Created by m-latifi on 7/26/2023.
 */

@AndroidEntryPoint
class EducationDetailFragment(
    override var layout: Int = R.layout.fragment_education_detail
) : ZarFragment<FragmentEducationDetailBinding>() {

    private val viewModel: EducationDetailViewModel by viewModels()
    private var title = ""
    private var categoryId = 0L
    private var adapter: EducationDetailAdapter? = null
    private var endlessScrollListener: EndlessScrollListener? = null

    //---------------------------------------------------------------------------------------------- onViewCreated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }
    //---------------------------------------------------------------------------------------------- onViewCreated


    //---------------------------------------------------------------------------------------------- initView
    private fun initView() {
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        binding.shimmerViewContainer.config(getShimmerBuild())
        getValuesFromArgument()
        observeLiveDate()
    }
    //---------------------------------------------------------------------------------------------- initView


    //---------------------------------------------------------------------------------------------- observeLiveDate
    private fun observeLiveDate() {
        viewModel.errorLiveDate.observe(viewLifecycleOwner) {
            binding.shimmerViewContainer.stopLoading()
            showMessage(it.message, it.type)
            when (it.type) {
                EnumApiError.UnAuthorization -> (activity as MainActivity?)?.gotoFirstFragmentWithDeleteUser()
                else -> {}
            }
        }


        viewModel.educationDetailModelLiveData.observe(viewLifecycleOwner){
            binding.shimmerViewContainer.stopLoading()
            setAdapter(it)
        }

    }
    //---------------------------------------------------------------------------------------------- observeLiveDate



    //---------------------------------------------------------------------------------------------- observeLiveDate
    private fun getValuesFromArgument(){
        arguments?.let {bundle ->
            title = bundle.getString(CompanionValues.EDUCATION_CAT_NAME, "")
            categoryId = bundle.getLong(CompanionValues.EDUCATION_CAT_ID, 0)
            val min = bundle.getInt(CompanionValues.EDUCATION_TIME, 0)
            val count = bundle.getInt(CompanionValues.EDUCATION_COUNT, 0)
            binding.textViewEducationTime.setEducationTime(min)
            binding.textViewEducationCount.text = binding
                .textViewEducationCount
                .context
                .getString(R.string.session, count.toString())
            binding.textViewDescription.text = title
            getEducation()
        } ?: run {
            activity?.onBackPressedDispatcher?.onBackPressed()
        }
    }
    //---------------------------------------------------------------------------------------------- observeLiveDate



    //---------------------------------------------------------------------------------------------- getEducation
    private fun getEducation() {
        binding.shimmerViewContainer.startLoading()
        viewModel.getVideoDetail(categoryId)
    }
    //---------------------------------------------------------------------------------------------- getEducation



    //---------------------------------------------------------------------------------------------- setAdapter
    private fun setAdapter(items: List<EducationDetailModel>?){
        if (items.isNullOrEmpty() || context == null)
            return

        adapter?.let {
            adapter?.addEducation(items)
            endlessScrollListener?.let {
                it.setLoading(false)
                if (items.isEmpty())
                    binding.recyclerMeeting.removeOnScrollListener(it)
            }
        } ?: run {
            adapter = EducationDetailAdapter(items.toMutableList()) {
                playVideo(it)
            }
            val manager = LinearLayoutManager(
                requireContext(), LinearLayoutManager.VERTICAL, false
            )
            endlessScrollListener = getEndlessScrollListener(manager) {
                getEducation()
            }
            binding.recyclerMeeting.adapter = adapter
            binding.recyclerMeeting.layoutManager = manager
            if (endlessScrollListener != null)
                binding.recyclerMeeting.addOnScrollListener(endlessScrollListener!!)
        }
    }
    //---------------------------------------------------------------------------------------------- setAdapter



    //---------------------------------------------------------------------------------------------- playVideo
    private fun playVideo(item: EducationDetailModel) {
        val bundle = Bundle()
        bundle.putString(CompanionValues.EDUCATION_FILE_NAME, item.videoFileName)
        gotoFragment(R.id.action_goto_videoFragment, bundle)
/*        val url = "${Providers.url}${item.videoFileName}"
        val intent = Intent(Intent.ACTION_VIEW)
        intent.setDataAndType(Uri.parse(url), "video/mp4")
        startActivity(intent)*/
    }
    //---------------------------------------------------------------------------------------------- playVideo


    //---------------------------------------------------------------------------------------------- onDestroyView
    override fun onDestroyView() {
        super.onDestroyView()
        adapter = null
    }
    //---------------------------------------------------------------------------------------------- onDestroyView
}