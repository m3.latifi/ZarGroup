package com.zarholding.zar.view.adapter.holder.taxi

import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemTaxiFavBinding
import com.zarholding.zar.model.data.response.taxi.TaxiFavPlaceModel

/**
 * Created by m-latifi on 10/25/2023.
 */

class TaxiFavHolder(
    private val binding: ItemTaxiFavBinding,
    private val onSelect: (TaxiFavPlaceModel) -> Unit
): RecyclerView.ViewHolder(binding.root) {


    //---------------------------------------------------------------------------------------------- bind
    fun bind(item: TaxiFavPlaceModel) {
        binding.textViewTitle.text = item.locationName
        binding.root.setOnClickListener { onSelect.invoke(item) }
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind

}