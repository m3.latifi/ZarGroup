package com.zarholding.zar.view.adapter.recycler.insurance

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemInsuranceLevelBinding
import com.zarholding.zar.model.data.response.insurance.SupplementalInsuranceLevelModel
import com.zarholding.zar.view.adapter.holder.insurance.InsuranceLevelHolder

/**
 * Created by m-latifi on 10/15/2023.
 */

class InsuranceLevelAdapter(
    private val items: List<SupplementalInsuranceLevelModel>,
    private val onSelected: (Int) -> Unit
): RecyclerView.Adapter<InsuranceLevelHolder>() {

    companion object {
        var selectedPosition = -1
    }

    init {
        selectedPosition = -1
    }


    private var inflater: LayoutInflater? = null

    //---------------------------------------------------------------------------------------------- onCreateViewHolder
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): InsuranceLevelHolder {
        if (inflater == null)
            inflater = LayoutInflater.from(parent.context)
        return InsuranceLevelHolder(
            ItemInsuranceLevelBinding.inflate(inflater!!,parent,false),
            onSelected
        )
    }
    //---------------------------------------------------------------------------------------------- onCreateViewHolder


    //---------------------------------------------------------------------------------------------- onBindViewHolder
    override fun onBindViewHolder(holder: InsuranceLevelHolder, position: Int) {
        holder.bind(item = items[position], position = position)
    }
    //---------------------------------------------------------------------------------------------- onBindViewHolder


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun getItemCount() = items.size
    //---------------------------------------------------------------------------------------------- getItemCount

}