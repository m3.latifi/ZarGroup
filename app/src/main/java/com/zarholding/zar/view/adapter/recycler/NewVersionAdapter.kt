package com.zarholding.zar.view.adapter.recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemNewVersionBinding
import com.zarholding.zar.model.data.other.version.VersionFeatureModel
import com.zarholding.zar.view.adapter.holder.NewVersionHolder

/**
 * Created by m-latifi on 9/30/2023.
 */

class NewVersionAdapter(
    private val versions: List<VersionFeatureModel>
): RecyclerView.Adapter<NewVersionHolder>() {

    private var inflater: LayoutInflater? = null


    //---------------------------------------------------------------------------------------------- onCreateViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewVersionHolder {
        if (inflater == null)
            inflater = LayoutInflater.from(parent.context)
        return NewVersionHolder(ItemNewVersionBinding.inflate(inflater!!,parent, false))
    }
    //---------------------------------------------------------------------------------------------- onCreateViewHolder


    //---------------------------------------------------------------------------------------------- onBindViewHolder
    override fun onBindViewHolder(holder: NewVersionHolder, position: Int) {
        holder.bind(versions[position])
    }
    //---------------------------------------------------------------------------------------------- onBindViewHolder


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun getItemCount() = versions.size
    //---------------------------------------------------------------------------------------------- getItemCount

}