package com.zarholding.zar.view.dialog.feature

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.zar.core.tools.manager.DeviceManager
import com.zarholding.zar.R
import com.zarholding.zar.databinding.DialogNewFeatureBinding
import com.zarholding.zar.model.data.other.version.VersionFeatureModel
import com.zarholding.zar.view.activity.MainActivity
import com.zarholding.zar.view.adapter.recycler.NewVersionAdapter
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * Created by m-latifi on 20/8/2022.
 */

@AndroidEntryPoint
class NewFeatureDialog() : DialogFragment() {

    lateinit var binding: DialogNewFeatureBinding

    @Inject
    lateinit var deviceManager: DeviceManager

    private val viewModel : NewFeatureViewModel by viewModels()


    //---------------------------------------------------------------------------------------------- onCreateView
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DialogNewFeatureBinding.inflate(inflater, container, false)
        return binding.root
    }
    //---------------------------------------------------------------------------------------------- onCreateView



    //---------------------------------------------------------------------------------------------- onCreateView
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity?)?.enableBlurView()
        val lp = WindowManager.LayoutParams()
        val window = dialog!!.window
        val back = ColorDrawable(Color.TRANSPARENT)
        val inset = InsetDrawable(back, 50)
        window!!.setBackgroundDrawable(inset)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        window.attributes = lp
        isCancelable = false
        observeLiveData()
        setListener()
        val title = getString(R.string.newFeature, deviceManager.appVersionName())
        binding.textViewTitle.text = title
    }
    //---------------------------------------------------------------------------------------------- onCreateView



    //---------------------------------------------------------------------------------------------- observeLiveData
    private fun observeLiveData() {
        viewModel.featureLiveData.observe(viewLifecycleOwner){
            setAdapter(it)
        }
    }
    //---------------------------------------------------------------------------------------------- observeLiveData


    //---------------------------------------------------------------------------------------------- showMessage
    private fun showMessage(message: String) {
        activity?.let {
            (it as MainActivity).showMessage(message)
        }
    }
    //---------------------------------------------------------------------------------------------- showMessage



    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener() {

        binding.cardViewParent.setOnClickListener { dismiss() }

        binding.imageViewClose.setOnClickListener { dismiss() }

    }
    //---------------------------------------------------------------------------------------------- setListener



    //---------------------------------------------------------------------------------------------- setAdapter
    private fun setAdapter(versions: List<VersionFeatureModel>) {
        if (context == null)
            return
        val adapter = NewVersionAdapter(versions)
        val manager = LinearLayoutManager(
            context,
            LinearLayoutManager.VERTICAL,
            false
        )
        binding.recyclerViewVersion.layoutManager = manager
        binding.recyclerViewVersion.adapter = adapter
    }
    //---------------------------------------------------------------------------------------------- setAdapter



    //---------------------------------------------------------------------------------------------- dismiss
    override fun dismiss() {
        super.dismiss()
        (activity as MainActivity?)?.disableBlurView()
    }
    //---------------------------------------------------------------------------------------------- dismiss
}