package com.zarholding.zar.view.adapter.holder.insurance

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.zar.core.tools.extensions.split
import com.zarholding.zar.R
import com.zarholding.zar.databinding.ItemInsuranceHistoryBinding
import com.zarholding.zar.ext.setTitleAndValue
import com.zarholding.zar.model.data.response.insurance.history.SupplementalInsuranceHistoryModel
import com.zarholding.zar.view.adapter.recycler.insurance.InsuranceHistoryDependentAdapter

/**
 * Created by m-latifi on 10/17/2023.
 */

class InsuranceHistoryHolder(
    private val binding: ItemInsuranceHistoryBinding,
    private val onDownload: (String) -> Unit
): RecyclerView.ViewHolder(binding.root) {


    //---------------------------------------------------------------------------------------------- bind
    fun bind(item: SupplementalInsuranceHistoryModel) {

        val context = binding.root.context
        binding.textViewTitle.setTitleAndValue(
            title = context.getString(R.string.title),
            splitter = context.getString(R.string.colon),
            value = item.title
        )
        binding.textViewPersonnel.setTitleAndValue(
            title = context.getString(R.string.headOfFamily),
            splitter = context.getString(R.string.colon),
            value = "${item.personnelFirstName} ${item.personnelLastName}"
        )
        binding.textViewLevel.setTitleAndValue(
            title = context.getString(R.string.insuranceLevel),
            splitter = context.getString(R.string.colon),
            value = "${item.levelTitle} - ${item.amount.split()} ${context.getString(R.string.rial)}"
        )
        binding.textViewAccountNumber.setTitleAndValue(
            title = context.getString(R.string.shabaNumber),
            splitter = context.getString(R.string.colon),
            value = "IR ${item.accountNumber}"
        )
        binding.textViewMobileNumber.setTitleAndValue(
            title = context.getString(R.string.mobile),
            splitter = context.getString(R.string.colon),
            value = item.personnelMobile
        )

        binding.textViewDownloadFile.setOnClickListener {
            item.fileName?.let { onDownload.invoke(it) }
        }

        binding.textViewDependentPersons.setOnClickListener {
            if (binding.expandableDependentPersons.isExpanded)
                binding.expandableDependentPersons.collapse()
            else {
                if (item.dependentPersons.isNullOrEmpty())
                    return@setOnClickListener
                val adapter = InsuranceHistoryDependentAdapter(item.dependentPersons)
                val manager = LinearLayoutManager(
                    context, LinearLayoutManager.VERTICAL, false
                )
                binding.recyclerViewDependentPersons.adapter = adapter
                binding.recyclerViewDependentPersons.layoutManager = manager
                binding.expandableDependentPersons.expand()
            }
        }
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind

}