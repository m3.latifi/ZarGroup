package com.zarholding.zar.view.adapter.recycler.insurance

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemInsuranceHistoryDependentBinding
import com.zarholding.zar.model.data.response.insurance.history.SupplementalInsuranceDependentPersonsModel
import com.zarholding.zar.view.adapter.holder.insurance.InsuranceHistoryDependentHolder

/**
 * Created by m-latifi on 10/15/2023.
 */

class InsuranceHistoryDependentAdapter(
    private val items: List<SupplementalInsuranceDependentPersonsModel>
): RecyclerView.Adapter<InsuranceHistoryDependentHolder>() {

    private var inflater: LayoutInflater? = null

    //---------------------------------------------------------------------------------------------- onCreateViewHolder
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): InsuranceHistoryDependentHolder {
        if (inflater == null)
            inflater = LayoutInflater.from(parent.context)
        return InsuranceHistoryDependentHolder(
            ItemInsuranceHistoryDependentBinding.inflate(inflater!!,parent,false)
        )
    }
    //---------------------------------------------------------------------------------------------- onCreateViewHolder


    //---------------------------------------------------------------------------------------------- onBindViewHolder
    override fun onBindViewHolder(holder: InsuranceHistoryDependentHolder, position: Int) {
        holder.bind(item = items[position])
    }
    //---------------------------------------------------------------------------------------------- onBindViewHolder


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun getItemCount() = items.size
    //---------------------------------------------------------------------------------------------- getItemCount

}