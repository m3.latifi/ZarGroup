package com.zarholding.zar.view.fragment.user.quota

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.zar.core.enums.EnumApiError
import com.zarholding.zar.R
import com.zarholding.zar.ZarFragment
import com.zarholding.zar.databinding.FragmentPersonnelQuotaBinding
import com.zarholding.zar.ext.config
import com.zarholding.zar.ext.stopLoading
import com.zarholding.zar.model.data.database.entity.UserInfoEntity
import com.zarholding.zar.model.data.response.quota.QuotaModel
import com.zarholding.zar.tools.CompanionValues
import com.zarholding.zar.tools.manager.getShimmerBuild
import com.zarholding.zar.view.activity.MainActivity
import com.zarholding.zar.view.adapter.recycler.quota.QuotaAdapter
import com.zarholding.zar.view.dialog.personnel.PersonnelDialog
import dagger.hilt.android.AndroidEntryPoint

/**
 * Created by m-latifi on 9/2/2023.
 */

@AndroidEntryPoint
class PersonnelQuotaFragment(
    override var layout: Int = R.layout.fragment_personnel_quota
): ZarFragment<FragmentPersonnelQuotaBinding>() {

    private val viewModel: PersonnelQuotaViewModel by viewModels()
    private var textViewItem: TextView? = null


    //---------------------------------------------------------------------------------------------- onViewCreated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }
    //---------------------------------------------------------------------------------------------- onViewCreated



    //---------------------------------------------------------------------------------------------- initView
    private fun initView() {
        binding.shimmerViewContainer.config(getShimmerBuild())
        setListener()
        observeLiveDate()
        getQuota()
    }
    //---------------------------------------------------------------------------------------------- initView




    //---------------------------------------------------------------------------------------------- observeLiveDate
    private fun observeLiveDate() {
        viewModel.errorLiveDate.observe(viewLifecycleOwner) {
            binding.shimmerViewContainer.stopLoading()
            textViewItem?.text = context?.getString(R.string.giveAccessToAnotherUser)
            showMessage(it.message, it.type)
            when (it.type) {
                EnumApiError.UnAuthorization -> (activity as MainActivity?)?.gotoFirstFragmentWithDeleteUser()

                else -> {}
            }
        }

        viewModel.quotaLiveData.observe(viewLifecycleOwner) {
            binding.shimmerViewContainer.stopLoading()
            setAdapter(it)
        }
    }
    //---------------------------------------------------------------------------------------------- observeLiveDate


    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener() {
        binding.buttonHistory.setOnClickListener {
            gotoFragment(R.id.personnelQuotaHistoryFragment)
        }
    }
    //---------------------------------------------------------------------------------------------- setListener


    //---------------------------------------------------------------------------------------------- getQuota
    private fun getQuota() {
        binding.shimmerViewContainer.startShimmer()
        viewModel.getPersonnelQuota()
    }
    //---------------------------------------------------------------------------------------------- getQuota



    //---------------------------------------------------------------------------------------------- setAdapter
    private fun setAdapter(items: List<QuotaModel>) {
        if (context == null)
            return
        val adapter = QuotaAdapter(
            items = items,
            userId = viewModel.getUser()?.id ?: 0,
            onClick = { gotoQrCreator(it.id) },
            onAccessUserClick = { quotaId, textView ->
                textViewItem = textView
                showPersonnelDialog(quotaId)
            }
        )
        val manager = LinearLayoutManager(
            requireContext(),LinearLayoutManager.VERTICAL, false
        )
        binding.recyclerViewQuota.adapter = adapter
        binding.recyclerViewQuota.layoutManager = manager
    }
    //---------------------------------------------------------------------------------------------- setAdapter



    //---------------------------------------------------------------------------------------------- showPersonnelDialog
    private fun showPersonnelDialog(quotaId: Long) {
        PersonnelDialog{ selectPerson(it, quotaId) }
            .show(childFragmentManager, "personnel dialog")
    }
    //---------------------------------------------------------------------------------------------- showPersonnelDialog



    //---------------------------------------------------------------------------------------------- selectPerson
    private fun selectPerson(user: UserInfoEntity, quotaId: Long) {
        textViewItem?.text = context?.getString(R.string.bePatient)
        viewModel.assignMyQuotaToAnother(user.id, quotaId)
    }
    //---------------------------------------------------------------------------------------------- selectPerson



    //---------------------------------------------------------------------------------------------- gotoQrCreator
    private fun gotoQrCreator(id: Long) {
        val bundle = Bundle()
        bundle.putString(CompanionValues.DOWNLOAD_URL, id.toString())
        gotoFragment(R.id.action_goto_qrCodeCreatorFragment, bundle)
    }
    //---------------------------------------------------------------------------------------------- gotoQrCreator

}