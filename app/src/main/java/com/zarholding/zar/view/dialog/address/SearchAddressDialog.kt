package com.zarholding.zar.view.dialog.address

import android.Manifest
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.LinearLayoutManager
import com.zar.core.tools.manager.PermissionManager
import com.zarholding.zar.R
import com.zarholding.zar.databinding.DialogSearchAddressBinding
import com.zarholding.zar.ext.config
import com.zarholding.zar.ext.startLoading
import com.zarholding.zar.ext.stopLoading
import com.zarholding.zar.model.data.response.ResponseResult
import com.zarholding.zar.model.data.response.address.AddressSuggestionModel
import com.zarholding.zar.tools.manager.speechtotext.SpeechToTextManager
import com.zarholding.zar.tools.manager.getShimmerBuild
import com.zarholding.zar.tools.manager.speechtotext.SpeechToTextInterface
import com.zarholding.zar.view.activity.MainActivity
import com.zarholding.zar.view.adapter.recycler.AddressSuggestionAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import javax.inject.Inject

/**
 * Created by m-latifi on 20/8/2022.
 */

@AndroidEntryPoint
class SearchAddressDialog(
    private val onChooseItem: (AddressSuggestionModel) -> Unit
) : DialogFragment() {

    lateinit var binding: DialogSearchAddressBinding

    @Inject
    lateinit var speechToTextManager: SpeechToTextManager

    @Inject
    lateinit var permissionManager: PermissionManager

    private val searchAddressViewModel: SearchAddressViewModel by viewModels()
    private var job: Job? = null
    private var adapter: AddressSuggestionAdapter? = null

/*
    //---------------------------------------------------------------------------------------------- launcher
    private val launcher: ActivityResultLauncher<Intent> =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            binding.textInputEditTextAddress.setText(speechToTextManager.splitResultToString(result))
            binding.textInputEditTextAddress.requestFocus()
        }
    //---------------------------------------------------------------------------------------------- launcher
*/



    //---------------------------------------------------------------------------------------------- recordAudioPermissionLauncher
    private val recordAudioPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions()
        ) { results ->
            permissionManager.checkPermissionResult(results) {
                speechToTextManager.speechRecognizer(textToSpeechObject)
            }
        }
    //---------------------------------------------------------------------------------------------- recordAudioPermissionLauncher



    //---------------------------------------------------------------------------------------------- textToSpeechObject
    private val textToSpeechObject = object : SpeechToTextInterface {
        override fun onReadyForSpeech() {
            if (context == null)
                return
            binding.textInputLayoutAddress.setStartIconTintList(
                ColorStateList.valueOf(requireContext().getColor(R.color.n_borderRed))
            )
        }

        override fun onEndOfSpeech() {
            if (context == null)
                return
            binding.textInputLayoutAddress.setStartIconTintList(
                ColorStateList.valueOf(requireContext().getColor(R.color.n_blueBottom))
            )
        }

        override fun onResults(result: String) {
            binding.textInputEditTextAddress.setText(result)
            binding.textInputEditTextAddress.requestFocus()
        }
    }
    //---------------------------------------------------------------------------------------------- textToSpeechObject



    //---------------------------------------------------------------------------------------------- onCreateView
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DialogSearchAddressBinding.inflate(inflater, container, false)
        return binding.root
    }
    //---------------------------------------------------------------------------------------------- onCreateView


    //---------------------------------------------------------------------------------------------- onCreateView
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity?)?.enableBlurView()
        val height: Int = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val metrics: WindowMetrics? =
                context?.getSystemService(WindowManager::class.java)?.currentWindowMetrics
            metrics?.bounds?.height() ?: run {
                WindowManager.LayoutParams.MATCH_PARENT
            }
        } else {
            val displayMetrics = DisplayMetrics()
            @Suppress("DEPRECATION")
            activity?.windowManager?.defaultDisplay?.getMetrics(displayMetrics)
            displayMetrics.heightPixels
        }
        val lp = WindowManager.LayoutParams()
        val window = dialog?.window
        val back = ColorDrawable(Color.TRANSPARENT)
        val inset = InsetDrawable(back, 50)
        window?.setBackgroundDrawable(inset)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = (height / 1.5).toInt()
        lp.gravity = Gravity.CENTER
        isCancelable = false
        window?.attributes = lp
        binding.shimmerViewContainer.config(getShimmerBuild())
        setListener()
        observeAddressSuggestionLiveData()
    }
    //---------------------------------------------------------------------------------------------- onCreateView


    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener() {
        binding.imageViewClose.setOnClickListener { dismiss() }
        binding.textInputEditTextAddress.addTextChangedListener { checkEmptyValueForSearch() }
        binding.buttonLoadMore.setOnClickListener {
            requestGetSuggestionAddress(
                binding.textInputEditTextAddress.text.toString()
            )
        }

        binding.textInputLayoutAddress.setStartIconOnClickListener {
            speechToTextManager.speechRecognizer(textToSpeechObject)
        }


        binding.textInputLayoutAddress.setStartIconOnClickListener {
            val permission = listOf(Manifest.permission.RECORD_AUDIO)
            val check = permissionManager.isPermissionGranted(
                permissions = permission,
                launcher = recordAudioPermissionLauncher
            )
            if (check) speechToTextManager.speechRecognizer(textToSpeechObject)
        }
    }
    //---------------------------------------------------------------------------------------------- setListener


    //---------------------------------------------------------------------------------------------- checkEmptyValueForSearch
    private fun checkEmptyValueForSearch() {

        job?.cancel()

        adapter = null
        binding.buttonLoadMore.visibility = View.GONE
        binding.recyclerViewSuggestion.adapter = null

/*        if (binding.textInputEditTextCity.text.isNullOrEmpty()) {
            binding.textInputEditTextCity.error = getString(R.string.cityIsEmpty)
            return
        }*/
        if (binding.textInputEditTextAddress.text.isNullOrEmpty()) {
            binding.textInputEditTextAddress.error = getString(R.string.addressIsEmpty)
            return
        }
        createJobForSearch(
            binding.textInputEditTextAddress.text.toString()
        )
    }
    //---------------------------------------------------------------------------------------------- checkEmptyValueForSearch


    //---------------------------------------------------------------------------------------------- createJobForSearch
    private fun createJobForSearch(address: String) {
        job = CoroutineScope(IO).launch {
            delay(1000)
            withContext(Main) {
                requestGetSuggestionAddress(address)
            }
        }
    }
    //---------------------------------------------------------------------------------------------- createJobForSearch


    //---------------------------------------------------------------------------------------------- observeAddressSuggestionLiveData
    private fun observeAddressSuggestionLiveData() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                searchAddressViewModel.searchAddress.collect {
                    when(it) {
                        is ResponseResult.Loading -> {}
                        is ResponseResult.Success -> {
                            stopLoading()
                            setAdapter(it.data)
                        }
                    }
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- observeAddressSuggestionLiveData


    //---------------------------------------------------------------------------------------------- requestGetSuggestionAddress
    private fun requestGetSuggestionAddress(address: String) {
        startLoading()
        searchAddressViewModel.requestGetSuggestionAddress(address, adapter?.getList())
    }
    //---------------------------------------------------------------------------------------------- requestGetSuggestionAddress


    //---------------------------------------------------------------------------------------------- setAdapter
    private fun setAdapter(items: List<AddressSuggestionModel>) {
        if (context == null)
            return
        binding.shimmerViewContainer.stopLoading()
        adapter?.addAddress(items) ?: run {
            adapter = AddressSuggestionAdapter(items.toMutableList()){
                onChooseItem(it)
                dismiss()
            }
            val manager = LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.VERTICAL,
                false
            )
            binding.recyclerViewSuggestion.layoutManager = manager
            binding.recyclerViewSuggestion.adapter = adapter
        }
    }
    //---------------------------------------------------------------------------------------------- setAdapter


    //---------------------------------------------------------------------------------------------- startLoading
    private fun startLoading() {
        binding.shimmerViewContainer.startLoading()
        binding.buttonLoadMore.visibility = View.GONE
        binding.nestedScrollView.fullScroll(View.FOCUS_DOWN)
    }
    //---------------------------------------------------------------------------------------------- startLoading


    //---------------------------------------------------------------------------------------------- stopLoading
    private fun stopLoading() {
        binding.buttonLoadMore.visibility = View.VISIBLE
        binding.shimmerViewContainer.stopLoading()
    }
    //---------------------------------------------------------------------------------------------- stopLoading


    //---------------------------------------------------------------------------------------------- dismiss
    override fun dismiss() {
        super.dismiss()
        job?.cancel()
        (activity as MainActivity?)?.disableBlurView()
    }
    //---------------------------------------------------------------------------------------------- dismiss
}