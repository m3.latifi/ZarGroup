package com.zarholding.zar.view.adapter.recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemAddressSuggestionBinding
import com.zarholding.zar.model.data.response.address.AddressSuggestionModel
import com.zarholding.zar.view.adapter.holder.AddressSuggestionItemHolder

/**
 * Created by m-latifi on 11/14/2022.
 */

class AddressSuggestionAdapter(
    private val items: MutableList<AddressSuggestionModel>,
    private val onClick: (AddressSuggestionModel) -> Unit
) : RecyclerView.Adapter<AddressSuggestionItemHolder>() {

    private var layoutInflater: LayoutInflater? = null


    //---------------------------------------------------------------------------------------------- onCreateViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressSuggestionItemHolder {
        if (layoutInflater == null)
            layoutInflater = LayoutInflater.from(parent.context)
        return AddressSuggestionItemHolder(
            binding = ItemAddressSuggestionBinding.inflate(layoutInflater!!, parent, false),
            onClick = onClick
        )
    }
    //---------------------------------------------------------------------------------------------- onCreateViewHolder


    //---------------------------------------------------------------------------------------------- onBindViewHolder
    override fun onBindViewHolder(holder: AddressSuggestionItemHolder, position: Int) {
        holder.bind(items[position])
    }
    //---------------------------------------------------------------------------------------------- onBindViewHolder


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun getItemCount() = items.size
    //---------------------------------------------------------------------------------------------- getItemCount



    //---------------------------------------------------------------------------------------------- addAddress
    fun addAddress(list : List<AddressSuggestionModel>) {
        val olsSize = items.size
        items.addAll(list)
        notifyItemRangeChanged(olsSize-1, items.size)
    }
    //---------------------------------------------------------------------------------------------- addAddress



    //---------------------------------------------------------------------------------------------- getList
    fun getList() = items
    //---------------------------------------------------------------------------------------------- getList

}