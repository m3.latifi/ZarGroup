package com.zarholding.zar.view.fragment.user.education.category

import androidx.lifecycle.viewModelScope
import com.zarholding.zar.ZarViewModel
import com.zarholding.zar.model.data.response.ResponseResult
import com.zarholding.zar.model.data.response.education.EducationCategoryModel
import com.zarholding.zar.model.repository.EducationRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by m-latifi on 7/26/2023.
 */

@HiltViewModel
class EducationCategoryViewModel @Inject constructor(
    private val educationRepository: EducationRepository
) : ZarViewModel() {

    private val _category =
        MutableStateFlow<ResponseResult<List<EducationCategoryModel>>>(ResponseResult.Loading(false))
    val category: StateFlow<ResponseResult<List<EducationCategoryModel>>> = _category


    //---------------------------------------------------------------------------------------------- getCategory
    fun getCategory() {
        viewModelScope.launch(IO + exceptionHandler()) {
            _category.value = ResponseResult.Loading(true)
            educationRepository.getVideoCategory().collect{networkResult ->
                checkResponse(networkResult = networkResult) {
                    _category.value = ResponseResult.Loading(false)
                    _category.value = ResponseResult.Success(it)
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- getCategory

}