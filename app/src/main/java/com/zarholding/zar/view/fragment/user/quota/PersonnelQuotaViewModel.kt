package com.zarholding.zar.view.fragment.user.quota

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.zarholding.zar.R
import com.zarholding.zar.ZarViewModel
import com.zarholding.zar.model.data.response.quota.QuotaModel
import com.zarholding.zar.model.repository.QuotaRepository
import com.zarholding.zar.model.repository.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by m-latifi on 9/2/2023.
 */

@HiltViewModel
class PersonnelQuotaViewModel @Inject constructor(
    private val quotaRepository: QuotaRepository,
    private val userRepository: UserRepository
) : ZarViewModel() {


    val quotaLiveData: MutableLiveData<List<QuotaModel>> by lazy {
        MutableLiveData<List<QuotaModel>>()
    }


    //---------------------------------------------------------------------------------------------- getUser
    fun getUser() = userRepository.getUser()
    //---------------------------------------------------------------------------------------------- getUser


    //---------------------------------------------------------------------------------------------- getPersonnelQuota
    fun getPersonnelQuota() {
        viewModelScope.launch(IO + exceptionHandler()) {
            callApi(
                request = quotaRepository.getMyQuota(),
                onReceiveData = {
                    if (it.isEmpty())
                        setMessage(resourcesProvider.getString(R.string.quotaIsEmpty))
                    quotaLiveData.postValue(it)
                }
            )
        }
    }
    //---------------------------------------------------------------------------------------------- getPersonnelQuota


    //---------------------------------------------------------------------------------------------- assignMyQuotaToAnother
    fun assignMyQuotaToAnother(userId: Int, quotaId: Long) {
        viewModelScope.launch(IO + exceptionHandler()) {
            callApi(
                request = quotaRepository.assignMyQuotaToAnother(userId, quotaId),
                showMessageAfterSuccessResponse = true,
                onReceiveData = { getPersonnelQuota() }
            )
        }
    }
    //---------------------------------------------------------------------------------------------- assignMyQuotaToAnother
}