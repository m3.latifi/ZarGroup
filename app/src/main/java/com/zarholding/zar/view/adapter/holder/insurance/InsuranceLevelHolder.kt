package com.zarholding.zar.view.adapter.holder.insurance

import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.R
import com.zarholding.zar.databinding.ItemInsuranceLevelBinding
import com.zarholding.zar.ext.setPrice
import com.zarholding.zar.model.data.response.insurance.SupplementalInsuranceLevelModel
import com.zarholding.zar.view.adapter.recycler.insurance.InsuranceLevelAdapter

/**
 * Created by m-latifi on 10/15/2023.
 */

class InsuranceLevelHolder(
    private val binding: ItemInsuranceLevelBinding,
    private val onSelected: (Int) -> Unit
) : RecyclerView.ViewHolder(binding.root) {


    //---------------------------------------------------------------------------------------------- bind
    fun bind(item: SupplementalInsuranceLevelModel, position: Int) {
        binding.textViewTitle.text = item.title
        binding.textViewPrice.setPrice(item.amount)

        binding.root.setOnClickListener {
            onSelected.invoke(position)
        }

        if (InsuranceLevelAdapter.selectedPosition == position)
            binding.linearLayoutParent.setBackgroundResource(R.drawable.a_drawable_select_level)
        else
            binding.linearLayoutParent.setBackgroundResource(R.drawable.a_drawable_input_border)
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind

}