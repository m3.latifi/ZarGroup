package com.zarholding.zar.view.fragment.admin.taxi

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.zar.core.enums.EnumApiError
import com.zarholding.zar.R
import com.zarholding.zar.ZarFragment
import com.zarholding.zar.databinding.FragmentAdminTaxiListBinding
import com.zarholding.zar.ext.config
import com.zarholding.zar.ext.startLoading
import com.zarholding.zar.ext.stopLoading
import com.zarholding.zar.model.data.enum.EnumAdminTaxiType
import com.zarholding.zar.model.data.enum.EnumPersonnelType
import com.zarholding.zar.model.data.enum.EnumTaxiRequestStatus
import com.zarholding.zar.model.data.request.TaxiChangeStatusRequest
import com.zarholding.zar.model.data.response.taxi.AdminTaxiRequestModel
import com.zarholding.zar.tools.CompanionValues
import com.zarholding.zar.tools.manager.ZarLocationManager
import com.zarholding.zar.tools.manager.getShimmerBuild
import com.zarholding.zar.view.activity.MainActivity
import com.zarholding.zar.view.adapter.recycler.AdminTaxiRequestAdapter
import com.zarholding.zar.view.adapter.recycler.DriverTaxiRequestAdapter
import com.zarholding.zar.view.adapter.recycler.MyTaxiAdapter
import com.zarholding.zar.view.custom.EndlessScrollListener
import com.zarholding.zar.view.dialog.ConfirmDialog
import com.zarholding.zar.view.dialog.RejectReasonDialog
import com.zarholding.zar.view.dialog.driver.DriverDialog
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


/**
 * Created by m-latifi on 11/19/2022.
 */

@AndroidEntryPoint
class AdminTaxiListFragment(override var layout: Int = R.layout.fragment_admin_taxi_list) :
    ZarFragment<FragmentAdminTaxiListBinding>() {


    private val adminTaxiListViewModel: AdminTaxiListViewModel by viewModels()

    @Inject
    lateinit var locationManager: ZarLocationManager

    private var myTaxiRequestAdapter: MyTaxiAdapter? = null
    private var adminTaxiRequestAdapter: AdminTaxiRequestAdapter? = null
    private var driverTaxiRequestAdapter: DriverTaxiRequestAdapter? = null
    private var endlessScrollListener: EndlessScrollListener? = null


    //---------------------------------------------------------------------------------------------- onViewCreated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }
    //---------------------------------------------------------------------------------------------- onViewCreated


    //---------------------------------------------------------------------------------------------- initView
    private fun initView() {
        binding.swipeContainer.setColorSchemeResources(
            android.R.color.holo_blue_bright,
            android.R.color.holo_green_light,
            android.R.color.holo_orange_light,
            android.R.color.holo_red_light
        )
        binding.shimmerViewContainer.config(getShimmerBuild())
        arguments?.let {
            val type = it.getString(CompanionValues.adminTaxiType, null)
            type?.let {
                binding.shimmerViewContainer.startLoading()
                adminTaxiListViewModel.setEnumAdminTaxiTypeAndGetRequestTaxiList(type)
                observeLiveDate()
            } ?: run {
                activity?.onBackPressedDispatcher?.onBackPressed()
            }
        } ?: run {
            activity?.onBackPressedDispatcher?.onBackPressed()
        }
        setOnListener()
    }
    //---------------------------------------------------------------------------------------------- initView


    //---------------------------------------------------------------------------------------------- setOnListener
    private fun setOnListener() {
        binding.swipeContainer.setOnRefreshListener { forceGetRequestTaxiList() }
    }
    //---------------------------------------------------------------------------------------------- setOnListener


    //---------------------------------------------------------------------------------------------- forceGetRequestTaxiList
    private fun forceGetRequestTaxiList() {
        binding.shimmerViewContainer.startLoading()
        adminTaxiRequestAdapter = null
        endlessScrollListener = null
        binding.recyclerView.adapter = null
        adminTaxiListViewModel.forceGetRequestTaxiList()
    }
    //---------------------------------------------------------------------------------------------- forceGetRequestTaxiList


    //---------------------------------------------------------------------------------------------- observeLiveDate
    private fun observeLiveDate() {
        adminTaxiListViewModel.errorLiveDate.observe(viewLifecycleOwner) {
            binding.shimmerViewContainer.stopLoading()
            binding.swipeContainer.isRefreshing = false
            binding.textViewLoading.visibility = View.GONE
            showMessage(it.message, it.type)
            when (it.type) {
                EnumApiError.UnAuthorization -> (activity as MainActivity?)?.gotoFirstFragmentWithDeleteUser()
                else -> {}
            }
        }

        adminTaxiListViewModel.taxiRequestListLiveData.observe(viewLifecycleOwner) {
            binding.textViewLoading.visibility = View.GONE
            binding.shimmerViewContainer.stopLoading()
            binding.swipeContainer.isRefreshing = false
            if (!it.isNullOrEmpty())
                setAdapter(it)
        }
    }
    //---------------------------------------------------------------------------------------------- observeLiveDate


    //---------------------------------------------------------------------------------------------- setAdapter
    private fun setAdapter(items: List<AdminTaxiRequestModel>) {
        when (adminTaxiListViewModel.getEnumAdminTaxiType()) {
            EnumAdminTaxiType.MY,
            EnumAdminTaxiType.HISTORY -> setMyTaxiRequestAdapter(items)

            EnumAdminTaxiType.REQUEST -> {
                when (adminTaxiListViewModel.getUserType()) {
                    EnumPersonnelType.Driver -> setDriverTaxiRequestAdapter(items)
                    else -> setTaxiRequestAdapter(items)
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- setAdapter


    //---------------------------------------------------------------------------------------------- setMyTaxiRequestAdapter
    private fun setMyTaxiRequestAdapter(items: List<AdminTaxiRequestModel>) {
        if (context == null)
            return
        myTaxiRequestAdapter?.let { adapter ->
            adapter.addRequest(items)
            endlessScrollListener?.let {
                it.setLoading(false)
                if (items.isEmpty())
                    binding.recyclerView.removeOnScrollListener(it)
            }
        } ?: run {
            myTaxiRequestAdapter = MyTaxiAdapter(
                items = items.toMutableList(),
                token = adminTaxiListViewModel.getBearerToken()
            ) {
                showOnMap(lat = it.lat, lng = it.long)
            }
            val manager = LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.VERTICAL,
                false
            )
            endlessScrollListener = getEndlessScrollListener(manager)
            binding.recyclerView.layoutManager = manager
            binding.recyclerView.adapter = myTaxiRequestAdapter
            if (endlessScrollListener != null)
                binding.recyclerView.addOnScrollListener(endlessScrollListener!!)
        }
    }
    //---------------------------------------------------------------------------------------------- setMyTaxiRequestAdapter


    //---------------------------------------------------------------------------------------------- setTaxiRequestAdapter
    private fun setTaxiRequestAdapter(items: List<AdminTaxiRequestModel>) {
        if (context == null)
            return
        adminTaxiRequestAdapter?.let { adapter ->
            adapter.addRequest(items)
            endlessScrollListener?.let {
                it.setLoading(false)
                if (items.isEmpty())
                    binding.recyclerView.removeOnScrollListener(it)
            }
        } ?: run {
            adminTaxiRequestAdapter = AdminTaxiRequestAdapter(
                items = items.toMutableList(),
                token = adminTaxiListViewModel.getBearerToken(),
                onAcceptClick = { accept(it) },
                onRejectClick = { showDialogReasonOfReject(it) }
            ) {
                showOnMap(lat = it.lat, lng = it.long)
            }
            val manager = LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.VERTICAL,
                false
            )
            endlessScrollListener = getEndlessScrollListener(manager)
            binding.recyclerView.layoutManager = manager
            binding.recyclerView.adapter = adminTaxiRequestAdapter
            if (endlessScrollListener != null)
                binding.recyclerView.addOnScrollListener(endlessScrollListener!!)
        }
    }
    //---------------------------------------------------------------------------------------------- setTaxiRequestAdapter


    //---------------------------------------------------------------------------------------------- accept
    private fun accept(item: AdminTaxiRequestModel) {
        when (adminTaxiListViewModel.getUserType()) {
            EnumPersonnelType.Administrative -> showDialogChooseDriver(item)

            else -> showDialogConfirm(item)
        }
    }
    //---------------------------------------------------------------------------------------------- accept


    //---------------------------------------------------------------------------------------------- setDriverTaxiRequestAdapter
    private fun setDriverTaxiRequestAdapter(items: List<AdminTaxiRequestModel>) {
        if (context == null)
            return
        driverTaxiRequestAdapter?.let { adapter ->
            adapter.addRequest(items)
            endlessScrollListener?.let {
                it.setLoading(false)
                if (items.isEmpty())
                    binding.recyclerView.removeOnScrollListener(it)
            }
        } ?: run {
            driverTaxiRequestAdapter = DriverTaxiRequestAdapter(
                items = items.toMutableList(),
                token = adminTaxiListViewModel.getBearerToken(),
                onChangeTripStatus = { changeTripStatus(it) },
            ) {
                showOnMap(lat = it.lat, lng = it.long)
            }
            val manager = LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.VERTICAL,
                false
            )
            endlessScrollListener = getEndlessScrollListener(manager)
            binding.recyclerView.layoutManager = manager
            binding.recyclerView.adapter = driverTaxiRequestAdapter
            if (endlessScrollListener != null)
                binding.recyclerView.addOnScrollListener(endlessScrollListener!!)
        }
    }
    //---------------------------------------------------------------------------------------------- setDriverTaxiRequestAdapter


    //---------------------------------------------------------------------------------------------- locationPermissionLauncher
    private val locationPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions()
        ) {}
    //---------------------------------------------------------------------------------------------- locationPermissionLauncher


    //---------------------------------------------------------------------------------------------- changeTripStatus
    private fun changeTripStatus(position: Int) {
        if (context == null)
            return
        locationManager.getCurrentLocation(
            launcher = locationPermissionLauncher,
            onFindCurrentLocation = { location ->
                val lat = location.latitude
                val lon = location.longitude
                requestDriverChangeTripStatus(position, lat, lon)
            },
            onFailedMessage = { message ->
                binding.shimmerViewContainer.stopLoading()
                binding.textViewLoading.visibility = View.GONE
                showMessage(message)
            }
        )
    }
    //---------------------------------------------------------------------------------------------- changeTripStatus


    //---------------------------------------------------------------------------------------------- showOnMap
    private fun showOnMap(lat: Double, lng: Double) {
/*        binding.recyclerView.adapter = null
        driverTaxiRequestAdapter = null*/
        val uri = ("geo:$lat" + ","
                + lng) + "?q=" + lat + "," + lng
        startActivity(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse(uri)
            )
        )
/*        val bundle = Bundle()
        bundle.putDouble(CompanionValues.latOrigin, latOrigin)
        bundle.putDouble(CompanionValues.lngOrigin, lngOrigin)
        bundle.putDouble(CompanionValues.latDestination, latDestination)
        bundle.putDouble(CompanionValues.lngDestination, lngDestination)
        gotoFragment(R.id.action_goto_MapFragment, bundle)*/
    }
    //---------------------------------------------------------------------------------------------- showOnMap


    //---------------------------------------------------------------------------------------------- requestDriverChangeTripStatus
    private fun requestDriverChangeTripStatus(position: Int, lat: Double, lon: Double) {
        driverTaxiRequestAdapter?.let {
            binding.shimmerViewContainer.stopLoading()
            driverTaxiRequestAdapter = null
            binding.recyclerView.adapter = null
            adminTaxiListViewModel.requestDriverChangeTripStatus(position, lat, lon)
        }
    }
    //---------------------------------------------------------------------------------------------- requestDriverChangeTripStatus


    //______________________________________________________________________________________________ getEndlessScrollListener
    private fun getEndlessScrollListener(manager: LinearLayoutManager): EndlessScrollListener {
        val endlessScrollListener = object : EndlessScrollListener(manager) {
            override fun loadMoreItems() {
                binding.textViewLoading.visibility = View.VISIBLE
                adminTaxiListViewModel.getRequestTaxiListNextPage()
            }
        }
        endlessScrollListener.setLoading(false)
        return endlessScrollListener
    }
    //______________________________________________________________________________________________ getEndlessScrollListener


    //---------------------------------------------------------------------------------------------- showDialogReasonOfReject
    private fun showDialogReasonOfReject(item: AdminTaxiRequestModel) {
        if (context == null)
            return
        RejectReasonDialog(
            requireContext(),
            onClickSend = { rejectRequest(item = item, reason = it) },
            onShowDialog = { (activity as MainActivity?)?.enableBlurView() },
            onDismissDialog = { (activity as MainActivity?)?.disableBlurView() }).show()
    }
    //---------------------------------------------------------------------------------------------- showDialogReasonOfReject


    //---------------------------------------------------------------------------------------------- rejectRequest
    private fun rejectRequest(item: AdminTaxiRequestModel, reason: String) {
        val request = TaxiChangeStatusRequest(
            item.id,
            EnumTaxiRequestStatus.Reject,
            reason,
            item.personnelJobKeyCode,
            item.fromCompany
        )
        adminTaxiRequestAdapter = null
        endlessScrollListener = null
        requestChangeStatusOfTaxiRequests(request)
    }
    //---------------------------------------------------------------------------------------------- rejectRequest


    //---------------------------------------------------------------------------------------------- showDialogChooseDriver
    private fun showDialogChooseDriver(item: AdminTaxiRequestModel) {
        DriverDialog(
            item = item,
            onConfirmClick = { chooseDriver() }
        ).show(childFragmentManager, "driver")
    }
    //---------------------------------------------------------------------------------------------- showDialogChooseDriver


    //---------------------------------------------------------------------------------------------- chooseDriver
    private fun chooseDriver() {
        binding.shimmerViewContainer.stopLoading()
        binding.textViewLoading.visibility = View.GONE
        adminTaxiRequestAdapter = null
        binding.recyclerView.adapter = null
        adminTaxiListViewModel.forceGetRequestTaxiList()
    }
    //---------------------------------------------------------------------------------------------- chooseDriver


    //---------------------------------------------------------------------------------------------- showDialogConfirm
    private fun showDialogConfirm(item: AdminTaxiRequestModel) {
        if (context == null)
            return
        ConfirmDialog(
            context = requireContext(),
            type = ConfirmDialog.ConfirmType.ADD,
            title = getString(R.string.confirmForTaxiRequestRegister, item.requesterName),
            onYesClick = { changeStatusOfTaxiRequests(item) },
            onShowDialog = { (activity as MainActivity?)?.enableBlurView() },
            onDismissDialog = { (activity as MainActivity?)?.disableBlurView() }
        ).show()
    }
    //---------------------------------------------------------------------------------------------- showDialogConfirm


    //---------------------------------------------------------------------------------------------- changeStatusOfTaxiRequests
    private fun changeStatusOfTaxiRequests(item: AdminTaxiRequestModel) {
        adminTaxiListViewModel.getUser()?.let {
            val request = TaxiChangeStatusRequest(
                item.id,
                EnumTaxiRequestStatus.Confirmed,
                null,
                it.personnelJobKeyCode,
                item.fromCompany
            )
            requestChangeStatusOfTaxiRequests(request)
        } ?: run {
            (activity as MainActivity?)?.gotoFirstFragmentWithDeleteUser()
        }
    }
    //---------------------------------------------------------------------------------------------- changeStatusOfTaxiRequests


    //---------------------------------------------------------------------------------------------- requestChangeStatusOfTaxiRequests
    private fun requestChangeStatusOfTaxiRequests(request: TaxiChangeStatusRequest) {
        adminTaxiRequestAdapter = null
        endlessScrollListener = null
        binding.recyclerView.adapter = null
        binding.shimmerViewContainer.startLoading()
        adminTaxiListViewModel.requestChangeStatusOfTaxiRequests(request)
    }
    //---------------------------------------------------------------------------------------------- requestChangeStatusOfTaxiRequests


    //---------------------------------------------------------------------------------------------- onDestroyView
    override fun onDestroyView() {
        super.onDestroyView()
        myTaxiRequestAdapter = null
        adminTaxiRequestAdapter = null
        endlessScrollListener = null
        driverTaxiRequestAdapter = null
        endlessScrollListener = null
    }
    //---------------------------------------------------------------------------------------------- onDestroyView


}