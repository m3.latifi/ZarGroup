package com.zarholding.zar.view.adapter.holder.survey

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.zarholding.zar.databinding.ItemSurveySatisfactionBinding
import com.zarholding.zar.ext.setSurveyNumber
import com.zarholding.zar.model.data.response.survey.SurveyQuestionModel

/**
 * Create by Mehrdad on 1/21/2023
 */

class SurveySatisfactionHolder(
    private val binding: ItemSurveySatisfactionBinding
) : SurveyQuestionHolder(binding.root) {

    private var textWatcher: TextWatcher? = null

    //---------------------------------------------------------------------------------------------- bind
    override fun bind(item: SurveyQuestionModel, position: Int) {
        setValueToXml(item, position)
        setListener(item)
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind



    //---------------------------------------------------------------------------------------------- hideAndShowEmoji
    private fun hideAndShowEmoji(selected : Int, item: SurveyQuestionModel) {
        if (!item.options.isNullOrEmpty())
         if (item.options[selected].hasAdditionalText)
             binding.editTextAnswer.visibility = View.VISIBLE
        else {
             binding.editTextAnswer.visibility = View.GONE
             binding.editTextAnswer.text.clear()
         }
        binding.editTextAnswer.setText(item.answer)
        binding.imageViewVeryBad.alpha = 0.25f
        binding.imageViewBad.alpha = 0.25f
        binding.imageViewNormal.alpha = 0.25f
        binding.imageViewGood.alpha = 0.25f
        binding.imageViewExcellent.alpha = 0.25f
        when(selected) {
            0 -> binding.imageViewVeryBad.alpha = 1.0f
            1 -> binding.imageViewBad.alpha = 1.0f
            2 -> binding.imageViewNormal.alpha = 1.0f
            3 -> binding.imageViewGood.alpha = 1.0f
            4 -> binding.imageViewExcellent.alpha = 1.0f
        }
    }
    //---------------------------------------------------------------------------------------------- hideAndShowEmoji


    //---------------------------------------------------------------------------------------------- setValueToXml
    private fun setValueToXml(item: SurveyQuestionModel, position: Int) {
        binding.textViewNumber.setSurveyNumber(position + 1)
        binding.editTextAnswer.removeTextChangedListener(textWatcher)
        binding.editTextAnswer.setText(item.answer)
        binding.textViewQuestion.text = item.questionTitle
        if (!item.options.isNullOrEmpty() && item.selectedAnswerId != null) {
            val index = item.options.indexOfFirst { it.questionOptionId == item.selectedAnswerId }
            if (index in 0..5)
                hideAndShowEmoji(selected = index, item = item)
        }
    }
    //---------------------------------------------------------------------------------------------- setValueToXml


    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener(item: SurveyQuestionModel) {
        if (item.options.isNullOrEmpty())
            return

        if (item.options.size != 5)
            return

        binding.imageViewVeryBad.setOnClickListener {
            item.selectedAnswerId = item.options[0].questionOptionId
            hideAndShowEmoji(selected = 0, item = item)
        }

        binding.imageViewBad.setOnClickListener {
            item.selectedAnswerId = item.options[1].questionOptionId
            hideAndShowEmoji(selected = 1, item = item)
        }

        binding.imageViewNormal.setOnClickListener {
            item.selectedAnswerId = item.options[2].questionOptionId
            hideAndShowEmoji(selected = 2, item = item)
        }

        binding.imageViewGood.setOnClickListener {
            item.selectedAnswerId = item.options[3].questionOptionId
            hideAndShowEmoji(selected = 3, item = item)
        }

        binding.imageViewExcellent.setOnClickListener {
            item.selectedAnswerId = item.options[4].questionOptionId
            hideAndShowEmoji(selected = 4, item = item)
        }

        textWatcher = object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                item.answer = s.toString()
            }
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        }

        binding.editTextAnswer.addTextChangedListener(textWatcher)
    }
    //---------------------------------------------------------------------------------------------- setListener

}
