package com.zarholding.zar.view.adapter.holder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemAppDashboardBinding
import com.zarholding.zar.model.data.other.AppModel

/**
 * Created by m-latifi on 11/14/2022.
 */

class DashboardItemHolder(
    private val binding: ItemAppDashboardBinding,
    private val onClick: (AppModel) -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    //---------------------------------------------------------------------------------------------- bind
    fun bind(item: AppModel) {
        setValueToXml(item)
        setListener(item)
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind


    //---------------------------------------------------------------------------------------------- setValueToXml
    private fun setValueToXml(item: AppModel) {
        binding.imageViewIcon.setImageResource(item.icon)
        binding.textViewTitle.text = item.title
        binding.linearLayoutLink.visibility = if (item.link == 0)
            View.VISIBLE
        else
            View.GONE
    }
    //---------------------------------------------------------------------------------------------- setValueToXml


    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener(item: AppModel) {
        binding.root.setOnClickListener { onClick(item) }
    }
    //---------------------------------------------------------------------------------------------- setListener
}