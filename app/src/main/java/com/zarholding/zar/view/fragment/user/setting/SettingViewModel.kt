package com.zarholding.zar.view.fragment.user.setting

import com.zarholding.zar.ZarViewModel
import com.zarholding.zar.tools.manager.SharedPreferencesManager
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

/**
 * Created by m-latifi on 7/17/2023.
 */

@HiltViewModel
class SettingViewModel @Inject constructor(
    private val sharedPreferencesManager: SharedPreferencesManager
): ZarViewModel(){


    //---------------------------------------------------------------------------------------------- isBiometricEnable
    fun isBiometricEnable() = sharedPreferencesManager.isBiometricEnable()
    //---------------------------------------------------------------------------------------------- isBiometricEnable



    //---------------------------------------------------------------------------------------------- changeBiometricEnable
    fun changeBiometricEnable() {
        val biometric = !isBiometricEnable()
        sharedPreferencesManager.changeBiometricEnable(biometric)
    }
    //---------------------------------------------------------------------------------------------- changeBiometricEnable

}