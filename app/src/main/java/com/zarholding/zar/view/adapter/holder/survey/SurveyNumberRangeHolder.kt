package com.zarholding.zar.view.adapter.holder.survey

import androidx.core.text.isDigitsOnly
import com.mohammedalaa.seekbar.DoubleValueSeekBarView
import com.mohammedalaa.seekbar.OnDoubleValueSeekBarChangeListener
import com.zarholding.zar.databinding.ItemSurveyNumberRangeBinding
import com.zarholding.zar.ext.setSurveyNumber
import com.zarholding.zar.model.data.response.survey.SurveyQuestionModel

/**
 * Create by Mehrdad on 1/21/2023
 */
class SurveyNumberRangeHolder(
    private val binding: ItemSurveyNumberRangeBinding
) : SurveyQuestionHolder(binding.root) {

    //---------------------------------------------------------------------------------------------- bind
    override fun bind(item: SurveyQuestionModel, position: Int) {
        setValueToXml(item, position)
        setListener(item)
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind


    //---------------------------------------------------------------------------------------------- setValueToXml
    private fun setValueToXml(item: SurveyQuestionModel, position: Int) {
        binding.textViewNumber.setSurveyNumber(position + 1)
        binding.textViewQuestion.text = item.questionTitle
    }
    //---------------------------------------------------------------------------------------------- setValueToXml


    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener(item: SurveyQuestionModel) {
        if (item.selectedAnswersId == null)
            item.selectedAnswersId = mutableListOf()

        if (!item.options.isNullOrEmpty()) {
            binding.doubleRangeSeekbar.minValue = item.options[0].rangeFrom
            binding.doubleRangeSeekbar.maxValue = item.options[0].rangeTo
            binding.doubleRangeSeekbar.currentMinValue = item.options[0].rangeFrom
            binding.doubleRangeSeekbar.currentMaxValue = item.options[0].rangeTo
        }

        item.answer?.let {
            val answer = it.split("-")
            if (answer.size == 2) {
                val first = answer[0]
                if (first.isDigitsOnly())
                    binding.doubleRangeSeekbar.currentMinValue = first.toInt()
                val two = answer[1]
                if (two.isDigitsOnly())
                    binding.doubleRangeSeekbar.currentMaxValue = two.toInt()
            }
        }


        binding.doubleRangeSeekbar
            .setOnRangeSeekBarViewChangeListener(object : OnDoubleValueSeekBarChangeListener {
                override fun onStartTrackingTouch(
                    seekBar: DoubleValueSeekBarView?,
                    min: Int,
                    max: Int
                ) {
                }

                override fun onStopTrackingTouch(
                    seekBar: DoubleValueSeekBarView?,
                    min: Int,
                    max: Int
                ) {
                    item.answer = "$min-$max"
                }

                override fun onValueChanged(
                    seekBar: DoubleValueSeekBarView?,
                    min: Int,
                    max: Int,
                    fromUser: Boolean
                ) {
                }

            })
    }
    //---------------------------------------------------------------------------------------------- setListener

}
