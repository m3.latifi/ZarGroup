package com.zarholding.zar.view.adapter.holder

import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.R
import com.zarholding.zar.databinding.ItemDriverTaxiRequestBinding
import com.zarholding.zar.ext.getPassengers
import com.zarholding.zar.ext.loadImageByToken
import com.zarholding.zar.ext.setTitleAndValue
import com.zarholding.zar.model.data.enum.EnumPersonnelType
import com.zarholding.zar.model.data.enum.EnumTaxiRequestType
import com.zarholding.zar.model.data.enum.EnumTaxiRequestTypePro
import com.zarholding.zar.model.data.enum.EnumTripStatus
import com.zarholding.zar.model.data.response.taxi.AdminTaxiRequestModel
import com.zarholding.zar.model.data.response.taxi.TaxiRequestPointModel
import com.zarholding.zar.view.adapter.recycler.taxi.AdminTaxiAddressAdapter
import com.zarholding.zar.view.fragment.user.taxi.pro.component.TaxiMapView

/**
 * Created by m-latifi on 11/19/2022.
 */

class DriverTaxiRequestHolder(
    private val binding: ItemDriverTaxiRequestBinding,
    private val onChangeTripStatus: (Int) -> Unit,
    private val onMapClick: (TaxiRequestPointModel) -> Unit
) : RecyclerView.ViewHolder(binding.root) {


    //---------------------------------------------------------------------------------------------- bind
    fun bind(item: AdminTaxiRequestModel, position : Int, token: String) {
        setValueToXml(item, token)
        setListener(item, position)
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind



    //---------------------------------------------------------------------------------------------- setValueToXml
    private fun setValueToXml(item: AdminTaxiRequestModel, token: String) {

        binding.textViewGoDate.text = item.departureDate
        binding.textViewGoTime.text = item.departureTime
        binding.textViewReturnDate.text = item.returnDate
        binding.textViewReturnTime.text = item.returnTime
        binding.imageViewIcon.loadImageByToken(
            url = item.userName,
            token = token
        )
        binding.textViewApplicatorName.setTitleAndValue(
            title = binding.textViewApplicatorName.context.getString(R.string.applicator),
            splitter = binding.textViewApplicatorName.context.getString(R.string.colon),
            value = item.requesterName
        )
        binding.textviewJobTitle.setTitleAndValue(
            title = binding.textviewJobTitle.context.getString(R.string.jobTitle),
            splitter = binding.textviewJobTitle.context.getString(R.string.colon),
            value = item.personnelJobKeyText
        )
        binding.textviewUnitTitle.setTitleAndValue(
            title = binding.textviewUnitTitle.context.getString(R.string.organizationUnit),
            splitter = binding.textviewUnitTitle.context.getString(R.string.colon),
            value = item.organizationUnitText
        )

        binding.textViewPassenger.setTitleAndValue(
            title = binding.textViewPassenger.context.getString(R.string.passengers),
            splitter = binding.textViewPassenger.context.getString(R.string.colon),
            value = item.listPassengers.getPassengers()
        )

        setLocationAdapter(
            items = item.locations?.filter {
                it.requestType == EnumTaxiRequestTypePro.Departure &&
                        it.type == TaxiMapView.TaxiMapType.Origin
            },
            recyclerView = binding.recyclerViewGoOrigin
        )
        setLocationAdapter(
            items = item.locations?.filter {
                it.requestType == EnumTaxiRequestTypePro.Departure &&
                        it.type == TaxiMapView.TaxiMapType.Destination
            },
            recyclerView = binding.recyclerViewGoDestination
        )

        when (item.type) {
            EnumTaxiRequestType.OneWay -> {
                binding.constraintLayoutReturnInformation.visibility = View.GONE
                binding.textViewReturnTitle.visibility = View.GONE
            }

            EnumTaxiRequestType.Return -> {
                binding.constraintLayoutReturnInformation.visibility = View.VISIBLE
                binding.textViewReturnTitle.visibility = View.VISIBLE
                setLocationAdapter(
                    items = item.locations?.filter {
                        it.requestType == EnumTaxiRequestTypePro.Return &&
                                it.type == TaxiMapView.TaxiMapType.Origin
                    },
                    recyclerView = binding.recyclerViewReturnOrigin
                )
                setLocationAdapter(
                    items = item.locations?.filter {
                        it.requestType == EnumTaxiRequestTypePro.Return &&
                                it.type == TaxiMapView.TaxiMapType.Destination
                    },
                    recyclerView = binding.recyclerViewReturnDestination
                )
            }
        }

        when(item.tripStatus) {
            EnumTripStatus.Assigned -> {
                binding.textViewTripStatus.background = AppCompatResources.getDrawable(
                    binding.textViewTripStatus.context,
                    R.drawable.a_drawable_confirm)
                binding.textViewTripStatus.text =
                    binding.textViewTripStatus.context.getString(R.string.startTrip)
            }
            EnumTripStatus.Started -> {
                binding.textViewTripStatus.background = AppCompatResources.getDrawable(
                    binding.textViewTripStatus.context, R.drawable.a_drawable_reject)
                binding.textViewTripStatus.text =
                    binding.textViewTripStatus.context.getString(R.string.finishTrip)
            }
            else ->{
                binding.textViewTripStatus.background = AppCompatResources.getDrawable(
                    binding.textViewTripStatus.context, R.drawable.a_drawable_wating)
                binding.textViewTripStatus.text =
                    binding.textViewTripStatus.context.getString(R.string.finishedTrip)
            }

        }
    }
    //---------------------------------------------------------------------------------------------- setValueToXml


    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener(item: AdminTaxiRequestModel, position : Int) {
        binding.textViewTripStatus.setOnClickListener {
            when(item.tripStatus) {
                EnumTripStatus.Assigned,
                EnumTripStatus.Started -> onChangeTripStatus(position)
                else -> {}
            }
        }

/*        binding.linearLayoutMap.setOnClickListener {
            onMapClick(
                item.originLat,
                item.originLong,
                item.destinationLat,
                item.destinationLong
            )
        }*/
    }
    //---------------------------------------------------------------------------------------------- setListener



    //---------------------------------------------------------------------------------------------- setLocationAdapter
    private fun setLocationAdapter(items: List<TaxiRequestPointModel>?, recyclerView: RecyclerView) {
        if (items.isNullOrEmpty())
            return
        val adapter = AdminTaxiAddressAdapter(items, EnumPersonnelType.Driver){
            onMapClick.invoke(it)
        }
        val manager = LinearLayoutManager(
            recyclerView.context,
            LinearLayoutManager.VERTICAL,
            false
        )
        recyclerView.adapter = adapter
        recyclerView.layoutManager = manager
    }
    //---------------------------------------------------------------------------------------------- setLocationAdapter

}