package com.zarholding.zar.view.adapter.holder.taxi

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemAdminTaxiAddressBinding
import com.zarholding.zar.model.data.enum.EnumPersonnelType
import com.zarholding.zar.model.data.response.taxi.TaxiRequestPointModel

/**
 * Created by m-latifi on 10/24/2023.
 */

class AdminTaxiAddressHolder(
    private val binding: ItemAdminTaxiAddressBinding,
    private val personnelType: EnumPersonnelType,
    private val onMapClick: (TaxiRequestPointModel) -> Unit
): RecyclerView.ViewHolder(binding.root) {


    //---------------------------------------------------------------------------------------------- bind
    fun bind(item: TaxiRequestPointModel) {
        binding.textViewAddress.isSelected = true
        binding.textViewAddress.text = item.address
        if (personnelType == EnumPersonnelType.Personnel)
            binding.imageViewLocation.visibility = View.GONE
        else
            binding.imageViewLocation.visibility = View.VISIBLE
        binding.imageViewLocation.setOnClickListener { onMapClick.invoke(item) }
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind

}