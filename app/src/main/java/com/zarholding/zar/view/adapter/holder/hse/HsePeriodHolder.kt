package com.zarholding.zar.view.adapter.holder.hse

import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemHsePeriodBinding
import com.zarholding.zar.model.data.response.dropdown.DropDownModel

/**
 * Created by m-latifi on 11/4/2023.
 */

class HsePeriodHolder(
    private val binding: ItemHsePeriodBinding,
    private val onClick: (DropDownModel) -> Unit
): RecyclerView.ViewHolder(binding.root) {


    //---------------------------------------------------------------------------------------------- bind
    fun bind(item: DropDownModel) {
        binding.textViewTitle.text = item.text
        binding.root.setOnClickListener { onClick.invoke(item) }
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind

}