package com.zarholding.zar.view.fragment.user.profile

import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.appcompat.content.res.AppCompatResources
import androidx.fragment.app.viewModels
import com.zarholding.zar.R
import com.zarholding.zar.view.activity.MainActivity
import com.zarholding.zar.view.dialog.ConfirmDialog
import com.zarholding.zar.view.dialog.EditPlaqueDialog
import com.zarholding.zar.view.dialog.ShowImageDialog
import com.zarholding.zar.ZarFragment
import com.zarholding.zar.databinding.FragmentProfileBinding
import com.zarholding.zar.ext.getModelForShowImageProfile
import com.zarholding.zar.ext.loadImageByToken
import com.zarholding.zar.ext.setCarPlaque
import com.zarholding.zar.ext.setJobKeyAndUnit
import com.zarholding.zar.ext.setProfileCarModel
import com.zarholding.zar.ext.setProfileValue
import com.zarholding.zar.ext.setTitleAndValue
import dagger.hilt.android.AndroidEntryPoint

/**
 * Created by m-latifi on 11/19/2022.
 */

@AndroidEntryPoint
class ProfileFragment(override var layout: Int = R.layout.fragment_profile) :
    ZarFragment<FragmentProfileBinding>() {

    private val profileViewModel: ProfileViewModel by viewModels()


    //---------------------------------------------------------------------------------------------- onViewCreated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }
    //---------------------------------------------------------------------------------------------- onViewCreated


    //---------------------------------------------------------------------------------------------- initView
    private fun initView() {
        setListener()
        setUserInfo()
    }
    //---------------------------------------------------------------------------------------------- initView


    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener() {
        binding.layoutLogout.root.setOnClickListener { logOut() }
        binding.layoutPersonalInformation.setOnClickListener { showAndHideInfo() }
        binding.layoutNews.root.setOnClickListener { disableFeature() }
        binding.layoutPlaque.root.setOnClickListener { showEditCarPlaque() }
        binding.cardViewProfile.setOnClickListener { showImageProfile() }
    }
    //---------------------------------------------------------------------------------------------- setListener


    //---------------------------------------------------------------------------------------------- disableFeature
    private fun disableFeature() {
        showMessage(getString(R.string.disableFeature))
    }
    //---------------------------------------------------------------------------------------------- disableFeature


    //---------------------------------------------------------------------------------------------- logOut
    private fun logOut() {
        if (context == null)
            return
        ConfirmDialog(
            context = requireContext(),
            type = ConfirmDialog.ConfirmType.DELETE,
            title = getString(R.string.doYouWantToLogoutAccount),
            onYesClick = { (activity as MainActivity).gotoFirstFragmentWithDeleteUser() },
            onShowDialog = { (activity as MainActivity?)?.enableBlurView() },
            onDismissDialog = { (activity as MainActivity?)?.disableBlurView() }
        ).show()
    }
    //---------------------------------------------------------------------------------------------- logOut


    //---------------------------------------------------------------------------------------------- setUserInfo
    private fun setUserInfo() {
        val item = profileViewModel.getUserInfo()
        binding.textViewProfileName.text = item?.fullName ?: ""
        binding.textViewDegree.setJobKeyAndUnit(user = item)
        binding.textViewPersonalCode.setTitleAndValue(
            title = getString(R.string.personalCode),
            splitter = getString(R.string.colon),
            value = item?.userName ?: ""
        )
        binding.imageviewProfile.loadImageByToken(
            url = item?.userName ?: "",
            token = profileViewModel.getBearerToken()
        )
        if (context == null)
            return

        binding.layoutProfileMobile.textViewTitle.text = getString(R.string.orgMobile)
        binding.layoutProfileMobile.textViewValue.setProfileValue(item?.mobile)
        binding.layoutProfileMobile.imageViewIcon.setImageDrawable(
            AppCompatResources.getDrawable(requireContext(), R.drawable.a_ic_mobile_message)
        )

        binding.layoutProfileEmail.textViewTitle.text = getString(R.string.orgEmail)
        binding.layoutProfileEmail.textViewValue.setProfileValue(item?.email)
        binding.layoutProfileEmail.imageViewIcon.setImageDrawable(
            AppCompatResources.getDrawable(requireContext(), R.drawable.a_ic_email)
        )

        binding.layoutProfilePhone.textViewTitle.text = getString(R.string.organizationPhone)
        binding.layoutProfilePhone.textViewValue.setProfileValue(item?.phone)
        binding.layoutProfilePhone.imageViewIcon.setImageDrawable(
            AppCompatResources.getDrawable(requireContext(), R.drawable.a_ic_phone)
        )

        binding.layoutProfileCarModel.textViewTitle.text = getString(R.string.carModel)
        binding.layoutProfileCarModel.textViewValue.setProfileCarModel(item?.carModel)
        binding.layoutProfileCarModel.imageViewIcon.setImageDrawable(
            AppCompatResources.getDrawable(requireContext(), R.drawable.a_ic_car)
        )

        binding.layoutPlaque.textViewTitle.text = getString(R.string.plaqueNumber)
        binding.layoutPlaque.textViewValue.setProfileCarModel(item?.pelak)
        binding.layoutPlaque.imageViewIcon.setImageDrawable(
            AppCompatResources.getDrawable(requireContext(), R.drawable.a_ic_license_plate_number)
        )
        binding.layoutPlaque.textNumber1.setCarPlaque(item?.pelak)
        binding.layoutPlaque.textAlphabet.setCarPlaque(item?.pelak)
        binding.layoutPlaque.textNumber2.setCarPlaque(item?.pelak)
        binding.layoutPlaque.textCityCode.setCarPlaque(item?.pelak)
        binding.layoutPlaque.constraintLayoutPlaque.visibility =
            if (item?.pelak?.isEmpty() == true)
                View.GONE
        else
            View.VISIBLE
        binding.layoutPlaque.textViewValue.visibility =
            if (item?.pelak?.isEmpty() == true)
                View.VISIBLE
        else
            View.GONE

        showMoreInfo()
    }
    //---------------------------------------------------------------------------------------------- setUserInfo


    //---------------------------------------------------------------------------------------------- showAndHideInfo
    private fun showAndHideInfo() {
        if (binding.expandableInfo.isExpanded)
            hideMoreInfo()
        else
            showMoreInfo()
    }
    //---------------------------------------------------------------------------------------------- showAndHideInfo


    //---------------------------------------------------------------------------------------------- showMoreInfo
    private fun showMoreInfo() {
        binding.expandableInfo.expand()
        val rotate = RotateAnimation(
            0f,
            -90f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 350
        rotate.interpolator = LinearInterpolator()
        rotate.fillAfter = true
        binding.imageViewMore.startAnimation(rotate)
    }
    //---------------------------------------------------------------------------------------------- showMoreInfo


    //---------------------------------------------------------------------------------------------- hideMoreInfo
    private fun hideMoreInfo() {
        binding.expandableInfo.collapse()
        val rotate = RotateAnimation(
            -90f,
            0f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 400
        rotate.interpolator = LinearInterpolator()
        rotate.fillAfter = true
        binding.imageViewMore.startAnimation(rotate)
    }
    //---------------------------------------------------------------------------------------------- hideMoreInfo


    //---------------------------------------------------------------------------------------------- showEditCarPlaque
    private fun showEditCarPlaque() {
        EditPlaqueDialog {
            setUserInfo()
        }.show(childFragmentManager, "edit plaque")
    }
    //---------------------------------------------------------------------------------------------- showEditCarPlaque


    //---------------------------------------------------------------------------------------------- showImageProfile
    private fun showImageProfile() {
        if (context == null)
            return
        ShowImageDialog(
            requireContext(),
            profileViewModel.getUserInfo().getModelForShowImageProfile(profileViewModel.getBearerToken()),
            onShowDialog = { (activity as MainActivity?)?.enableBlurView() },
            onDismissDialog = { (activity as MainActivity?)?.disableBlurView() }
        ).show()
    }
    //---------------------------------------------------------------------------------------------- showImageProfile


/*
    //---------------------------------------------------------------------------------------------- showNotificationDialog
    private fun showNotificationDialog() {
        val position = binding.textViewProfile.top +
                binding.textViewProfile.measuredHeight
        NotificationDialog(position).show(childFragmentManager, "notification dialog")
    }
    //---------------------------------------------------------------------------------------------- showNotificationDialog
*/


}