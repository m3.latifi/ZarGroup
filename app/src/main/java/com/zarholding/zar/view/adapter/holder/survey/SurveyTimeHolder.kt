package com.zarholding.zar.view.adapter.holder.survey

import com.zar.core.view.picker.time.ZarTimePicker
import com.zarholding.zar.databinding.ItemSurveyTimeBinding
import com.zarholding.zar.ext.setSurveyNumber
import com.zarholding.zar.model.data.response.survey.SurveyQuestionModel
import com.zarholding.zar.view.dialog.TimeDialog

/**
 * Create by Mehrdad on 1/21/2023
 */
class SurveyTimeHolder(
    private val binding: ItemSurveyTimeBinding
) : SurveyQuestionHolder(binding.root) {

    //---------------------------------------------------------------------------------------------- bind
    override fun bind(item: SurveyQuestionModel, position: Int) {
        setValueToXml(item, position)
        setListener(item)
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind



    //---------------------------------------------------------------------------------------------- setValueToXml
    private fun setValueToXml(item: SurveyQuestionModel, position: Int) {
        binding.textViewNumber.setSurveyNumber(position + 1)
        binding.textViewQuestion.text = item.questionTitle
    }
    //---------------------------------------------------------------------------------------------- setValueToXml


    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener(item: SurveyQuestionModel) {
        binding.buttonTime.setOnClickListener {
            TimeDialog(
                context = binding.buttonTime.context,
                pickerMode = ZarTimePicker.PickerMode.TIME,
                onChooseTime = { timeDeparture, _ ->
                    binding.buttonTime.text = timeDeparture
                    item.answer = timeDeparture
                }).show()
        }
    }
    //---------------------------------------------------------------------------------------------- setListener

}
