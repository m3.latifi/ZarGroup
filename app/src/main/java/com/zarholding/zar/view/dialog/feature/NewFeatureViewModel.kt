package com.zarholding.zar.view.dialog.feature

import androidx.lifecycle.MutableLiveData
import com.zarholding.zar.ZarViewModel
import com.zarholding.zar.model.data.other.version.VersionFeatureModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

/**
 * Created by m-latifi on 20/8/2022.
 */

@HiltViewModel
class NewFeatureViewModel @Inject constructor() : ZarViewModel() {

    val featureLiveData: MutableLiveData<List<VersionFeatureModel>> by lazy {
        MutableLiveData<List<VersionFeatureModel>>()
    }

    //---------------------------------------------------------------------------------------------- init
    init {
        val versions = mutableListOf<VersionFeatureModel>()
        versions.add(version10())
        versions.add(version9())
        versions.add(version8())
        versions.add(version7())
        versions.add(version6())
        versions.add(version5())
        versions.add(version4())
        versions.add(version3())
        versions.add(version2())
        versions.add(version1())
        featureLiveData.postValue(versions)
    }
    //---------------------------------------------------------------------------------------------- init


    //---------------------------------------------------------------------------------------------- version10
    private fun version10(): VersionFeatureModel {
        val features = mutableListOf<String>()
        features.add("- بهبود مراحل ثبت بیمه تکمیلی")
        return VersionFeatureModel(version = "نسخه 0.1.0", features = features)
    }
    //---------------------------------------------------------------------------------------------- version10



    //---------------------------------------------------------------------------------------------- version9
    private fun version9(): VersionFeatureModel {
        val features = mutableListOf<String>()
        features.add("- رفع باگ های گزارش شده")
        return VersionFeatureModel(version = "نسخه 0.0.9", features = features)
    }
    //---------------------------------------------------------------------------------------------- version9


    //---------------------------------------------------------------------------------------------- version8
    private fun version8(): VersionFeatureModel {
        val features = mutableListOf<String>()
        features.add("-انتخاب سایت، در درخواست خودرو")
        return VersionFeatureModel(version = "نسخه 0.0.8", features = features)
    }
    //---------------------------------------------------------------------------------------------- version8


    //---------------------------------------------------------------------------------------------- version7
    private fun version7(): VersionFeatureModel {
        val features = mutableListOf<String>()
        features.add("-امکان انتخاب چندین مبدا و مقصد در درخواست خودرو")
        features.add("-امکان مشاهده لیست پرسنل به همراه مشخصات آنها")
        features.add("-فعال شدن بخش نظرسنجی")
        features.add("-امکان دریافت سهمیه های پرسنلی")
        return VersionFeatureModel(version = "نسخه 0.0.7", features = features)
    }
    //---------------------------------------------------------------------------------------------- version7



    //---------------------------------------------------------------------------------------------- version6
    private fun version6(): VersionFeatureModel {
        val features = mutableListOf<String>()
        features.add("-امکان ثبت نام بیمه تکمیلی")
        features.add("-امکان نمایش جواب آزمایشات دوره ای")
        return VersionFeatureModel(version = "نسخه 0.0.6", features = features)
    }
    //---------------------------------------------------------------------------------------------- version6



    //---------------------------------------------------------------------------------------------- version5
    private fun version5(): VersionFeatureModel {
        val features = mutableListOf<String>()
        features.add("-نمایش شماره داخلی در پروفایل")
        features.add("-بهبود عملکرد سیستم")
        return VersionFeatureModel(version = "نسخه 0.0.5", features = features)
    }
    //---------------------------------------------------------------------------------------------- version5


    //---------------------------------------------------------------------------------------------- version4
    private fun version4(): VersionFeatureModel {
        val features = mutableListOf<String>()
        features.add("-جستجوی پرسنل بصورت هوشمند")
        features.add("-جستجوی آدرس بصورت هوشمند")
        return VersionFeatureModel(version = "نسخه 0.0.4", features = features)
    }
    //---------------------------------------------------------------------------------------------- version4


    //---------------------------------------------------------------------------------------------- version3
    private fun version3(): VersionFeatureModel {
        val features = mutableListOf<String>()
        features.add("-راه اندازی سهمیه پرسنلی")
        features.add("-جستجو در سرویس های رفت و آمد")
        features.add("-جزییات بیشتر در نمایش ایستگاههای سرویس رفت و آمد")
        return VersionFeatureModel(version = "نسخه 0.0.3", features = features)
    }
    //---------------------------------------------------------------------------------------------- version3



    //---------------------------------------------------------------------------------------------- version2
    private fun version2(): VersionFeatureModel {
        val features = mutableListOf<String>()
        features.add("-مشاهده مشخصات مالک خودرو برای ادمین")
        features.add("-راه اندازی مرکز آموزش")
        return VersionFeatureModel(version = "نسخه 0.0.2", features = features)
    }
    //---------------------------------------------------------------------------------------------- version2


    //---------------------------------------------------------------------------------------------- version1
    private fun version1(): VersionFeatureModel {
        val features = mutableListOf<String>()
        features.add("-ثبت نام در سرویس رفت و آمد")
        features.add("-نمایش آنلاین موقعیت مکانی سرویس")
        features.add("-ثبت درخواست آژانس")
        features.add("-نمایش خبرنامه")
        features.add("-نمایش خبرنامه")
        return VersionFeatureModel(version = "نسخه 0.0.1", features = features)
    }
    //---------------------------------------------------------------------------------------------- version1

}