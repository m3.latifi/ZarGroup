package com.zarholding.zar.view.dialog.personnel

import android.Manifest
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.view.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.zar.core.tools.manager.PermissionManager
import com.zarholding.zar.R
import com.zarholding.zar.databinding.DialogPersonnelBinding
import com.zarholding.zar.model.data.database.entity.UserInfoEntity
import com.zarholding.zar.tools.manager.speechtotext.SpeechToTextInterface
import com.zarholding.zar.tools.manager.speechtotext.SpeechToTextManager
import com.zarholding.zar.view.activity.MainActivity
import com.zarholding.zar.view.adapter.recycler.PersonnelSelectAdapter
import com.zarholding.zar.view.custom.EndlessScrollListener
import com.zarholding.zar.view.custom.getEndlessScrollListener
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import javax.inject.Inject


/**
 * Created by m-latifi on 11/26/2022.
 */

@AndroidEntryPoint
class PersonnelDialog @Inject constructor(
    private val onChoosePerson: (UserInfoEntity) -> Unit
) : DialogFragment() {

    @Inject
    lateinit var speechToTextManager: SpeechToTextManager

    @Inject
    lateinit var permissionManager: PermissionManager

    lateinit var binding: DialogPersonnelBinding

    private val personnelViewModel: PersonnelViewModel by viewModels()

    private var job: Job? = null

    private var adapterPersonnel: PersonnelSelectAdapter? = null

    private var endlessScrollListener: EndlessScrollListener? = null


    //---------------------------------------------------------------------------------------------- textToSpeechObject
    private val textToSpeechObject = object : SpeechToTextInterface {
        override fun onReadyForSpeech() {
            if (context == null)
                return
            binding.textInputLayoutSearch.setStartIconTintList(
                ColorStateList.valueOf(requireContext().getColor(R.color.n_borderRed))
            )
        }

        override fun onEndOfSpeech() {
            if (context == null)
                return
            binding.textInputLayoutSearch.setStartIconTintList(
                ColorStateList.valueOf(requireContext().getColor(R.color.n_blueBottom))
            )
        }

        override fun onResults(result: String) {
            binding.textInputEditTextSearch.setText(result)
            binding.textInputEditTextSearch.requestFocus()
        }
    }
    //---------------------------------------------------------------------------------------------- textToSpeechObject


/*
    //---------------------------------------------------------------------------------------------- launcher
    private val launcher: ActivityResultLauncher<Intent> =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            binding.textInputEditTextSearch.setText(speechToTextManager.splitResultToString(result))
            binding.textInputEditTextSearch.requestFocus()
        }
    //---------------------------------------------------------------------------------------------- launcher
*/


    //---------------------------------------------------------------------------------------------- recordAudioPermissionLauncher
    private val recordAudioPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions()
        ) { results ->
            permissionManager.checkPermissionResult(results) {
                speechToTextManager.speechRecognizer(textToSpeechObject)
            }
        }
    //---------------------------------------------------------------------------------------------- recordAudioPermissionLauncher


    //---------------------------------------------------------------------------------------------- onCreateView
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DialogPersonnelBinding.inflate(inflater, container, false)
        return binding.root
    }
    //---------------------------------------------------------------------------------------------- onCreateView


    //---------------------------------------------------------------------------------------------- onCreateView
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity?)?.enableBlurView()
        val lp = WindowManager.LayoutParams()
        val window = dialog?.window
        val back = ColorDrawable(Color.TRANSPARENT)
        val inset = InsetDrawable(back, 50)
        window?.setBackgroundDrawable(inset)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        isCancelable = false
        window?.attributes = lp
        setListener()
        observeUserMutableLiveData()
    }
    //---------------------------------------------------------------------------------------------- onCreateView


    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener() {

        binding.imageViewClose.setOnClickListener { dismiss() }

        binding.textInputEditTextSearch.addTextChangedListener {
            adapterPersonnel = null
            personnelViewModel.getFilterUserRequestModel().PageNumber = 0
            binding.recyclerViewPersonnel.adapter = null
            job?.cancel()
            createJobForSearch(it.toString())
        }

        binding.textInputLayoutSearch.setStartIconOnClickListener {
            val permission = listOf(Manifest.permission.RECORD_AUDIO)
            val check = permissionManager.isPermissionGranted(
                permissions = permission,
                launcher = recordAudioPermissionLauncher
            )
            if (check) speechToTextManager.speechRecognizer(textToSpeechObject)
        }

    }
    //---------------------------------------------------------------------------------------------- setListener


    //---------------------------------------------------------------------------------------------- createJobForSearch
    private fun createJobForSearch(search: String) {
        job = CoroutineScope(IO).launch {
            delay(800)
            withContext(Main) {
                if (search.isNotEmpty())
                    requestGetUser(search)
                else
                    binding.constraintLayoutHeader.visibility = View.GONE
            }
        }
    }
    //---------------------------------------------------------------------------------------------- createJobForSearch


    //---------------------------------------------------------------------------------------------- observeUserMutableLiveData
    private fun observeUserMutableLiveData() {
        personnelViewModel.userListMutableLiveData.observe(viewLifecycleOwner) {
            binding.textViewLoading.visibility = View.GONE
            setPersonnelAdapter(it)
        }
    }
    //---------------------------------------------------------------------------------------------- observeUserMutableLiveData


    //---------------------------------------------------------------------------------------------- requestGetUser
    private fun requestGetUser(search: String) {
        binding.textViewLoading.visibility = View.VISIBLE
        personnelViewModel.requestGetUser(search)
    }
    //---------------------------------------------------------------------------------------------- requestGetUser


    //---------------------------------------------------------------------------------------------- setPersonnelAdapter
    private fun setPersonnelAdapter(items: List<UserInfoEntity>) {
        binding.constraintLayoutHeader.visibility = View.VISIBLE
        adapterPersonnel?.let {
            adapterPersonnel?.addPerson(items)
            endlessScrollListener?.let {
                it.setLoading(false)
                if (items.isEmpty())
                    binding.recyclerViewPersonnel.removeOnScrollListener(it)
            }
        } ?: run {
            adapterPersonnel = PersonnelSelectAdapter(
                items = items.toMutableList(),
                token = personnelViewModel.getBearerToken()
            ) {
                onChoosePerson(it)
                dismiss()
            }
            val manager = LinearLayoutManager(
                context,
                LinearLayoutManager.VERTICAL,
                false
            )
            endlessScrollListener = getEndlessScrollListener(manager) {
                requestGetUser(personnelViewModel.getFilterUserRequestModel().Search)
            }
            binding.recyclerViewPersonnel.layoutManager = manager
            binding.recyclerViewPersonnel.adapter = adapterPersonnel
            if (endlessScrollListener != null)
                binding.recyclerViewPersonnel.addOnScrollListener(endlessScrollListener!!)
        }

    }
    //---------------------------------------------------------------------------------------------- setPersonnelAdapter


    //---------------------------------------------------------------------------------------------- dismiss
    override fun dismiss() {
        super.dismiss()
        job?.cancel()
        (activity as MainActivity?)?.disableBlurView()
    }
    //---------------------------------------------------------------------------------------------- dismiss
}