package com.zarholding.zar.view.fragment.admin.taxi

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.google.android.material.tabs.TabLayoutMediator
import com.zarholding.zar.R
import com.zarholding.zar.tools.CompanionValues
import com.zarholding.zar.view.adapter.ViewPagerAdapter
import com.zarholding.zar.ZarFragment
import com.zarholding.zar.databinding.FragmentAdminTaxiBinding
import com.zarholding.zar.model.data.enum.EnumPersonnelType
import dagger.hilt.android.AndroidEntryPoint

/**
 * Created by m-latifi on 11/19/2022.
 */

@AndroidEntryPoint
class AdminTaxiFragment(override var layout: Int = R.layout.fragment_admin_taxi) :
    ZarFragment<FragmentAdminTaxiBinding>() {

    private val adminTaxiViewModel: AdminTaxiViewModel by viewModels()
    private var isMyRequest = false


    //---------------------------------------------------------------------------------------------- onViewCreated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isMyRequest = arguments?.getBoolean(CompanionValues.myRequest, false) ?: false
        setFragmentToViewPager()
        observeLiveData()
    }
    //---------------------------------------------------------------------------------------------- onViewCreated


    //---------------------------------------------------------------------------------------------- setFragmentToViewPager
    private fun setFragmentToViewPager() {
        if (isMyRequest)
            setFragmentMyRequestHistory()
        else
            setFragmentRequest()
    }
    //---------------------------------------------------------------------------------------------- setFragmentToViewPager


    //---------------------------------------------------------------------------------------------- setFragmentRequest
    private fun setFragmentRequest() {
        when (adminTaxiViewModel.getUserType()) {
            EnumPersonnelType.Administrative -> setAdministrativeFragment()
            else -> setNotAdministrativeFragment()
        }
    }
    //---------------------------------------------------------------------------------------------- setFragmentRequest


    //---------------------------------------------------------------------------------------------- setFragmentMyRequestHistory
    private fun setFragmentMyRequestHistory() {
        val adapter = ViewPagerAdapter(this@AdminTaxiFragment)
        val myRequestBundle = Bundle()
        myRequestBundle.putString(
            CompanionValues.adminTaxiType,
            com.zarholding.zar.model.data.enum.EnumAdminTaxiType.MY.name
        )
        val myRequestFragment = AdminTaxiListFragment().apply {
            arguments = myRequestBundle
        }
        adapter.addFragment(myRequestFragment, getString(R.string.myRequests))
        binding.viewPager.adapter = adapter
        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->
            tab.text = adapter.getTabTitle(position)
        }.attach()
    }
    //---------------------------------------------------------------------------------------------- setFragmentMyRequestHistory


    //---------------------------------------------------------------------------------------------- setAdministrativeFragment
    private fun setAdministrativeFragment() {
        val adapter = ViewPagerAdapter(this@AdminTaxiFragment)
        val requestBundle = Bundle()
        val historyBundle = Bundle()
        requestBundle.putString(
            CompanionValues.adminTaxiType,
            com.zarholding.zar.model.data.enum.EnumAdminTaxiType.REQUEST.name
        )
        historyBundle.putString(
            CompanionValues.adminTaxiType,
            com.zarholding.zar.model.data.enum.EnumAdminTaxiType.HISTORY.name
        )
        val requestFragment = AdminTaxiListFragment().apply {
            arguments = requestBundle
        }
        val historyFragment = AdminTaxiListFragment().apply {
            arguments = historyBundle
        }
        adapter.addFragment(requestFragment, getString(R.string.requests))
        adapter.addFragment(historyFragment, getString(R.string.historyOfRequests))
        binding.viewPager.adapter = adapter
        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->
            tab.text = adapter.getTabTitle(position)
        }.attach()
    }
    //---------------------------------------------------------------------------------------------- setAdministrativeFragment


    //---------------------------------------------------------------------------------------------- setNotAdministrativeFragment
    private fun setNotAdministrativeFragment() {
        val adapter = ViewPagerAdapter(this@AdminTaxiFragment)
        val requestBundle = Bundle()
        requestBundle.putString(
            CompanionValues.adminTaxiType,
            com.zarholding.zar.model.data.enum.EnumAdminTaxiType.REQUEST.name
        )
        var myRequestFragment: AdminTaxiListFragment? = null
        if (adminTaxiViewModel.getUserType() != EnumPersonnelType.Driver) {
            val myRequestBundle = Bundle()
            myRequestBundle.putString(
                CompanionValues.adminTaxiType,
                com.zarholding.zar.model.data.enum.EnumAdminTaxiType.MY.name
            )
            myRequestFragment = AdminTaxiListFragment().apply {
                arguments = myRequestBundle
            }
        }
        val requestFragment = AdminTaxiListFragment().apply {
            arguments = requestBundle
        }
        adapter.addFragment(requestFragment, getString(R.string.requests))
        myRequestFragment?.let {
            adapter.addFragment(myRequestFragment, getString(R.string.myRequests))
        }
        binding.viewPager.adapter = adapter
        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->
            tab.text = adapter.getTabTitle(position)
        }.attach()

    }
    //---------------------------------------------------------------------------------------------- setNotAdministrativeFragment


    //---------------------------------------------------------------------------------------------- observeLiveData
    private fun observeLiveData() {

        AdminTaxiViewModel.requestTaxiLiveDate.observe(viewLifecycleOwner) {
            if (it > 0)
                binding.tabLayout.getTabAt(0)?.apply {
                    orCreateBadge
                    badge?.isVisible = true
                    badge?.number = it
                }
            else
                binding.tabLayout.getTabAt(0)?.removeBadge()
        }

        AdminTaxiViewModel.myTaxiLiveDate.observe(viewLifecycleOwner) {
            if (it > 0)
                binding.tabLayout.getTabAt(1)?.apply {
                    orCreateBadge
                    badge?.isVisible = true
                    badge?.number = it
                }
            else
                binding.tabLayout.getTabAt(1)?.removeBadge()
        }
    }
    //---------------------------------------------------------------------------------------------- observeLiveData


    //---------------------------------------------------------------------------------------------- onDestroyView
    override fun onDestroyView() {
        super.onDestroyView()
        binding.tabLayout.getTabAt(0)?.removeBadge()
        binding.tabLayout.getTabAt(1)?.removeBadge()
    }
    //---------------------------------------------------------------------------------------------- onDestroyView


}