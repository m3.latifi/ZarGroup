package com.zarholding.zar.view.adapter.recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemAppDashboardBinding
import com.zarholding.zar.model.data.other.AppModel
import com.zarholding.zar.view.adapter.holder.DashboardItemHolder

/**
 * Created by m-latifi on 11/14/2022.
 */

class DashboardAppAdapter(
    private val apps: MutableList<AppModel>,
    private val onClick: (AppModel) -> Unit
) : RecyclerView.Adapter<DashboardItemHolder>() {

    private var layoutInflater: LayoutInflater? = null


    //---------------------------------------------------------------------------------------------- onCreateViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DashboardItemHolder {
        if (layoutInflater == null)
            layoutInflater = LayoutInflater.from(parent.context)
        return DashboardItemHolder(
            binding = ItemAppDashboardBinding.inflate(layoutInflater!!, parent, false),
            onClick = onClick
        )
    }
    //---------------------------------------------------------------------------------------------- onCreateViewHolder


    //---------------------------------------------------------------------------------------------- onBindViewHolder
    override fun onBindViewHolder(holder: DashboardItemHolder, position: Int) {
        holder.bind(apps[position])
    }
    //---------------------------------------------------------------------------------------------- onBindViewHolder


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun getItemCount() = apps.size
    //---------------------------------------------------------------------------------------------- getItemCount

}