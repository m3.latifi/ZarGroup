package com.zarholding.zar.view.adapter.recycler.insurance

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemInsuranceHistoryBinding
import com.zarholding.zar.model.data.response.insurance.history.SupplementalInsuranceHistoryModel
import com.zarholding.zar.view.adapter.holder.insurance.InsuranceHistoryHolder

/**
 * Created by m-latifi on 10/15/2023.
 */

class InsuranceHistoryAdapter(
    private val items: List<SupplementalInsuranceHistoryModel>,
    private val onDownload: (String) -> Unit
): RecyclerView.Adapter<InsuranceHistoryHolder>() {

    private var inflater: LayoutInflater? = null

    //---------------------------------------------------------------------------------------------- onCreateViewHolder
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): InsuranceHistoryHolder {
        if (inflater == null)
            inflater = LayoutInflater.from(parent.context)
        return InsuranceHistoryHolder(
            ItemInsuranceHistoryBinding.inflate(inflater!!,parent,false), onDownload
        )
    }
    //---------------------------------------------------------------------------------------------- onCreateViewHolder


    //---------------------------------------------------------------------------------------------- onBindViewHolder
    override fun onBindViewHolder(holder: InsuranceHistoryHolder, position: Int) {
        holder.bind(item = items[position])
    }
    //---------------------------------------------------------------------------------------------- onBindViewHolder


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun getItemCount() = items.size
    //---------------------------------------------------------------------------------------------- getItemCount

}