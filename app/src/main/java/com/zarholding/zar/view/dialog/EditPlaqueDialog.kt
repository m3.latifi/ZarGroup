package com.zarholding.zar.view.dialog

import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.view.*
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.LinearLayoutManager
import com.skydoves.powerspinner.DefaultSpinnerAdapter
import com.zar.core.enums.EnumApiError
import com.zarholding.zar.R
import com.zarholding.zar.databinding.DialogEditPlaqueBinding
import com.zarholding.zar.ext.hideKeyboard
import com.zarholding.zar.model.data.response.ResponseResult
import com.zarholding.zar.view.activity.MainActivity
import com.zarholding.zar.view.fragment.admin.parking.ParkingViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

/**
 * Created by m-latifi on 11/27/2022.
 */

@AndroidEntryPoint
class EditPlaqueDialog(
    private val onEditPlaque: () -> Unit
) : DialogFragment() {

    lateinit var binding: DialogEditPlaqueBinding

    private val parkingViewModel: ParkingViewModel by viewModels()

    //---------------------------------------------------------------------------------------------- onCreateView
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DialogEditPlaqueBinding.inflate(inflater, container, false)
        return binding.root
    }
    //---------------------------------------------------------------------------------------------- onCreateView


    //---------------------------------------------------------------------------------------------- onCreateView
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity?)?.enableBlurView()
        isCancelable = false
        val lp = WindowManager.LayoutParams()
        val window = dialog!!.window
        val back = ColorDrawable(Color.TRANSPARENT)
        val inset = InsetDrawable(back, 50)
        window!!.setBackgroundDrawable(inset)
        lp.copyFrom(window.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        lp.horizontalMargin = 50f
        window.attributes = lp
        initAlphabetSpinner()
        initOldPlaque()
        observeSuccessLiveData()
        observeErrorLiveDate()
        setListener()
    }
    //---------------------------------------------------------------------------------------------- onCreateView


    //---------------------------------------------------------------------------------------------- showMessage
    private fun showMessage(message: String) {
        activity?.let {
            (it as MainActivity).showMessage(message)
        }
    }
    //---------------------------------------------------------------------------------------------- showMessage



    //---------------------------------------------------------------------------------------------- initOldPlaque
    private fun initOldPlaque(){
        binding.editTextNumber1.setText(parkingViewModel.getPlaqueNumber1())
        binding.editTextNumber2.setText(parkingViewModel.getPlaqueNumber2())
        binding.editTextCityCode.setText(parkingViewModel.getPlaqueCity())
        binding.textInputEditTextCarModel.setText(parkingViewModel.getCarModel())
        binding.powerSpinnerAlphabet.text = parkingViewModel.getPlaqueAlphabet()
    }
    //---------------------------------------------------------------------------------------------- initOldPlaque



    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener() {
        binding.cardViewParent.setOnClickListener { binding.powerSpinnerAlphabet.dismiss() }
        binding.imageViewClose.setOnClickListener { dismiss() }
        binding.buttonYes.setOnClickListener { requestChangeCarPlaque() }
        binding.buttonNo.setOnClickListener { dismiss() }
    }
    //---------------------------------------------------------------------------------------------- setListener


    //---------------------------------------------------------------------------------------------- initAlphabetSpinner
    private fun initAlphabetSpinner() {
        binding.powerSpinnerAlphabet.apply {
            setSpinnerAdapter(DefaultSpinnerAdapter(this))
            setItems(parkingViewModel.getAlphabet())
            setOnSpinnerOutsideTouchListener { _, _ ->  dismiss()}
            getSpinnerRecyclerView().layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            lifecycleOwner = viewLifecycleOwner
            setOnSpinnerItemSelectedListener<String> { _, _, _, _ -> }
        }
        binding.powerSpinnerAlphabet.selectItemByIndex(0)
    }
    //---------------------------------------------------------------------------------------------- initAlphabetSpinner


    //---------------------------------------------------------------------------------------------- observeLoginLiveDate
    private fun observeErrorLiveDate() {
        parkingViewModel.errorLiveDate.observe(viewLifecycleOwner) {
            binding.buttonYes.stopLoading()
            showMessage(it.message)
            when (it.type) {
                EnumApiError.UnAuthorization -> (activity as MainActivity?)?.gotoFirstFragmentWithDeleteUser()

                else -> {}
            }
        }
    }
    //---------------------------------------------------------------------------------------------- observeLoginLiveDate


    //---------------------------------------------------------------------------------------------- observeSuccessLiveData
    private fun observeSuccessLiveData() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                parkingViewModel.userInfo.collect {
                    when(it) {
                        is ResponseResult.Loading -> {}
                        is ResponseResult.Success -> {
                            onEditPlaque.invoke()
                            dismiss()
                        }
                    }
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- observeSuccessLiveData


    //---------------------------------------------------------------------------------------------- requestChangeCarPlaque
    private fun requestChangeCarPlaque() {
        if (binding.buttonYes.isLoading)
            return
        hideKeyboard()
        binding.buttonYes.startLoading(getString(R.string.bePatient))
        parkingViewModel.changeCarPlaque(
            plaqueNumber1 = binding.editTextNumber1.text.toString(),
            plaqueAlphabet = binding.powerSpinnerAlphabet.text.toString(),
            plaqueNumber2 = binding.editTextNumber2.text.toString(),
            plaqueCity = binding.editTextCityCode.text.toString(),
            carModel = binding.textInputEditTextCarModel.text.toString()
        )
    }
    //---------------------------------------------------------------------------------------------- requestChangeCarPlaque


    //---------------------------------------------------------------------------------------------- onDismiss
    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        (activity as MainActivity?)?.disableBlurView()
    }
    //---------------------------------------------------------------------------------------------- onDismis
}