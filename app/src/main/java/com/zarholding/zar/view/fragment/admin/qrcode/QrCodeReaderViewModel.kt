package com.zarholding.zar.view.fragment.admin.qrcode

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.zarholding.zar.ZarViewModel
import com.zarholding.zar.model.data.enum.EnumQuotaApprove
import com.zarholding.zar.model.repository.QuotaRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by m-latifi on 9/3/2023.
 */

@HiltViewModel
class QrCodeReaderViewModel @Inject constructor(
    private val quotaRepository: QuotaRepository
): ZarViewModel(){

    val approveLiveData: MutableLiveData<Boolean> by lazy { MutableLiveData<Boolean>() }

    var type: EnumQuotaApprove? = null

    //---------------------------------------------------------------------------------------------- approveQuota
    fun approveQuota(quotaId: Long) {
        viewModelScope.launch(IO + exceptionHandler()){
            callApi(
                request = quotaRepository.approveQuota(type, quotaId),
                showMessageAfterSuccessResponse = true,
                onReceiveData = {
                    approveLiveData.postValue(it)
                }
            )
        }
    }
    //---------------------------------------------------------------------------------------------- approveQuota


}