package com.zarholding.zar.view.adapter.recycler.insurance

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemSpecificInsurancePointBinding
import com.zarholding.zar.model.data.response.insurance.SupplementalInsurancePointModel
import com.zarholding.zar.view.adapter.holder.insurance.SpecificInsurancePointHolder

/**
 * Created by m-latifi on 10/15/2023.
 */

class SpecificInsurancePointAdapter(
    private val items: List<SupplementalInsurancePointModel>
): RecyclerView.Adapter<SpecificInsurancePointHolder>() {

    private var inflater: LayoutInflater? = null

    //---------------------------------------------------------------------------------------------- onCreateViewHolder
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SpecificInsurancePointHolder {
        if (inflater == null)
            inflater = LayoutInflater.from(parent.context)
        return SpecificInsurancePointHolder(
            ItemSpecificInsurancePointBinding.inflate(inflater!!,parent,false)
        )
    }
    //---------------------------------------------------------------------------------------------- onCreateViewHolder


    //---------------------------------------------------------------------------------------------- onBindViewHolder
    override fun onBindViewHolder(holder: SpecificInsurancePointHolder, position: Int) {
        holder.bind(item = items[position])
    }
    //---------------------------------------------------------------------------------------------- onBindViewHolder


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun getItemCount() = items.size
    //---------------------------------------------------------------------------------------------- getItemCount

}