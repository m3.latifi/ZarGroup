package com.zarholding.zar.view.dialog

import android.content.Context
import android.content.res.Resources
import com.zar.core.view.picker.date.customviews.DateRangeCalendarView
import com.zar.core.view.picker.date.dialog.DatePickerDialog
import com.zarholding.zar.R

/**
 * Created by m-latifi on 10/22/2023.
 */

class DateDialog(
    private val context: Context,
    private val resources: Resources,
    private val onSingleDateSelectedListener: (String) -> Unit
) {

    init {
        showDatePickerDialog()
    }

    //---------------------------------------------------------------------------------------------- showDatePickerDialog
    private fun showDatePickerDialog() {
        val datePickerDialog = DatePickerDialog(context)
        datePickerDialog.selectionMode = DateRangeCalendarView.SelectionMode.Single

        datePickerDialog.isDisableDaysAgo = false
        datePickerDialog.acceptButtonColor =
            resources.getColor(R.color.n_blueBottom, context.theme)
        datePickerDialog.headerBackgroundColor =
            resources.getColor(R.color.n_blueBottom, context.theme)
        datePickerDialog.headerTextColor =
            resources.getColor(R.color.white, context.theme)
        datePickerDialog.weekColor =
            resources.getColor(R.color.n_textHint, context.theme)
        datePickerDialog.disableDateColor =
            resources.getColor(R.color.n_textHint, context.theme)
        datePickerDialog.defaultDateColor =
            resources.getColor(R.color.n_blueBottom, context.theme)
        datePickerDialog.selectedDateCircleColor =
            resources.getColor(R.color.n_blueBottom, context.theme)
        datePickerDialog.selectedDateColor =
            resources.getColor(R.color.white, context.theme)
        datePickerDialog.rangeDateColor =
            resources.getColor(R.color.n_blueBottom, context.theme)
        datePickerDialog.rangeStripColor =
            resources.getColor(R.color.n_primaryColorAlpha, context.theme)
        datePickerDialog.holidayColor =
            resources.getColor(R.color.n_reject, context.theme)
        datePickerDialog.textSizeWeek = 12.0f
        datePickerDialog.textSizeDate = 14.0f
        datePickerDialog.textSizeTitle = 18.0f
        datePickerDialog.setCanceledOnTouchOutside(true)
        datePickerDialog.onSingleDateSelectedListener =
            DatePickerDialog.OnSingleDateSelectedListener { startDate ->
                onSingleDateSelectedListener.invoke(startDate.persianShortDate)
            }
        datePickerDialog.showDialog()

    }
    //---------------------------------------------------------------------------------------------- showDatePickerDialog


}