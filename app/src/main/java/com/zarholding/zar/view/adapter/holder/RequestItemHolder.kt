package com.zarholding.zar.view.adapter.holder

import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemRequestBinding
import com.zarholding.zar.ext.setAppComingSoon
import com.zarholding.zar.ext.setAppIcon
import com.zarholding.zar.model.data.other.AppModel

/**
 * Created by m-latifi on 11/14/2022.
 */

class RequestItemHolder(
    private val binding : ItemRequestBinding,
    private val onClick: (Int) -> Unit
): RecyclerView.ViewHolder(binding.root)
{


    //---------------------------------------------------------------------------------------------- bind
    fun bind(item: AppModel) {
        setValueToXml(item)
        setListener(item)
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind



    //---------------------------------------------------------------------------------------------- setValueToXml
    private fun setValueToXml(item: AppModel) {
        binding.imageViewLogo.setAppIcon(icon = item.icon)
        binding.textViewTitle.text = item.title
        binding.linearLayoutComingSoon.setAppComingSoon(link = item.link)
    }
    //---------------------------------------------------------------------------------------------- setValueToXml


    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener(item: AppModel) {
        binding.root.setOnClickListener {
            onClick(item.link)
        }
    }
    //---------------------------------------------------------------------------------------------- setListener

}