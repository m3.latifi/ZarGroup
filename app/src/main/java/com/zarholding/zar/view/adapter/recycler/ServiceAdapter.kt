package com.zarholding.zar.view.adapter.recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemServiceBinding
import com.zarholding.zar.model.data.other.ShowImageModel
import com.zarholding.zar.model.data.response.trip.TripModel
import com.zarholding.zar.view.adapter.holder.ServiceHolder

/**
 * Created by m-latifi on 11/20/2022.
 */

class ServiceAdapter(
    private val tripList: List<TripModel>,
    private val onClick: (TripModel) -> Unit,
    private val onRegisterStation: (TripModel) -> Unit,
    private val onClickImage: (ShowImageModel) -> Unit
) :
    RecyclerView.Adapter<ServiceHolder>() {

    private var layoutInflater: LayoutInflater? = null

    //---------------------------------------------------------------------------------------------- onCreateViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServiceHolder {
        if (layoutInflater == null)
            layoutInflater = LayoutInflater.from(parent.context)
        return ServiceHolder(
            binding = ItemServiceBinding.inflate(layoutInflater!!, parent, false),
            onClick = onClick,
            onRegisterStation = onRegisterStation,
            onClickImage = onClickImage
        )
    }
    //---------------------------------------------------------------------------------------------- onCreateViewHolder


    //---------------------------------------------------------------------------------------------- onBindViewHolder
    override fun onBindViewHolder(holder: ServiceHolder, position: Int) {
        holder.bind(tripList[position])
    }
    //---------------------------------------------------------------------------------------------- onBindViewHolder


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun getItemCount() = tripList.size
    //---------------------------------------------------------------------------------------------- getItemCount


}