package com.zarholding.zar.view.adapter.holder.survey

import android.text.Editable
import android.text.TextWatcher
import androidx.core.text.isDigitsOnly
import com.zarholding.zar.R
import com.zarholding.zar.databinding.ItemSurveyNumberBinding
import com.zarholding.zar.ext.setSurveyNumber
import com.zarholding.zar.model.data.response.survey.SurveyQuestionModel

/**
 * Create by Mehrdad on 1/21/2023
 */

class SurveyNumberHolder(
    private val binding: ItemSurveyNumberBinding
) : SurveyQuestionHolder(binding.root) {

    private var textWatcher: TextWatcher? = null

    //---------------------------------------------------------------------------------------------- bind
    override fun bind(item: SurveyQuestionModel, position: Int) {
        setValueToXml(item, position)
        setListener(item)
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind


    //---------------------------------------------------------------------------------------------- setValueToXml
    private fun setValueToXml(item: SurveyQuestionModel, position: Int) {
        binding.textViewNumber.setSurveyNumber(position + 1)
        binding.textViewQuestion.text = item.questionTitle
        binding.textInputEditTextAnswer.removeTextChangedListener(textWatcher)
        binding.textInputEditTextAnswer.setText(item.answer)
    }
    //---------------------------------------------------------------------------------------------- setValueToXml


    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener(item: SurveyQuestionModel) {
        if (item.selectedAnswersId == null)
            item.selectedAnswersId = mutableListOf()

        textWatcher = object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                if (s.toString().isNotEmpty() && s.toString().isDigitsOnly() && !item.options.isNullOrEmpty()) {
                    val min = item.options[0].rangeFrom
                    val max = item.options[0].rangeTo
                    val value = s.toString().toLong()
                    if (min != 0 && min > value) {
                        binding.textInputEditTextAnswer.error =
                            binding.textInputEditTextAnswer.context.getString(R.string.valueIsLower, min.toString())
                        return
                    }
                    if (max != 0 && max < value) {
                        binding.textInputEditTextAnswer.error =
                            binding.textInputEditTextAnswer.context.getString(R.string.valueIsBigger, max.toString())
                        return
                    }
                    item.answer = s.toString()
                }
            }
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        }

        binding.textInputEditTextAnswer.addTextChangedListener(textWatcher)

    }
    //---------------------------------------------------------------------------------------------- setListener

}
