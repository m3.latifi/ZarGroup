package com.zarholding.zar.view.fragment.user.login

import android.os.Bundle
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.zarholding.zar.R
import com.zarholding.zar.view.activity.MainActivity
import com.zarholding.zar.ext.hideKeyboard
import com.zarholding.zar.ZarFragment
import com.zarholding.zar.databinding.FragmentLoginBinding
import com.zarholding.zar.model.data.response.ResponseResult
import com.zarholding.zar.tools.manager.BiometricManager
import com.zarholding.zar.view.dialog.ConfirmDialog
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by m-latifi on 11/9/2022.
 */

@AndroidEntryPoint
class LoginFragment(override var layout: Int = R.layout.fragment_login) :
    ZarFragment<FragmentLoginBinding>() {

    @Inject
    lateinit var biometricManager: BiometricManager

    private val viewModel: LoginViewModel by viewModels()


    //---------------------------------------------------------------------------------------------- onViewCreated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        setListener()
        backClickControl()
    }
    //---------------------------------------------------------------------------------------------- onViewCreated


    //---------------------------------------------------------------------------------------------- initView
    private fun initView() {
        observeLiveDate()
        activity?.let { (it as MainActivity).deleteAllData() }
        if (viewModel.isBiometricEnable())
            binding.imageViewFingerLogin.visibility = View.VISIBLE
        else
            binding.imageViewFingerLogin.visibility = View.GONE
    }
    //---------------------------------------------------------------------------------------------- initView


    //---------------------------------------------------------------------------------------------- backClickControl
    private fun backClickControl() {
        activity?.onBackPressedDispatcher?.addCallback(viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    context?.let {
                        ConfirmDialog(
                            context = it,
                            type = ConfirmDialog.ConfirmType.DELETE,
                            title = getString(R.string.doYouWantToExitApp),
                            onYesClick = { activity?.finish() },
                            onShowDialog = { (activity as MainActivity?)?.enableBlurView() },
                            onDismissDialog = { (activity as MainActivity?)?.disableBlurView() }).show()
                    }
                }
            })
    }
    //---------------------------------------------------------------------------------------------- backClickControl


    //---------------------------------------------------------------------------------------------- observeLiveDate
    private fun observeLiveDate() {
        viewModel.errorLiveDate.observe(viewLifecycleOwner) {
            stopLoading()
            showMessage(message = it.message, type = it.type)
        }

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED){
                viewModel.loginResult.collect {
                    when(it){
                        is ResponseResult.Loading -> {}
                        is ResponseResult.Success -> {
                            if (it.data)
                                gotoFragment(R.id.action_goto_SplashFragment)
                            else
                                stopLoading()
                        }
                    }
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- observeLiveDate


    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener() {

        binding.buttonLogin.setOnClickListener { login() }

        binding
            .textInputEditTextUserName
            .setOnClickListener { binding.textInputLayoutUserName.error = null }

        binding
            .textInputEditTextPasscode
            .setOnClickListener { binding.textInputLayoutPasscode.error = null }

        binding
            .imageViewFingerLogin
            .setOnClickListener { showBiometricDialog() }
    }
    //---------------------------------------------------------------------------------------------- setListener


    //---------------------------------------------------------------------------------------------- showBiometricDialog
    private fun showBiometricDialog() {
        if (activity == null)
            return
        biometricManager.showBiometricDialog(
            fragment = requireActivity(),
            onAuthenticationError = {
                stopLoading()
                showMessage(message = getString(R.string.onAuthenticationError))
            },
            onAuthenticationSucceeded = {
                getSavedUserNameAndPassword()
            }
        )
    }
    //---------------------------------------------------------------------------------------------- showBiometricDialog


    //---------------------------------------------------------------------------------------------- getSavedUserNameAndPassword
    private fun getSavedUserNameAndPassword() {
        binding.textInputEditTextUserName.setText(viewModel.getSavedUserName())
        binding.textInputEditTextPasscode.setText(viewModel.getSavedPassword())
        login()
    }
    //---------------------------------------------------------------------------------------------- getSavedUserNameAndPassword


    //---------------------------------------------------------------------------------------------- login
    private fun login() {
        var isValidUseName = true
        if (binding.textInputEditTextUserName.text.isNullOrEmpty()) {
            binding.textInputEditTextUserName.error = getString(R.string.userNameIsEmpty)
            isValidUseName = false
        }
        var isValidPassword = true
        if (binding.textInputEditTextPasscode.text.isNullOrEmpty()) {
            binding.textInputEditTextPasscode.error = getString(R.string.passcodeIsEmpty)
            isValidPassword = false
        }
        if (isValidUseName && isValidPassword) {
            startLoading()
            viewModel.login(
                userName = binding.textInputEditTextUserName.text.toString(),
                password = binding.textInputEditTextPasscode.text.toString()
            )
        }
    }
    //---------------------------------------------------------------------------------------------- login


    //---------------------------------------------------------------------------------------------- startLoading
    private fun startLoading() {
        hideKeyboard()
        binding.textInputLayoutUserName.error = null
        binding.textInputLayoutPasscode.error = null
        binding.textInputEditTextUserName.isEnabled = false
        binding.textInputEditTextPasscode.isEnabled = false
        binding.buttonLogin.startLoading(getString(R.string.bePatient))
    }
    //---------------------------------------------------------------------------------------------- startLoading


    //---------------------------------------------------------------------------------------------- stopLoading
    private fun stopLoading() {
        binding.textInputEditTextUserName.isEnabled = true
        binding.textInputEditTextPasscode.isEnabled = true
        binding.buttonLogin.stopLoading()
    }
    //---------------------------------------------------------------------------------------------- stopLoading


}