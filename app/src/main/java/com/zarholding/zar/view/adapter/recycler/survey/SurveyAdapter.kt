package com.zarholding.zar.view.adapter.recycler.survey

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemSurveyBinding
import com.zarholding.zar.model.data.response.survey.SurveyModel
import com.zarholding.zar.view.adapter.holder.survey.SurveyHolder

/**
 * Created by m-latifi on ۰۸/۰۲/۲۰۲۳.
 */
class SurveyAdapter(
    private val surveys: List<SurveyModel>,
    private val onClick: (Int) -> Unit
) : RecyclerView.Adapter<SurveyHolder>() {

    private var inflater: LayoutInflater? = null

    //---------------------------------------------------------------------------------------------- onCreateViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SurveyHolder {
        if (inflater == null)
            inflater = LayoutInflater.from(parent.context)
        return SurveyHolder(
            binding = ItemSurveyBinding.inflate(inflater!!, parent, false),
            onClick = onClick
        )
    }
    //---------------------------------------------------------------------------------------------- onCreateViewHolder


    //---------------------------------------------------------------------------------------------- onBindViewHolder
    override fun onBindViewHolder(holder: SurveyHolder, position: Int) {
        holder.bind(surveys[position], position)
    }
    //---------------------------------------------------------------------------------------------- onBindViewHolder


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun getItemCount() = surveys.size
    //---------------------------------------------------------------------------------------------- getItemCount

}