package com.zarholding.zar.view.activity

import android.content.*
import android.content.res.Configuration
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.google.android.material.snackbar.Snackbar
import com.zar.core.enums.EnumApiError
import com.zarholding.zar.databinding.ActivityMainBinding
import com.zarholding.zar.tools.CompanionValues
import com.zarholding.zar.view.dialog.notification.NotificationDialog
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import com.zarholding.zar.R
import com.zarholding.zar.ext.loadImageByToken
import com.zarholding.zar.ext.setTitleAndValue
import kotlinx.coroutines.Job


/**
 * Created by m-latifi on 11/8/2022.
 */

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

//    @Inject
//    lateinit var loggerManager: LoggerManager

    lateinit var binding: ActivityMainBinding
    private var navController: NavController? = null
    private var job: Job? = null

    private val mainViewModel: MainViewModel by viewModels()

    private var broadcastReceiver: BroadcastReceiver? = null


    //---------------------------------------------------------------------------------------------- onCreate
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        initView()
    }
    //---------------------------------------------------------------------------------------------- onCreate


    //---------------------------------------------------------------------------------------------- initView
    private fun initView() {
        mainViewModel.setLastNotificationIdToZero()
        binding.blurView.setupWith(binding.constraintLayoutParent)
            .setBlurRadius(10f).setBlurEnabled(false)
        setAppTheme()
        registerReceiver()
        setUserInfo()
        setListener()
    }
    //---------------------------------------------------------------------------------------------- initView


    //______________________________________________________________________________________________ setAppTheme
    private fun setAppTheme() {
        when (mainViewModel.applicationTheme()) {
            Configuration.UI_MODE_NIGHT_YES ->
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)

            Configuration.UI_MODE_NIGHT_NO ->
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        }
    }
    //______________________________________________________________________________________________ setAppTheme


    //---------------------------------------------------------------------------------------------- setNotificationCount
    fun setNotificationCount(count: Int) {
        MainViewModel.notificationCount = count
        binding.textViewNotificationCount.text = count.toString()
        if (count < 1)
            binding.cardViewNotification.visibility = View.GONE
        else
            binding.cardViewNotification.visibility = View.VISIBLE
    }
    //---------------------------------------------------------------------------------------------- setNotificationCount


    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener() {

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment?
        navController = navHostFragment?.navController
        navController?.addOnDestinationChangedListener { _, destination, _ ->
            showAndHideBottomNavigationMenu(destination.id)
        }


        binding.imageViewNotification.setOnClickListener { showNotificationDialog() }

        binding.cardViewNotification.setOnClickListener { showNotificationDialog() }

        binding.cardViewProfile.setOnClickListener {
            gotoFragment(R.id.action_goto_ProfileFragment)
        }

        binding.customMenuProfile.setOnClickListener {
            gotoFragment(R.id.action_goto_ProfileFragment)
        }

        binding.customMenuSetting.setOnClickListener {
            gotoFragment(R.id.action_goto_SettingFragment)
        }

        binding.customMenuHome.setOnClickListener {
            gotoFragment(R.id.action_goto_HomeFragment)
        }

        binding.customMenuAdmin.setOnClickListener {
            gotoFragment(R.id.action_goto_DashboardFragment)
        }

    }
    //---------------------------------------------------------------------------------------------- setListener


    //---------------------------------------------------------------------------------------------- showNotificationDialog
    private fun showNotificationDialog() {
        val position = binding.constraintLayoutProfile.top +
                binding.constraintLayoutProfile.measuredHeight
        NotificationDialog(position).show(supportFragmentManager, "notification dialog")
    }
    //---------------------------------------------------------------------------------------------- showNotificationDialog


    //---------------------------------------------------------------------------------------------- showAndHideBottomNavigationMenu
    private fun showAndHideBottomNavigationMenu(fragmentId: Int) {
        resetBottomMenu()
        when (fragmentId) {
            R.id.splashFragment,
            R.id.loginFragment,
            R.id.mapFragment,
            R.id.videoFragment,
            R.id.downloadUpdateFragment -> {
                binding.constraintLayoutFooterMenu.visibility = View.GONE
                binding.constraintLayoutProfile.visibility = View.GONE
            }

            R.id.profileFragment -> {
                binding.constraintLayoutFooterMenu.visibility = View.VISIBLE
                binding.constraintLayoutProfile.visibility = View.GONE
                binding.customMenuProfile.selected()
            }

            R.id.dashboardFragment,
            R.id.adminTaxiFragment,
            R.id.adminBusFragment -> {
                binding.constraintLayoutFooterMenu.visibility = View.VISIBLE
                binding.constraintLayoutProfile.visibility = View.VISIBLE
                binding.customMenuAdmin.selected()
            }

            R.id.settingFragment -> {
                binding.constraintLayoutFooterMenu.visibility = View.VISIBLE
                binding.constraintLayoutProfile.visibility = View.VISIBLE
                binding.customMenuSetting.selected()
            }

            R.id.homeFragment -> {
                binding.constraintLayoutFooterMenu.visibility = View.VISIBLE
                binding.constraintLayoutProfile.visibility = View.VISIBLE
                binding.customMenuHome.selected()
            }
            else -> {
                binding.constraintLayoutFooterMenu.visibility = View.VISIBLE
                binding.constraintLayoutProfile.visibility = View.VISIBLE
            }
        }
    }
    //---------------------------------------------------------------------------------------------- showAndHideBottomNavigationMenu


    //---------------------------------------------------------------------------------------------- resetBottomMenu
    private fun resetBottomMenu() {
        binding.customMenuAdmin.unSelect()
        binding.customMenuHome.unSelect()
        binding.customMenuProfile.unSelect()
        binding.customMenuSetting.unSelect()
    }
    //---------------------------------------------------------------------------------------------- resetBottomMenu


    //---------------------------------------------------------------------------------------------- gotoFirstFragment
    fun gotoFirstFragmentWithDeleteUser() {
        CoroutineScope(IO).launch {
            deleteAllData()
            delay(500)
            withContext(Main) {
                gotoFragment(R.id.action_goto_SplashFragment)
            }
        }
    }
    //---------------------------------------------------------------------------------------------- gotoFirstFragment


    //---------------------------------------------------------------------------------------------- deleteAllData
    fun deleteAllData() {
        mainViewModel.deleteAllData()
    }
    //---------------------------------------------------------------------------------------------- deleteAllData


    //---------------------------------------------------------------------------------------------- gotoFragment
    private fun gotoFragment(fragment: Int) {
        try {
            navController?.navigate(fragment, null)
        } catch (e: java.lang.Exception) {
            showMessage(getString(R.string.notFoundPage))
        }
    }
    //---------------------------------------------------------------------------------------------- gotoFragment


    //---------------------------------------------------------------------------------------------- setUserInfo
    fun setUserInfo() {
        job = CoroutineScope(Main).launch {
            delay(500)
            val user = mainViewModel.getUserInfo()
            binding.imageviewProfile.loadImageByToken(
                url = user?.userName ?: "",
                token = mainViewModel.getBearerToken()
            )
            binding.textViewProfileName.text = user?.fullName ?: ""
            binding.textViewPersonalCode.setTitleAndValue(
                title = getString(R.string.personalCode),
                splitter = getString(R.string.colon),
                value = user?.userName ?: ""
            )
            if (mainViewModel.getAdminRole())
                binding.customMenuAdmin.visibility = View.VISIBLE
            else
                binding.customMenuAdmin.visibility = View.GONE
            binding.notifyChange()
        }
    }
    //---------------------------------------------------------------------------------------------- setUserInfo


    //---------------------------------------------------------------------------------------------- registerReceiver
    private fun registerReceiver() {
        broadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent) {
                val item = intent.getStringExtra(CompanionValues.notificationLast)
                if (item.isNullOrEmpty())
                    MainViewModel.notificationCount -= 1
                else MainViewModel.notificationCount += 1
                setNotificationCount(MainViewModel.notificationCount)
            }
        }
        registerReceiver(
            broadcastReceiver,
            IntentFilter("com.zarholding.zar.receive.message"),
            RECEIVER_EXPORTED
        )
    }
    //---------------------------------------------------------------------------------------------- registerReceiver


    //---------------------------------------------------------------------------------------------- showMessage
    fun showMessage(message: String, type: EnumApiError = EnumApiError.Warning, duration: Int = 3) {
        val snack = Snackbar.make(binding.constraintLayoutParent, message, duration * 1000)
        snack.setTextColor(resources.getColor(R.color.textViewColor3, theme))
        snack.setAction(getString(R.string.dismiss)) { snack.dismiss() }
        snack.setActionTextColor(resources.getColor(R.color.textViewColor1, theme))
        val view = snack.view
        val params = view.layoutParams as FrameLayout.LayoutParams
        params.gravity = Gravity.TOP
        when (type) {
            EnumApiError.Done -> {
                snack.setBackgroundTint(resources.getColor(R.color.positive, theme))
            }

            EnumApiError.Warning -> {
                snack.setBackgroundTint(resources.getColor(R.color.n_waiting, theme))
            }

            else -> {
                snack.setBackgroundTint(resources.getColor(R.color.negative, theme))
            }
        }
        snack.show()
    }
    //---------------------------------------------------------------------------------------------- showMessage


    //---------------------------------------------------------------------------------------------- enableBlurView
    fun enableBlurView() {
        binding.blurView.setBlurEnabled(true)
    }
    //---------------------------------------------------------------------------------------------- enableBlurView


    //---------------------------------------------------------------------------------------------- disableBlurView
    fun disableBlurView() {
        binding.blurView.setBlurEnabled(false)
    }
    //---------------------------------------------------------------------------------------------- disableBlurView


    //---------------------------------------------------------------------------------------------- onDestroy
    override fun onDestroy() {
        super.onDestroy()
        job?.cancel()
        unregisterReceiver(broadcastReceiver)
    }
    //---------------------------------------------------------------------------------------------- onDestroy

}