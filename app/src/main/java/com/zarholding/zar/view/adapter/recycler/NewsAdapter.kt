package com.zarholding.zar.view.adapter.recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemNewsBinding
import com.zarholding.zar.model.data.database.entity.ArticleEntity
import com.zarholding.zar.view.adapter.holder.NewsItemHolder

/**
 * Created by m-latifi on 11/15/2022.
 */

class NewsAdapter(
    private val news: List<ArticleEntity>,
    private val onClick: (ArticleEntity) -> Unit
) :
    RecyclerView.Adapter<NewsItemHolder>() {


    private var layoutInflater: LayoutInflater? = null


    //---------------------------------------------------------------------------------------------- onCreateViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsItemHolder {
        if (layoutInflater == null)
            layoutInflater = LayoutInflater.from(parent.context)
        return NewsItemHolder(
            binding = ItemNewsBinding.inflate(layoutInflater!!, parent, false),
            onClick = onClick
        )
    }
    //---------------------------------------------------------------------------------------------- onCreateViewHolder


    //---------------------------------------------------------------------------------------------- onBindViewHolder
    override fun onBindViewHolder(holder: NewsItemHolder, position: Int) {
        holder.bind(news[position])
    }
    //---------------------------------------------------------------------------------------------- onBindViewHolder


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun getItemCount() = news.size
    //---------------------------------------------------------------------------------------------- getItemCount


}