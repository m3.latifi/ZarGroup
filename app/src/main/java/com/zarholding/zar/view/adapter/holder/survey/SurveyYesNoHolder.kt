package com.zarholding.zar.view.adapter.holder.survey

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.zarholding.zar.databinding.ItemSurveyYesNoBinding
import com.zarholding.zar.ext.setSurveyNumber
import com.zarholding.zar.model.data.response.survey.SurveyQuestionModel

/**
 * Create by Mehrdad on 1/21/2023
 */
class SurveyYesNoHolder(
    private val binding: ItemSurveyYesNoBinding
) : SurveyQuestionHolder(binding.root) {

    private var textWatcher: TextWatcher? = null

    //---------------------------------------------------------------------------------------------- bind
    override fun bind(item: SurveyQuestionModel, position: Int) {
        setValueToXml(item, position)
        setListener(item)
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind


    //---------------------------------------------------------------------------------------------- setValueToXml
    private fun setValueToXml(item: SurveyQuestionModel, position: Int) {
        binding.textViewNumber.setSurveyNumber(position + 1)
        binding.textViewQuestion.text = item.questionTitle
        binding.editTextAnswer.removeTextChangedListener(textWatcher)
        binding.editTextAnswer.setText(item.answer)
        item.selectedAnswerId = item.options?.get(0)?.questionOptionId
        binding.editTextAnswer.visibility = if (item.options?.get(0)?.hasAdditionalText == true)
            View.VISIBLE else View.GONE
        if (!item.options.isNullOrEmpty() && item.selectedAnswerId != null) {
            when(item.options.indexOfFirst { it.questionOptionId == item.selectedAnswerId }) {
                0 -> binding.switchActive.isChecked = false
                1 -> binding.switchActive.isChecked = true
            }
        }
    }
    //---------------------------------------------------------------------------------------------- setValueToXml


    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener(item: SurveyQuestionModel) {
        textWatcher = object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                item.answer = s.toString()
            }
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        }
        binding.editTextAnswer.addTextChangedListener(textWatcher)

        binding.switchActive.setOnCheckedChangeListener { _, isChecked ->
            if (!item.options.isNullOrEmpty() && item.options.size > 1) {
                if (isChecked) {
                    item.selectedAnswerId = item.options[1].questionOptionId
                    binding.editTextAnswer.visibility = if (item.options[1].hasAdditionalText)
                        View.VISIBLE else View.GONE
                } else {
                    item.selectedAnswerId = item.options[0].questionOptionId
                    binding.editTextAnswer.visibility = if (item.options[0].hasAdditionalText)
                        View.VISIBLE else View.GONE
                }
            }
        }

    }
    //---------------------------------------------------------------------------------------------- setListener
}
