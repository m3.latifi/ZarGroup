package com.zarholding.zar.view.adapter.holder.quota

import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.R
import com.zarholding.zar.databinding.ItemPersonnelQuotaHistoryBinding
import com.zarholding.zar.ext.setQuotaStatus
import com.zarholding.zar.ext.setTitleAndValue
import com.zarholding.zar.model.data.response.quota.QuotaHistoryModel

/**
 * Created by m-latifi on 7/26/2023.
 */

class QuotaHistoryHolder(
    private val binding: ItemPersonnelQuotaHistoryBinding
) : RecyclerView.ViewHolder(binding.root) {


    //---------------------------------------------------------------------------------------------- bind
    fun bind(item: QuotaHistoryModel) {
        setValueToXml(item)
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind


    //---------------------------------------------------------------------------------------------- setValueToXml
    private fun setValueToXml(item: QuotaHistoryModel) {
        binding.textViewTitle.text = item.quotaName
        binding.textViewReceiverName.setTitleAndValue(
            title = binding.textViewReceiverName.context.getString(R.string.receiverName),
            splitter = binding.textViewReceiverName.context.getString(R.string.colon),
            value = item.receiverName
        )
        binding.textViewDeliverName.setTitleAndValue(
            title = binding.textViewDeliverName.context.getString(R.string.deliverName),
            splitter = binding.textViewDeliverName.context.getString(R.string.colon),
            value = item.deliverName
        )
        binding.textViewExtruderName.setTitleAndValue(
            title = binding.textViewExtruderName.context.getString(R.string.extruderName),
            splitter = binding.textViewExtruderName.context.getString(R.string.colon),
            value = item.extruderName
        )
        binding.textViewReceiveDate.text = binding.textViewReceiveDate.context
            .getString(R.string.receiveDate, item.receiveDate ?: "")
        binding.textViewExitDate.text = binding.textViewReceiveDate.context
            .getString(R.string.exitDate, item.exitDate ?: "")

/*        binding.textViewReceiveDate.text = binding.textViewReceiveDate.context
            .getString(R.string.receiveDate, item.receiveDate.toSolarDate()?.getSolarDate())
        binding.textViewExitDate.text = binding.textViewReceiveDate.context
            .getString(R.string.exitDate, item.exitDate.toSolarDate()?.getSolarDate())*/
        binding.textViewStatus.setQuotaStatus(item.status)
    }
    //---------------------------------------------------------------------------------------------- setValueToXml

}