package com.zarholding.zar.view.dialog.personnel

import androidx.lifecycle.viewModelScope
import com.zarholding.zar.ZarViewModel
import com.zarholding.zar.model.data.database.entity.UserInfoEntity
import com.zarholding.zar.model.data.request.FilterUserRequestModel
import com.zarholding.zar.model.data.response.ResponseResult
import com.zarholding.zar.model.repository.TokenRepository
import com.zarholding.zar.model.repository.UserRepository
import com.zarholding.zar.tools.SingleLiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by m-latifi on 11/26/2022.
 */

@HiltViewModel
class PersonnelViewModel @Inject constructor(
    private val tokenRepository: TokenRepository,
    private val userRepository: UserRepository
) : ZarViewModel() {

    val userListMutableLiveData: SingleLiveEvent<List<UserInfoEntity>> by lazy {
        SingleLiveEvent<List<UserInfoEntity>>()
    }

    private val _userInfo =
        MutableStateFlow<ResponseResult<UserInfoEntity>>(ResponseResult.Loading(false))
    val userInfo: StateFlow<ResponseResult<UserInfoEntity>> = _userInfo
    private var userInfoEntity: UserInfoEntity? = null

    //---------------------------------------------------------------------------------------------- requestGetUser
    fun requestGetUser(search: String) {
        viewModelScope.launch(IO + exceptionHandler()){
            callApi(
                request = userRepository.requestGetUser(search, tokenRepository.getBearerToken()),
                onReceiveData = {userItem ->
                    userItem.items?.let {
                        userListMutableLiveData.postValue(it)
                    }
                }
            )
        }
    }
    //---------------------------------------------------------------------------------------------- requestGetUser



    //---------------------------------------------------------------------------------------------- requestGetUserInfoById
    fun requestGetUserInfoById(id: Int) {
        viewModelScope.launch(IO + exceptionHandler()) {
            _userInfo.value = ResponseResult.Loading(true)
            userRepository.requestGetUserInfo(id = id).collect {networkResult ->
                checkResponse(networkResult = networkResult) {
                    _userInfo.value = ResponseResult.Loading(false)
                    userInfoEntity = it
                    _userInfo.value = ResponseResult.Success(it)
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- requestGetUserInfoById


    //---------------------------------------------------------------------------------------------- getFilterUserRequestModel
    fun getFilterUserRequestModel(): FilterUserRequestModel = userRepository.filterUser
    //---------------------------------------------------------------------------------------------- getFilterUserRequestModel


    //---------------------------------------------------------------------------------------------- getBearerToken
    fun getBearerToken() = userRepository.getBearerToken()
    //---------------------------------------------------------------------------------------------- getBearerToken


    //---------------------------------------------------------------------------------------------- getModelForShowImageProfile
    fun getModelForShowImageProfile() =
        userRepository.getModelForShowImageProfile(userInfoEntity = userInfoEntity)
    //---------------------------------------------------------------------------------------------- getModelForShowImageProfile

}