package com.zarholding.zar.view.fragment.user.taxi.pro


import android.os.Bundle
import android.view.*
import androidx.activity.OnBackPressedCallback
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.skydoves.powerspinner.IconSpinnerAdapter
import com.skydoves.powerspinner.IconSpinnerItem
import com.zar.core.enums.EnumApiError
import com.zarholding.zar.R
import com.zarholding.zar.view.activity.MainActivity
import com.zarholding.zar.ZarFragment
import com.zarholding.zar.databinding.FragmentTaxiBinding
import com.zarholding.zar.ext.hideKeyboard
import com.zarholding.zar.model.data.database.entity.UserInfoEntity
import com.zarholding.zar.model.data.request.TaxiAddFavPlaceRequest
import com.zarholding.zar.model.data.response.ResponseResult
import com.zarholding.zar.model.data.response.dropdown.DropDownModel
import com.zarholding.zar.model.data.response.taxi.TaxiFavPlaceModel
import com.zarholding.zar.tools.CompanionValues
import com.zarholding.zar.view.adapter.recycler.PassengerAdapter
import com.zarholding.zar.view.fragment.user.taxi.pro.component.TaxiTripView
import com.zarholding.zar.view.dialog.ConfirmDialog
import com.zarholding.zar.view.dialog.FavPlaceDialog
import com.zarholding.zar.view.dialog.personnel.PersonnelDialog
import com.zarholding.zar.view.fragment.user.taxi.pro.component.TaxiMapView
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import org.osmdroid.util.GeoPoint

/**
 * Created by m-latifi on 10/22/2023.
 */

@AndroidEntryPoint
class TaxiReservationFragment(override var layout: Int = R.layout.fragment_taxi) :
    ZarFragment<FragmentTaxiBinding>() {


    private val viewModel: TaxiReservationViewModel by viewModels()


    //---------------------------------------------------------------------------------------------- locationPermissionLauncher
    private val locationPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions()
        ) { results ->
            binding.taxiMapView.checkLocationPermission(results = results)
        }
    //---------------------------------------------------------------------------------------------- locationPermissionLauncher


    //---------------------------------------------------------------------------------------------- OnBackPressedCallback
    private val backClick = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            if (binding.taxiTripReturnView.visibility == View.VISIBLE)
                showGoTripLayout()
            else if (binding.taxiTripGoView.visibility == View.VISIBLE)
                showInformationLayout()
            else {
                this.isEnabled = false
                activity?.onBackPressedDispatcher?.onBackPressed()
            }
        }
    }
    //---------------------------------------------------------------------------------------------- OnBackPressedCallback


    //---------------------------------------------------------------------------------------------- onViewCreated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }
    //---------------------------------------------------------------------------------------------- onViewCreated


    //---------------------------------------------------------------------------------------------- initView
    private fun initView() {
        activity?.onBackPressedDispatcher?.addCallback(viewLifecycleOwner, backClick)
        initTaxiMap()
        initTaxiTripGoView()
        initTaxiTripReturnView()
        showInformationLayout()
        setListener()
        observeLiveDate()
        requestGetSites()
    }
    //---------------------------------------------------------------------------------------------- initView


    //---------------------------------------------------------------------------------------------- initTaxiMap
    private fun initTaxiMap() {
        binding.taxiMapView.manager = childFragmentManager
        binding.taxiMapView.locationPermissionLauncher = locationPermissionLauncher
        binding.taxiMapView.listenr = object : TaxiMapView.TaxiMapInterface {
            override fun showAlert(message: String) {
                showMessage(message)
            }

            override fun onClickCenterMarker(
                center: GeoPoint,
                mapType: TaxiMapView.TaxiMapType,
                tripType: TaxiTripView.EnumTripType
            ) {
                viewModel.getAddress(
                    geoPoint = center,
                    mapType = mapType,
                    tripType = tripType,
                    taxiFavPlaceModel = null
                )
            }

            override fun onSelectFavLocation(
                fav: TaxiFavPlaceModel,
                mapType: TaxiMapView.TaxiMapType,
                tripType: TaxiTripView.EnumTripType
            ) {
                viewModel.getAddress(
                    geoPoint = GeoPoint(fav.lat, fav.long),
                    mapType = mapType,
                    tripType = tripType,
                    taxiFavPlaceModel = fav
                )
            }
        }
        viewModel.requestGetTaxiFavPlace()
    }
    //---------------------------------------------------------------------------------------------- initTaxiMap


    //---------------------------------------------------------------------------------------------- initTaxiTripGoView
    private fun initTaxiTripGoView() {
        binding.taxiTripGoView.listener = object : TaxiTripView.TaxiOriginInterface {
            override fun onShowTimeDialog() {
                (activity as MainActivity?)?.enableBlurView()
            }

            override fun onDismissTimeDialog() {
                (activity as MainActivity?)?.disableBlurView()
            }

            override fun onSelectDate(date: String) {
                viewModel.setGoDate(date = date)
            }

            override fun onSelectTime(time: String) {
                viewModel.setGoTime(time = time)
            }


            override fun onClickAddLocation(mapType: TaxiMapView.TaxiMapType) {
                when (mapType) {
                    TaxiMapView.TaxiMapType.Origin -> binding.taxiMapView.showCenterMarker(
                        mapType = TaxiMapView.TaxiMapType.Origin,
                        tripType = TaxiTripView.EnumTripType.Go
                    )

                    TaxiMapView.TaxiMapType.Destination -> binding.taxiMapView.showCenterMarker(
                        mapType = TaxiMapView.TaxiMapType.Destination,
                        tripType = TaxiTripView.EnumTripType.Go
                    )
                }
            }

            override fun onclickRemoveLocation(position: Int, mapType: TaxiMapView.TaxiMapType) {
                when (mapType) {
                    TaxiMapView.TaxiMapType.Origin -> viewModel.removeOrigin(
                        position = position,
                        tripType = TaxiTripView.EnumTripType.Go
                    )

                    TaxiMapView.TaxiMapType.Destination -> viewModel.removeDestination(
                        position = position,
                        tripType = TaxiTripView.EnumTripType.Go
                    )
                }
            }

            override fun onClickFavDelete(id: Int) {
                deleteFavPlace(id = id)
            }

            override fun onClickFacAdd(
                geoPoint: GeoPoint,
                address: String,
                mapType: TaxiMapView.TaxiMapType,
                position: Int
            ) {
                showAddToFavPlaceDialog(
                    geoPoint = geoPoint,
                    address = address,
                    tripType = TaxiTripView.EnumTripType.Go,
                    mapType = mapType,
                    position = position
                )
            }

            override fun onClickRequestTaxi() {
                binding.taxiTripGoView.buttonContinue.startLoading(getString(R.string.bePatient))
                viewModel.requestTaxiPro()
            }

            override fun onClickReturnAction() {
                if (viewModel.validationGoTrip())
                    showReturnTripLayout()
            }
        }
    }
    //---------------------------------------------------------------------------------------------- initTaxiTripGoView


    //---------------------------------------------------------------------------------------------- initTaxiTripReturnView
    private fun initTaxiTripReturnView() {
        binding.taxiTripReturnView.listener = object : TaxiTripView.TaxiOriginInterface {
            override fun onShowTimeDialog() {
                (activity as MainActivity?)?.enableBlurView()
            }

            override fun onDismissTimeDialog() {
                (activity as MainActivity?)?.disableBlurView()
            }

            override fun onSelectDate(date: String) {
                viewModel.setReturnDate(date = date)
            }

            override fun onSelectTime(time: String) {
                viewModel.setReturnTime(time = time)
            }

            override fun onClickAddLocation(mapType: TaxiMapView.TaxiMapType) {
                when (mapType) {
                    TaxiMapView.TaxiMapType.Origin -> binding.taxiMapView.showCenterMarker(
                        mapType = TaxiMapView.TaxiMapType.Origin,
                        tripType = TaxiTripView.EnumTripType.Return
                    )

                    TaxiMapView.TaxiMapType.Destination -> binding.taxiMapView.showCenterMarker(
                        mapType = TaxiMapView.TaxiMapType.Destination,
                        tripType = TaxiTripView.EnumTripType.Return
                    )
                }
            }

            override fun onclickRemoveLocation(position: Int, mapType: TaxiMapView.TaxiMapType) {
                when (mapType) {
                    TaxiMapView.TaxiMapType.Origin -> viewModel.removeOrigin(
                        position = position,
                        tripType = TaxiTripView.EnumTripType.Return
                    )

                    TaxiMapView.TaxiMapType.Destination -> viewModel.removeDestination(
                        position = position,
                        tripType = TaxiTripView.EnumTripType.Return
                    )
                }
            }

            override fun onClickFavDelete(id: Int) {
                deleteFavPlace(id = id)
            }


            override fun onClickFacAdd(
                geoPoint: GeoPoint,
                address: String,
                mapType: TaxiMapView.TaxiMapType,
                position: Int
            ) {
                showAddToFavPlaceDialog(
                    geoPoint = geoPoint,
                    address = address,
                    tripType = TaxiTripView.EnumTripType.Return,
                    mapType = mapType,
                    position = position
                )
            }

            override fun onClickRequestTaxi() {
                binding.taxiTripReturnView.buttonContinue.startLoading(getString(R.string.bePatient))
                viewModel.requestTaxiPro()
            }


            override fun onClickReturnAction() {
                showGoTripLayout()
            }
        }
    }
    //---------------------------------------------------------------------------------------------- initTaxiTripReturnView


    //---------------------------------------------------------------------------------------------- showAddToFavPlaceDialog
    private fun showAddToFavPlaceDialog(
        geoPoint: GeoPoint,
        address: String,
        tripType: TaxiTripView.EnumTripType,
        mapType: TaxiMapView.TaxiMapType,
        position: Int
    ) {
        context?.let {
            FavPlaceDialog(
                it,
                onAddPlace = { name ->
                    val request = TaxiAddFavPlaceRequest(
                        locationName = name,
                        locationAddress = address,
                        lat = geoPoint.latitude,
                        long = geoPoint.longitude
                    )
                    viewModel.requestAddFavPlace(
                        request = request,
                        tripType = tripType,
                        mapType = mapType,
                        position = position
                    )
                },
                onShowDialog = { (activity as MainActivity?)?.enableBlurView() },
                onDismissDialog = { (activity as MainActivity?)?.disableBlurView() }).show()
        }
    }
    //---------------------------------------------------------------------------------------------- showAddToFavPlaceDialog


    //---------------------------------------------------------------------------------------------- deleteFavPlace
    private fun deleteFavPlace(id: Int) {
        viewModel.requestDeleteFavPlace(id = id)
    }
    //---------------------------------------------------------------------------------------------- deleteFavPlace


    //---------------------------------------------------------------------------------------------- observeLiveDate
    private fun observeLiveDate() {
        viewModel.errorLiveDate.observe(viewLifecycleOwner) {
            binding.taxiTripGoView.buttonContinue.stopLoading()
            binding.taxiTripReturnView.buttonContinue.stopLoading()
            binding.taxiMapView.resetMapView()
            binding.taxiTripGoView.resetView()
            binding.taxiTripReturnView.resetView()
            showMessage(it.message, it.type)
            when (it.type) {
                EnumApiError.UnAuthorization -> (activity as MainActivity?)?.gotoFirstFragmentWithDeleteUser()
                else -> {}
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {

                launch {
                    viewModel.siteState.collect {
                        when(it) {
                            is ResponseResult.Loading -> {}
                            is ResponseResult.Success -> { initSitesSpinner(it.data) }
                        }
                    }
                }

                launch {
                    viewModel.favoriteState.collect {
                        when(it) {
                            is ResponseResult.Loading -> {}
                            is ResponseResult.Success -> {
                                if (it.data.isNotEmpty())
                                    binding.taxiMapView.setFavList(it.data)
                            }
                        }
                    }
                }

                launch {
                    viewModel.passengerState.collect {
                        when(it) {
                            is ResponseResult.Loading -> {}
                            is ResponseResult.Success -> { setPassengersAdapter(it.data) }
                        }
                    }
                }

                launch {
                    viewModel.sendRequestState.collect {
                        when(it){
                            is ResponseResult.Loading -> {}
                            is ResponseResult.Success -> {
                                binding.taxiTripGoView.buttonContinue.stopLoading()
                                binding.taxiTripReturnView.buttonContinue.stopLoading()
                                showMessage(it.data, EnumApiError.Done)
                                backClick.isEnabled = false
                                activity?.onBackPressedDispatcher?.onBackPressed()
                            }
                        }
                    }
                }

                launch {
                    viewModel.goDate.collect {
                        binding.taxiTripGoView.setDate(it)
                    }
                }

                launch {
                    viewModel.goTime.collect {
                        binding.taxiTripGoView.setTime(it)
                    }
                }

                launch {
                    viewModel.returnDate.collect {
                        binding.taxiTripReturnView.setDate(it)
                    }
                }

                launch {
                    viewModel.returnTime.collect {
                        binding.taxiTripReturnView.setTime(it)
                    }
                }
            }
        }


        viewModel.taxiGoAddressLiveDate.observe(viewLifecycleOwner) {
            binding.taxiTripGoView.successAddAddress(it)
            binding.taxiMapView.successChangeAddress(it)
        }

        viewModel.taxiReturnAddressLiveDate.observe(viewLifecycleOwner) {
            binding.taxiTripReturnView.successAddAddress(it)
            binding.taxiMapView.successChangeAddress(it)
        }

    }
    //---------------------------------------------------------------------------------------------- observeLiveDate


    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener() {
        binding.informationTaxi.buttonContinue.setOnClickListener { validationToGoTripLayout() }
        binding.informationTaxi.textViewMyRequest.setOnClickListener { gotoMyRequestHistory() }
    }
    //---------------------------------------------------------------------------------------------- setListener


    //---------------------------------------------------------------------------------------------- requestGetSites
    private fun requestGetSites() {
        binding.informationTaxi.powerSpinnerSite.hint = getString(R.string.bePatient)
        viewModel.requestGetSites()
    }
    //---------------------------------------------------------------------------------------------- requestGetSites


    //---------------------------------------------------------------------------------------------- initSitesSpinner
    private fun initSitesSpinner(sites: List<DropDownModel>) {
        if (sites.isNotEmpty() && context != null) {
            val items = sites.map { site ->
                IconSpinnerItem(site.text)
            }
            binding.informationTaxi.powerSpinnerSite.apply {
                hint = getString(R.string.chooseFromList)
                setSpinnerAdapter(IconSpinnerAdapter(this))
                setItems(items)
                getSpinnerRecyclerView().layoutManager =
                    LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                lifecycleOwner = viewLifecycleOwner

                setOnSpinnerOutsideTouchListener { _, _ -> dismiss() }

                setOnSpinnerItemSelectedListener<IconSpinnerItem> { _, _, newIndex, _ ->
                    viewModel.companySelected = sites[newIndex]
                    binding.informationTaxi.powerSpinnerSite
                        .setBackgroundResource(R.drawable.a_drawable_primary_curve)
                    binding.informationTaxi.textViewSite.text = sites[newIndex].text
                }
            }
        }

    }
    //---------------------------------------------------------------------------------------------- initSitesSpinner


    //---------------------------------------------------------------------------------------------- setPassengersAdapter
    private fun setPassengersAdapter(items: List<UserInfoEntity>) {
        if (context == null)
            return
        /*        val users: MutableList<UserInfoEntity> = mutableListOf()
                val user = viewModel.getUser()
                user?.let {
                    users.add(it)
                }*/
        val passengersAdapter = PassengerAdapter(
            users = items.toMutableList(),
            onClick = {
                showDialogDeletePassenger(it)
            },
            addClick = {
                showPersonnelDialog()
            }
        )
        val gridLayoutManager = GridLayoutManager(
            requireContext(),
            2,
            GridLayoutManager.VERTICAL,
            false
        )
        binding.informationTaxi.recyclerViewPassengers.layoutManager = gridLayoutManager
        binding.informationTaxi.recyclerViewPassengers.layoutDirection = View.LAYOUT_DIRECTION_RTL
        binding.informationTaxi.recyclerViewPassengers.adapter = passengersAdapter
    }
    //---------------------------------------------------------------------------------------------- setPassengersAdapter


    //---------------------------------------------------------------------------------------------- showDialogDeletePassenger
    private fun showDialogDeletePassenger(item: UserInfoEntity) {
        if (context == null)
            return
        ConfirmDialog(
            context = requireContext(),
            type = ConfirmDialog.ConfirmType.DELETE,
            title = getString(R.string.confirmForDelete, item.fullName),
            onYesClick = { viewModel.deletePassenger(item) },
            onShowDialog = { (activity as MainActivity?)?.enableBlurView() },
            onDismissDialog = { (activity as MainActivity?)?.disableBlurView() }
        ).show()
    }
    //---------------------------------------------------------------------------------------------- showDialogDeletePassenger


    //---------------------------------------------------------------------------------------------- showPersonnelDialog
    private fun showPersonnelDialog() {
        PersonnelDialog { viewModel.selectPerson(it) }
            .show(childFragmentManager, "personnel dialog")
    }
    //---------------------------------------------------------------------------------------------- showPersonnelDialog


    //---------------------------------------------------------------------------------------------- resetComponent
    private fun resetComponent() {
        hideKeyboard()
        binding.taxiMapView.resetMapView()
        binding.taxiTripGoView.resetView()
        binding.taxiTripReturnView.resetView()
    }
    //---------------------------------------------------------------------------------------------- resetComponent


    //---------------------------------------------------------------------------------------------- showInformationLayout
    private fun showInformationLayout() {
        resetComponent()
        binding.informationTaxi.root.visibility = View.VISIBLE
        binding.taxiMapView.visibility = View.GONE
        binding.taxiTripGoView.visibility = View.GONE
        binding.taxiTripReturnView.visibility = View.GONE
    }
    //---------------------------------------------------------------------------------------------- showInformationLayout


    //---------------------------------------------------------------------------------------------- validationToGoTripLayout
    private fun validationToGoTripLayout() {
        if (binding.informationTaxi.powerSpinnerSite.selectedIndex == -1) {
            showMessage(getString(R.string.pleaseSelectSite))
            return
        }
        if (binding.informationTaxi.editTextReason.text.isNullOrEmpty()) {
            showMessage(getString(R.string.pleaseEnterTheReason))
            return
        }
        viewModel.reason = binding.informationTaxi.editTextReason.text.toString()
        showGoTripLayout()
    }
    //---------------------------------------------------------------------------------------------- validationToGoTripLayout


    //---------------------------------------------------------------------------------------------- showGoTripLayout
    private fun showGoTripLayout() {
        resetComponent()
        binding.informationTaxi.root.visibility = View.GONE
        binding.taxiTripReturnView.visibility = View.GONE
        binding.taxiMapView.visibility = View.VISIBLE
        binding.taxiTripGoView.visibility = View.VISIBLE
        viewModel.taxiGoAddressLiveDate.value?.let {
            binding.taxiMapView.successChangeAddress(it)
        } ?: run { binding.taxiMapView.clearOverlays() }
    }
    //---------------------------------------------------------------------------------------------- showGoTripLayout


    //---------------------------------------------------------------------------------------------- showReturnTripLayout
    private fun showReturnTripLayout() {
        resetComponent()
        binding.informationTaxi.root.visibility = View.GONE
        binding.taxiTripGoView.visibility = View.GONE
        binding.taxiMapView.visibility = View.VISIBLE
        binding.taxiTripReturnView.visibility = View.VISIBLE
        viewModel.taxiReturnAddressLiveDate.value?.let {
            binding.taxiMapView.successChangeAddress(it)
        } ?: run { binding.taxiMapView.clearOverlays() }
    }
    //---------------------------------------------------------------------------------------------- showReturnTripLayout


    //---------------------------------------------------------------------------------------------- gotoMyRequestHistory
    private fun gotoMyRequestHistory() {
        val bundle = Bundle()
        bundle.putBoolean(CompanionValues.myRequest, true)
        gotoFragment(R.id.action_goto_AdminTaxiFragment, bundle)
    }
    //---------------------------------------------------------------------------------------------- gotoMyRequestHistory

}