package com.zarholding.zar.view.adapter.recycler.taxi

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemAdminTaxiAddressBinding
import com.zarholding.zar.model.data.enum.EnumPersonnelType
import com.zarholding.zar.model.data.response.taxi.TaxiRequestPointModel
import com.zarholding.zar.view.adapter.holder.taxi.AdminTaxiAddressHolder

/**
 * Created by m-latifi on 10/24/2023.
 */

class AdminTaxiAddressAdapter(
    private val items: List<TaxiRequestPointModel>,
    private val personnelType: EnumPersonnelType,
    private val onMapClick: (TaxiRequestPointModel) -> Unit
) : RecyclerView.Adapter<AdminTaxiAddressHolder>() {

    private var layoutInflater: LayoutInflater? = null

    //---------------------------------------------------------------------------------------------- onCreateViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdminTaxiAddressHolder {
        if (layoutInflater == null)
            layoutInflater = LayoutInflater.from(parent.context)
        return AdminTaxiAddressHolder(
            binding = ItemAdminTaxiAddressBinding.inflate(layoutInflater!!, parent, false),
            personnelType = personnelType,
            onMapClick = onMapClick
        )
    }
    //---------------------------------------------------------------------------------------------- onCreateViewHolder


    //---------------------------------------------------------------------------------------------- onBindViewHolder
    override fun onBindViewHolder(holder: AdminTaxiAddressHolder, position: Int) {
        holder.bind(items[position])
    }
    //---------------------------------------------------------------------------------------------- onBindViewHolder


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun getItemCount() = items.size
    //---------------------------------------------------------------------------------------------- getItemCount

}