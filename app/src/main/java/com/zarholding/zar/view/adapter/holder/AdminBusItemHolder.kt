package com.zarholding.zar.view.adapter.holder

import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.R
import com.zarholding.zar.databinding.ItemAdminBusBinding
import com.zarholding.zar.ext.setDriverAndStation
import com.zarholding.zar.ext.setStartEndStation
import com.zarholding.zar.ext.setTitleAndValue
import com.zarholding.zar.model.data.response.trip.TripRequestRegisterModel

/**
 * Created by m-latifi on 11/17/2022.
 */

class AdminBusItemHolder(
    private val binding: ItemAdminBusBinding,
    private val onClick: () -> Unit
) :
    RecyclerView.ViewHolder(binding.root) {


    //---------------------------------------------------------------------------------------------- bind
    fun bind(item: TripRequestRegisterModel) {
        setValueToXml(item)
        binding.checkboxChoose.setOnClickListener {
            item.choose = binding.checkboxChoose.isChecked
            onClick()
        }
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind



    //---------------------------------------------------------------------------------------------- setValueToXml
    private fun setValueToXml(item: TripRequestRegisterModel) {
        binding.checkboxChoose.isChecked = item.choose
        binding.textViewUserName.setTitleAndValue(
            title = binding.textViewUserName.context.getString(R.string.requester),
            splitter = binding.textViewUserName.context.getString(R.string.colon),
            value = item.userName
        )
        binding.textviewBus.setDriverAndStation(
            driverName = item.driverName,
            tripName = item.commuteTripName,
            stationName = item.stationName
        )
        binding.textViewDirection.setStartEndStation(
            originName = item.originName,
            destinationName = item.destinationName
        )
    }
    //---------------------------------------------------------------------------------------------- setValueToXml

}