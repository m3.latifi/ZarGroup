package com.zarholding.zar.view.adapter.holder.insurance

import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemInsuranceRelationshipBinding
import com.zarholding.zar.model.data.request.insurance.InsuranceRelationShipModel

/**
 * Created by m-latifi on 10/16/2023.
 */

class InsuranceRelationShipHolder(
    private val binding: ItemInsuranceRelationshipBinding,
    private val onDelete: (InsuranceRelationShipModel) -> Unit
): RecyclerView.ViewHolder(binding.root) {

    //---------------------------------------------------------------------------------------------- bind
    fun bind(item: InsuranceRelationShipModel) {
        val title = "${item.firstName} ${item.lastName}"
        binding.textViewTitle.text = title
        binding.imageViewDelete.setOnClickListener { onDelete.invoke(item) }
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind

}