package com.zarholding.zar.view.adapter.recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemPersonnelSelectBinding
import com.zarholding.zar.model.data.database.entity.UserInfoEntity
import com.zarholding.zar.view.adapter.holder.PersonnelSelectHolder

/**
 * Created by m-latifi on 11/22/2022.
 */

class PersonnelSelectAdapter(
    private val items: MutableList<UserInfoEntity>,
    private val token: String,
    private val onClick: (UserInfoEntity) -> Unit,

) : RecyclerView.Adapter<PersonnelSelectHolder>() {

    private var layoutInflater: LayoutInflater? = null


    //---------------------------------------------------------------------------------------------- onCreateViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonnelSelectHolder {
        if (layoutInflater == null)
            layoutInflater = LayoutInflater.from(parent.context)
        return PersonnelSelectHolder(
            binding = ItemPersonnelSelectBinding.inflate(layoutInflater!!, parent, false),
            onClick = onClick
        )
    }
    //---------------------------------------------------------------------------------------------- onCreateViewHolder


    //---------------------------------------------------------------------------------------------- onBindViewHolder
    override fun onBindViewHolder(holder: PersonnelSelectHolder, position: Int) {
        holder.bind(item = items[position], token = token)
    }
    //---------------------------------------------------------------------------------------------- onBindViewHolder


    //---------------------------------------------------------------------------------------------- onBindViewHolder
    override fun getItemCount() = items.size
    //---------------------------------------------------------------------------------------------- onBindViewHolder


    //---------------------------------------------------------------------------------------------- addPerson
    fun addPerson(list: List<UserInfoEntity>) {
        val oldSize = items.size
        items.addAll(list)
        notifyItemRangeChanged(oldSize-1, items.size)
    }
    //---------------------------------------------------------------------------------------------- addPerson

}