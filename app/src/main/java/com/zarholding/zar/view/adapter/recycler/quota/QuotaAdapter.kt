package com.zarholding.zar.view.adapter.recycler.quota

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemPersonnelQuotaBinding
import com.zarholding.zar.model.data.response.quota.QuotaModel
import com.zarholding.zar.view.adapter.holder.quota.QuotaHolder

/**
 * Created by m-latifi on 7/26/2023.
 */

class QuotaAdapter(
    private val items: List<QuotaModel>,
    private val userId: Int,
    private val onClick: (QuotaModel) -> Unit,
    private val onAccessUserClick: (Long, TextView) -> Unit
) : RecyclerView.Adapter<QuotaHolder>() {

    private var inflater: LayoutInflater? = null

    //---------------------------------------------------------------------------------------------- onCreateViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuotaHolder {
        if (inflater == null)
            inflater = LayoutInflater.from(parent.context)
        return QuotaHolder(
            binding = ItemPersonnelQuotaBinding.inflate(inflater!!, parent, false),
            onClick = onClick,
            onAccessUserClick = onAccessUserClick
        )
    }
    //---------------------------------------------------------------------------------------------- onCreateViewHolder


    //---------------------------------------------------------------------------------------------- onBindViewHolder
    override fun onBindViewHolder(holder: QuotaHolder, position: Int) {
        holder.bind(items[position], userId)
    }
    //---------------------------------------------------------------------------------------------- onBindViewHolder


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun getItemCount() = items.size
    //---------------------------------------------------------------------------------------------- getItemCount

}