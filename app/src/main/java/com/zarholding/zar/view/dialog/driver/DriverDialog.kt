package com.zarholding.zar.view.dialog.driver

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.skydoves.powerspinner.IconSpinnerAdapter
import com.skydoves.powerspinner.IconSpinnerItem
import com.zar.core.enums.EnumApiError
import com.zarholding.zar.R
import com.zarholding.zar.databinding.DialogDriverBinding
import com.zarholding.zar.model.data.enum.EnumDriverType
import com.zarholding.zar.model.data.response.taxi.AdminTaxiRequestModel
import com.zarholding.zar.view.activity.MainActivity
import dagger.hilt.android.AndroidEntryPoint

/**
 * Created by m-latifi on 20/8/2022.
 */

@AndroidEntryPoint
class DriverDialog(
    private val item: AdminTaxiRequestModel,
    private val onConfirmClick: () -> Unit
) : DialogFragment() {

    lateinit var binding: DialogDriverBinding

    private val driverViewModel : DriverViewModel by viewModels()


    //---------------------------------------------------------------------------------------------- onCreateView
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DialogDriverBinding.inflate(inflater, container, false)
        return binding.root
    }
    //---------------------------------------------------------------------------------------------- onCreateView



    //---------------------------------------------------------------------------------------------- onCreateView
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity?)?.enableBlurView()
        val lp = WindowManager.LayoutParams()
        val window = dialog!!.window
        val back = ColorDrawable(Color.TRANSPARENT)
        val inset = InsetDrawable(back, 50)
        window!!.setBackgroundDrawable(inset)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        window.attributes = lp
        isCancelable = false
        initView()
        setListener()
        val title = getString(R.string.taxiRequestIsConfirm, item.requesterName)
        binding.textViewTitle.text = title
    }
    //---------------------------------------------------------------------------------------------- onCreateView



    //---------------------------------------------------------------------------------------------- initView
    private fun initView() {
        observeErrorLiveDate()
        binding.powerSpinnerDriver.visibility = View.GONE
        binding.textViewDriver.visibility = View.GONE
        initPowerSpinnerTaxiType()
        observeDriversListLiveData()
        observeAssignDriverLiveData()
    }
    //---------------------------------------------------------------------------------------------- initView


    //---------------------------------------------------------------------------------------------- showMessage
    private fun showMessage(message: String, type: EnumApiError = EnumApiError.Warning, duration: Int = 3) {
        activity?.let {
            (it as MainActivity).showMessage(
                message = message,
                type = type,
                duration = duration)
        }
    }
    //---------------------------------------------------------------------------------------------- showMessage



    //---------------------------------------------------------------------------------------------- observeLoginLiveDate
    private fun observeErrorLiveDate() {
        driverViewModel.errorLiveDate.observe(viewLifecycleOwner) {
            showMessage(message = it.message, type = it.type)
            when (it.type) {
                EnumApiError.UnAuthorization -> (activity as MainActivity?)?.gotoFirstFragmentWithDeleteUser()
                else -> {}
            }
        }
    }
    //---------------------------------------------------------------------------------------------- observeLoginLiveDate



    //---------------------------------------------------------------------------------------------- observeDriversListLiveData
    private fun observeDriversListLiveData() {
        driverViewModel.driversListLiveData.observe(viewLifecycleOwner) {
            initPowerSpinnerDriver()
        }
    }
    //---------------------------------------------------------------------------------------------- observeDriversListLiveData


    //---------------------------------------------------------------------------------------------- observeAssignDriverLiveData
    private fun observeAssignDriverLiveData() {
        driverViewModel.assignDriverLiveData.observe(viewLifecycleOwner) {
            onConfirmClick()
            dismiss()
        }
    }
    //---------------------------------------------------------------------------------------------- observeAssignDriverLiveData



    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener() {

        binding.imageViewClose.setOnClickListener { dismiss() }

        binding.buttonNo.setOnClickListener { dismiss() }

        binding.buttonYes.setOnClickListener { requestAssignDriverToRequest() }

    }
    //---------------------------------------------------------------------------------------------- setListener



    //---------------------------------------------------------------------------------------------- initPowerSpinnerTaxiType
    private fun initPowerSpinnerTaxiType() {
        val items = listOf(
            IconSpinnerItem(getString(R.string.companyDriver)),
            IconSpinnerItem(getString(R.string.agencyDriver)))

        binding.powerSpinnerType.apply {
            setSpinnerAdapter(IconSpinnerAdapter(this))
            setItems(items)
            getSpinnerRecyclerView().layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            lifecycleOwner = viewLifecycleOwner
            setOnSpinnerOutsideTouchListener { _, _ ->  dismiss()}
            setOnSpinnerItemSelectedListener<IconSpinnerItem> { _, _, newIndex, _ ->
                binding.powerSpinnerType.setBackgroundResource(R.drawable.a_drawable_primary_curve)
                val type = if (newIndex == 0)
                    EnumDriverType.Personnel
                else
                    EnumDriverType.Agency
                getDriver(type)
            }
        }
    }
    //---------------------------------------------------------------------------------------------- initPowerSpinnerTaxiType



    //---------------------------------------------------------------------------------------------- initPowerSpinnerDriver
    private fun initPowerSpinnerDriver() {
        driverViewModel.getDriverList()?.let {
            val items = it.map { driver ->
                driver.fullName?.let { name ->
                    IconSpinnerItem(name)
                }
            }
            binding.powerSpinnerDriver.apply {
                hint = getString(R.string.chooseFromList)
                setSpinnerAdapter(IconSpinnerAdapter(this))
                setItems(items)
                getSpinnerRecyclerView().layoutManager =
                    LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                lifecycleOwner = viewLifecycleOwner
                setOnSpinnerOutsideTouchListener { _, _ ->  dismiss()}
                setOnSpinnerItemSelectedListener<IconSpinnerItem> { _, _, newIndex, _ ->
                    driverViewModel.selectDriverByIndex(newIndex)
                    binding.powerSpinnerDriver
                        .setBackgroundResource(R.drawable.a_drawable_primary_curve)
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- initPowerSpinnerDriver



    //---------------------------------------------------------------------------------------------- getDriver
    private fun getDriver(type : EnumDriverType) {
        binding.powerSpinnerDriver.visibility = View.VISIBLE
        binding.textViewDriver.visibility = View.VISIBLE
        binding.powerSpinnerDriver.hint = getString(R.string.bePatient)
        driverViewModel.requestGetDriver(type, item.fromCompany)
    }
    //---------------------------------------------------------------------------------------------- getDriver



    //---------------------------------------------------------------------------------------------- requestAssignDriverToRequest
    private fun requestAssignDriverToRequest() {
        if (driverViewModel.selectedDriver == null) {
            showMessage(getString(R.string.pleaseSelectDriver))
            return
        }
        binding.buttonYes.startLoading(getString(R.string.bePatient))
        driverViewModel.requestAssignDriverToRequest(item.id.toString())
    }
    //---------------------------------------------------------------------------------------------- requestAssignDriverToRequest



    //---------------------------------------------------------------------------------------------- dismiss
    override fun dismiss() {
        super.dismiss()
        (activity as MainActivity?)?.disableBlurView()
    }
    //---------------------------------------------------------------------------------------------- dismiss
}