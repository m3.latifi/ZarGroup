package com.zarholding.zar.view.adapter.holder.notification

import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemNotificationCategoryBinding
import com.zarholding.zar.ext.setFullDateTime
import com.zarholding.zar.model.data.other.notification.NotificationCategoryModel
import com.zarholding.zar.view.adapter.recycler.NotificationAdapter

/**
 * Created by m-latifi on 11/17/2022.
 */

class NotificationCategoryHolder(
    private val binding: ItemNotificationCategoryBinding,
    private val onClick: (categoryPosition: Int, notificationPosition: Int) -> Unit
) :
    RecyclerView.ViewHolder(binding.root) {


    //---------------------------------------------------------------------------------------------- bind
    fun bind(item: NotificationCategoryModel, categoryPosition : Int) {
        setValueToXml(item)
        val adapter = NotificationAdapter(item.notifications, categoryPosition, onClick)
        binding.recyclerViewNotification.adapter = adapter
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind


    //---------------------------------------------------------------------------------------------- setValueToXml
    private fun setValueToXml(item: NotificationCategoryModel) {
        binding.categoryName.text = item.name
        binding.categoryDate.setFullDateTime(item.date)
    }
    //---------------------------------------------------------------------------------------------- setValueToXml

}