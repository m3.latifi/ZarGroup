package com.zarholding.zar.view.fragment.user.education.education

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.zar.core.enums.EnumApiError
import com.zarholding.zar.R
import com.zarholding.zar.ZarFragment
import com.zarholding.zar.databinding.FragmentEducationBinding
import com.zarholding.zar.ext.config
import com.zarholding.zar.ext.startLoading
import com.zarholding.zar.ext.stopLoading
import com.zarholding.zar.model.data.response.education.EducationModel
import com.zarholding.zar.tools.CompanionValues
import com.zarholding.zar.tools.manager.getShimmerBuild
import com.zarholding.zar.view.activity.MainActivity
import com.zarholding.zar.view.adapter.recycler.education.EducationAdapter
import com.zarholding.zar.view.custom.EndlessScrollListener
import com.zarholding.zar.view.custom.getEndlessScrollListener
import dagger.hilt.android.AndroidEntryPoint

/**
 * Created by m-latifi on 7/26/2023.
 */

@AndroidEntryPoint
class EducationFragment(
    override var layout: Int = R.layout.fragment_education
) : ZarFragment<FragmentEducationBinding>() {

    private val viewModel: EducationViewModel by viewModels()
    private var title = ""
    private var categoryId = 0L
    private var adapter: EducationAdapter? = null
    private var endlessScrollListener: EndlessScrollListener? = null

    //---------------------------------------------------------------------------------------------- onViewCreated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }
    //---------------------------------------------------------------------------------------------- onViewCreated


    //---------------------------------------------------------------------------------------------- initView
    private fun initView() {
        binding.shimmerViewContainer.config(getShimmerBuild())
        getValuesFromArgument()
        observeLiveDate()
    }
    //---------------------------------------------------------------------------------------------- initView


    //---------------------------------------------------------------------------------------------- observeLiveDate
    private fun observeLiveDate() {
        viewModel.errorLiveDate.observe(viewLifecycleOwner) {
            binding.shimmerViewContainer.stopLoading()
            showMessage(it.message, it.type)
            when (it.type) {
                EnumApiError.UnAuthorization -> (activity as MainActivity?)?.gotoFirstFragmentWithDeleteUser()
                else -> {}
            }
        }


        viewModel.educationLiveData.observe(viewLifecycleOwner) {
            binding.shimmerViewContainer.stopLoading()
            setAdapter(it)
        }

    }
    //---------------------------------------------------------------------------------------------- observeLiveDate


    //---------------------------------------------------------------------------------------------- observeLiveDate
    private fun getValuesFromArgument(){
        arguments?.let {bundle ->
            title = bundle.getString(CompanionValues.EDUCATION_CAT_NAME, "")
            categoryId = bundle.getLong(CompanionValues.EDUCATION_CAT_ID, 0)
        }

        binding.textViewTitle.text = title
        getEducation()
    }
    //---------------------------------------------------------------------------------------------- observeLiveDate


    //---------------------------------------------------------------------------------------------- getEducation
    private fun getEducation() {
        binding.shimmerViewContainer.startLoading()
        viewModel.getVideoList(categoryId)
    }
    //---------------------------------------------------------------------------------------------- getEducation


    //---------------------------------------------------------------------------------------------- setAdapter
    private fun setAdapter(items: List<EducationModel>?) {
        if (items.isNullOrEmpty() || context == null)
            return

        adapter?.let {
            adapter?.addEducation(items)
            endlessScrollListener?.let {
                it.setLoading(false)
                if (items.isEmpty())
                    binding.recyclerEducation.removeOnScrollListener(it)
            }
        } ?: run {
            adapter = EducationAdapter(items.toMutableList()) { item ->
                educationDetail(item)
            }
            val manager = LinearLayoutManager(
                requireContext(), LinearLayoutManager.VERTICAL, false
            )
            endlessScrollListener = getEndlessScrollListener(manager) {
                binding.shimmerViewContainer.startLoading()
                viewModel.gelMoreVideoList(categoryId)
            }
            binding.recyclerEducation.layoutManager = manager
            binding.recyclerEducation.adapter = adapter
            if (endlessScrollListener != null)
                binding.recyclerEducation.addOnScrollListener(endlessScrollListener!!)
        }
    }
    //---------------------------------------------------------------------------------------------- setAdapter


    //---------------------------------------------------------------------------------------------- educationDetail
    private fun educationDetail(item: EducationModel) {
        val bundle = Bundle()
        bundle.putString(CompanionValues.EDUCATION_CAT_NAME, item.description)
        bundle.putLong(CompanionValues.EDUCATION_CAT_ID, item.id)
        bundle.putInt(CompanionValues.EDUCATION_TIME, item.sessionsMin)
        bundle.putInt(CompanionValues.EDUCATION_COUNT, item.sessionCount)
        gotoFragment(R.id.action_goto_educationDetailFragment, bundle)
    }
    //---------------------------------------------------------------------------------------------- educationDetail


    //---------------------------------------------------------------------------------------------- onDestroyView
    override fun onDestroyView() {
        super.onDestroyView()
        adapter = null
    }
    //---------------------------------------------------------------------------------------------- onDestroyView
}