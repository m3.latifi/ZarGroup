package com.zarholding.zar.view.adapter.recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemNotificationBinding
import com.zarholding.zar.model.data.response.notification.NotificationModel
import com.zarholding.zar.view.adapter.holder.notification.NotificationItemHolder

/**
 * Created by m-latifi on 11/16/2022.
 */

class NotificationAdapter(
    private var notificationModels: List<NotificationModel>,
    private var categoryPosition: Int,
    private val onClick: (categoryPosition: Int, notificationPosition: Int) -> Unit
) :
    RecyclerView.Adapter<NotificationItemHolder>() {

    private var layoutInflater: LayoutInflater? = null


    //---------------------------------------------------------------------------------------------- onCreateViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationItemHolder {

        if (layoutInflater == null)
            layoutInflater = LayoutInflater.from(parent.context)

        return NotificationItemHolder(
            binding = ItemNotificationBinding.inflate(layoutInflater!!, parent, false),
            categoryPosition = categoryPosition,
            onClick = onClick
        )
    }
    //---------------------------------------------------------------------------------------------- onCreateViewHolder


    //---------------------------------------------------------------------------------------------- onBindViewHolder
    override fun onBindViewHolder(holder: NotificationItemHolder, position: Int) {
        holder.bind(notificationModels[position], position)
    }
    //---------------------------------------------------------------------------------------------- onBindViewHolder


    //---------------------------------------------------------------------------------------------- onBindViewHolder
    override fun getItemCount() = notificationModels.size
    //---------------------------------------------------------------------------------------------- onBindViewHolder

}