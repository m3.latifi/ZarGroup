package com.zarholding.zar.view.adapter.holder.survey

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.model.data.response.survey.SurveyQuestionModel

/**
 * Created by zar on 2/15/2023.
 */

abstract class SurveyQuestionHolder(val view: View) :
    RecyclerView.ViewHolder(view) {

    abstract fun bind(item: SurveyQuestionModel, position: Int)

}