package com.zarholding.zar.view.adapter.holder.survey

import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.R
import com.zarholding.zar.databinding.ItemSurveyBinding
import com.zarholding.zar.ext.setTitleAndValue
import com.zarholding.zar.model.data.response.survey.SurveyModel

/**
 * Created by m-latifi on ۰۸/۰۲/۲۰۲۳.
 */
class SurveyHolder(
    private val binding : ItemSurveyBinding,
    private val onClick: (Int) -> Unit
): RecyclerView.ViewHolder(binding.root) {


    //---------------------------------------------------------------------------------------------- bind
    fun bind(item : SurveyModel, position: Int) {
        setValueToXml(item)
        setListener(position)
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind


    //---------------------------------------------------------------------------------------------- setValueToXml
    private fun setValueToXml(item : SurveyModel) {
        binding.textViewTitle.text = item.title
        binding.textViewPublisher.setTitleAndValue(
            title = binding.textViewPublisher.context.getString(R.string.publisherBy),
            splitter = binding.textViewPublisher.context.getString(R.string.colon),
            value = item.publishByUnit
        )
        binding.textViewPublishDate.setTitleAndValue(
            title = binding.textViewPublishDate.context.getString(R.string.startDate),
            splitter = binding.textViewPublishDate.context.getString(R.string.colon),
            value = item.startDate
        )
        binding.textViewExpireDate.setTitleAndValue(
            title = binding.textViewExpireDate.context.getString(R.string.endDate),
            splitter = binding.textViewExpireDate.context.getString(R.string.colon),
            value = item.endDate
        )
    }
    //---------------------------------------------------------------------------------------------- setValueToXml


    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener(position: Int) {
        binding.materialButtonStart.setOnClickListener {
            binding.materialButtonStart.startLoading(
                binding.materialButtonStart.context.getString(R.string.bePatient)
            )
            onClick(position)
        }
    }
    //---------------------------------------------------------------------------------------------- setListener

}