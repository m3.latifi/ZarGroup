package com.zarholding.zar.view.adapter.holder

import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemAddressSuggestionBinding
import com.zarholding.zar.ext.getAddress
import com.zarholding.zar.model.data.response.address.AddressModel
import com.zarholding.zar.model.data.response.address.AddressSuggestionModel

/**
 * Created by m-latifi on 11/14/2022.
 */

class AddressSuggestionItemHolder(
    private val binding: ItemAddressSuggestionBinding,
    private val onClick: (AddressSuggestionModel) -> Unit
) : RecyclerView.ViewHolder(binding.root) {


    //---------------------------------------------------------------------------------------------- bind
    fun bind(item: AddressSuggestionModel) {
        setValueToXml(item.address)
        setListener(item)
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind


    //---------------------------------------------------------------------------------------------- setValueToXml
    private fun setValueToXml(address: AddressModel) {
        binding.textViewAddress.text = address.getAddress()
    }
    //---------------------------------------------------------------------------------------------- setValueToXml


    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener(item: AddressSuggestionModel) {
        binding.root.setOnClickListener { onClick(item) }
    }
    //---------------------------------------------------------------------------------------------- setListener
}