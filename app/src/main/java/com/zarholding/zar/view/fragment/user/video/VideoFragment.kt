package com.zarholding.zar.view.fragment.user.video

import android.net.Uri
import android.os.Bundle
import android.view.View
import com.zarholding.zar.R
import com.zarholding.zar.ZarFragment
import com.zarholding.zar.databinding.FragmentVideoBinding
import com.zarholding.zar.di.Providers
import com.zarholding.zar.tools.CompanionValues


/**
 * Created by m-latifi on 8/25/2023.
 */

class VideoFragment(
    override var layout: Int = R.layout.fragment_video
): ZarFragment<FragmentVideoBinding>() {

    //---------------------------------------------------------------------------------------------- onViewCreated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }
    //---------------------------------------------------------------------------------------------- onViewCreated



    //---------------------------------------------------------------------------------------------- initView
    private fun initView() {
        arguments?.let {
            val file = it.getString(CompanionValues.EDUCATION_FILE_NAME, "")
            if (file.isNotEmpty()) {
                val url = "${Providers.url}$file"
                binding.universalVideoViewMovie.setMediaController(binding.universalMediaController)
                binding.universalVideoViewMovie.setVideoURI(Uri.parse(url))
                binding.universalVideoViewMovie.start()

/*                binding.simpleVideoView.setVideoURI(Uri.parse(url))
                val mediaControls = MediaController(requireContext())
                mediaControls.setAnchorView(binding.simpleVideoView)
                binding.simpleVideoView.setMediaController(mediaControls)
                binding.simpleVideoView.start()*/
/*                val extraHeaders = HashMap<String, String>()
                extraHeaders["foo"] = "bar"
                binding.andExoPlayerView.setSource(url, extraHeaders)*/
            }
        }
    }
    //---------------------------------------------------------------------------------------------- initView

}