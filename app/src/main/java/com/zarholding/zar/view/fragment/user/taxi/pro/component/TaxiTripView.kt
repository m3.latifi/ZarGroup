package com.zarholding.zar.view.fragment.user.taxi.pro.component

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.zar.core.view.picker.time.ZarTimePicker
import com.zarholding.zar.R
import com.zarholding.zar.ext.getAddress
import com.zarholding.zar.model.data.taxi.TaxiGoAddressModel
import com.zarholding.zar.view.adapter.recycler.taxi.TaxiAddressAdapter
import com.zarholding.zar.view.custom.ZarButton
import com.zarholding.zar.view.dialog.DateDialog
import com.zarholding.zar.view.dialog.TimeDialog
import org.osmdroid.util.GeoPoint

/**
 * Created by m-latifi on 10/22/2023.
 */

class TaxiTripView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {


    private var textViewTitle: TextView
    private var buttonDate: MaterialButton
    private var buttonTime: MaterialButton
    private var buttonAddOrigin: MaterialButton
    private var buttonAddDestination: MaterialButton
    private var buttonReturnAction: MaterialButton
    private var recyclerViewOrigin: RecyclerView
    private var recyclerViewDestination: RecyclerView
    private var tripType: EnumTripType = EnumTripType.Go
    var buttonContinue: ZarButton
    var listener: TaxiOriginInterface? = null

    interface TaxiOriginInterface {
        fun onShowTimeDialog()
        fun onDismissTimeDialog()
        fun onSelectDate(date: String)
        fun onSelectTime(time: String)
        fun onClickAddLocation(mapType: TaxiMapView.TaxiMapType)
        fun onclickRemoveLocation(position: Int, mapType: TaxiMapView.TaxiMapType)
        fun onClickFavDelete(id: Int)
        fun onClickFacAdd(
            geoPoint: GeoPoint,
            address: String,
            mapType: TaxiMapView.TaxiMapType,
            position: Int
        )
        fun onClickRequestTaxi()
        fun onClickReturnAction()
    }

    //---------------------------------------------------------------------------------------------- EnumTripType
    enum class EnumTripType {
        Go,
        Return;

        companion object {
            fun fromInt(value: Int) =
                if (value == 0)
                    Go
                else
                    Return
        }
    }
    //---------------------------------------------------------------------------------------------- EnumTripType


    //---------------------------------------------------------------------------------------------- init
    init {
        View.inflate(context, R.layout.layout_taxi_trip, this)
        val ta = context.obtainStyledAttributes(attrs, R.styleable.TaxiTripView)
        tripType = EnumTripType.fromInt(ta.getInt(R.styleable.TaxiTripView_taxiTripType, 0))

        textViewTitle = findViewById(R.id.textViewTitle)
        buttonDate = findViewById(R.id.buttonDate)
        buttonTime = findViewById(R.id.buttonTime)
        buttonAddOrigin = findViewById(R.id.buttonAddOrigin)
        buttonAddDestination = findViewById(R.id.buttonAddDestination)
        buttonReturnAction = findViewById(R.id.buttonReturnAction)
        buttonContinue = findViewById(R.id.buttonContinue)
        recyclerViewOrigin = findViewById(R.id.recyclerViewOrigin)
        recyclerViewDestination = findViewById(R.id.recyclerViewDestination)
        when (tripType) {
            EnumTripType.Go -> {
                textViewTitle.text = context.getString(R.string.enterDepartureInformation)
                buttonReturnAction.text = context.getString(R.string.addReturnInformation)
            }

            EnumTripType.Return -> {
                textViewTitle.text = context.getString(R.string.enterReturnInformation)
                buttonReturnAction.text = context.getString(R.string.goInformation)
            }
        }
        ta.recycle()
        setListener()
    }
    //---------------------------------------------------------------------------------------------- init


    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener() {

        buttonDate.setOnClickListener {
            if (context != null)
                DateDialog(
                    context = context,
                    resources = resources
                ) {
                    listener?.onSelectDate(date = it)
                }
        }

        buttonTime.setOnClickListener {
            if (context != null)
                TimeDialog(
                    context = context,
                    pickerMode = ZarTimePicker.PickerMode.TIME,
                    onChooseTime = { timeDeparture, _ ->
                        listener?.onSelectTime(time = timeDeparture)
                    },
                    onShowDialog = { listener?.onShowTimeDialog() },
                    onDismissDialog = { listener?.onDismissTimeDialog() }).show()
        }

        buttonAddOrigin.setOnClickListener {
            resetView()
            if (listener != null) {
                listener?.onClickAddLocation(TaxiMapView.TaxiMapType.Origin)
                buttonAddOrigin.visibility = View.INVISIBLE
            }
        }

        buttonAddDestination.setOnClickListener {
            resetView()
            if (listener != null) {
                listener?.onClickAddLocation(TaxiMapView.TaxiMapType.Destination)
                buttonAddDestination.visibility = View.INVISIBLE
            }
        }

        buttonReturnAction.setOnClickListener {
            listener?.onClickReturnAction()
        }

        buttonContinue.setOnClickListener {
            listener?.onClickRequestTaxi()
        }
    }
    //---------------------------------------------------------------------------------------------- setListener


    //---------------------------------------------------------------------------------------------- setDate
    fun setDate(date: String) {
        buttonDate.text = date
    }
    //---------------------------------------------------------------------------------------------- setDate


    //---------------------------------------------------------------------------------------------- setTime
    fun setTime(time: String) {
        buttonTime.text = time
    }
    //---------------------------------------------------------------------------------------------- setTime


    //---------------------------------------------------------------------------------------------- resetView
    fun resetView() {
        buttonAddOrigin.visibility = View.VISIBLE
        buttonAddDestination.visibility = View.VISIBLE
    }
    //---------------------------------------------------------------------------------------------- resetView


    //---------------------------------------------------------------------------------------------- successAddAddress
    fun successAddAddress(taxiGoAddressModel: TaxiGoAddressModel) {
        resetView()
        setOriginAdapter(taxiGoAddressModel)
        setDestinationAdapter(taxiGoAddressModel)
    }
    //---------------------------------------------------------------------------------------------- successAddAddress


    //---------------------------------------------------------------------------------------------- setOriginAdapter
    private fun setOriginAdapter(taxiGoAddressModel: TaxiGoAddressModel) {
        if (context == null)
            return
        val origins = taxiGoAddressModel.origins.map {
            it.address
        }
        val adapter = TaxiAddressAdapter(
            items = origins,
            onDelete = {
                listener?.onclickRemoveLocation(
                    position = it,
                    mapType = TaxiMapView.TaxiMapType.Origin
                )
            },
            onFavDeleteClick = { id -> id.let { listener?.onClickFavDelete(id = it) } },
            onFavAddClick = { addressModel, position ->
                listener?.onClickFacAdd(
                    geoPoint = taxiGoAddressModel.origins[position].geoPoint,
                    address = addressModel.getAddress(),
                    mapType = TaxiMapView.TaxiMapType.Origin,
                    position = position
                )
            }
        )
        val manager = LinearLayoutManager(
            context,
            LinearLayoutManager.VERTICAL,
            false
        )
        recyclerViewOrigin.adapter = adapter
        recyclerViewOrigin.layoutManager = manager
    }
    //---------------------------------------------------------------------------------------------- setOriginAdapter


    //---------------------------------------------------------------------------------------------- setDestinationAdapter
    private fun setDestinationAdapter(taxiGoAddressModel: TaxiGoAddressModel) {
        if (context == null)
            return
        val destination = taxiGoAddressModel.destinations.map {
            it.address
        }
        val adapter = TaxiAddressAdapter(
            items = destination,
            onDelete = {
                listener?.onclickRemoveLocation(
                    position = it,
                    mapType = TaxiMapView.TaxiMapType.Destination
                )
            },
            onFavDeleteClick = { id -> id.let { listener?.onClickFavDelete(id = it) } },
            onFavAddClick = { addressModel, position ->
                listener?.onClickFacAdd(
                    geoPoint = taxiGoAddressModel.destinations[position].geoPoint,
                    address = addressModel.getAddress(),
                    mapType = TaxiMapView.TaxiMapType.Destination,
                    position = position
                )
            }
        )
        val manager = LinearLayoutManager(
            context,
            LinearLayoutManager.VERTICAL,
            false
        )
        recyclerViewDestination.adapter = adapter
        recyclerViewDestination.layoutManager = manager
    }
    //---------------------------------------------------------------------------------------------- setDestinationAdapter

}