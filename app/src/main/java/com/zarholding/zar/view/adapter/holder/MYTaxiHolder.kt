package com.zarholding.zar.view.adapter.holder

import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.R
import com.zarholding.zar.databinding.ItemMyTaxiBinding
import com.zarholding.zar.ext.getPassengers
import com.zarholding.zar.ext.loadImageByToken
import com.zarholding.zar.ext.setTitleAndValue
import com.zarholding.zar.ext.setWaitingTimeToTextView
import com.zarholding.zar.model.data.enum.EnumPersonnelType
import com.zarholding.zar.model.data.enum.EnumTaxiRequestStatus
import com.zarholding.zar.model.data.enum.EnumTaxiRequestType
import com.zarholding.zar.model.data.enum.EnumTaxiRequestTypePro
import com.zarholding.zar.model.data.response.taxi.AdminTaxiRequestModel
import com.zarholding.zar.model.data.response.taxi.TaxiRequestPointModel
import com.zarholding.zar.view.adapter.recycler.taxi.AdminTaxiAddressAdapter
import com.zarholding.zar.view.fragment.user.taxi.pro.component.TaxiMapView

/**
 * Created by m-latifi on 11/19/2022.
 */

class MYTaxiHolder(
    private val binding: ItemMyTaxiBinding,
    private val onMapClick: (TaxiRequestPointModel) -> Unit
) : RecyclerView.ViewHolder(binding.root) {


    //---------------------------------------------------------------------------------------------- bind
    fun bind(item: AdminTaxiRequestModel, token: String) {
        setValueToXml(item, token)
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind


    //---------------------------------------------------------------------------------------------- setValueToXml
    private fun setValueToXml(item: AdminTaxiRequestModel, token: String) {
        binding.textViewApproverName.text = item.approverName
        binding.textViewRejectReason.text = item.rejectReason
        binding.textViewTimeElapse.setWaitingTimeToTextView(item.waitingTime)
        binding.textViewGoDate.text = item.departureDate
        binding.textViewGoTime.text = item.departureTime
        binding.textViewReturnDate.text = item.returnDate
        binding.textViewReturnTime.text = item.returnTime
        binding.textViewApplicatorName.setTitleAndValue(
            title = binding.textViewApplicatorName.context.getString(R.string.applicator),
            splitter = binding.textViewApplicatorName.context.getString(R.string.colon),
            value = item.requesterName
        )

        binding.textViewPassenger.setTitleAndValue(
            title = binding.textViewPassenger.context.getString(R.string.passengers),
            splitter = binding.textViewPassenger.context.getString(R.string.colon),
            value = item.listPassengers.getPassengers()
        )
        binding.textViewReason.setTitleAndValue(
            title = binding.textViewReason.context.getString(R.string.reasonOfTrip),
            splitter = binding.textViewReason.context.getString(R.string.colon),
            value = item.travelReason
        )
        binding.imageViewIcon.loadImageByToken(
            url = item.userName,
            token = token
        )

        setLocationAdapter(
            items = item.locations?.filter {
                it.requestType == EnumTaxiRequestTypePro.Departure &&
                        it.type == TaxiMapView.TaxiMapType.Origin
            },
            recyclerView = binding.recyclerViewGoOrigin
        )
        setLocationAdapter(
            items = item.locations?.filter {
                it.requestType == EnumTaxiRequestTypePro.Departure &&
                        it.type == TaxiMapView.TaxiMapType.Destination
            },
            recyclerView = binding.recyclerViewGoDestination
        )


        when (item.type) {
            EnumTaxiRequestType.OneWay -> {
                binding.constraintLayoutReturnInformation.visibility = View.GONE
                binding.textViewReturnTitle.visibility = View.GONE
            }

            EnumTaxiRequestType.Return -> {
                binding.constraintLayoutReturnInformation.visibility = View.VISIBLE
                binding.textViewReturnTitle.visibility = View.VISIBLE
                setLocationAdapter(
                    items = item.locations?.filter {
                        it.requestType == EnumTaxiRequestTypePro.Return &&
                                it.type == TaxiMapView.TaxiMapType.Origin
                    },
                    recyclerView = binding.recyclerViewReturnOrigin
                )
                setLocationAdapter(
                    items = item.locations?.filter {
                        it.requestType == EnumTaxiRequestTypePro.Return &&
                                it.type == TaxiMapView.TaxiMapType.Destination
                    },
                    recyclerView = binding.recyclerViewReturnDestination
                )
            }
        }

        when (item.status) {
            EnumTaxiRequestStatus.Pending -> {
                binding.linearLayoutStatus.background = ContextCompat.getDrawable(
                    binding.linearLayoutStatus.context, R.drawable.a_drawable_wating
                )
                binding.textViewStatus.text =
                    binding.textViewStatus.context.getString(R.string.pendingForAccept)
                binding.textViewRejectReason.visibility = View.GONE
            }

            EnumTaxiRequestStatus.Confirm,
            EnumTaxiRequestStatus.Confirmed -> {
                binding.linearLayoutStatus.background =
                    ContextCompat.getDrawable(binding.root.context, R.drawable.a_drawable_confirm)
                binding.textViewStatus.text =
                    binding.textViewStatus.context.getString(R.string.confirmed)
                binding.textViewRejectReason.visibility = View.GONE
            }

            EnumTaxiRequestStatus.Reject -> {
                binding.linearLayoutStatus.background =
                    ContextCompat.getDrawable(binding.root.context, R.drawable.a_drawable_reject)
                binding.textViewStatus.text =
                    binding.textViewStatus.context.getString(R.string.rejected)
                binding.textViewRejectReason.visibility = View.VISIBLE
            }
        }
    }
    //---------------------------------------------------------------------------------------------- setValueToXml


    //---------------------------------------------------------------------------------------------- setLocationAdapter
    private fun setLocationAdapter(items: List<TaxiRequestPointModel>?, recyclerView: RecyclerView) {
        if (items.isNullOrEmpty())
            return
        val adapter = AdminTaxiAddressAdapter(items, EnumPersonnelType.Personnel) {
            onMapClick.invoke(it)
        }
        val manager = LinearLayoutManager(
            recyclerView.context,
            LinearLayoutManager.VERTICAL,
            false
        )
        recyclerView.adapter = adapter
        recyclerView.layoutManager = manager
    }
    //---------------------------------------------------------------------------------------------- setLocationAdapter

}