package com.zarholding.zar.view.adapter.holder

import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.R
import com.zarholding.zar.databinding.ItemMyServiceBinding
import com.zarholding.zar.ext.getStation
import com.zarholding.zar.ext.loadImage
import com.zarholding.zar.ext.setMyStation
import com.zarholding.zar.model.data.other.ShowImageModel
import com.zarholding.zar.model.data.response.trip.TripModel
import com.zarholding.zar.ext.setRegisterStationStatus
import com.zarholding.zar.ext.setStartEndStation
import com.zarholding.zar.ext.setTitleAndValue
import com.zarholding.zar.model.data.enum.EnumEntityType
import com.zarholding.zar.model.data.enum.EnumStatus

/**
 * Created by m-latifi on 11/20/2022.
 */

class MyServiceHolder(
    private val binding: ItemMyServiceBinding,
    private val onClick: (TripModel) -> Unit,
    private val onDeleteRegisterStation: (TripModel) -> Unit,
    private val onClickImage: (ShowImageModel) -> Unit,
    private val onClickCall: (String?) -> Unit
) : RecyclerView.ViewHolder(binding.root) {


    //---------------------------------------------------------------------------------------------- bind
    fun bind(item: TripModel) {
        setValueToXml(item)
        setListener(item)
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind


    //---------------------------------------------------------------------------------------------- setValueToXml
    private fun setValueToXml(item: TripModel) {
        binding.cardViewImage.setRegisterStationStatus(item.myStationTripStatus)
        binding.imageViewStatus.setRegisterStationStatus(item.myStationTripStatus)
        binding.textViewStatus.setRegisterStationStatus(item.myStationTripStatus)
        item.myStationTripStatus?.let {
            if (it == EnumStatus.Reject)
                binding.textViewReason.visibility = View.VISIBLE
            else
                binding.textViewReason.visibility = View.GONE
        } ?: run {
            binding.textViewReason.visibility = View.GONE
        }
        binding.imageViewBus.loadImage(
            url = item.carImageName,
            entityType = EnumEntityType.cars.name
        )
        binding.imageViewDriver.loadImage(
            url = item.driverImageName,
            entityType = EnumEntityType.drivers.name
        )
        binding.textViewTitle.setTitleAndValue(
            title = item.commuteTripName,
            splitter = binding.textViewTitle.context.getString(R.string.dash),
            value = item.commuteDriverName
        )
        binding.textViewDirection.setStartEndStation(
            originName = item.originName,
            destinationName = item.destinationName
        )
        binding.textViewMyState.setMyStation(
            myStation = item.myStationName,
            arriveTime = item.myStationArriveTime
        )
        binding.textViewStations.setTitleAndValue(
            title = binding.textViewStations.context.getString(R.string.stations),
            splitter = binding.textViewStations.context.getString(R.string.colon),
            value = item.stations.getStation()
        )
        binding.textViewReason.text = binding.textViewReason.context.getString(
            R.string.reasonOfReject1, item.myStationTripRequestResone
        )
        binding.textViewReason.setTextColor(
            binding.textViewReason.context.getColor(R.color.n_reject)
        )
    }
    //---------------------------------------------------------------------------------------------- setValueToXml


    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener(item: TripModel) {
        binding.root.setOnClickListener { onClick(item) }
        binding.textViewDelete.setOnClickListener { onDeleteRegisterStation(item) }
        binding.imageViewDelete.setOnClickListener { onDeleteRegisterStation(item) }
        binding.imageViewCall.setOnClickListener { onClickCall(item.commuteDriverMobile) }
        binding.imageViewBus.setOnClickListener {
            onClickImage(
                ShowImageModel(
                    item.carImageName!!,
                    binding.imageViewBus.context.resources.getString(R.string.carEntityType),
                    null
                )
            )
        }
        binding.imageViewDriver.setOnClickListener {
            onClickImage(
                ShowImageModel(
                    item.driverImageName!!,
                    binding.imageViewBus.context.resources.getString(R.string.driversEntityType),
                    null
                )
            )
        }
        binding.imageViewShowMore.setOnClickListener {
            if (binding.expandableMore.isExpanded)
                hideMore()
            else
                showMore()
        }
    }
    //---------------------------------------------------------------------------------------------- setListener


    //---------------------------------------------------------------------------------------------- showMore
    private fun showMore() {
        binding.expandableMore.expand()

        val rotate = RotateAnimation(
            0f,
            -90f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 350
        rotate.interpolator = LinearInterpolator()
        rotate.fillAfter = true
        binding.imageViewShowMore.startAnimation(rotate)
    }
    //---------------------------------------------------------------------------------------------- showMore


    //---------------------------------------------------------------------------------------------- hideMore
    private fun hideMore() {
        binding.expandableMore.collapse()
        val rotate = RotateAnimation(
            -90f,
            0f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 400
        rotate.interpolator = LinearInterpolator()
        rotate.fillAfter = true
        binding.imageViewShowMore.startAnimation(rotate)
    }
    //---------------------------------------------------------------------------------------------- hideMore

}