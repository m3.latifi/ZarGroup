package com.zarholding.zar.view.dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.view.*
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.skydoves.powerspinner.IconSpinnerAdapter
import com.skydoves.powerspinner.IconSpinnerItem
import com.zar.core.enums.EnumApiError
import com.zar.core.tools.extensions.isNationalCode
import com.zar.core.view.picker.date.customviews.DateRangeCalendarView
import com.zar.core.view.picker.date.dialog.DatePickerDialog
import com.zarholding.zar.R
import com.zarholding.zar.databinding.DialogInsuranceRegisterBinding
import com.zarholding.zar.model.data.request.insurance.InsuranceRelationShipModel
import com.zarholding.zar.model.data.response.dropdown.DropDownModel
import com.zarholding.zar.view.activity.MainActivity
import com.zarholding.zar.view.adapter.recycler.insurance.InsuranceRelationAdapter
import com.zarholding.zar.view.fragment.user.insurance.signup.InsuranceSignupViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * Created by m-latifi on 11/26/2022.
 */

@AndroidEntryPoint
class InsuranceRegisterDialog @Inject constructor(
    private val viewModel: InsuranceSignupViewModel,
    private val onConfirm: () -> Unit
) : DialogFragment() {

    lateinit var binding: DialogInsuranceRegisterBinding


    //---------------------------------------------------------------------------------------------- onCreateView
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DialogInsuranceRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }
    //---------------------------------------------------------------------------------------------- onCreateView


    //---------------------------------------------------------------------------------------------- onCreateView
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity?)?.enableBlurView()
        val lp = WindowManager.LayoutParams()
        val window = dialog?.window
        val back = ColorDrawable(Color.TRANSPARENT)
        val inset = InsetDrawable(back, 50)
        window?.setBackgroundDrawable(inset)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        isCancelable = false
        window?.attributes = lp
        setListener()
        observeLiveDate()
        setRelationAdapter()
    }
    //---------------------------------------------------------------------------------------------- onCreateView


    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener() {

        binding.imageViewClose.setOnClickListener { dismiss() }

        binding.buttonBirthDay.setOnClickListener { showDatePickerDialog() }

        binding.buttonAddPerson.setOnClickListener { addPerson() }

        binding.buttonRegister.setOnClickListener { registerInsurance() }

        binding.buttonNewPerson.setOnClickListener {
            binding.buttonNewPerson.visibility = View.GONE
            binding.recyclerRelation.visibility = View.GONE
            binding.buttonRegister.visibility = View.GONE
            binding.textViewRelationLabel.visibility = View.GONE
            binding.expandableContent.expand()
        }

    }
    //---------------------------------------------------------------------------------------------- setListener


    //---------------------------------------------------------------------------------------------- setOnListener
    private fun observeLiveDate() {
        viewModel.errorLiveDate.observe(viewLifecycleOwner) {
            binding.buttonRegister.stopLoading()
            onConfirm
            when (it.type) {
                EnumApiError.UnAuthorization ->
                    (activity as MainActivity?)?.gotoFirstFragmentWithDeleteUser()

                else -> {}
            }
        }

        viewModel.registerLiveData.observe(viewLifecycleOwner) {
            onConfirm
            dismiss()
        }


        viewModel.relationShipLiveData.observe(viewLifecycleOwner) {
            setRelationShipSpinner(it)
        }
    }
    //---------------------------------------------------------------------------------------------- setOnListener


    //---------------------------------------------------------------------------------------------- setRelationShipSpinner
    private fun setRelationShipSpinner(dropdowns: List<DropDownModel>) {
        val items = dropdowns.map { company ->
            IconSpinnerItem(company.text)
        }
        binding.powerSpinnerRelation.apply {
            setSpinnerAdapter(IconSpinnerAdapter(this))
            setItems(items)
            setOnSpinnerOutsideTouchListener { _, _ ->  dismiss()}
            getSpinnerRecyclerView().layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            lifecycleOwner = viewLifecycleOwner
            setOnSpinnerItemSelectedListener<IconSpinnerItem> { _, _, _, _ -> }
        }
    }
    //---------------------------------------------------------------------------------------------- setRelationShipSpinner


    //---------------------------------------------------------------------------------------------- addPerson
    private fun addPerson() {
        if (checkValidation()) {
            val relation = InsuranceRelationShipModel(
                firstName = binding.textInputEditTextFirstName.text.toString(),
                lastName = binding.textInputEditTextLastName.text.toString(),
                nationalCode = binding.textInputEditNationalCode.text.toString(),
                birthCertificateNumber = binding.textInputEditCertificateNumber.text.toString(),
                birthdate = binding.buttonBirthDay.text.toString().replace("/", ""),
                genderCode = if (binding.radioMan.isChecked) "Male" else "Female",
                fatherName = binding.textInputEditFatherName.text.toString(),
                relationShipId = viewModel
                    .relationShipLiveData
                    .value?.get(binding.powerSpinnerRelation.selectedIndex)?.value?.toLong() ?: 0
            )
            viewModel.insuranceRelationShips.add(relation)
            setRelationAdapter()
        }
    }
    //---------------------------------------------------------------------------------------------- addPerson


    //---------------------------------------------------------------------------------------------- checkValidation
    private fun checkValidation(): Boolean {
        var valid = true
        if (binding.textInputEditTextFirstName.text.toString().isEmpty()) {
            binding.textInputEditTextFirstName.error = getString(R.string.firstName)
            valid = false
        }
        if (binding.textInputEditTextLastName.text.toString().isEmpty()) {
            binding.textInputEditTextLastName.error = getString(R.string.lastName)
            valid = false
        }
        if (!binding.textInputEditNationalCode.text.toString().isNationalCode()) {
            binding.textInputEditNationalCode.error = getString(R.string.nationalCode)
            valid = false
        }
        if (binding.textInputEditCertificateNumber.text.toString().isEmpty()) {
            binding.textInputEditCertificateNumber.error = getString(R.string.certificateNumber)
            valid = false
        }
        if (binding.buttonBirthDay.text.toString().isEmpty()) {
            binding.buttonBirthDay.setStrokeColorResource(R.color.n_borderRed)
            valid = false
        } else
            binding.buttonBirthDay.setStrokeColorResource(R.color.n_InputBorderColor)

        if (binding.textInputEditFatherName.text.toString().isEmpty()) {
            binding.textInputEditFatherName.error = getString(R.string.fatherName)
            valid = false
        }
        if (!binding.radioMan.isChecked && !binding.radioWoman.isChecked) {
            binding.radioGroup.setBackgroundResource(R.drawable.a_drawable_border_red)
            valid = false
        } else
            binding.radioGroup.setBackgroundResource(R.drawable.a_drawable_input_border)
        if (binding.powerSpinnerRelation.selectedIndex == -1) {
            binding.powerSpinnerRelation.setBackgroundResource(R.drawable.a_drawable_border_red)
            valid = false
        } else
            binding.powerSpinnerRelation.setBackgroundResource(R.drawable.a_drawable_input_border)
        return valid
    }
    //---------------------------------------------------------------------------------------------- checkValidation


    //---------------------------------------------------------------------------------------------- showDatePickerDialog
    private fun showDatePickerDialog() {
        binding.powerSpinnerRelation.dismiss()
        if (context == null)
            return
        val datePickerDialog = DatePickerDialog(requireContext())
        datePickerDialog.selectionMode = DateRangeCalendarView.SelectionMode.Single
        datePickerDialog.isDisableDaysAgo = false
        datePickerDialog.acceptButtonColor =
            resources.getColor(R.color.n_blueBottom, requireContext().theme)
        datePickerDialog.headerBackgroundColor =
            resources.getColor(R.color.n_blueBottom, requireContext().theme)
        datePickerDialog.headerTextColor =
            resources.getColor(R.color.white, requireContext().theme)
        datePickerDialog.weekColor =
            resources.getColor(R.color.n_textHint, requireContext().theme)
        datePickerDialog.disableDateColor =
            resources.getColor(R.color.n_textHint, requireContext().theme)
        datePickerDialog.defaultDateColor =
            resources.getColor(R.color.n_blueBottom, requireContext().theme)
        datePickerDialog.selectedDateCircleColor =
            resources.getColor(R.color.n_blueBottom, requireContext().theme)
        datePickerDialog.selectedDateColor =
            resources.getColor(R.color.white, requireContext().theme)
        datePickerDialog.rangeDateColor =
            resources.getColor(R.color.n_blueBottom, requireContext().theme)
        datePickerDialog.rangeStripColor =
            resources.getColor(R.color.n_primaryColorAlpha, requireContext().theme)
        datePickerDialog.holidayColor =
            resources.getColor(R.color.n_reject, requireContext().theme)
        datePickerDialog.textSizeWeek = 12.0f
        datePickerDialog.textSizeDate = 14.0f
        datePickerDialog.textSizeTitle = 18.0f
        datePickerDialog.setCanceledOnTouchOutside(true)
        datePickerDialog.onSingleDateSelectedListener =
            DatePickerDialog.OnSingleDateSelectedListener { startDate ->
                binding.buttonBirthDay.text = startDate.persianShortDate
            }
        datePickerDialog.onRangeDateSelectedListener =
            DatePickerDialog.OnRangeDateSelectedListener { _, _ ->
            }
        datePickerDialog.showDialog()
    }
    //---------------------------------------------------------------------------------------------- showDatePickerDialog


    //---------------------------------------------------------------------------------------------- setRelationAdapter
    private fun setRelationAdapter() {
        clearField()
        if (context == null)
            return
        if (viewModel.insuranceRelationShips.size == 0)
            binding.textViewNoBody.visibility = View.VISIBLE
        else
            binding.textViewNoBody.visibility = View.GONE
        val adapter = InsuranceRelationAdapter(viewModel.insuranceRelationShips) {
            viewModel.insuranceRelationShips.remove(it)
            setRelationAdapter()
        }
        val manager = LinearLayoutManager(
            requireContext(), LinearLayoutManager.VERTICAL, false
        )
        binding.recyclerRelation.layoutManager = manager
        binding.recyclerRelation.adapter = adapter
    }
    //---------------------------------------------------------------------------------------------- setRelationAdapter


    //---------------------------------------------------------------------------------------------- clearField
    private fun clearField() {
        binding.textInputEditTextFirstName.text?.clear()
        binding.textInputEditTextLastName.text?.clear()
        binding.textInputEditNationalCode.text?.clear()
        binding.textInputEditCertificateNumber.text?.clear()
        binding.textInputEditFatherName.text?.clear()
        binding.buttonBirthDay.text = ""
        binding.radioGroup.clearCheck()
        binding.powerSpinnerRelation.clearSelectedItem()
//        binding.textInputEditTextFirstName.requestFocus()
        binding.expandableContent.collapse()
        binding.buttonNewPerson.visibility = View.VISIBLE
        binding.buttonRegister.visibility = View.VISIBLE
        binding.recyclerRelation.visibility = View.VISIBLE
        binding.textViewRelationLabel.visibility = View.VISIBLE
    }
    //---------------------------------------------------------------------------------------------- clearField


    //---------------------------------------------------------------------------------------------- registerInsurance
    private fun registerInsurance() {
        binding.buttonRegister.startLoading(getString(R.string.bePatient))
        viewModel.requestRegisterSupplementalInsurance()
    }
    //---------------------------------------------------------------------------------------------- registerInsurance


    //---------------------------------------------------------------------------------------------- dismiss
    override fun dismiss() {
        super.dismiss()
        (activity as MainActivity?)?.disableBlurView()
    }
    //---------------------------------------------------------------------------------------------- dismiss
}