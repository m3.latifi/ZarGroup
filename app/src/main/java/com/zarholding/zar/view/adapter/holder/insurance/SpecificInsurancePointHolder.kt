package com.zarholding.zar.view.adapter.holder.insurance

import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemSpecificInsurancePointBinding
import com.zarholding.zar.model.data.response.insurance.SupplementalInsurancePointModel

/**
 * Created by m-latifi on 10/15/2023.
 */

class SpecificInsurancePointHolder(
    private val binding: ItemSpecificInsurancePointBinding
): RecyclerView.ViewHolder(binding.root) {


    //---------------------------------------------------------------------------------------------- bind
    fun bind(item: SupplementalInsurancePointModel) {
        binding.textViewTitle.text = item.description
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind


}