package com.zarholding.zar.view.fragment.user.taxi.pro


import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.zarholding.zar.R
import com.zarholding.zar.ZarViewModel
import com.zarholding.zar.ext.getAddress
import com.zarholding.zar.model.data.database.entity.UserInfoEntity
import com.zarholding.zar.model.data.enum.EnumTaxiRequestType
import com.zarholding.zar.model.data.enum.EnumTaxiRequestTypePro
import com.zarholding.zar.model.data.other.AddressWithPointModel
import com.zarholding.zar.model.data.request.TaxiAddFavPlaceRequest
import com.zarholding.zar.model.data.response.taxi.TaxiRequestPointModel
import com.zarholding.zar.model.data.response.ResponseResult
import com.zarholding.zar.model.data.response.address.AddressModel
import com.zarholding.zar.model.data.response.dropdown.DropDownModel
import com.zarholding.zar.model.data.response.taxi.TaxiFavPlaceModel
import com.zarholding.zar.model.data.taxi.TaxiGoAddressModel
import com.zarholding.zar.model.repository.AddressRepository
import com.zarholding.zar.model.repository.DropdownRepository
import com.zarholding.zar.model.repository.FavoriteRepository
import com.zarholding.zar.model.repository.PassengerRepository
import com.zarholding.zar.model.repository.TaxiRepository
import com.zarholding.zar.view.fragment.user.taxi.pro.component.TaxiMapView
import com.zarholding.zar.view.fragment.user.taxi.pro.component.TaxiTripView
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import org.osmdroid.util.GeoPoint
import javax.inject.Inject

/**
 * Created by m-latifi on 10/22/2023.
 */

@HiltViewModel
class TaxiReservationViewModel @Inject constructor(
    private val dropdownRepository: DropdownRepository,
    private val addressRepository: AddressRepository,
    private val taxiRepository: TaxiRepository,
    private val favoriteRepository: FavoriteRepository,
    private val passengerRepository: PassengerRepository
) : ZarViewModel() {

    private val _siteState =
        MutableStateFlow<ResponseResult<List<DropDownModel>>>(ResponseResult.Loading(false))
    val siteState: StateFlow<ResponseResult<List<DropDownModel>>> = _siteState

    private val _favoriteState =
        MutableStateFlow<ResponseResult<List<TaxiFavPlaceModel>>>(ResponseResult.Loading(false))
    val favoriteState: StateFlow<ResponseResult<List<TaxiFavPlaceModel>>> = _favoriteState

    private val _passengerState =
        MutableStateFlow<ResponseResult<List<UserInfoEntity>>>(ResponseResult.Loading(false))
    val passengerState: StateFlow<ResponseResult<List<UserInfoEntity>>> = _passengerState

    private val _sendRequestState =
        MutableStateFlow<ResponseResult<String>>(ResponseResult.Loading(false))
    val sendRequestState: StateFlow<ResponseResult<String>> = _sendRequestState

    private val _goDate = MutableStateFlow("")
    val goDate: StateFlow<String> = _goDate

    private val _goTime = MutableStateFlow("")
    val goTime: StateFlow<String> = _goTime

    private val _returnDate = MutableStateFlow("")
    val returnDate: StateFlow<String> = _returnDate

    private val _returnTime = MutableStateFlow("")
    val returnTime: StateFlow<String> = _returnTime

    private val taxiGoAddressModel = TaxiGoAddressModel()
    val taxiGoAddressLiveDate: MutableLiveData<TaxiGoAddressModel> by lazy {
        MutableLiveData<TaxiGoAddressModel>()
    }

    private val taxiReturnAddressModel = TaxiGoAddressModel()
    val taxiReturnAddressLiveDate: MutableLiveData<TaxiGoAddressModel> by lazy {
        MutableLiveData<TaxiGoAddressModel>()
    }

    var companySelected: DropDownModel? = null
    var reason: String = ""


    //---------------------------------------------------------------------------------------------- init
    init {
        viewModelScope.launch(IO + exceptionHandler()) {
            _passengerState.value = ResponseResult.Loading(true)
            passengerRepository.addCurrentUserToPassenger().collect {
                _passengerState.value = ResponseResult.Loading(false)
                _passengerState.value = ResponseResult.Success(it)
            }
        }
    }
    //---------------------------------------------------------------------------------------------- init



    //---------------------------------------------------------------------------------------------- setGoDate
    fun setGoDate(date: String) {
        _goDate.value = date
    }
    //---------------------------------------------------------------------------------------------- setGoDate


    //---------------------------------------------------------------------------------------------- setGoTime
    fun setGoTime(time: String) {
        _goTime.value = time
    }
    //---------------------------------------------------------------------------------------------- setGoTime


    //---------------------------------------------------------------------------------------------- setReturnDate
    fun setReturnDate(date: String) {
        _returnDate.value = date
    }
    //---------------------------------------------------------------------------------------------- setReturnDate


    //---------------------------------------------------------------------------------------------- setReturnTime
    fun setReturnTime(time: String) {
        _returnTime.value = time
    }
    //---------------------------------------------------------------------------------------------- setReturnTime



    //---------------------------------------------------------------------------------------------- validationGoTrip
    fun validationGoTrip(): Boolean {
        if (goDate.value.isEmpty()) {
            setMessage(resourcesProvider.getString(R.string.selectTheDepartureDate))
            return false
        }
        if (goTime.value.isEmpty()) {
            setMessage(resourcesProvider.getString(R.string.selectTheDepartureTime))
            return false
        }
        if (taxiGoAddressModel.origins.isEmpty()) {
            setMessage(resourcesProvider.getString(R.string.pleaseChooseGoOriginLocation))
            return false
        }
        if (taxiGoAddressModel.destinations.isEmpty()) {
            setMessage(resourcesProvider.getString(R.string.pleaseChooseGoDestinationLocation))
            return false
        }
        return true
    }
    //---------------------------------------------------------------------------------------------- validationGoTrip


    //---------------------------------------------------------------------------------------------- validationReturnTrip
    private fun validationReturnTrip(): Boolean {
        if (returnDate.value.isEmpty()) {
            setMessage(resourcesProvider.getString(R.string.selectTheReturnDate))
            return false
        }
        if (returnTime.value.isEmpty()) {
            setMessage(resourcesProvider.getString(R.string.selectTheReturnTime))
            return false
        }
        if (taxiReturnAddressModel.origins.isEmpty()) {
            setMessage(resourcesProvider.getString(R.string.pleaseChooseReturnOriginLocation))
            return false
        }
        if (taxiGoAddressModel.destinations.isEmpty()) {
            setMessage(resourcesProvider.getString(R.string.pleaseChooseReturnDestinationLocation))
            return false
        }
        return true
    }
    //---------------------------------------------------------------------------------------------- validationReturnTrip


    //---------------------------------------------------------------------------------------------- requestTaxiPro
    fun requestTaxiPro() {
        viewModelScope.launch(IO + exceptionHandler()) {
            if (!validationGoTrip())
                return@launch
            if (getEnumTaxiRequestType() == EnumTaxiRequestType.Return && !validationReturnTrip())
                return@launch
            _sendRequestState.value = ResponseResult.Loading(true)
            taxiRepository.requestTaxi(
                type = getEnumTaxiRequestType(),
                departureDate = goDate.value,
                departureTime = goTime.value,
                returnDate = returnDate.value,
                returnTime = returnTime.value,
                passengers = passengerRepository.getPassengersId(),
                travelReason = reason,
                siteId = companySelected?.value?.toInt(),
                items = getLocationsList()
            ).collect { networkResult ->
                checkResponse(networkResult = networkResult) {
                    _sendRequestState.value = ResponseResult.Loading(false)
                    _sendRequestState.value = ResponseResult
                        .Success(resourcesProvider.getString(R.string.successRequest))
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- requestTaxiPro


    //---------------------------------------------------------------------------------------------- requestGetSites
    fun requestGetSites() {
        viewModelScope.launch(IO + exceptionHandler()) {
            _siteState.value = ResponseResult.Loading(true)
            dropdownRepository.requestGetSites().collect { networkResult ->
                checkResponse(networkResult = networkResult) {
                    _siteState.value = ResponseResult.Loading(false)
                    _siteState.value = ResponseResult.Success(it)
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- requestGetSites


    //---------------------------------------------------------------------------------------------- requestGetTaxiFavPlace
    fun requestGetTaxiFavPlace() {
        viewModelScope.launch(IO + exceptionHandler()) {
            _favoriteState.value = ResponseResult.Loading(true)
            favoriteRepository.requestGetTaxiFavPlace().collect { networkResult ->
                _favoriteState.value = ResponseResult.Loading(false)
                checkResponse(networkResult = networkResult) {
                    _favoriteState.value = ResponseResult.Success(it)
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- requestGetTaxiFavPlace


    //---------------------------------------------------------------------------------------------- getAddress
    fun getAddress(
        geoPoint: GeoPoint,
        mapType: TaxiMapView.TaxiMapType,
        tripType: TaxiTripView.EnumTripType,
        taxiFavPlaceModel: TaxiFavPlaceModel?
    ) {
        viewModelScope.launch(IO + exceptionHandler()) {
            addressRepository.requestGetAddress(geoPoint).collect { networkResult ->
                checkResponse(networkResult = networkResult) { model ->
                    model.address?.let {
                        it.addressTitle = taxiFavPlaceModel?.locationName
                        it.favId = taxiFavPlaceModel?.id
                        addAddress(
                            mapType = mapType,
                            tripType = tripType,
                            geoPoint = geoPoint,
                            address = it
                        )
                    } ?: run {
                        setMessage(resourcesProvider.getString(R.string.dataReceivedIsEmpty))
                    }
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- getAddress


    //---------------------------------------------------------------------------------------------- addAddress
    private fun addAddress(
        mapType: TaxiMapView.TaxiMapType,
        tripType: TaxiTripView.EnumTripType,
        address: AddressModel,
        geoPoint: GeoPoint
    ) {
        val point = AddressWithPointModel(address = address, geoPoint = geoPoint)
        when (mapType) {
            TaxiMapView.TaxiMapType.Origin -> addOrigin(origin = point, tripType = tripType)
            TaxiMapView.TaxiMapType.Destination -> addDestination(
                destination = point,
                tripType = tripType
            )
        }
    }
    //---------------------------------------------------------------------------------------------- addAddress


    //---------------------------------------------------------------------------------------------- addOrigin
    private fun addOrigin(origin: AddressWithPointModel, tripType: TaxiTripView.EnumTripType) {
        when (tripType) {
            TaxiTripView.EnumTripType.Go -> {
                taxiGoAddressModel.origins.add(origin)
                taxiGoAddressLiveDate.postValue(taxiGoAddressModel)
            }

            TaxiTripView.EnumTripType.Return -> {
                taxiReturnAddressModel.origins.add(origin)
                taxiReturnAddressLiveDate.postValue(taxiReturnAddressModel)
            }
        }
    }
    //---------------------------------------------------------------------------------------------- addOrigin


    //---------------------------------------------------------------------------------------------- removeOrigin
    fun removeOrigin(position: Int, tripType: TaxiTripView.EnumTripType) {
        when (tripType) {
            TaxiTripView.EnumTripType.Go -> {
                val origin = taxiGoAddressModel.origins[position]
                taxiGoAddressModel.origins.remove(origin)
                taxiGoAddressLiveDate.postValue(taxiGoAddressModel)
            }

            TaxiTripView.EnumTripType.Return -> {
                val origin = taxiReturnAddressModel.origins[position]
                taxiReturnAddressModel.origins.remove(origin)
                taxiReturnAddressLiveDate.postValue(taxiReturnAddressModel)
            }
        }
    }
    //---------------------------------------------------------------------------------------------- removeOrigin


    //---------------------------------------------------------------------------------------------- addDestination
    private fun addDestination(
        destination: AddressWithPointModel,
        tripType: TaxiTripView.EnumTripType
    ) {
        when (tripType) {
            TaxiTripView.EnumTripType.Go -> {
                taxiGoAddressModel.destinations.add(destination)
                taxiGoAddressLiveDate.postValue(taxiGoAddressModel)
            }

            TaxiTripView.EnumTripType.Return -> {
                taxiReturnAddressModel.destinations.add(destination)
                taxiReturnAddressLiveDate.postValue(taxiReturnAddressModel)
            }
        }
    }
    //---------------------------------------------------------------------------------------------- addDestination


    //---------------------------------------------------------------------------------------------- removeDestination
    fun removeDestination(position: Int, tripType: TaxiTripView.EnumTripType) {
        when (tripType) {
            TaxiTripView.EnumTripType.Go -> {
                val destination = taxiGoAddressModel.destinations[position]
                taxiGoAddressModel.destinations.remove(destination)
                taxiGoAddressLiveDate.postValue(taxiGoAddressModel)
            }

            TaxiTripView.EnumTripType.Return -> {
                val destination = taxiReturnAddressModel.destinations[position]
                taxiReturnAddressModel.destinations.remove(destination)
                taxiReturnAddressLiveDate.postValue(taxiReturnAddressModel)
            }
        }
    }
    //---------------------------------------------------------------------------------------------- removeDestination


    //---------------------------------------------------------------------------------------------- requestAddFavPlace
    fun requestAddFavPlace(
        request: TaxiAddFavPlaceRequest,
        tripType: TaxiTripView.EnumTripType,
        mapType: TaxiMapView.TaxiMapType,
        position: Int
    ) {
        viewModelScope.launch(IO + exceptionHandler()) {
            _favoriteState.value = ResponseResult.Loading(true)
            favoriteRepository.requestAddFavPlace(request).collect { networkResult ->
                checkResponse(networkResult = networkResult) {
                    _favoriteState.value = ResponseResult.Loading(false)
                    _favoriteState.value = ResponseResult.Success(favoriteRepository.getFavorites())
                    addFavInLocationList(
                        taxiFavPlaceModel = it,
                        tripType = tripType,
                        mapType = mapType,
                        position = position
                    )
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- requestAddFavPlace


    //---------------------------------------------------------------------------------------------- addFavInLocationList
    private fun addFavInLocationList(
        taxiFavPlaceModel: TaxiFavPlaceModel,
        tripType: TaxiTripView.EnumTripType,
        mapType: TaxiMapView.TaxiMapType,
        position: Int
    ) {
        when (tripType) {
            TaxiTripView.EnumTripType.Go ->
                addFavInGoLocationList(
                    taxiFavPlaceModel = taxiFavPlaceModel,
                    mapType = mapType,
                    position = position
                )

            TaxiTripView.EnumTripType.Return ->
                addFavInReturnLocationList(
                    taxiFavPlaceModel = taxiFavPlaceModel,
                    mapType = mapType,
                    position = position
                )
        }
    }
    //---------------------------------------------------------------------------------------------- addFavInLocationList


    //---------------------------------------------------------------------------------------------- addFavInGoLocationList
    private fun addFavInGoLocationList(
        taxiFavPlaceModel: TaxiFavPlaceModel,
        mapType: TaxiMapView.TaxiMapType,
        position: Int
    ) {
        when (mapType) {
            TaxiMapView.TaxiMapType.Origin -> {
                if (taxiGoAddressModel.origins.size < position)
                    return
                taxiGoAddressModel.origins[position].address.favId = taxiFavPlaceModel.id
                taxiGoAddressModel.origins[position].address.addressTitle =
                    taxiFavPlaceModel.locationName
                taxiGoAddressLiveDate.postValue(taxiGoAddressModel)
            }

            TaxiMapView.TaxiMapType.Destination -> {
                if (taxiGoAddressModel.destinations.size < position)
                    return
                taxiGoAddressModel.destinations[position].address.favId = taxiFavPlaceModel.id
                taxiGoAddressModel.destinations[position].address.addressTitle =
                    taxiFavPlaceModel.locationName
                taxiGoAddressLiveDate.postValue(taxiGoAddressModel)
            }
        }
    }
    //---------------------------------------------------------------------------------------------- addFavInGoLocationList


    //---------------------------------------------------------------------------------------------- addFavInReturnLocationList
    private fun addFavInReturnLocationList(
        taxiFavPlaceModel: TaxiFavPlaceModel,
        mapType: TaxiMapView.TaxiMapType,
        position: Int
    ) {
        when (mapType) {
            TaxiMapView.TaxiMapType.Origin -> {
                if (taxiReturnAddressModel.origins.size < position)
                    return
                taxiReturnAddressModel.origins[position].address.favId = taxiFavPlaceModel.id
                taxiReturnAddressModel.origins[position].address.addressTitle =
                    taxiFavPlaceModel.locationName
                taxiReturnAddressLiveDate.postValue(taxiReturnAddressModel)
            }

            TaxiMapView.TaxiMapType.Destination -> {
                if (taxiReturnAddressModel.destinations.size < position)
                    return
                taxiReturnAddressModel.destinations[position].address.favId = taxiFavPlaceModel.id
                taxiReturnAddressModel.destinations[position].address.addressTitle =
                    taxiFavPlaceModel.locationName
                taxiReturnAddressLiveDate.postValue(taxiReturnAddressModel)
            }
        }
    }
    //---------------------------------------------------------------------------------------------- addFavInReturnLocationList


    //---------------------------------------------------------------------------------------------- requestDeleteFavPlace
    fun requestDeleteFavPlace(id: Int) {
        viewModelScope.launch(IO + exceptionHandler()) {
            _favoriteState.value = ResponseResult.Loading(true)
            favoriteRepository.requestDeleteFavPlace(id).collect { networkResult ->
                checkResponse(networkResult = networkResult) {
                    _favoriteState.value = ResponseResult.Loading(false)
                    removeItemInFavPlace(id = id)
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- requestDeleteFavPlace


    //---------------------------------------------------------------------------------------------- removeItemInFavPlace
    private fun removeItemInFavPlace(id: Int) {
        viewModelScope.launch(IO + exceptionHandler()) {
            taxiGoAddressModel.origins.forEach { item ->
                if (item.address.favId == id)
                    item.address.favId = null
            }
            taxiGoAddressModel.destinations.forEach { item ->
                if (item.address.favId == id)
                    item.address.favId = null
            }
            taxiReturnAddressModel.origins.forEach { item ->
                if (item.address.favId == id)
                    item.address.favId = null
            }
            taxiReturnAddressModel.destinations.forEach { item ->
                if (item.address.favId == id)
                    item.address.favId = null
            }
            taxiGoAddressLiveDate.postValue(taxiGoAddressModel)
            taxiReturnAddressLiveDate.postValue(taxiReturnAddressModel)
            _favoriteState.value = ResponseResult.Success(favoriteRepository.getFavorites())
        }
    }
    //---------------------------------------------------------------------------------------------- removeItemInFavPlace


    //---------------------------------------------------------------------------------------------- getEnumTaxiRequestType
    private fun getEnumTaxiRequestType() =
        if (taxiReturnAddressModel.origins.isEmpty() && taxiReturnAddressModel.destinations.isEmpty())
            EnumTaxiRequestType.OneWay
        else
            EnumTaxiRequestType.Return
    //---------------------------------------------------------------------------------------------- getEnumTaxiRequestType


    //---------------------------------------------------------------------------------------------- getLocationsList
    private fun getLocationsList(): List<TaxiRequestPointModel> {
        val items = mutableListOf<TaxiRequestPointModel>()
        val goOrigins = taxiGoAddressModel.origins.map {
            val favAddress = favoriteRepository.getFavoriteTitle(favId = it.address.favId)
            TaxiRequestPointModel(
                type = TaxiMapView.TaxiMapType.Origin,
                requestType = EnumTaxiRequestTypePro.Departure,
                lat = it.geoPoint.latitude,
                long = it.geoPoint.longitude,
                address = if (favAddress.isEmpty())
                    it.address.getAddress()
                else
                    "$favAddress - ${it.address.getAddress()}"
            )
        }
        val goDestinations = taxiGoAddressModel.destinations.map {
            val favAddress = favoriteRepository.getFavoriteTitle(favId = it.address.favId)
            TaxiRequestPointModel(
                type = TaxiMapView.TaxiMapType.Destination,
                requestType = EnumTaxiRequestTypePro.Departure,
                lat = it.geoPoint.latitude,
                long = it.geoPoint.longitude,
                address = if (favAddress.isEmpty())
                    it.address.getAddress()
                else
                    "$favAddress - ${it.address.getAddress()}"
            )
        }
        val returnOrigins = taxiReturnAddressModel.origins.map {
            TaxiRequestPointModel(
                type = TaxiMapView.TaxiMapType.Origin,
                requestType = EnumTaxiRequestTypePro.Return,
                lat = it.geoPoint.latitude,
                long = it.geoPoint.longitude,
                address = it.address.getAddress()
            )
        }
        val returnDestinations = taxiReturnAddressModel.destinations.map {
            TaxiRequestPointModel(
                type = TaxiMapView.TaxiMapType.Destination,
                requestType = EnumTaxiRequestTypePro.Return,
                lat = it.geoPoint.latitude,
                long = it.geoPoint.longitude,
                address = it.address.getAddress()
            )
        }
        items.addAll(goOrigins)
        items.addAll(goDestinations)
        items.addAll(returnOrigins)
        items.addAll(returnDestinations)
        return items
    }
    //---------------------------------------------------------------------------------------------- getLocationsList


    //---------------------------------------------------------------------------------------------- selectPerson
    fun selectPerson(item: UserInfoEntity) {
        viewModelScope.launch(IO + exceptionHandler()) {
            _passengerState.value = ResponseResult.Loading(true)
            delay(100)
            passengerRepository.addPersonToPassengers(item = item).collect { networkResult ->
                checkResponse(networkResult = networkResult) {
                    _passengerState.value = ResponseResult.Loading(false)
                    _passengerState.value = ResponseResult.Success(it)
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- selectPerson


    //---------------------------------------------------------------------------------------------- deletePassenger
    fun deletePassenger(item: UserInfoEntity) {
        viewModelScope.launch(IO + exceptionHandler()) {
            _passengerState.value = ResponseResult.Loading(true)
            delay(100)
            passengerRepository.deletePassenger(item = item).collect {
                _passengerState.value = ResponseResult.Loading(false)
                _passengerState.value = ResponseResult.Success(it)
            }
        }
    }
    //---------------------------------------------------------------------------------------------- deletePassenger


}