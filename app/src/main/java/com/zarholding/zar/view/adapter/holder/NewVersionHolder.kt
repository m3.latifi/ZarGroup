package com.zarholding.zar.view.adapter.holder

import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemNewVersionBinding
import com.zarholding.zar.model.data.other.version.VersionFeatureModel

/**
 * Created by m-latifi on 9/30/2023.
 */

class NewVersionHolder(
    val binding: ItemNewVersionBinding
) : RecyclerView.ViewHolder(binding.root) {

    //---------------------------------------------------------------------------------------------- bind
    fun bind(item: VersionFeatureModel) {
        binding.textViewVersion.text = item.version
        var features = ""
        for (feature in item.features)
            if (features.isEmpty())
                features = feature
            else
                features += System.getProperty("line.separator") + feature
        binding.textViewFeature.text = features
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind

}