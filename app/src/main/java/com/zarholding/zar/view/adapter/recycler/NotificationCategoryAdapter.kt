package com.zarholding.zar.view.adapter.recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemNotificationCategoryBinding
import com.zarholding.zar.model.data.other.notification.NotificationCategoryModel
import com.zarholding.zar.view.adapter.holder.notification.NotificationCategoryHolder

/**
 * Created by m-latifi on 11/16/2022.
 */

class NotificationCategoryAdapter(
    private val listOfCategories: List<NotificationCategoryModel>,
    private val onClick: (categoryPosition: Int, notificationPosition: Int) -> Unit
) : RecyclerView.Adapter<NotificationCategoryHolder>() {

    private var layoutInflater: LayoutInflater? = null


    //---------------------------------------------------------------------------------------------- onCreateViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationCategoryHolder {

        if (layoutInflater == null)
            layoutInflater = LayoutInflater.from(parent.context)

        return NotificationCategoryHolder(
            binding = ItemNotificationCategoryBinding.inflate(layoutInflater!!, parent, false),
            onClick = onClick
        )
    }
    //---------------------------------------------------------------------------------------------- onCreateViewHolder


    //---------------------------------------------------------------------------------------------- onCreateViewHolder
    override fun onBindViewHolder(holder: NotificationCategoryHolder, position: Int) {
        holder.bind(listOfCategories[position], position)
    }
    //---------------------------------------------------------------------------------------------- onCreateViewHolder


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun getItemCount() = listOfCategories.size
    //---------------------------------------------------------------------------------------------- getItemCount


    //---------------------------------------------------------------------------------------------- setReadNotification
    fun setReadNotification(categoryPosition: Int, notificationPosition: Int) {
        listOfCategories[categoryPosition].notifications[notificationPosition].isRead = true
        notifyItemRangeChanged(categoryPosition, 1)
    }
    //---------------------------------------------------------------------------------------------- setReadNotification


    /*
        //---------------------------------------------------------------------------------------------- addNotification
        fun addNotification(item : NotificationModel) {
            listOfCategories[0].notifications.add(0, item)
            notifyItemRangeChanged(0,1)
        }
        //---------------------------------------------------------------------------------------------- addNotification
    */


    //---------------------------------------------------------------------------------------------- getListOfCategories
    fun getListOfCategories() = listOfCategories
    //---------------------------------------------------------------------------------------------- getListOfCategories
}