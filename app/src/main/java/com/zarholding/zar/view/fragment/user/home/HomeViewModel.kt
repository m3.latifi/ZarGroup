package com.zarholding.zar.view.fragment.user.home

import androidx.lifecycle.viewModelScope
import com.zarholding.zar.R
import com.zarholding.zar.model.data.database.entity.ArticleEntity
import com.zarholding.zar.model.data.other.AppModel
import com.zarholding.zar.model.repository.ArticleRepository
import com.zarholding.zar.ZarViewModel
import com.zarholding.zar.model.data.response.ResponseResult
import com.zarholding.zar.model.repository.AppUpdateRepository
import com.zarholding.zar.tools.manager.RoleManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

/**
 * Created by m-latifi on 11/13/2022.
 */

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val articleRepository: ArticleRepository,
    private val appUpdateRepository: AppUpdateRepository,
    private val roleManager: RoleManager
) : ZarViewModel() {

    private val _articleList = MutableStateFlow<ResponseResult<List<ArticleEntity>>>(ResponseResult.Loading(false))
    val articleList: StateFlow<ResponseResult<List<ArticleEntity>>> = _articleList

    private val _isNewVersion = MutableStateFlow(false)
    val isNewVersion: StateFlow<Boolean> = _isNewVersion


    init {
        checkVersionIsNew()
    }


    //---------------------------------------------------------------------------------------------- checkVersionIsNew
    private fun checkVersionIsNew() {
        viewModelScope.launch(IO + exceptionHandler()) {
            appUpdateRepository.checkVersionIsNew().collect {
                _isNewVersion.value = it
            }
        }
    }
    //---------------------------------------------------------------------------------------------- checkVersionIsNew



    //---------------------------------------------------------------------------------------------- requestGetArticles
    fun requestGetArticles() {
        viewModelScope.launch(IO + exceptionHandler()) {
            articleRepository.requestGetArticles().collect {networkResult ->
                checkResponse(networkResult = networkResult) {
                    _articleList.value = ResponseResult.Success(it)
                }
                delay(1000)
                _isNewVersion.value = false
            }
        }
    }
    //---------------------------------------------------------------------------------------------- requestGetArticles



    //---------------------------------------------------------------------------------------------- getApp
    fun getApp(): MutableList<AppModel> {
        val apps: MutableList<AppModel> = mutableListOf()
        if (roleManager.isAccessBusRegisterAdd())
            apps.add(
                AppModel(
                    R.drawable.a_ic_bus,
                    resourcesProvider.getString(R.string.tripAndMap),
                    R.id.action_goto_BusServiceFragment
                )
            )
        if (roleManager.isAccessTaxiRequestAdd())
            apps.add(
                AppModel(
                    R.drawable.a_ic_taxi,
                    resourcesProvider.getString(R.string.taxiReservation),
                    R.id.action_goto_taxiReservationProFragment
                )
            )
        apps.add(
                AppModel(
                    R.drawable.a_ic_hse2,
                    resourcesProvider.getString(R.string.hse),
                    R.id.action_goto_hsePeriodFragment
                )
            )
        apps.add(
            AppModel(
                R.drawable.a_ic_food,
                resourcesProvider.getString(R.string.foodReservation),
                0
            )
        )
        apps.add(
            AppModel(
                R.drawable.a_ic_conference_room,
                resourcesProvider.getString(R.string.ceremonies),
                0
            )
        )

        apps.add(
            AppModel(
                R.drawable.a_ic_shopping_cart,
                resourcesProvider.getString(R.string.personalPurchase),
                0
            )
        )

        return apps
    }
    //---------------------------------------------------------------------------------------------- getApp


    //---------------------------------------------------------------------------------------------- getPersonnelRequest
    fun getPersonnelRequest(): MutableList<AppModel> {
        val apps: MutableList<AppModel> = mutableListOf()
        if (roleManager.isAccessPlaqueView())
            apps.add(
                AppModel(
                    R.drawable.a_ic_parking,
                    resourcesProvider.getString(R.string.parking),
                    R.id.action_goto_parkingFragment
                )
            )

        if (roleManager.isAccessEducationView())
            apps.add(
                AppModel(
                    R.drawable.a_ic_library,
                    resourcesProvider.getString(R.string.learningCenter),
                    R.id.action_goto_educationCategoryFragment
                )
            )
        apps.add(
            AppModel(
                R.drawable.a_ic_home_gift,
                resourcesProvider.getString(R.string.personnelQuota),
                R.id.action_goto_personnelQuotaFragment
            )
        )
/*        apps.add(
            AppModel(
                R.drawable.a_ic_credit,
                resourcesProvider.getString(R.string.giftCard),
                0
            )
        )*/

        apps.add(
            AppModel(
                R.drawable.a_ic_insurance,
                resourcesProvider.getString(R.string.supplementaryInsurance),
                R.id.action_goto_insuranceSignupFragment
            )
        )

        apps.add(
            AppModel(
                R.drawable.a_ic_personnel_list,
                resourcesProvider.getString(R.string.personnelList),
                0//R.id.action_goto_personnelFragment
            )
        )
        return apps
    }
    //---------------------------------------------------------------------------------------------- getPersonnelRequest


}