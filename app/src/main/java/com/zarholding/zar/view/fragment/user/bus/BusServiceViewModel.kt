package com.zarholding.zar.view.fragment.user.bus

import androidx.lifecycle.viewModelScope
import com.zarholding.zar.model.data.request.RequestRegisterStationModel
import com.zarholding.zar.model.data.response.trip.TripModel
import com.zarholding.zar.model.repository.TripRepository
import com.zarholding.zar.ZarViewModel
import com.zarholding.zar.model.data.response.ResponseResult
import com.zarholding.zar.model.data.response.dropdown.DropDownModel
import com.zarholding.zar.model.repository.DropdownRepository
import com.zarholding.zar.model.repository.TokenRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

/**
 * Created by m-latifi on 11/19/2022.
 */

@HiltViewModel
class BusServiceViewModel @Inject constructor(
    private val tripRepository: TripRepository,
    private val tokenRepository: TokenRepository,
    private val dropdownRepository: DropdownRepository
) : ZarViewModel() {

    var companyCode: Int = -1
    var searchText: String = ""

    private val _allTrip =
        MutableStateFlow<ResponseResult<List<TripModel>>>(ResponseResult.Loading(false))
    val allTrip: StateFlow<ResponseResult<List<TripModel>>> = _allTrip

    private val _myTrip =
        MutableStateFlow<ResponseResult<List<TripModel>>>(ResponseResult.Loading(false))
    val myTrip: StateFlow<ResponseResult<List<TripModel>>> = _myTrip


    var companiesList = emptyList<DropDownModel>()
    private val _getCompanies =
        MutableStateFlow<ResponseResult<Boolean>>(ResponseResult.Loading(false))
    val getCompanies: StateFlow<ResponseResult<Boolean>> = _getCompanies


    init {
        requestGetCompanies()
    }

    //---------------------------------------------------------------------------------------------- getTripList
    fun getTripList(
        refreshForce: Boolean = false,
        isMyTrip: Boolean
    ) {
        viewModelScope.launch(IO + exceptionHandler()) {
            _myTrip.value = ResponseResult.Loading(true)
            _allTrip.value = ResponseResult.Loading(true)
            delay(250)
            tripRepository.getTrip(
                refreshForce = refreshForce,
                isMyTrip = isMyTrip,
                siteId = companyCode,
                searchText = searchText
            ).collect { networkResult ->
                checkResponse(networkResult = networkResult) {
                    if (isMyTrip) {
                        _myTrip.value = ResponseResult.Success(it)
                    } else {
                        _allTrip.value = ResponseResult.Success(it)
                    }
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- getTripList


    //---------------------------------------------------------------------------------------------- requestGetCompanies
    private fun requestGetCompanies() {
        viewModelScope.launch(IO + exceptionHandler()) {
            dropdownRepository.requestGetSites().collect { networkResult ->
                checkResponse(networkResult = networkResult) {
                    companiesList = it
                    _getCompanies.value = ResponseResult.Success(true)
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- requestGetCompanies


    //---------------------------------------------------------------------------------------------- requestRegisterStation
    fun requestRegisterStation(tripId: Int, stationId: Int) {
        viewModelScope.launch(IO + exceptionHandler()) {
            val requestModel = RequestRegisterStationModel(tripId, stationId)
            callApi(
                request = tripRepository.requestRegisterStation(requestModel),
                onReceiveData = {
                    getTripList(
                        refreshForce = true,
                        isMyTrip = true
                    )
                }
            )
        }
    }
    //---------------------------------------------------------------------------------------------- requestRegisterStation


    //---------------------------------------------------------------------------------------------- requestDeleteRegisteredStation
    fun requestDeleteRegisteredStation(id: Int) {
        viewModelScope.launch(IO + exceptionHandler()) {
            callApi(
                request = tripRepository.requestDeleteRegisteredStation(id),
                onReceiveData = {
                    getTripList(
                        refreshForce = true,
                        isMyTrip = true
                    )
                }
            )
        }
    }
    //---------------------------------------------------------------------------------------------- requestDeleteRegisteredStation


    //---------------------------------------------------------------------------------------------- getToken
    fun getToken() = tokenRepository.getToken()
    //---------------------------------------------------------------------------------------------- getToken

}