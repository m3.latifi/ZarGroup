package com.zarholding.zar.view.fragment.user.food.reservation

import com.zarholding.zar.ZarViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

/**
 * Created by m-latifi on 10/11/2023.
 */

@HiltViewModel
class FoodReservationViewModel @Inject constructor(
) : ZarViewModel() {

}