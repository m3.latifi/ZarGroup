package com.zarholding.zar.view.fragment.user.taxi.pro.component

import android.content.Context
import android.util.AttributeSet
import android.util.Size
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.result.ActivityResultLauncher
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.zar.core.tools.manager.PermissionManager
import com.zarholding.zar.R
import com.zarholding.zar.model.data.response.taxi.TaxiFavPlaceModel
import com.zarholding.zar.model.data.taxi.TaxiGoAddressModel
import com.zarholding.zar.tools.manager.OsmManager
import com.zarholding.zar.tools.manager.ZarLocationManager
import com.zarholding.zar.view.adapter.recycler.taxi.TaxiFavAdapter
import com.zarholding.zar.view.dialog.address.SearchAddressDialog
import dagger.hilt.android.AndroidEntryPoint
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import javax.inject.Inject

/**
 * Created by m-latifi on 10/22/2023.
 */

@AndroidEntryPoint
class TaxiMapView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {


    interface TaxiMapInterface {
        fun showAlert(message: String)
        fun onClickCenterMarker(center: GeoPoint, mapType: TaxiMapType, tripType: TaxiTripView.EnumTripType)
        fun onSelectFavLocation(
            fav: TaxiFavPlaceModel,
            mapType: TaxiMapType,
            tripType: TaxiTripView.EnumTripType
        )
    }


/*    enum class TripTypeEnum {
        Go,
        Return
    }*/

    enum class TaxiMapType {
        Origin,
        Destination
    }

    @Inject
    lateinit var locationManager: ZarLocationManager

    @Inject
    lateinit var permissionManager: PermissionManager


    var listenr: TaxiMapInterface? = null
    var manager: FragmentManager? = null
    var locationPermissionLauncher: ActivityResultLauncher<Array<String>>? = null
    private var mapType = TaxiMapType.Origin
    private var tripType = TaxiTripView.EnumTripType.Go
    private var mapView: MapView
    private var osmManager: OsmManager
    private var imageViewMarker: ImageView
    private var imageViewMyLocation: ImageView
    private var imageViewSearch: ImageView
    private var textViewLoading: TextView
    private var cardViewFav: CardView
    private var recyclerViewFav: RecyclerView
    private var lastCenterGeoPoint = GeoPoint(35.905761706210, 50.8194283136846)


    //---------------------------------------------------------------------------------------------- init
    init {
        View.inflate(context, R.layout.layout_taxi_map, this)
        mapView = findViewById(R.id.mapView)
        osmManager = OsmManager(mapView)
        osmManager.mapInitialize()
        imageViewMarker = findViewById(R.id.imageViewMarker)
        imageViewMyLocation = findViewById(R.id.imageViewMyLocation)
        imageViewSearch = findViewById(R.id.imageViewSearch)
        textViewLoading = findViewById(R.id.textViewLoading)
        cardViewFav = findViewById(R.id.cardViewFav)
        recyclerViewFav = findViewById(R.id.recyclerViewFav)

        imageViewMarker.setImageResource(R.drawable.a_ic_marker)
        imageViewMarker.visibility = View.GONE
        imageViewSearch.visibility = View.GONE
        cardViewFav.visibility = View.GONE
        setListener()
    }
    //---------------------------------------------------------------------------------------------- init


    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener() {
        imageViewMyLocation.setOnClickListener { findMyLocation() }
        imageViewSearch.setOnClickListener { showDialogSearchAddress() }
        imageViewMarker.setOnClickListener { getCenterLocationOfMap() }
    }
    //---------------------------------------------------------------------------------------------- setListener


    //---------------------------------------------------------------------------------------------- showDialogSearchAddress
    private fun showDialogSearchAddress() {
        manager?.let {
            SearchAddressDialog { item ->
                osmManager.moveCamera(GeoPoint(item.lat, item.lon))
            }.show(it, "search address")
        }
    }
    //---------------------------------------------------------------------------------------------- showDialogSearchAddress


    //---------------------------------------------------------------------------------------------- checkLocationPermission
    fun checkLocationPermission(results: Map<String, Boolean>) {
        permissionManager.checkPermissionResult(results) {
            if (it)
                findMyLocation()
        }
    }
    //---------------------------------------------------------------------------------------------- checkLocationPermission


    //---------------------------------------------------------------------------------------------- findMyLocation
    private fun findMyLocation() {
        if (locationPermissionLauncher != null)
            locationManager.getCurrentLocation(
                launcher = locationPermissionLauncher!!,
                onFindCurrentLocation = {
                    val current = GeoPoint(it.latitude, it.longitude)
                    osmManager.moveCamera(current)
                },
                onFailedMessage = { listenr?.showAlert(it) }
            )
    }
    //---------------------------------------------------------------------------------------------- findMyLocation


    //---------------------------------------------------------------------------------------------- getCenterLocationOfMap
    private fun getCenterLocationOfMap() {
        resetMapView()
        setLastLocation(GeoPoint(mapView.mapCenter.latitude, mapView.mapCenter.longitude))
        listenr?.onClickCenterMarker(
            center = lastCenterGeoPoint,
            mapType = mapType,
            tripType = tripType
        )
    }
    //---------------------------------------------------------------------------------------------- getCenterLocationOfMap


    //---------------------------------------------------------------------------------------------- resetMapView
    fun resetMapView() {
        imageViewMarker.visibility = View.GONE
        imageViewSearch.visibility = View.GONE
        cardViewFav.visibility = View.GONE
        textViewLoading.visibility = View.GONE
    }
    //---------------------------------------------------------------------------------------------- resetMapView


    //---------------------------------------------------------------------------------------------- setLastLocation
    private fun setLastLocation(geoPoint: GeoPoint) {
        lastCenterGeoPoint = geoPoint
        textViewLoading.visibility = View.VISIBLE
    }
    //---------------------------------------------------------------------------------------------- setLastLocation


    //---------------------------------------------------------------------------------------------- showCenterMarker
    fun showCenterMarker(mapType: TaxiMapType, tripType: TaxiTripView.EnumTripType) {
        this.tripType = tripType
        this.mapType = mapType
        val center =
            GeoPoint(lastCenterGeoPoint.latitude + 0.0002, lastCenterGeoPoint.longitude + 0.0002)
        osmManager.moveCamera(center)
        imageViewMarker.visibility = View.VISIBLE
        imageViewSearch.visibility = View.VISIBLE
        if (recyclerViewFav.adapter != null)
            cardViewFav.visibility = View.VISIBLE
    }
    //---------------------------------------------------------------------------------------------- showCenterMarker


    //---------------------------------------------------------------------------------------------- clearOverlays
    fun clearOverlays() {
        osmManager.clearOverlays()
    }
    //---------------------------------------------------------------------------------------------- clearOverlays


    //---------------------------------------------------------------------------------------------- successChangeAddress
    fun successChangeAddress(taxiGoAddressModel: TaxiGoAddressModel) {
        clearOverlays()
        val points = mutableListOf<GeoPoint>()
        for (origin in taxiGoAddressModel.origins) {
            points.add(origin.geoPoint)
            addMarkerOrigin(origin.geoPoint)
        }
        for (destination in taxiGoAddressModel.destinations) {
            points.add(destination.geoPoint)
            addMarkerDestination(destination.geoPoint)
        }
        if (points.isNotEmpty())
            osmManager.zoomWithBoundingBox(points)
    }
    //---------------------------------------------------------------------------------------------- successChangeAddress


    //---------------------------------------------------------------------------------------------- setFavList
    fun setFavList(items: List<TaxiFavPlaceModel>) {
        if (context == null)
            return
        val adapter = TaxiFavAdapter(items) {
            resetMapView()
            setLastLocation(GeoPoint(it.lat, it.long))
            listenr?.onSelectFavLocation(
                fav = it,
                mapType = mapType,
                tripType = tripType
            )
        }
        val manager = LinearLayoutManager(
            context,
            LinearLayoutManager.HORIZONTAL,
            true
        )
        recyclerViewFav.adapter = adapter
        recyclerViewFav.layoutManager = manager
    }
    //---------------------------------------------------------------------------------------------- setFavList


    //---------------------------------------------------------------------------------------------- addMarkerOrigin
    private fun addMarkerOrigin(geoPoint: GeoPoint) {
        textViewLoading.visibility = View.GONE
        val icon = osmManager.createMarkerIconDrawable(
            Size(100, 135),
            R.drawable.a_ic_origin
        )
        osmManager.addMarker(
            icon,
            geoPoint,
            null
        )
    }
    //---------------------------------------------------------------------------------------------- addMarkerOrigin


    //---------------------------------------------------------------------------------------------- addMarkerDestination
    private fun addMarkerDestination(geoPoint: GeoPoint) {
        textViewLoading.visibility = View.GONE
        val icon = osmManager.createMarkerIconDrawable(
            Size(100, 135),
            R.drawable.a_ic_destination
        )
        osmManager.addMarker(
            icon,
            geoPoint,
            null
        )
    }
    //---------------------------------------------------------------------------------------------- addMarkerDestination
}