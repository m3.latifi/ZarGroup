package com.zarholding.zar.view.fragment.user.survey

import androidx.core.text.isDigitsOnly
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.zarholding.zar.R
import com.zarholding.zar.ZarViewModel
import com.zarholding.zar.model.data.enum.EnumSurveyQuestionType
import com.zarholding.zar.model.data.request.survey.SubmitSurveyQuestionRequestModel
import com.zarholding.zar.model.data.response.survey.SurveyModel
import com.zarholding.zar.model.data.response.survey.SurveyQuestionModel
import com.zarholding.zar.model.repository.SurveyRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Create by Mehrdad on 1/21/2023
 */
@HiltViewModel
class SurveyViewModel @Inject constructor(
    private val surveyRepository: SurveyRepository
) : ZarViewModel() {

    val surveysLiveData: MutableLiveData<List<SurveyModel>> by lazy {
        MutableLiveData<List<SurveyModel>>()
    }
    val surveyDetailLiveData: MutableLiveData<List<SurveyQuestionModel>> by lazy {
        MutableLiveData<List<SurveyQuestionModel>>()
    }

    val surveySubmit: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>()
    }

    private var surveyId: Long? = null

    //---------------------------------------------------------------------------------------------- getSurveys
    fun getSurveys() {
        viewModelScope.launch(IO + exceptionHandler()) {
            callApi(
                request = surveyRepository.requestGetUserSurveys(),
                onReceiveData = { surveyItem ->
                    surveyItem.items?.let { surveysLiveData.postValue(it) }
                }
            )
        }
    }
    //---------------------------------------------------------------------------------------------- getSurveys


    //---------------------------------------------------------------------------------------------- requestGetSurveyDetail
    fun requestGetSurveyDetail(position: Int) {
        viewModelScope.launch(IO + exceptionHandler()) {
            surveyId = surveysLiveData.value?.get(position)?.id
            if (surveyId != null)
                callApi(
                    request = surveyRepository.requestGetSurveyDetail(surveyId!!),
                    onReceiveData = { surveyDetail ->
                        surveyDetail.questions?.let {
                            surveyDetailLiveData.postValue(it)
                        } ?: run {
                            setMessage(resourcesProvider.getString(R.string.dataReceivedIsEmpty))
                        }
                    }
                )
        }
    }
    //---------------------------------------------------------------------------------------------- requestGetSurveyDetail


    //---------------------------------------------------------------------------------------------- submitSurvey
    fun submitSurvey() {
        viewModelScope.launch(IO + exceptionHandler()) {
            if (surveyId == null)
                return@launch
            val answers = mutableListOf<SubmitSurveyQuestionRequestModel>()
            surveyDetailLiveData.value?.let { questions ->
                for (index in questions.indices)
                    if (checkValidationAnswerByIndex(index)) {
                        val question = questions[index]
                        answers.add(getAnswer(question = question))

                    } else {
                        setMessage(
                            resourcesProvider.getString(R.string.answerTheSurveyQuestionsCompletely)
                        )
                        break
                    }
                if (questions.size == answers.size)
                    callApi(
                        request = surveyRepository.requestSubmitSurvey(answers),
                        showMessageAfterSuccessResponse = true,
                        onReceiveData = { surveySubmit.postValue(it) }
                    )
            }
        }
    }
    //---------------------------------------------------------------------------------------------- submitSurvey


    //---------------------------------------------------------------------------------------------- getAnswer
    private fun getAnswer(question: SurveyQuestionModel): SubmitSurveyQuestionRequestModel {
        when (question.questionType) {
            EnumSurveyQuestionType.singlelinettext,
            EnumSurveyQuestionType.multilinetext ->
                return SubmitSurveyQuestionRequestModel(
                    surveyId = surveyId!!,
                    questionId = question.questionId,
                    objectIds = null,
                    additionalText = null,
                    text = question.answer,
                    numericAnswer = 0,
                    selectedDate = null,
                    selectedTime = null
                )

            EnumSurveyQuestionType.choicesingleselect_dropdown,
            EnumSurveyQuestionType.choicesingleselect_radio,
            EnumSurveyQuestionType.choicemultipleselect,
            EnumSurveyQuestionType.yesno -> {
                val optionIds = if (question.selectedAnswerId != null)
                    listOf(question.selectedAnswerId!!)
                else
                    question.selectedAnswersId?.toList()
                return SubmitSurveyQuestionRequestModel(
                    surveyId = surveyId!!,
                    questionId = question.questionId,
                    objectIds = optionIds,
                    additionalText = question.answer,
                    text = null,
                    numericAnswer = 0,
                    selectedDate = null,
                    selectedTime = null
                )
            }

            EnumSurveyQuestionType.numericrange ->
                return SubmitSurveyQuestionRequestModel(
                    surveyId = surveyId!!,
                    questionId = question.questionId,
                    objectIds = null,
                    additionalText = null,
                    text = question.answer,
                    numericAnswer = 0,
                    selectedDate = null,
                    selectedTime = null
                )

            EnumSurveyQuestionType.date ->
                return SubmitSurveyQuestionRequestModel(
                    surveyId = surveyId!!,
                    questionId = question.questionId,
                    objectIds = null,
                    additionalText = null,
                    text = null,
                    numericAnswer = 0,
                    selectedDate = question.answer?.replace("/", ""),
                    selectedTime = null
                )

            EnumSurveyQuestionType.time ->
                return SubmitSurveyQuestionRequestModel(
                    surveyId = surveyId!!,
                    questionId = question.questionId,
                    objectIds = null,
                    additionalText = null,
                    text = null,
                    numericAnswer = 0,
                    selectedDate = null,
                    selectedTime = question.answer
                )

            EnumSurveyQuestionType.satisfaction -> {
                val optionIds = if (question.selectedAnswerId != null)
                    listOf(question.selectedAnswerId!!)
                else
                    question.selectedAnswersId?.toList()
                return SubmitSurveyQuestionRequestModel(
                    surveyId = surveyId!!,
                    questionId = question.questionId,
                    objectIds = optionIds,
                    additionalText = question.answer,
                    text = null,
                    numericAnswer = 0,
                    selectedDate = null,
                    selectedTime = null
                )
            }

            EnumSurveyQuestionType.numeric -> {
                val numericAnswer =
                    if (!question.answer.isNullOrEmpty() && question.answer!!.isDigitsOnly())
                        question.answer!!.toInt() else 0
                return SubmitSurveyQuestionRequestModel(
                    surveyId = surveyId!!,
                    questionId = question.questionId,
                    objectIds = null,
                    additionalText = null,
                    text = null,
                    numericAnswer = numericAnswer,
                    selectedDate = null,
                    selectedTime = null
                )
            }
        }
    }
    //---------------------------------------------------------------------------------------------- getAnswer


    //---------------------------------------------------------------------------------------------- checkValidationAnswerByIndex
    fun checkValidationAnswerByIndex(index: Int): Boolean {
        val item = surveyDetailLiveData.value?.get(index)
        item?.let { question ->
            if (!question.isRequired)
                return true
            if (question.selectedAnswerId == null &&
                (question.selectedAnswersId == null || question.selectedAnswersId?.size == 0) &&
                question.answer == null
            ) return false

            if (!question.options.isNullOrEmpty() && question.selectedAnswerId != null) {
                val optionSelected = question.options
                    .findLast { select -> select.questionOptionId == question.selectedAnswerId }
                optionSelected?.let { option ->
                    if (option.hasAdditionalText)
                        return !question.answer.isNullOrEmpty()
                    else return true
                } ?: run { return false }
            } else return true
        } ?: run { return false }
    }
    //---------------------------------------------------------------------------------------------- checkValidationAnswerByIndex


}