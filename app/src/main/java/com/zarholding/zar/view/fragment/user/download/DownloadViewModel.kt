package com.zarholding.zar.view.fragment.user.download

/**
 * Create by Mehrdad on 1/16/2023
 */

import android.os.Environment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.zarholding.zar.ZarViewModel
import com.zarholding.zar.model.data.enum.EnumEntityType
import com.zarholding.zar.model.repository.DownloadFileRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import okhttp3.*
import java.io.File
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import javax.inject.Inject

@HiltViewModel
class DownloadViewModel @Inject constructor(
    private val downloadFileRepository: DownloadFileRepository
) : ZarViewModel() {

    private var destinationFile: File? = null
    val downloadProgress: MutableLiveData<Int> by lazy { MutableLiveData<Int>() }
    val downloadSuccessLiveData: MutableLiveData<File> by lazy { MutableLiveData<File>() }


    //---------------------------------------------------------------------------------------------- init
    private fun initDestinationDir(type: EnumEntityType) {
        val extension = when (type) {
            EnumEntityType.APK -> "apk"
            EnumEntityType.insurance,
            EnumEntityType.HSE -> "pdf"

            else -> "pdf"
        }
        val downloadFolder =
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
        if (!downloadFolder.exists())
            downloadFolder.mkdir()
        val destinationDir = File(downloadFolder.absolutePath, "Zar")
        if (!destinationDir.exists())
            destinationDir.mkdir()
        val destinationApkDir = File(destinationDir, type.name)
        if (!destinationApkDir.exists())
            destinationApkDir.mkdir()
        destinationFile = File(
            destinationApkDir.absolutePath,
            "Zar_${getNewFileName()}.${extension}"
        )

    }
    //---------------------------------------------------------------------------------------------- init


    //______________________________________________________________________________________________ getNewFileName
    private fun getNewFileName(): String? {
        return SimpleDateFormat("yyyy_MM_dd__HH_mm_ss", Locale.getDefault()).format(Date())
    }
    //______________________________________________________________________________________________ getNewFileName


    //---------------------------------------------------------------------------------------------- downloadFile
    fun downloadFile(fileName: String, type: EnumEntityType) {
        viewModelScope.launch(IO) {
            initDestinationDir(type)
            delay(500)
            if (destinationFile?.exists() == true) {
                destinationFile?.deleteRecursively()
                destinationFile?.delete()
            }
            delay(500)
            when (type) {
                EnumEntityType.APK -> downloadApk(fileName, type)
                EnumEntityType.insurance,
                EnumEntityType.HSE -> downloadContentFile(fileName, type)

                else -> {}
            }
        }
    }
    //---------------------------------------------------------------------------------------------- downloadFile


    //---------------------------------------------------------------------------------------------- downloadApk
    private fun downloadApk(fileName: String, type: EnumEntityType) {
        viewModelScope.launch(IO + exceptionHandler()) {
            downloadFileRepository.downloadApkFile(fileName, type)
                .body()?.saveFile()?.collect { downloadState ->
                    when (downloadState) {
                        is DownloadState.Downloading -> {
                            downloadProgress.postValue(downloadState.progress)
                        }

                        is DownloadState.Failed -> {
                            setMessage(downloadState.error?.message ?: "Failed Download!")
                        }

                        DownloadState.Finished -> {
                            downloadSuccessLiveData.postValue(destinationFile)
                        }
                    }
                }
        }
    }
    //---------------------------------------------------------------------------------------------- downloadApk


    //---------------------------------------------------------------------------------------------- downloadContentFile
    private fun downloadContentFile(fileName: String, type: EnumEntityType) {
        viewModelScope.launch(IO + exceptionHandler()) {
            val res = downloadFileRepository.downloadContentFile(fileName, type)
            if (res.errorBody() != null && res.body() == null) {
                setMessage(res.errorBody()!!.string())
            } else
                res.body()?.saveFile()?.collect { downloadState ->
                    when (downloadState) {
                        is DownloadState.Downloading -> {
                            downloadProgress.postValue(downloadState.progress)
                        }

                        is DownloadState.Failed -> {
                            setMessage(downloadState.error?.message ?: "Failed Download!")
                        }

                        DownloadState.Finished -> {
                            downloadSuccessLiveData.postValue(destinationFile)
                        }
                    }
                }
        }
    }
    //---------------------------------------------------------------------------------------------- downloadContentFile


    //---------------------------------------------------------------------------------------------- DownloadState
    private sealed class DownloadState {
        data class Downloading(val progress: Int) : DownloadState()
        object Finished : DownloadState()
        data class Failed(val error: Throwable? = null) : DownloadState()
    }
    //---------------------------------------------------------------------------------------------- DownloadState


    //---------------------------------------------------------------------------------------------- saveFile
    private fun ResponseBody.saveFile(): Flow<DownloadState> {
        return flow {
            emit(DownloadState.Downloading(0))
            try {
                byteStream().use { inputStream ->
                    destinationFile?.outputStream().use { outputStream ->
                        val totalBytes = contentLength()
                        val buffer = ByteArray(DEFAULT_BUFFER_SIZE)
                        var progressBytes = 0L

                        var bytes = inputStream.read(buffer)
                        while (bytes >= 0) {
                            outputStream?.write(buffer, 0, bytes)
                            progressBytes += bytes
                            bytes = inputStream.read(buffer)
                            emit(DownloadState.Downloading(((progressBytes * 100) / totalBytes).toInt()))
                        }
                    }
                }
                emit(DownloadState.Finished)
            } catch (e: Exception) {
                emit(DownloadState.Failed(e))
            }
        }
            .flowOn(IO)
            .distinctUntilChanged()
    }
    //---------------------------------------------------------------------------------------------- saveFile


}