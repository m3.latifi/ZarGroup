package com.zarholding.zar.view.adapter.recycler.insurance

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemInsuranceRelationshipBinding
import com.zarholding.zar.model.data.request.insurance.InsuranceRelationShipModel
import com.zarholding.zar.view.adapter.holder.insurance.InsuranceRelationShipHolder

/**
 * Created by m-latifi on 10/15/2023.
 */

class InsuranceRelationAdapter(
    private val items: List<InsuranceRelationShipModel>,
    private val onDelete: (InsuranceRelationShipModel) -> Unit
): RecyclerView.Adapter<InsuranceRelationShipHolder>() {

    private var inflater: LayoutInflater? = null

    //---------------------------------------------------------------------------------------------- onCreateViewHolder
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): InsuranceRelationShipHolder {
        if (inflater == null)
            inflater = LayoutInflater.from(parent.context)
        return InsuranceRelationShipHolder(
            ItemInsuranceRelationshipBinding.inflate(inflater!!,parent,false),
            onDelete
        )
    }
    //---------------------------------------------------------------------------------------------- onCreateViewHolder


    //---------------------------------------------------------------------------------------------- onBindViewHolder
    override fun onBindViewHolder(holder: InsuranceRelationShipHolder, position: Int) {
        holder.bind(item = items[position])
    }
    //---------------------------------------------------------------------------------------------- onBindViewHolder


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun getItemCount() = items.size
    //---------------------------------------------------------------------------------------------- getItemCount

}