package com.zarholding.zar.view.adapter.holder.taxi

import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.R
import com.zarholding.zar.databinding.ItemTaxiAddressBinding
import com.zarholding.zar.ext.getAddress
import com.zarholding.zar.model.data.response.address.AddressModel

/**
 * Created by m-latifi on 10/24/2023.
 */

class TaxiAddressHolder(
    private val binding: ItemTaxiAddressBinding,
    private val onFavDeleteClick: (Int) -> Unit,
    private val onFavAddClick: (AddressModel, Int) -> Unit,
    private val onDelete: (Int) -> Unit
): RecyclerView.ViewHolder(binding.root) {


    //---------------------------------------------------------------------------------------------- bind
    fun bind(addressModel: AddressModel, position: Int) {
        binding.textViewAddress.isSelected = true
        addressModel.favId?.let { favId ->
            binding.textViewAddress.text =  addressModel.addressTitle
            binding.imageViewFav.setImageResource(R.drawable.a_ic_favorite)
            binding.imageViewFav.setOnClickListener {
                onFavDeleteClick.invoke(favId)
            }
        } ?: run {
            binding.textViewAddress.text = addressModel.getAddress()
            binding.imageViewFav.setImageResource(R.drawable.a_ic_favorite_outline)
            binding.imageViewFav.setOnClickListener {
                onFavAddClick.invoke(addressModel, position)
            }
        }

        binding.imageViewDelete.setOnClickListener { onDelete.invoke(position) }
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind

}