package com.zarholding.zar.view.fragment.user.setting

import android.content.Intent
import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.zar.core.enums.EnumApiError
import com.zar.core.tools.manager.DeviceManager
import com.zar.core.tools.manager.ThemeManager
import com.zarholding.zar.R
import com.zarholding.zar.ZarFragment
import com.zarholding.zar.databinding.FragmentSettingBinding
import com.zarholding.zar.tools.manager.BiometricManager
import dagger.hilt.android.AndroidEntryPoint
import ir.agaring.animationswitch.EnumSwitchPosition
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by m-latifi on 11/19/2022.
 */

@AndroidEntryPoint
class SettingFragment(override var layout: Int = R.layout.fragment_setting) :
    ZarFragment<FragmentSettingBinding>() {

    private val viewModel: SettingViewModel by viewModels()

    @Inject
    lateinit var themeManagers: ThemeManager

    @Inject
    lateinit var deviceManager: DeviceManager

    @Inject
    lateinit var biometricManager: BiometricManager

    private var job: Job? = null

    //---------------------------------------------------------------------------------------------- onViewCreated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        setOnListener()

    }
    //---------------------------------------------------------------------------------------------- onViewCreated


    //---------------------------------------------------------------------------------------------- initView
    private fun initView() {
        job = CoroutineScope(Main).launch {
            delay(100)
            when (themeManagers.applicationTheme()) {
                Configuration.UI_MODE_NIGHT_YES -> binding.dayNightSwitch.setPosition(
                    position = EnumSwitchPosition.RIGHT,
                    isAnimated = false
                )

                Configuration.UI_MODE_NIGHT_NO -> binding.dayNightSwitch.setPosition(
                    position = EnumSwitchPosition.LEFT,
                    isAnimated = false
                )
            }

            binding
                .layoutActiveFingerPrint
                .switchActive
                .isChecked = viewModel.isBiometricEnable()
        }

        binding.layoutAppVersion.title =
            "${getString(R.string.appVersion)} : ${deviceManager.appVersionName()}"
    }
    //---------------------------------------------------------------------------------------------- initView


    //---------------------------------------------------------------------------------------------- setOnListener
    private fun setOnListener() {
        binding.dayNightSwitch.setOnSwitchListener { changeAppTheme() }
        binding.layoutActiveFingerPrint.switchActive.setOnClickListener { showBiometricDialog() }
        binding.layoutChangePass.root.setOnClickListener { disableFeature() }
        binding.buttonSendTicket.setOnClickListener { disableFeature() }
        binding.textViewMobile.setOnClickListener { openCall() }
    }
    //---------------------------------------------------------------------------------------------- setOnListener


    //---------------------------------------------------------------------------------------------- changeAppTheme
    private fun changeAppTheme() {
        when (binding.dayNightSwitch.currentPosition()) {
            EnumSwitchPosition.RIGHT ->
                themeManagers.changeApplicationTheme(Configuration.UI_MODE_NIGHT_YES)

            EnumSwitchPosition.LEFT ->
                themeManagers.changeApplicationTheme(Configuration.UI_MODE_NIGHT_NO)
        }
    }
    //---------------------------------------------------------------------------------------------- changeAppTheme


    //---------------------------------------------------------------------------------------------- showBiometricDialog
    private fun showBiometricDialog() {
        if (activity == null)
            return
        val error = biometricManager.showBiometricDialog(
            fragment = requireActivity(),
            onAuthenticationError = {
                showMessage(getString(R.string.onAuthenticationError))
                binding
                    .layoutActiveFingerPrint
                    .switchActive
                    .isChecked = viewModel.isBiometricEnable()
            },
            onAuthenticationFailed = {
                binding
                    .layoutActiveFingerPrint
                    .switchActive
                    .isChecked = viewModel.isBiometricEnable()
            },
            onAuthenticationSucceeded = {
                val biometric = !viewModel.isBiometricEnable()
                binding
                    .layoutActiveFingerPrint
                    .switchActive
                    .isChecked = biometric
                viewModel.changeBiometricEnable()
                showMessage(getString(R.string.actionIsDone), EnumApiError.Done)
            }
        )
        if (!error.isNullOrEmpty()) {
            showMessage(error)
            binding
                .layoutActiveFingerPrint
                .switchActive
                .isChecked = viewModel.isBiometricEnable()
        }
    }
    //---------------------------------------------------------------------------------------------- showBiometricDialog


    //---------------------------------------------------------------------------------------------- disableFeature
    private fun disableFeature() {
        showMessage(getString(R.string.disableFeature))
    }
    //---------------------------------------------------------------------------------------------- disableFeature


    //---------------------------------------------------------------------------------------------- openCall
    private fun openCall() {
        val intent = Intent(
            Intent.ACTION_DIAL,
            Uri.fromParts("tel", "09367085703", null)
        )
        startActivity(intent)
    }
    //---------------------------------------------------------------------------------------------- openCall


    override fun onDestroyView() {
        super.onDestroyView()
        job?.cancel()
    }
}