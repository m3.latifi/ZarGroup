package com.zarholding.zar.view.adapter.recycler.taxi

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemTaxiAddressBinding
import com.zarholding.zar.model.data.response.address.AddressModel
import com.zarholding.zar.view.adapter.holder.taxi.TaxiAddressHolder

/**
 * Created by m-latifi on 10/24/2023.
 */

class TaxiAddressAdapter(
    private val items: List<AddressModel>,
    private val onFavDeleteClick: (Int) -> Unit,
    private val onFavAddClick: (AddressModel, Int) -> Unit,
    private val onDelete: (Int) -> Unit
) : RecyclerView.Adapter<TaxiAddressHolder>() {

    private var layoutInflater: LayoutInflater? = null

    //---------------------------------------------------------------------------------------------- onCreateViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaxiAddressHolder {
        if (layoutInflater == null)
            layoutInflater = LayoutInflater.from(parent.context)
        return TaxiAddressHolder(
            binding = ItemTaxiAddressBinding.inflate(layoutInflater!!, parent, false),
            onDelete = onDelete,
            onFavDeleteClick = onFavDeleteClick,
            onFavAddClick = onFavAddClick
        )
    }
    //---------------------------------------------------------------------------------------------- onCreateViewHolder


    //---------------------------------------------------------------------------------------------- onBindViewHolder
    override fun onBindViewHolder(holder: TaxiAddressHolder, position: Int) {
        holder.bind(items[position], position)
    }
    //---------------------------------------------------------------------------------------------- onBindViewHolder


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun getItemCount() = items.size
    //---------------------------------------------------------------------------------------------- getItemCount

}