package com.zarholding.zar.view.fragment.user.education.category

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.GridLayoutManager
import com.zar.core.enums.EnumApiError
import com.zarholding.zar.R
import com.zarholding.zar.ZarFragment
import com.zarholding.zar.databinding.FragmentEducationCategoryBinding
import com.zarholding.zar.ext.config
import com.zarholding.zar.ext.startLoading
import com.zarholding.zar.ext.stopLoading
import com.zarholding.zar.model.data.response.ResponseResult
import com.zarholding.zar.model.data.response.education.EducationCategoryModel
import com.zarholding.zar.tools.CompanionValues
import com.zarholding.zar.tools.manager.getShimmerBuild
import com.zarholding.zar.view.activity.MainActivity
import com.zarholding.zar.view.adapter.recycler.education.EducationCategoryAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

/**
 * Created by m-latifi on 7/26/2023.
 */

@AndroidEntryPoint
class EducationCategoryFragment(
    override var layout: Int = R.layout.fragment_education_category
) : ZarFragment<FragmentEducationCategoryBinding>() {

    private val viewModel: EducationCategoryViewModel by viewModels()

    //---------------------------------------------------------------------------------------------- onViewCreated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }
    //---------------------------------------------------------------------------------------------- onViewCreated


    //---------------------------------------------------------------------------------------------- initView
    private fun initView() {
        binding.shimmerViewContainer.config(getShimmerBuild())
        observeLiveDate()
        getEducationCategoryList()
    }
    //---------------------------------------------------------------------------------------------- initView


    //---------------------------------------------------------------------------------------------- observeLiveDate
    private fun observeLiveDate() {
        viewModel.errorLiveDate.observe(viewLifecycleOwner) {
            binding.shimmerViewContainer.stopLoading()
            showMessage(it.message, it.type)
            when (it.type) {
                EnumApiError.UnAuthorization -> (activity as MainActivity?)?.gotoFirstFragmentWithDeleteUser()
                else -> {}
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.category.collect {
                    when(it) {
                        is ResponseResult.Loading -> {}
                        is ResponseResult.Success -> {
                            binding.shimmerViewContainer.stopLoading()
                            setAdapter(it.data)
                        }
                    }
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- observeLiveDate


    //---------------------------------------------------------------------------------------------- getEducationCategoryList
    private fun getEducationCategoryList() {
        binding.shimmerViewContainer.startLoading()
        viewModel.getCategory()
    }
    //---------------------------------------------------------------------------------------------- getEducationCategoryList



    //---------------------------------------------------------------------------------------------- setAdapter
    private fun setAdapter(items: List<EducationCategoryModel>?){
        if (items.isNullOrEmpty()){
            binding.recyclerCategory.adapter = null
            return
        }
        val adapter = EducationCategoryAdapter(items){item ->
            selectCategory(item)
        }
        val gridLayoutManager = GridLayoutManager(
            requireContext(),
            3,
            GridLayoutManager.VERTICAL,
            false
        )
        binding.recyclerCategory.layoutManager = gridLayoutManager
        binding.recyclerCategory.layoutDirection = View.LAYOUT_DIRECTION_RTL
        binding.recyclerCategory.adapter = adapter
    }
    //---------------------------------------------------------------------------------------------- setAdapter



    //---------------------------------------------------------------------------------------------- selectCategory
    private fun selectCategory(item: EducationCategoryModel){
        val bundle = Bundle()
        bundle.putString(CompanionValues.EDUCATION_CAT_NAME, item.value)
        bundle.putLong(CompanionValues.EDUCATION_CAT_ID, item.id)
        gotoFragment(R.id.action_goto_educationFragment, bundle)
    }
    //---------------------------------------------------------------------------------------------- selectCategory

}