package com.zarholding.zar.view.fragment.admin.taxi

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.zarholding.zar.model.data.request.DriverChangeTripStatus
import com.zarholding.zar.model.data.request.TaxiChangeStatusRequest
import com.zarholding.zar.model.repository.TaxiRepository
import com.zarholding.zar.model.repository.UserRepository
import com.zarholding.zar.ZarViewModel
import com.zarholding.zar.model.data.enum.EnumAdminTaxiType
import com.zarholding.zar.model.data.enum.EnumPersonnelType
import com.zarholding.zar.model.data.enum.EnumTripStatus
import com.zarholding.zar.model.data.response.taxi.AdminTaxiRequestModel
import com.zarholding.zar.model.repository.TokenRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import javax.inject.Inject

/**
 * Created by m-latifi on 11/19/2022.
 */

@HiltViewModel
class AdminTaxiListViewModel @Inject constructor(
    private val taxiRepository: TaxiRepository,
    private val userRepository: UserRepository,
    private val tokenRepository: TokenRepository
) : ZarViewModel() {

    private lateinit var enumAdminTaxiType: EnumAdminTaxiType
    private val requestList: MutableList<AdminTaxiRequestModel> = mutableListOf()

    val taxiRequestListLiveData: MutableLiveData<List<AdminTaxiRequestModel>?> by lazy {
        MutableLiveData<List<AdminTaxiRequestModel>?>()
    }


    //---------------------------------------------------------------------------------------------- setEnumAdminTaxiTypeAndGetRequestTaxiList
    fun setEnumAdminTaxiTypeAndGetRequestTaxiList(type: String) {
        enumAdminTaxiType = try {
            enumValueOf(type)
        } catch (e: Exception) {
            EnumAdminTaxiType.MY
        }
        getRequestTaxiList()
    }
    //---------------------------------------------------------------------------------------------- setEnumAdminTaxiTypeAndGetRequestTaxiList


    //---------------------------------------------------------------------------------------------- getEnumAdminTaxiType
    fun getEnumAdminTaxiType() = enumAdminTaxiType
    //---------------------------------------------------------------------------------------------- getEnumAdminTaxiType


    //---------------------------------------------------------------------------------------------- getUserType
    fun getUserType() = userRepository.getUserType()
    //---------------------------------------------------------------------------------------------- getUserType


    //---------------------------------------------------------------------------------------------- forceGetRequestTaxiList
    fun forceGetRequestTaxiList() {
        requestList.clear()
        taxiRepository.request.PageNumber = 0
        getRequestTaxiList()
    }
    //---------------------------------------------------------------------------------------------- forceGetRequestTaxiList


    //---------------------------------------------------------------------------------------------- getRequestTaxiList
    private fun getRequestTaxiList() {
        if (requestList.isEmpty())
            when (enumAdminTaxiType) {
                EnumAdminTaxiType.MY -> requestMyTaxiRequestList()
                EnumAdminTaxiType.REQUEST -> requestTaxiList()
                EnumAdminTaxiType.HISTORY -> requestHistoryTaxiList()
            }
        else {
            when (enumAdminTaxiType) {
                EnumAdminTaxiType.MY ->
                    AdminTaxiViewModel.myTaxiLiveDate.postValue(requestList.size)

                EnumAdminTaxiType.REQUEST ->
                    AdminTaxiViewModel.requestTaxiLiveDate.postValue(requestList.size)

                EnumAdminTaxiType.HISTORY -> {}
            }
            taxiRequestListLiveData.value = null
            taxiRequestListLiveData.postValue(requestList)
        }

    }
    //---------------------------------------------------------------------------------------------- getRequestTaxiList


    //---------------------------------------------------------------------------------------------- getTaxiList
    fun getRequestTaxiListNextPage() {
        when (enumAdminTaxiType) {
            EnumAdminTaxiType.MY -> requestMyTaxiRequestList()
            EnumAdminTaxiType.REQUEST -> requestTaxiList()
            EnumAdminTaxiType.HISTORY -> requestHistoryTaxiList()
        }
    }
    //---------------------------------------------------------------------------------------------- getTaxiList


    //---------------------------------------------------------------------------------------------- requestMyTaxiRequestList
    private fun requestMyTaxiRequestList() {
        viewModelScope.launch(IO + exceptionHandler()) {
            callApi(
                request = taxiRepository.requestMyTaxiRequestList(),
                onReceiveData = {
                    requestList.addAll(it)
                    AdminTaxiViewModel.myTaxiLiveDate.postValue(requestList.size)
                    taxiRequestListLiveData.postValue(it)
                }
            )
        }
    }
    //---------------------------------------------------------------------------------------------- requestMyTaxiRequestList


    //---------------------------------------------------------------------------------------------- requestTaxiList
    private fun requestTaxiList() {
        viewModelScope.launch(IO + exceptionHandler()) {
            val type = if (getUserType() == EnumPersonnelType.Driver)
                EnumPersonnelType.Driver
            else
                EnumPersonnelType.Personnel
            callApi(
                request = taxiRepository.requestTaxiList(type),
                onReceiveData = {
                    requestList.addAll(it)
                    AdminTaxiViewModel.requestTaxiLiveDate.postValue(requestList.size)
                    taxiRequestListLiveData.postValue(it)
                }
            )
        }
    }
    //---------------------------------------------------------------------------------------------- requestTaxiList


    //---------------------------------------------------------------------------------------------- requestHistoryTaxiList
    private fun requestHistoryTaxiList() {
        viewModelScope.launch(IO + exceptionHandler()) {
            callApi(
                request = taxiRepository.requestTaxiList(getUserType()),
                onReceiveData = {
                    requestList.addAll(it)
                    taxiRequestListLiveData.postValue(it)
                }
            )
        }
    }
    //---------------------------------------------------------------------------------------------- requestHistoryTaxiList


    //---------------------------------------------------------------------------------------------- requestChangeStatusOfTaxiRequests
    fun requestChangeStatusOfTaxiRequests(request: TaxiChangeStatusRequest) {
        viewModelScope.launch(IO + exceptionHandler()) {
            callApi(
                request = taxiRepository.requestChangeStatusOfTaxiRequests(request),
                showMessageAfterSuccessResponse = true,
                onReceiveData = { forceGetRequestTaxiList() }
            )
        }
    }
    //---------------------------------------------------------------------------------------------- requestChangeStatusOfTaxiRequests


    //---------------------------------------------------------------------------------------------- requestDriverChangeTripStatus
    fun requestDriverChangeTripStatus(itemPosition: Int, lat: Double, lon: Double) {
        viewModelScope.launch(IO + exceptionHandler()) {
            val item = requestList[itemPosition]
            val status = when (item.tripStatus) {
                EnumTripStatus.Assigned -> EnumTripStatus.Started
                EnumTripStatus.Started -> EnumTripStatus.Finished
                else -> EnumTripStatus.None
            }

            val request = DriverChangeTripStatus(item.id, status, lat, lon)
            callApi(
                request = taxiRepository.requestDriverChangeTripStatus(request),
                showMessageAfterSuccessResponse = true,
                onReceiveData = { forceGetRequestTaxiList() }
            )
        }
    }
    //---------------------------------------------------------------------------------------------- requestDriverChangeTripStatus


    //---------------------------------------------------------------------------------------------- userRepository
    fun getUser() = userRepository.getUser()
    //---------------------------------------------------------------------------------------------- userRepository


    //---------------------------------------------------------------------------------------------- getBearerToken
    fun getBearerToken() = tokenRepository.getBearerToken()
    //---------------------------------------------------------------------------------------------- getBearerToken

}