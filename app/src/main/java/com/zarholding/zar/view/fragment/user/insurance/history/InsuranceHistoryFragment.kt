package com.zarholding.zar.view.fragment.user.insurance.history

import android.Manifest
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.zar.core.enums.EnumApiError
import com.zar.core.tools.manager.PermissionManager
import com.zarholding.zar.R
import com.zarholding.zar.ZarFragment
import com.zarholding.zar.databinding.FragmentInsuranceHistoryBinding
import com.zarholding.zar.ext.config
import com.zarholding.zar.ext.startLoading
import com.zarholding.zar.ext.stopLoading
import com.zarholding.zar.model.data.enum.EnumEntityType
import com.zarholding.zar.model.data.response.insurance.history.SupplementalInsuranceHistoryModel
import com.zarholding.zar.tools.CompanionValues
import com.zarholding.zar.tools.manager.MemoryManager
import com.zarholding.zar.tools.manager.getShimmerBuild
import com.zarholding.zar.view.activity.MainActivity
import com.zarholding.zar.view.adapter.recycler.insurance.InsuranceHistoryAdapter
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * Created by m-latifi on 10/11/2023.
 */

@AndroidEntryPoint
class InsuranceHistoryFragment(
    override var layout: Int = R.layout.fragment_insurance_history
): ZarFragment<FragmentInsuranceHistoryBinding>() {

    private val viewModel: InsuranceHistoryViewModel by viewModels()

    @Inject
    lateinit var permissionManager: PermissionManager

    @Inject
    lateinit var memoryManager: MemoryManager

    private var selectedFileName = ""


    //---------------------------------------------------------------------------------------------- storagePermissionLauncher
    private val storagePermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions()
        ) { results ->
            permissionManager.checkPermissionResult(results) {
                if (it)
                    downloadPdfFile()
            }
        }
    //---------------------------------------------------------------------------------------------- storagePermissionLauncher



    //---------------------------------------------------------------------------------------------- onViewCreated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()

    }
    //---------------------------------------------------------------------------------------------- onViewCreated


    //---------------------------------------------------------------------------------------------- initView
    private fun initView() {
        binding.shimmerViewContainer.config(getShimmerBuild())
        observeLiveDate()
        setOnListener()
        getSupplementalHistory()
    }
    //---------------------------------------------------------------------------------------------- initView


    //---------------------------------------------------------------------------------------------- setOnListener
    private fun setOnListener() {

    }
    //---------------------------------------------------------------------------------------------- setOnListener


    //---------------------------------------------------------------------------------------------- setOnListener
    private fun observeLiveDate() {
        viewModel.errorLiveDate.observe(viewLifecycleOwner) {
            binding.shimmerViewContainer.stopLoading()
            showMessage(it.message, it.type)
            when (it.type) {
                EnumApiError.UnAuthorization ->
                    (activity as MainActivity?)?.gotoFirstFragmentWithDeleteUser()

                else -> {}
            }
        }

        viewModel.historyLiveData.observe(viewLifecycleOwner) {
            binding.shimmerViewContainer.stopLoading()
            setHistoryAdapter(it)
        }

    }
    //---------------------------------------------------------------------------------------------- setOnListener


    //---------------------------------------------------------------------------------------------- getSupplementalHistory
    private fun getSupplementalHistory() {
        binding.shimmerViewContainer.startLoading()
        viewModel.requestSupplementalHistory()
    }
    //---------------------------------------------------------------------------------------------- getSupplementalHistory


    //---------------------------------------------------------------------------------------------- setHistoryAdapter
    private fun setHistoryAdapter(items: List<SupplementalInsuranceHistoryModel>) {
        if (context == null)
            return
        val adapter = InsuranceHistoryAdapter(items){
            selectedFileName = it
            storagePermission()
        }
        val manager = LinearLayoutManager(
            requireContext(),LinearLayoutManager.VERTICAL, false
        )
        binding.recyclerHistory.layoutManager = manager
        binding.recyclerHistory.adapter = adapter
    }
    //---------------------------------------------------------------------------------------------- setHistoryAdapter


    //---------------------------------------------------------------------------------------------- cameraPermission
    private fun storagePermission() {
        if (!memoryManager.checkFreeSpaceForDownloading()) {
            showMessage(getString(R.string.internalMemoryIsFull))
            return
        }
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.Q) {
            val permissions = listOf(
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            val check = permissionManager.isPermissionGranted(
                permissions = permissions,
                launcher = storagePermissionLauncher
            )
            if (check)
                downloadPdfFile()
        } else
            downloadPdfFile()
    }
    //---------------------------------------------------------------------------------------------- cameraPermission



    //---------------------------------------------------------------------------------------------- downloadPdfFile
    private fun downloadPdfFile() {
        val bundle = Bundle()
        bundle.putString(CompanionValues.DOWNLOAD_URL, selectedFileName)
        bundle.putString(CompanionValues.DOWNLOAD_TYPE, EnumEntityType.insurance.name)
        gotoFragment(R.id.action_goto_DownloadUpdateFragment, bundle)
    }
    //---------------------------------------------------------------------------------------------- downloadPdfFile


}