package com.zarholding.zar.view.adapter.holder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemPassengerBinding
import com.zarholding.zar.model.data.database.entity.UserInfoEntity

/**
 * Created by m-latifi on 11/14/2022.
 */

class PassengerItemHolder(
    private val binding: ItemPassengerBinding,
    private val onClick: (UserInfoEntity) -> Unit,
    private val addClick: () -> Unit
) : RecyclerView.ViewHolder(binding.root) {


    //---------------------------------------------------------------------------------------------- bind
    fun bind(item: UserInfoEntity, last : Boolean, first : Boolean) {
        setValueToXml(item, last)
        setListener(item, first)
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind



    //---------------------------------------------------------------------------------------------- setValueToXml
    private fun setValueToXml(item: UserInfoEntity, last : Boolean) {
        binding.materialButtonTitle.text = item.fullName
        if (last)
            binding.linearLayoutAdd.visibility = View.VISIBLE
        else
            binding.linearLayoutAdd.visibility = View.GONE
    }
    //---------------------------------------------------------------------------------------------- setValueToXml



    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener(item: UserInfoEntity, first : Boolean) {
        if (first)
            binding.materialButtonTitle.icon = null
        else
            binding.materialButtonTitle.setOnClickListener { onClick(item) }

        binding.imageViewAdd.setOnClickListener {
            addClick()
        }
    }
    //---------------------------------------------------------------------------------------------- setListener

}