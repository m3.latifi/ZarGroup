package com.zarholding.zar.view.adapter.recycler.survey

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.*
import com.zarholding.zar.model.data.enum.EnumSurveyQuestionType
import com.zarholding.zar.model.data.response.survey.SurveyQuestionModel
import com.zarholding.zar.view.adapter.holder.survey.*

class SurveyQuestionAdapter(
    private val questions: List<SurveyQuestionModel>
) : RecyclerView.Adapter<SurveyQuestionHolder>() {

    private var inflater: LayoutInflater? = null

    //---------------------------------------------------------------------------------------------- onCreateViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SurveyQuestionHolder {
        if (inflater == null)
            inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            0 -> SurveySingleLineTextHolder(
                ItemSurveySingleLineBinding.inflate(inflater!!, parent, false)
            )
            1 -> SurveySingleChooseHolder(
                ItemSurveySingleChooseBinding.inflate(inflater!!, parent, false)
            )
            2 -> SurveyMultiChooseHolder(
                ItemSurveyMultiChooseBinding.inflate(inflater!!, parent, false)
            )
            3 -> SurveyYesNoHolder(
                ItemSurveyYesNoBinding.inflate(inflater!!, parent, false)
            )
            4 -> SurveyNumberRangeHolder(
                ItemSurveyNumberRangeBinding.inflate(inflater!!, parent, false)
            )
            5 -> SurveyDateHolder(
                ItemSurveyDateBinding.inflate(inflater!!, parent, false)
            )
            6 -> SurveyTimeHolder(
                ItemSurveyTimeBinding.inflate(inflater!!, parent, false)
            )
            7 -> SurveySatisfactionHolder(
                ItemSurveySatisfactionBinding.inflate(inflater!!, parent, false)
            )
            8 -> SurveyNumberHolder(
                ItemSurveyNumberBinding.inflate(inflater!!, parent, false)
            )
            else -> SurveySingleLineTextHolder(
                ItemSurveySingleLineBinding.inflate(inflater!!, parent, false)
            )
        }
    }
    //---------------------------------------------------------------------------------------------- onCreateViewHolder


    //---------------------------------------------------------------------------------------------- onBindViewHolder
    override fun onBindViewHolder(holder: SurveyQuestionHolder, position: Int) {
        holder.bind(questions[position], position)
    }
    //---------------------------------------------------------------------------------------------- onBindViewHolder


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun getItemCount() = questions.size
    //---------------------------------------------------------------------------------------------- getItemCount


    //---------------------------------------------------------------------------------------------- getItemViewType
    override fun getItemViewType(position: Int) =
        when (questions[position].questionType) {
            EnumSurveyQuestionType.singlelinettext,
            EnumSurveyQuestionType.multilinetext -> 0
            EnumSurveyQuestionType.choicesingleselect_dropdown,
            EnumSurveyQuestionType.choicesingleselect_radio -> 1
            EnumSurveyQuestionType.choicemultipleselect -> 2
            EnumSurveyQuestionType.yesno -> 3
            EnumSurveyQuestionType.numericrange -> 4
            EnumSurveyQuestionType.date -> 5
            EnumSurveyQuestionType.time -> 6
            EnumSurveyQuestionType.satisfaction -> 7
            EnumSurveyQuestionType.numeric -> 8

        }
    //---------------------------------------------------------------------------------------------- getItemViewType
}