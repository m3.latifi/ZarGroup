package com.zarholding.zar.view.adapter.recycler.education

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemEducationDetailBinding
import com.zarholding.zar.model.data.response.education.EducationDetailModel
import com.zarholding.zar.view.adapter.holder.education.EducationDetailHolder

/**
 * Created by m-latifi on 7/26/2023.
 */

class EducationDetailAdapter(
    private val items: MutableList<EducationDetailModel>,
    private val onClick: (EducationDetailModel) -> Unit
) : RecyclerView.Adapter<EducationDetailHolder>() {

    private var inflater: LayoutInflater? = null


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EducationDetailHolder {
        if (inflater == null)
            inflater = LayoutInflater.from(parent.context)
        return EducationDetailHolder(
            binding = ItemEducationDetailBinding.inflate(inflater!!, parent, false),
            onClick = onClick
        )
    }
    //---------------------------------------------------------------------------------------------- getItemCount


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun onBindViewHolder(holder: EducationDetailHolder, position: Int) {
        holder.bind(items[position])
    }
    //---------------------------------------------------------------------------------------------- getItemCount


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun getItemCount() = items.size
    //---------------------------------------------------------------------------------------------- getItemCount


    //---------------------------------------------------------------------------------------------- addEducation
    fun addEducation(list : List<EducationDetailModel>) {
        val olsSize = items.size
        items.clear()
        items.addAll(list)
        notifyItemRangeChanged(olsSize-1, items.size)
    }
    //---------------------------------------------------------------------------------------------- addEducation


}