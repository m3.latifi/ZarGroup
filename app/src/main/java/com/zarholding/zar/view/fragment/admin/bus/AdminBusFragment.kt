package com.zarholding.zar.view.fragment.admin.bus

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.zar.core.enums.EnumApiError
import com.zarholding.zar.R
import com.zarholding.zar.model.data.request.TripRequestRegisterStatusModel
import com.zarholding.zar.model.data.response.trip.TripRequestRegisterModel
import com.zarholding.zar.view.activity.MainActivity
import com.zarholding.zar.view.dialog.ConfirmDialog
import com.zarholding.zar.view.dialog.RejectReasonDialog
import com.zarholding.zar.view.adapter.recycler.AdminBusAdapter
import com.zarholding.zar.ZarFragment
import com.zarholding.zar.databinding.FragmentAdminBusBinding
import com.zarholding.zar.ext.config
import com.zarholding.zar.ext.startLoading
import com.zarholding.zar.ext.stopLoading
import com.zarholding.zar.model.data.enum.EnumStatus
import com.zarholding.zar.tools.manager.getShimmerBuild
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.internal.filterList

@AndroidEntryPoint
class AdminBusFragment(override var layout: Int = R.layout.fragment_admin_bus) :
    ZarFragment<FragmentAdminBusBinding>() {

    private val viewModel: AdminBusServiceViewModel by viewModels()

    lateinit var adapter: AdminBusAdapter


    //---------------------------------------------------------------------------------------------- onViewCreated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }
    //---------------------------------------------------------------------------------------------- onViewCreated


    //---------------------------------------------------------------------------------------------- initView
    private fun initView() {
        binding.shimmerViewContainer.config(getShimmerBuild())
        binding.swipeContainer.setColorSchemeResources(
            android.R.color.holo_blue_bright,
            android.R.color.holo_green_light,
            android.R.color.holo_orange_light,
            android.R.color.holo_red_light
        )
        setOnListener()
        observeLiveDate()
        requestGetTripRequestRegister()
    }
    //---------------------------------------------------------------------------------------------- initView


    //---------------------------------------------------------------------------------------------- setOnListener
    private fun setOnListener() {

        binding.linearLayoutConfirm.setOnClickListener {
            showDialogConfirm()
        }

        binding.linearLayoutReject.setOnClickListener {
            showDialogReasonOfReject()
        }

        binding.swipeContainer.setOnRefreshListener { requestGetTripRequestRegister() }

    }
    //---------------------------------------------------------------------------------------------- setOnListener


    //---------------------------------------------------------------------------------------------- observeLiveDate
    private fun observeLiveDate() {
        viewModel.errorLiveDate.observe(viewLifecycleOwner) {
            binding.shimmerViewContainer.stopLoading()
            binding.swipeContainer.isRefreshing = false
            showMessage(it.message, it.type)
            when (it.type) {
                EnumApiError.UnAuthorization -> (activity as MainActivity?)?.gotoFirstFragmentWithDeleteUser()
                else -> {}
            }
        }

        viewModel.tripModelLiveData.observe(viewLifecycleOwner) {
            binding.shimmerViewContainer.stopLoading()
            binding.swipeContainer.isRefreshing = false
            setRequestAdapter(it)
        }
    }
    //---------------------------------------------------------------------------------------------- observeLiveDate


    //---------------------------------------------------------------------------------------------- requestGetTripRequestRegister
    private fun requestGetTripRequestRegister() {
        binding.shimmerViewContainer.startLoading()
        viewModel.requestGetTripForRegister()
    }
    //---------------------------------------------------------------------------------------------- requestGetTripRequestRegister


    //---------------------------------------------------------------------------------------------- setRequestAdapter
    private fun setRequestAdapter(items: List<TripRequestRegisterModel>?) {
        if (items.isNullOrEmpty() || context == null) {
            binding.recyclerViewRequest.adapter = null
            binding.textViewConfirmCount.text = "0"
            binding.textViewRejectCount.text = "0"
            return
        }
        adapter = AdminBusAdapter(items) {
            countChooseItem()
        }
        val layoutManager = LinearLayoutManager(
            requireContext(),
            LinearLayoutManager.VERTICAL,
            false
        )
        binding.recyclerViewRequest.layoutManager = layoutManager
        binding.recyclerViewRequest.adapter = adapter
        countChooseItem()
    }
    //---------------------------------------------------------------------------------------------- setRequestAdapter


    //---------------------------------------------------------------------------------------------- countChooseItem
    private fun countChooseItem() {
//        val chosen = adapter.getItems().filterList { choose }
        val chosen = viewModel.tripModelLiveData.value?.let {
            it.filterList { choose }
        }
        binding.textViewConfirmCount.text = chosen?.size.toString()
        binding.textViewRejectCount.text = chosen?.size.toString()
    }
    //---------------------------------------------------------------------------------------------- countChooseItem


    //---------------------------------------------------------------------------------------------- showDialogReasonOfReject
    private fun showDialogReasonOfReject() {
        if (context == null)
            return
        RejectReasonDialog(
            requireContext(),
            onClickSend = {reason->
                requestConfirmAndRejectTripRequestRegister(
                EnumStatus.Reject,
                reason
            )},
            onShowDialog = { (activity as MainActivity?)?.enableBlurView() },
            onDismissDialog = { (activity as MainActivity?)?.disableBlurView() }).show()
    }
    //---------------------------------------------------------------------------------------------- showDialogReasonOfReject


    //---------------------------------------------------------------------------------------------- showDialogConfirm
    private fun showDialogConfirm() {
        if (context == null)
            return
        ConfirmDialog(
            context = requireContext(),
            type = ConfirmDialog.ConfirmType.ADD,
            title = getString(R.string.confirmForTripRequestRegister),
            onYesClick = {
                requestConfirmAndRejectTripRequestRegister(
                    EnumStatus.Confirmed,
                    ""
                )
            },
            onShowDialog = { (activity as MainActivity?)?.enableBlurView() },
            onDismissDialog = { (activity as MainActivity?)?.disableBlurView() }
        ).show()
    }
    //---------------------------------------------------------------------------------------------- showDialogConfirm


    //---------------------------------------------------------------------------------------------- requestConfirmAndRejectTripRequestRegister
    private fun requestConfirmAndRejectTripRequestRegister(
        status: EnumStatus,
        reason: String?
    ) {
        val chosen = viewModel.tripModelLiveData.value?.let {
            it.filterList { choose }
        }
        if (chosen.isNullOrEmpty()) {
            showMessage(getString(R.string.chooseItemsIsEmpty))
            return
        }
        val request = mutableListOf<TripRequestRegisterStatusModel>()
        for (item in chosen)
            request.add(
                TripRequestRegisterStatusModel(
                    item.id,
                    status,
                    reason
                )
            )

        binding.shimmerViewContainer.startLoading()
        viewModel.requestChangeStatusTripRegister(request)
    }
    //---------------------------------------------------------------------------------------------- requestConfirmAndRejectTripRequestRegister


}