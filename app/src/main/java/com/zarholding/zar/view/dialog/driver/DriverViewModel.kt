package com.zarholding.zar.view.dialog.driver


import androidx.lifecycle.viewModelScope
import com.zarholding.zar.model.data.request.AssignDriverRequest
import com.zarholding.zar.model.data.response.driver.DriverModel
import com.zarholding.zar.model.repository.DriverRepository
import com.zarholding.zar.model.repository.TaxiRepository
import com.zarholding.zar.tools.SingleLiveEvent
import com.zarholding.zar.ZarViewModel
import com.zarholding.zar.model.data.enum.EnumDriverType
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import javax.inject.Inject

/**
 * Created by m-latifi on 20/8/2022.
 */

@HiltViewModel
class DriverViewModel @Inject constructor(
    private val driverRepository: DriverRepository,
    private val taxiRepository: TaxiRepository
) : ZarViewModel() {

    private var drivers: List<DriverModel>? = null
    var selectedDriver: DriverModel? = null
    val assignDriverLiveData: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }
    val driversListLiveData: SingleLiveEvent<List<DriverModel>> by lazy {
        SingleLiveEvent<List<DriverModel>>()
    }


    //---------------------------------------------------------------------------------------------- requestGetDriver
    fun requestGetDriver(type: EnumDriverType, companyCode: String?) {
        viewModelScope.launch(IO + exceptionHandler()) {
            callApi(
                request = driverRepository.requestGetDriver(type, companyCode),
                onReceiveData = {
                    drivers = it
                    driversListLiveData.postValue(it)
                }
            )
        }
    }
    //---------------------------------------------------------------------------------------------- requestGetDriver


    //---------------------------------------------------------------------------------------------- requestAssignDriverToRequest
    fun requestAssignDriverToRequest(requestId: String) {
        viewModelScope.launch(IO + exceptionHandler()) {
            selectedDriver?.let { driver ->
                val request = AssignDriverRequest(requestId, driver.id.toString())
                callApi(
                    request = taxiRepository.requestAssignDriverToRequest(request),
                    showMessageAfterSuccessResponse = true,
                    onReceiveData = {
                        assignDriverLiveData.postValue(true)
                    }
                )
            }
        }
    }
    //---------------------------------------------------------------------------------------------- requestAssignDriverToRequest


    //---------------------------------------------------------------------------------------------- getDriverList
    fun getDriverList() = drivers
    //---------------------------------------------------------------------------------------------- getDriverList


    //---------------------------------------------------------------------------------------------- selectDriverByIndex
    fun selectDriverByIndex(index: Int) {
        drivers?.let {
            selectedDriver = it[index]
        }
    }
    //---------------------------------------------------------------------------------------------- selectDriverByIndex


}