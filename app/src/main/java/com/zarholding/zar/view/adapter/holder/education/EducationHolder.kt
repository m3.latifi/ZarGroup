package com.zarholding.zar.view.adapter.holder.education

import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.R
import com.zarholding.zar.databinding.ItemEducationBinding
import com.zarholding.zar.ext.setEducationTime
import com.zarholding.zar.model.data.response.education.EducationModel

/**
 * Created by m-latifi on 7/26/2023.
 */

class EducationHolder(
    private val binding: ItemEducationBinding,
    private val onClick: (EducationModel) -> Unit
) : RecyclerView.ViewHolder(binding.root) {


    //---------------------------------------------------------------------------------------------- bind
    fun bind(item: EducationModel) {
        setValueToXml(item)
        binding.root.setOnClickListener { onClick(item) }
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind


    //---------------------------------------------------------------------------------------------- bind
    private fun setValueToXml(item: EducationModel) {
        binding.textViewTitle.text = item.description
        binding.textViewTime.setEducationTime(item.sessionsMin)
        binding.textViewCount.text = binding
            .textViewCount
            .context
            .getString(R.string.session, item.sessionCount.toString())
        binding.textViewExpire.text = binding
            .textViewExpire
            .context
            .getString(R.string.expireDate, item.toDate)

    }
    //---------------------------------------------------------------------------------------------- bind
}