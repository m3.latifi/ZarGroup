package com.zarholding.zar.view.fragment.user.qrcode

import android.graphics.Path
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.github.alexzhirkevich.customqrgenerator.QrData
import com.github.alexzhirkevich.customqrgenerator.style.Neighbors
import com.github.alexzhirkevich.customqrgenerator.vector.QrCodeDrawable
import com.github.alexzhirkevich.customqrgenerator.vector.QrVectorOptions
import com.github.alexzhirkevich.customqrgenerator.vector.style.QrVectorBackground
import com.github.alexzhirkevich.customqrgenerator.vector.style.QrVectorBallShape
import com.github.alexzhirkevich.customqrgenerator.vector.style.QrVectorColor
import com.github.alexzhirkevich.customqrgenerator.vector.style.QrVectorColors
import com.github.alexzhirkevich.customqrgenerator.vector.style.QrVectorFrameShape
import com.github.alexzhirkevich.customqrgenerator.vector.style.QrVectorLogo
import com.github.alexzhirkevich.customqrgenerator.vector.style.QrVectorLogoPadding
import com.github.alexzhirkevich.customqrgenerator.vector.style.QrVectorLogoShape
import com.github.alexzhirkevich.customqrgenerator.vector.style.QrVectorPixelShape
import com.github.alexzhirkevich.customqrgenerator.vector.style.QrVectorShapes
import com.zarholding.zar.R
import com.zarholding.zar.ZarFragment
import com.zarholding.zar.databinding.FragmentQrCreatorBinding
import com.zarholding.zar.tools.CompanionValues

/**
 * Created by m-latifi on 9/2/2023.
 */

class QrCodeCreatorFragment(
    override var layout: Int = R.layout.fragment_qr_creator
): ZarFragment<FragmentQrCreatorBinding>() {


    //---------------------------------------------------------------------------------------------- onViewCreated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }
    //---------------------------------------------------------------------------------------------- onViewCreated


    //---------------------------------------------------------------------------------------------- initView
    private fun initView() {
        arguments?.let {
            val url = it.getString(CompanionValues.DOWNLOAD_URL, "")
            if (url.isEmpty())
                activity?.onBackPressedDispatcher?.onBackPressed()
            else
                createQrCode(url)

        } ?: run {
            activity?.onBackPressedDispatcher?.onBackPressed()
        }
    }
    //---------------------------------------------------------------------------------------------- initView



    //---------------------------------------------------------------------------------------------- createQrCode
    private fun createQrCode(url: String) {
        val data = QrData.Url(url)
        val options = QrVectorOptions.Builder()
            .setPadding(.25f)
            .setLogo(
                QrVectorLogo(
                    drawable = ContextCompat
                        .getDrawable(requireContext(), R.drawable.a_solid_logo_qr),
                    size = .25f,
                    padding = QrVectorLogoPadding.Natural(.2f),
                    shape = QrVectorLogoShape
                        .Circle
                )
            )
            .setBackground(
                QrVectorBackground(
                    drawable = ContextCompat
                        .getDrawable(requireContext(), R.drawable.a_drawable_qr_border),
                )
            )
            .setColors(
                QrVectorColors(
                    dark = QrVectorColor//رنگ دایره های گوچک
                        .LinearGradient(
                            colors = listOf(
                                0f to requireContext().getColor(R.color.qrGradientStart),
                                1f to requireContext().getColor(R.color.qrGradientEnd),
                            ),
                    orientation = QrVectorColor.LinearGradient
                        .Orientation.LeftDiagonal
                ),
                    ball = QrVectorColor//رنگ مربع داخل گوشه ها
                    .Solid(requireContext().getColor(R.color.n_textColorPrimary)),
                    frame = QrVectorColor//رنگ مربع های گوشه
                        .LinearGradient(
                        colors = listOf(
                            0f to requireContext().getColor(R.color.qrGradientEnd),
                            1f to requireContext().getColor(R.color.qrGradientStart),
                        ),
                        orientation = QrVectorColor.LinearGradient
                            .Orientation.LeftDiagonal
                    )
                )
            )
            .setShapes(
                QrVectorShapes(
                    darkPixel = Circle,
                    ball = QrVectorBallShape
                        .RoundCorners(.25f),
                    frame = QrVectorFrameShape
                        .RoundCorners(corner = .25f, width = 0.5f)
                )
            )
            .build()

        val drawable : Drawable = QrCodeDrawable(data, options)
        binding.qrImage.setImageDrawable(drawable)
    }
    //---------------------------------------------------------------------------------------------- createQrCode


    object Circle : QrVectorPixelShape {

        override fun createPath(size: Float, neighbors: Neighbors): Path = Path().apply {
            addCircle(size/2f, size/2f, size/2, Path.Direction.CW)
        }
    }
}