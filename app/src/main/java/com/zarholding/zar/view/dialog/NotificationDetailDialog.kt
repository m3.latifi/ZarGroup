package com.zarholding.zar.view.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import com.zar.core.tools.extensions.toSolarDate
import com.zarholding.zar.R
import com.zarholding.zar.ext.setTitleAndValue
import com.zarholding.zar.model.data.response.notification.NotificationModel
import java.time.format.DateTimeFormatter
import java.util.Locale

/**
 * Created by m-latifi on 11/26/2022.
 */

class NotificationDetailDialog(
    context: Context,
    private val item: NotificationModel
) : Dialog(context) {


    //---------------------------------------------------------------------------------------------- onCreate
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_detail_notification)
        val lp = WindowManager.LayoutParams()
        this.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        this.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        this.window?.setGravity(Gravity.CENTER)
        lp.copyFrom(this.window?.attributes)
        lp.horizontalMargin = 50f
        this.window?.attributes = lp
    }
    //---------------------------------------------------------------------------------------------- onCreate


    //---------------------------------------------------------------------------------------------- onStart
    override fun onStart() {
        initDialog()
        super.onStart()
    }
    //---------------------------------------------------------------------------------------------- onStart


    //---------------------------------------------------------------------------------------------- initDialog
    private fun initDialog() {
        val textViewDate = this.findViewById<TextView>(R.id.textViewDate)
        val textViewSender = this.findViewById<TextView>(R.id.textViewSender)
        val textViewSubject = this.findViewById<TextView>(R.id.textViewSubject)
        val textViewSummary = this.findViewById<TextView>(R.id.textViewSummary)
        val imageViewClose = this.findViewById<ImageView>(R.id.imageViewClose)

        val time = "${item.createDate.toSolarDate()?.getFullDate()} - ${
            item
                .createDate?.format(DateTimeFormatter.ofPattern("HH:mm", Locale.getDefault())) ?: ""
        }"
        textViewDate.text = time
        textViewSender.text = item.senderName
        textViewSubject.setTitleAndValue(
            title = context.getString(R.string.subject),
            splitter = context.getString(R.string.colon),
            value = item.subject
        )
        textViewSummary.text = item.message

        imageViewClose.setOnClickListener { dismiss() }

    }
    //---------------------------------------------------------------------------------------------- initDialog


}