package com.zarholding.zar.view.fragment.user.profile

import com.zarholding.zar.model.data.database.dao.UserInfoDao
import com.zarholding.zar.model.data.other.ShowImageModel
import com.zarholding.zar.model.repository.TokenRepository
import com.zarholding.zar.ZarViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

/**
 * Created by m-latifi on 11/19/2022.
 */

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val tokenRepository: TokenRepository,
    private val userInfoDao: UserInfoDao
) : ZarViewModel() {


    //---------------------------------------------------------------------------------------------- getBearerToken
    fun getBearerToken() = tokenRepository.getBearerToken()
    //---------------------------------------------------------------------------------------------- getBearerToken


    //---------------------------------------------------------------------------------------------- getUserInfo
    fun getUserInfo() = userInfoDao.getUserInfo()
    //---------------------------------------------------------------------------------------------- getUserInfo


    //---------------------------------------------------------------------------------------------- getModelForShowImageProfile
    fun getModelForShowImageProfile(): ShowImageModel {
        val model = ShowImageModel("", null, null)
        getUserInfo()?.userName?.let {
            model.imageName = it
        }
        model.token = tokenRepository.getBearerToken()
        return model
    }
    //---------------------------------------------------------------------------------------------- getModelForShowImageProfile

}