package com.zarholding.zar.view.fragment.user.food.reservation

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.zar.core.enums.EnumApiError
import com.zarholding.zar.R
import com.zarholding.zar.ZarFragment
import com.zarholding.zar.databinding.FragmentFoodReservationBinding
import com.zarholding.zar.view.activity.MainActivity
import dagger.hilt.android.AndroidEntryPoint

/**
 * Created by m-latifi on 10/11/2023.
 */

@AndroidEntryPoint
class FoodReservationFragment(
    override var layout: Int = R.layout.fragment_food_reservation
): ZarFragment<FragmentFoodReservationBinding>() {

    private val viewModel: FoodReservationViewModel by viewModels()


    //---------------------------------------------------------------------------------------------- onViewCreated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }
    //---------------------------------------------------------------------------------------------- onViewCreated


    //---------------------------------------------------------------------------------------------- initView
    private fun initView() {
        observeLiveDate()
        setOnListener()
    }
    //---------------------------------------------------------------------------------------------- initView


    //---------------------------------------------------------------------------------------------- setOnListener
    private fun setOnListener() {

    }
    //---------------------------------------------------------------------------------------------- setOnListener


    //---------------------------------------------------------------------------------------------- setOnListener
    private fun observeLiveDate() {
        viewModel.errorLiveDate.observe(viewLifecycleOwner) {
            showMessage(it.message, it.type)
            when (it.type) {
                EnumApiError.UnAuthorization ->
                    (activity as MainActivity?)?.gotoFirstFragmentWithDeleteUser()

                else -> {}
            }
        }
    }
    //---------------------------------------------------------------------------------------------- setOnListener

}