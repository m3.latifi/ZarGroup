package com.zarholding.zar.view.custom

import androidx.recyclerview.widget.LinearLayoutManager

/**
 * Created by m-latifi on 8/25/2023.
 */

//______________________________________________________________________________________________ getEndlessScrollListener
fun getEndlessScrollListener(manager: LinearLayoutManager, loadMore: () -> Unit): EndlessScrollListener {
    val endlessScrollListener = object : EndlessScrollListener(manager) {
        override fun loadMoreItems() {
            loadMore()
        }
    }
    endlessScrollListener.setLoading(false)
    return endlessScrollListener
}
//______________________________________________________________________________________________ getEndlessScrollListener
