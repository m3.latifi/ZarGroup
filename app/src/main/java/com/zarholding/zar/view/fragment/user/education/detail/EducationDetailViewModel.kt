package com.zarholding.zar.view.fragment.user.education.detail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.zarholding.zar.ZarViewModel
import com.zarholding.zar.model.data.response.education.EducationDetailModel
import com.zarholding.zar.model.repository.EducationRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by m-latifi on 7/26/2023.
 */

@HiltViewModel
class EducationDetailViewModel @Inject constructor(
    private val educationRepository: EducationRepository
): ZarViewModel() {

    private val educationList = mutableListOf<EducationDetailModel>()

    val educationDetailModelLiveData: MutableLiveData<List<EducationDetailModel>?> by lazy {
        MutableLiveData<List<EducationDetailModel>?>()
    }


    //---------------------------------------------------------------------------------------------- getVideoDetail
    fun getVideoDetail(videoId: Long){
        viewModelScope.launch(IO + exceptionHandler()) {
            if (educationList.isEmpty())
                getMoreVideoDetail(videoId)
            else
                educationDetailModelLiveData.postValue(educationList)
        }
    }
    //---------------------------------------------------------------------------------------------- getVideoDetail


    //---------------------------------------------------------------------------------------------- getMoreVideoDetail
    private fun getMoreVideoDetail(videoId: Long){
        viewModelScope.launch(IO + exceptionHandler()){
            callApi(
                request = educationRepository.getVideoDetail(videoId),
                onReceiveData = {
                    if (!it.items.isNullOrEmpty()){
                        educationList.addAll(it.items)
                        educationDetailModelLiveData.postValue(educationList)
                    }
                }
            )
        }
    }
    //---------------------------------------------------------------------------------------------- getMoreVideoDetail

}