package com.zarholding.zar.view.dialog.notification

import androidx.lifecycle.viewModelScope
import com.zarholding.zar.ZarViewModel
import com.zarholding.zar.model.data.other.notification.NotificationCategoryModel
import com.zarholding.zar.model.data.response.notification.NotificationModel
import com.zarholding.zar.model.repository.NotificationRepository
import com.zarholding.zar.tools.SingleLiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import javax.inject.Inject
import java.time.LocalDateTime
/**
 * Created by m-latifi on 20/8/2022.
 */

@HiltViewModel
class NotificationViewModel @Inject constructor(
    private val notificationRepository: NotificationRepository
) : ZarViewModel() {

    private var rawListNotification: MutableList<NotificationModel>? = null
    private var categoryNotification = mutableListOf<NotificationCategoryModel>()
    val readLiveData: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }
    val notificationResponseLiveData: SingleLiveEvent<MutableList<NotificationCategoryModel>>
            by lazy { SingleLiveEvent<MutableList<NotificationCategoryModel>>() }


    //---------------------------------------------------------------------------------------------- forceGetListNotification
    fun forceGetListNotification() {
        rawListNotification?.clear()
        categoryNotification.clear()
        requestGetNotification()
    }
    //---------------------------------------------------------------------------------------------- forceGetListNotification


    //---------------------------------------------------------------------------------------------- getNotification
    fun getNotification() {
        if (rawListNotification.isNullOrEmpty())
            requestGetNotification()
        else
            initListNotification()
    }
    //---------------------------------------------------------------------------------------------- getNotification


    //---------------------------------------------------------------------------------------------- requestGetNotification
    private fun requestGetNotification() {
        viewModelScope.launch(IO + exceptionHandler()) {
            callApi(
                request = notificationRepository.requestGetNotification(),
                onReceiveData = {
                    rawListNotification = it.toMutableList()
                    initListNotification()
                }
            )
        }
    }
    //---------------------------------------------------------------------------------------------- requestGetNotification


    //---------------------------------------------------------------------------------------------- requestReadNotification
    fun requestReadNotification(ids: List<Int>) {
        viewModelScope.launch(IO + exceptionHandler()) {
            callApi(
                request = notificationRepository.requestReadNotification(ids),
                onReceiveData = { readLiveData.postValue(it) }
            )
        }
    }
    //---------------------------------------------------------------------------------------------- requestReadNotification


    //---------------------------------------------------------------------------------------------- initListNotification
    private fun initListNotification() {
        viewModelScope.launch(IO + exceptionHandler()) {
            rawListNotification?.let {
                setTodayNotification()
                setYesterdayNotification()
                setOldDayNotification()
                withContext(Main) {
                    notificationResponseLiveData.value = categoryNotification
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- initListNotification


    //---------------------------------------------------------------------------------------------- addNotification
    fun addNotification(item: NotificationModel) {
        rawListNotification?.add(0, item)
        categoryNotification.clear()
        CoroutineScope(IO).launch {
            initListNotification()
        }
    }
    //---------------------------------------------------------------------------------------------- addNotification


    //---------------------------------------------------------------------------------------------- setTodayNotification
    private fun setTodayNotification() {
        val now = LocalDateTime.now()
        val todayDate = LocalDateTime.of(
            now.year,
            now.month,
            now.dayOfMonth,
            0, 0, 0
        )
        val todayNotification = rawListNotification?.filter { item ->
            item.createDate?.let {
                item.createDate >= todayDate
            } ?: run {
                false
            }
/*            val strDate = item.lastUpdate?.substring(0, 11) + "00:00:00"
            val date = LocalDateTime.parse(strDate)*/
/*            val dayBetween = Duration.between(todayDate, item.createDate).toDays()
            dayBetween == 0L*/
        }
        if (!todayNotification.isNullOrEmpty())
            categoryNotification.add(
                NotificationCategoryModel(
                    "امروز",
                    todayDate,
                    todayNotification.toMutableList()
                )
            )
    }
    //---------------------------------------------------------------------------------------------- setTodayNotification


    //---------------------------------------------------------------------------------------------- setYesterdayNotification
    private fun setYesterdayNotification() {
        val now = LocalDateTime.now().minusDays(1)
        val startYesterdayDate = LocalDateTime.of(
            now.year,
            now.month,
            now.dayOfMonth,
            0, 0, 1
        )
        val endYesterdayDate = LocalDateTime.of(
            now.year,
            now.month,
            now.dayOfMonth,
            23, 59, 59
        )
        val yesterday = rawListNotification?.filter { item ->
            item.createDate?.let {
                (item.createDate >= startYesterdayDate) && (item.createDate <= endYesterdayDate)
            } ?: run {
                false
            }
/*            val strDate = item.lastUpdate?.substring(0, 11) + "00:00:00"
            val date = LocalDateTime.parse(strDate)*/
/*            val dayBetween = Duration.between(yesterdayDate, item.createDate).toDays()
            dayBetween == 0L*/
        }
        if (!yesterday.isNullOrEmpty())
            categoryNotification.add(
                NotificationCategoryModel(
                    "دیروز",
                    startYesterdayDate,
                    yesterday.toMutableList()
                )
            )
    }
    //---------------------------------------------------------------------------------------------- setYesterdayNotification


    //---------------------------------------------------------------------------------------------- setOldDayNotification
    private fun setOldDayNotification() {
        val now = LocalDateTime.now().minusDays(2)
        val yesterdayDate = LocalDateTime.of(
            now.year,
            now.month,
            now.dayOfMonth,
            23, 59, 59
        )
        val yesterday = rawListNotification?.filter { item ->
            item.createDate?.let {
                item.createDate < yesterdayDate
            } ?: run {
                false
            }
/*            val strDate = item.lastUpdate?.substring(0, 11) + "00:00:00"
            val date = LocalDateTime.parse(strDate)*/
/*            val dayBetween = Duration.between(yesterdayDate, item.createDate).toDays()
            dayBetween < 0*/
        }
        if (!yesterday.isNullOrEmpty())
            categoryNotification.add(
                NotificationCategoryModel(
                    "گذشته",
                    yesterdayDate,
                    yesterday.toMutableList()
                )
            )
    }
    //---------------------------------------------------------------------------------------------- setOldDayNotification


}