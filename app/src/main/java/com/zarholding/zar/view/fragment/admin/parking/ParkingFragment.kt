package com.zarholding.zar.view.fragment.admin.parking

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.LinearLayoutManager
import com.skydoves.powerspinner.DefaultSpinnerAdapter
import com.zar.core.enums.EnumApiError
import com.zar.core.tools.extensions.persianNumberToEnglishNumber
import com.zarholding.zar.R
import com.zarholding.zar.ext.hideKeyboard
import com.zarholding.zar.view.activity.MainActivity
import com.zarholding.zar.ZarFragment
import com.zarholding.zar.databinding.FragmentParkingBinding
import com.zarholding.zar.ext.loadImageByToken
import com.zarholding.zar.model.data.database.entity.UserInfoEntity
import com.zarholding.zar.model.data.response.ResponseResult
import com.zarholding.zar.view.dialog.ShowImageDialog
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

/**
 * Created by m-latifi on 12/19/2022.
 */

@AndroidEntryPoint
class ParkingFragment(override var layout: Int = R.layout.fragment_parking) :
    ZarFragment<FragmentParkingBinding>() {

    private val parkingViewModel: ParkingViewModel by viewModels()


    //---------------------------------------------------------------------------------------------- onViewCreated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }
    //---------------------------------------------------------------------------------------------- onViewCreated



    //---------------------------------------------------------------------------------------------- initView
    private fun initView() {
        if (binding.expandableInfo.isExpanded)
            binding.expandableInfo.collapse()
        setListener()
        initAlphabetSpinner()
        observeLiveDate()
//        startQRCodeReader()
    }
    //---------------------------------------------------------------------------------------------- initView



    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener() {
        binding.materialButtonSearch.setOnClickListener { requestGetUserInfo() }

        binding.imageViewRetry.setOnClickListener { resetSearch() }

        binding.materialButtonSms.setOnClickListener {
            parkingViewModel.userInfoEntity?.let {
                val number = it.mobile.persianNumberToEnglishNumber()
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.fromParts("sms", number, null)
                    )
                )
            }
        }

        binding.materialButtonNotify.setOnClickListener {
            showMessage(getString(R.string.disableFeature))
        }

        binding.cardViewProfile.setOnClickListener { showImageProfile() }
    }
    //---------------------------------------------------------------------------------------------- setListener


    //---------------------------------------------------------------------------------------------- observeLiveDate
    private fun observeLiveDate() {
        parkingViewModel.errorLiveDate.observe(viewLifecycleOwner) {
            binding.materialButtonSearch.stopLoading()
            showMessage(it.message, it.type)
            when (it.type) {
                EnumApiError.UnAuthorization -> (activity as MainActivity?)?.gotoFirstFragmentWithDeleteUser()
                else -> {}
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                parkingViewModel.userInfo.collect {
                    when(it) {
                        is ResponseResult.Loading -> {}
                        is ResponseResult.Success -> {
                            binding.materialButtonSearch.visibility = View.INVISIBLE
                            binding.imageViewRetry.visibility = View.VISIBLE
                            binding.materialButtonSearch.stopLoading()
                            binding.expandableInfo.expand()
                            setUserInfo(it.data)
                        }
                    }
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- observeLiveDate



    //---------------------------------------------------------------------------------------------- initAlphabetSpinner
    private fun initAlphabetSpinner() {
        binding.powerSpinnerAlphabet.apply {
            setSpinnerAdapter(DefaultSpinnerAdapter(this))
            setItems(parkingViewModel.getAlphabet())
            setOnSpinnerOutsideTouchListener { _, _ -> dismiss() }
            getSpinnerRecyclerView().layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            lifecycleOwner = viewLifecycleOwner
        }
        binding.powerSpinnerAlphabet.selectItemByIndex(0)
    }
    //---------------------------------------------------------------------------------------------- initAlphabetSpinner


    //---------------------------------------------------------------------------------------------- resetSearch
    private fun resetSearch() {
        if (binding.materialButtonSearch.isLoading)
            return
        binding.materialButtonSearch.visibility = View.VISIBLE
        binding.imageViewRetry.visibility = View.GONE
        binding.expandableInfo.requestFocus()
        binding.expandableInfo.collapse()
        binding.editTextNumber1.text.clear()
        binding.editTextNumber2.text.clear()
        binding.editTextCityCode.text.clear()
        binding.powerSpinnerAlphabet.selectItemByIndex(0)
        binding.powerSpinnerAlphabet.showArrow = true
        binding.powerSpinnerAlphabet.setBackgroundResource(R.drawable.a_drawable_border_car_number)
    }
    //---------------------------------------------------------------------------------------------- resetSearch


    //---------------------------------------------------------------------------------------------- requestGetUserInfo
    private fun requestGetUserInfo() {
        if (binding.materialButtonSearch.isLoading)
            return
        hideKeyboard()
        binding.expandableInfo.collapse()
        binding.materialButtonSearch.startLoading(getString(R.string.bePatient))
        parkingViewModel.getUserInfoByPlaque(
            plaqueNumber1 = binding.editTextNumber1.text.toString(),
            plaqueAlphabet = binding.powerSpinnerAlphabet.text.toString(),
            plaqueNumber2 = binding.editTextNumber2.text.toString(),
            plaqueCity = binding.editTextCityCode.text.toString()
        )
    }
    //---------------------------------------------------------------------------------------------- requestGetUserInfo



    //---------------------------------------------------------------------------------------------- setUserInfo
    private fun setUserInfo(item: UserInfoEntity){
        binding.textviewName.text = item.fullName
        binding.textviewCode.text = item.userName
        binding.textviewJobKeyText.text = item.personnelJobKeyText
        binding.textViewOrganizationUnit.text = item.organizationUnit
        binding.textViewPhone.text = item.phone
        binding.textViewMobile.text = item.mobile
        binding.textViewEmail.text = item.email
        binding.imageViewProfile.loadImageByToken(
            url = item.userName,
            token = parkingViewModel.getBearerToken()
        )

    }
    //---------------------------------------------------------------------------------------------- setUserInfo



    //---------------------------------------------------------------------------------------------- showImageProfile
    private fun showImageProfile() {
        if (context == null)
            return
        ShowImageDialog(
            context = requireContext(),
            item = parkingViewModel.getModelForShowImageProfile(),
            onShowDialog = { (activity as MainActivity?)?.enableBlurView() },
            onDismissDialog = { (activity as MainActivity?)?.disableBlurView() }
        ).show()
    }
    //---------------------------------------------------------------------------------------------- showImageProfile

}