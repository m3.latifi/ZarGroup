package com.zarholding.zar.view.adapter.holder.survey

import android.util.TypedValue
import android.widget.CheckBox
import android.widget.LinearLayout
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.res.ResourcesCompat
import com.zarholding.zar.R
import com.zarholding.zar.databinding.ItemSurveyMultiChooseBinding
import com.zarholding.zar.ext.setSurveyNumber
import com.zarholding.zar.model.data.response.survey.SurveyQuestionModel


/**
 * Create by Mehrdad on 1/21/2023
 */

class SurveyMultiChooseHolder(
    private val binding: ItemSurveyMultiChooseBinding
) : SurveyQuestionHolder(binding.root) {

    //---------------------------------------------------------------------------------------------- bind
    override fun bind(item: SurveyQuestionModel, position: Int) {
        setValueToXml(item, position)
        setOptions(item)
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind


    //---------------------------------------------------------------------------------------------- setValueToXml
    private fun setValueToXml(item: SurveyQuestionModel,position: Int) {
        binding.textViewNumber.setSurveyNumber(number = position + 1 )
        binding.textViewQuestion.text = item.questionTitle
    }
    //---------------------------------------------------------------------------------------------- setValueToXml


    //---------------------------------------------------------------------------------------------- setOptions
    private fun setOptions(item: SurveyQuestionModel) {
        binding.linearLayoutAnswer.removeAllViews()
        val param = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        param.setMargins(10, 15, 10, 10)
        val typeface = ResourcesCompat.getFont(binding.root.context, R.font.kalameh_regular)
        if (item.selectedAnswersId == null)
            item.selectedAnswersId = mutableListOf()
        item.options?.let {options ->
            for (i in options.indices) {
                val checkBox = CheckBox(binding.root.context).apply {
                    setTextSize(TypedValue.COMPLEX_UNIT_SP, 13f)
                    setTextColor(binding.root.context.getColor(R.color.n_textColorPrimary))
                    layoutParams = param
                    buttonDrawable = AppCompatResources.getDrawable(
                        binding.linearLayoutAnswer.context,
                        R.drawable.a_drawable_checkbox
                    )
                    val find = item.selectedAnswersId?.find { it == options[i].questionOptionId }
                    isChecked = find != null
                    text = options[i].optionTitle
                    layoutDirection = LinearLayout.LAYOUT_DIRECTION_RTL
                    setTypeface(typeface)
                    setPadding(0,0,15,0)
                    setOnCheckedChangeListener { _ , isChecked ->
                        if (isChecked) {
                            setTextColor(binding.linearLayoutAnswer.context.getColor(R.color.primaryColor))
                            item.selectedAnswersId?.add(options[i].questionOptionId)
                        } else {
                            setTextColor(binding.linearLayoutAnswer.context.getColor(R.color.n_textColorPrimary))
                            item.selectedAnswersId?.remove(options[i].questionOptionId)
                        }
                    }
                }
                binding.linearLayoutAnswer.addView(checkBox, param)
            }
        }
    }
    //---------------------------------------------------------------------------------------------- setOptions

}
