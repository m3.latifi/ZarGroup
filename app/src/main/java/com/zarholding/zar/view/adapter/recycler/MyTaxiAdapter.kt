package com.zarholding.zar.view.adapter.recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemMyTaxiBinding
import com.zarholding.zar.model.data.response.taxi.AdminTaxiRequestModel
import com.zarholding.zar.model.data.response.taxi.TaxiRequestPointModel
import com.zarholding.zar.view.adapter.holder.MYTaxiHolder

/**
 * Created by m-latifi on 11/22/2022.
 */

class MyTaxiAdapter(
    private val items: MutableList<AdminTaxiRequestModel>,
    private val token: String,
    private val onMapClick: (TaxiRequestPointModel) -> Unit
) : RecyclerView.Adapter<MYTaxiHolder>() {

    private var layoutInflater: LayoutInflater? = null


    //---------------------------------------------------------------------------------------------- onCreateViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MYTaxiHolder {
        if (layoutInflater == null)
            layoutInflater = LayoutInflater.from(parent.context)
        return MYTaxiHolder(
            binding = ItemMyTaxiBinding.inflate(layoutInflater!!, parent, false),
            onMapClick = onMapClick
        )
    }
    //---------------------------------------------------------------------------------------------- onCreateViewHolder


    //---------------------------------------------------------------------------------------------- onBindViewHolder
    override fun onBindViewHolder(holder: MYTaxiHolder, position: Int) {
        holder.bind(items[position], token)
    }
    //---------------------------------------------------------------------------------------------- onBindViewHolder


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun getItemCount() = items.size
    //---------------------------------------------------------------------------------------------- getItemCount


    //---------------------------------------------------------------------------------------------- addRequest
    fun addRequest(list: List<AdminTaxiRequestModel>) {
        val oldSize = items.size
        items.addAll(list)
        notifyItemRangeChanged(oldSize-1, items.size)
    }
    //---------------------------------------------------------------------------------------------- addRequest


}