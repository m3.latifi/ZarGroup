package com.zarholding.zar.view.adapter.recycler.hse

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemHseExaminationBinding
import com.zarholding.zar.model.data.request.hse.HseExaminationModel
import com.zarholding.zar.view.adapter.holder.hse.HseExaminationHolder

/**
 * Created by m-latifi on 7/26/2023.
 */

class HseExaminationAdapter(
    private val items: List<HseExaminationModel>,
    private val onClick: (String) -> Unit
) : RecyclerView.Adapter<HseExaminationHolder>() {

    private var inflater: LayoutInflater? = null

    //---------------------------------------------------------------------------------------------- onCreateViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HseExaminationHolder {
        if (inflater == null)
            inflater = LayoutInflater.from(parent.context)
        return HseExaminationHolder(
            binding = ItemHseExaminationBinding.inflate(inflater!!, parent, false),
            onClick = onClick
        )
    }
    //---------------------------------------------------------------------------------------------- onCreateViewHolder


    //---------------------------------------------------------------------------------------------- onBindViewHolder
    override fun onBindViewHolder(holder: HseExaminationHolder, position: Int) {
        holder.bind(items[position])
    }
    //---------------------------------------------------------------------------------------------- onBindViewHolder


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun getItemCount() = items.size
    //---------------------------------------------------------------------------------------------- getItemCount

}