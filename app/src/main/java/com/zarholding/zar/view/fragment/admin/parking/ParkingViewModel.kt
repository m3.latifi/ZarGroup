package com.zarholding.zar.view.fragment.admin.parking

import androidx.lifecycle.viewModelScope
import com.zarholding.zar.model.data.database.entity.UserInfoEntity
import com.zarholding.zar.model.repository.UserRepository
import com.zarholding.zar.ZarViewModel
import com.zarholding.zar.model.data.response.ResponseResult
import com.zarholding.zar.model.repository.PlaqueRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

/**
 * Created by m-latifi on 12/19/2022.
 */

@HiltViewModel
class ParkingViewModel @Inject constructor(
    private val plaqueRepository: PlaqueRepository,
    private val userRepository: UserRepository
) : ZarViewModel() {

    private val _userInfo =
        MutableStateFlow<ResponseResult<UserInfoEntity>>(ResponseResult.Loading(false))
    val userInfo: StateFlow<ResponseResult<UserInfoEntity>> = _userInfo
    var userInfoEntity: UserInfoEntity? = null


    //---------------------------------------------------------------------------------------------- init
    init {
        userInfoEntity = userRepository.getUser()
    }
    //---------------------------------------------------------------------------------------------- init



    //---------------------------------------------------------------------------------------------- getCarModel
    fun getCarModel() = plaqueRepository.getCarModel(userInfoEntity = userInfoEntity)
    //---------------------------------------------------------------------------------------------- getCarModel


    //---------------------------------------------------------------------------------------------- getPlaqueNumber1
    fun getPlaqueNumber1() = plaqueRepository.getPlaqueNumber1(userInfoEntity = userInfoEntity)
    //---------------------------------------------------------------------------------------------- getPlaqueNumber1


    //---------------------------------------------------------------------------------------------- getPlaqueNumber2
    fun getPlaqueNumber2() = plaqueRepository.getPlaqueNumber2(userInfoEntity = userInfoEntity)
    //---------------------------------------------------------------------------------------------- getPlaqueNumber2


    //---------------------------------------------------------------------------------------------- getPlaqueCity
    fun getPlaqueCity() = plaqueRepository.getPlaqueCity(userInfoEntity = userInfoEntity)
    //---------------------------------------------------------------------------------------------- getPlaqueCity


    //---------------------------------------------------------------------------------------------- getPlaqueAlphabet
    fun getPlaqueAlphabet() = plaqueRepository.getPlaqueAlphabet(userInfoEntity = userInfoEntity)
    //---------------------------------------------------------------------------------------------- getPlaqueAlphabet



    //---------------------------------------------------------------------------------------------- changeCarPlaque
    fun changeCarPlaque(
        carModel: String?,
        plaqueNumber1: String?,
        plaqueAlphabet: String?,
        plaqueNumber2: String?,
        plaqueCity: String?
    ) {
        viewModelScope.launch(IO + exceptionHandler()) {
            _userInfo.value = ResponseResult.Loading(true)
            plaqueRepository.changeCarPlaque(
                carModel = carModel,
                plaqueNumber1 = plaqueNumber1,
                plaqueAlphabet = plaqueAlphabet,
                plaqueNumber2 = plaqueNumber2,
                plaqueCity = plaqueCity
            ).collect {networkResult ->
                checkResponse(networkResult = networkResult) {
                    _userInfo.value = ResponseResult.Loading(false)
                    userRepository.getUser()?.let {user ->
                        _userInfo.value = ResponseResult.Success(user)
                    }
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- changeCarPlaque


    //---------------------------------------------------------------------------------------------- getUserInfoByPlaque
    fun getUserInfoByPlaque(
        plaqueNumber1: String?,
        plaqueAlphabet: String?,
        plaqueNumber2: String?,
        plaqueCity: String?
    ) {
        viewModelScope.launch(IO + exceptionHandler()) {
            _userInfo.value = ResponseResult.Loading(true)
            plaqueRepository.getUserInfoByPlaque(
                plaqueNumber1 = plaqueNumber1,
                plaqueAlphabet = plaqueAlphabet,
                plaqueNumber2 = plaqueNumber2,
                plaqueCity = plaqueCity
            ).collect {networkResult ->
                checkResponse(networkResult = networkResult) {
                    _userInfo.value = ResponseResult.Loading(false)
                    _userInfo.value = ResponseResult.Success(it)
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- getUserInfoByPlaque


    //---------------------------------------------------------------------------------------------- getBearerToken
    fun getBearerToken() = userRepository.getBearerToken()
    //---------------------------------------------------------------------------------------------- getBearerToken


    //---------------------------------------------------------------------------------------------- getAlphabet
    fun getAlphabet() = plaqueRepository.getAlphabet()
    //---------------------------------------------------------------------------------------------- getAlphabet


    //---------------------------------------------------------------------------------------------- getModelForShowImageProfile
    fun getModelForShowImageProfile() =
        userRepository.getModelForShowImageProfile(userInfoEntity = userInfoEntity)
    //---------------------------------------------------------------------------------------------- getModelForShowImageProfile

}