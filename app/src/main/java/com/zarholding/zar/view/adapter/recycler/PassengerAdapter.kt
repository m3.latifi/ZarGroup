package com.zarholding.zar.view.adapter.recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemPassengerBinding
import com.zarholding.zar.model.data.database.entity.UserInfoEntity
import com.zarholding.zar.view.adapter.holder.PassengerItemHolder

/**
 * Created by m-latifi on 11/14/2022.
 */

class PassengerAdapter(
    private val users: MutableList<UserInfoEntity>,
    private val onClick: (UserInfoEntity) -> Unit,
    private val addClick: () -> Unit
) : RecyclerView.Adapter<PassengerItemHolder>() {

    private var layoutInflater: LayoutInflater? = null


    //---------------------------------------------------------------------------------------------- onCreateViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PassengerItemHolder {
        if (layoutInflater == null)
            layoutInflater = LayoutInflater.from(parent.context)
        return PassengerItemHolder(
            binding = ItemPassengerBinding.inflate(layoutInflater!!, parent, false),
            onClick = onClick,
            addClick = addClick
        )
    }
    //---------------------------------------------------------------------------------------------- onCreateViewHolder


    //---------------------------------------------------------------------------------------------- onBindViewHolder
    override fun onBindViewHolder(holder: PassengerItemHolder, position: Int) {
        holder.bind(users[position], position == users.size-1, position == 0)
    }
    //---------------------------------------------------------------------------------------------- onBindViewHolder


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun getItemCount() = users.size
    //---------------------------------------------------------------------------------------------- getItemCount



    //---------------------------------------------------------------------------------------------- addUser
    fun addUser(item: UserInfoEntity) {
        users.add(item)
        notifyItemRangeChanged(0, users.size)
    }
    //---------------------------------------------------------------------------------------------- addUser



    //---------------------------------------------------------------------------------------------- deleteUser
    fun deleteUser(item: UserInfoEntity) {
        notifyItemRangeRemoved(0, users.size)
        users.remove(item)
        notifyItemRangeInserted(0, users.size)
    }
    //---------------------------------------------------------------------------------------------- deleteUser



    //---------------------------------------------------------------------------------------------- getList
    fun getList() = users
    //---------------------------------------------------------------------------------------------- getList

}