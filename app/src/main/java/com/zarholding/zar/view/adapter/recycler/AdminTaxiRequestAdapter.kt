package com.zarholding.zar.view.adapter.recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemAdminTaxiRequestBinding
import com.zarholding.zar.model.data.response.taxi.AdminTaxiRequestModel
import com.zarholding.zar.model.data.response.taxi.TaxiRequestPointModel
import com.zarholding.zar.view.adapter.holder.AdminTaxiRequestHolder

/**
 * Created by m-latifi on 11/18/2022.
 */

class AdminTaxiRequestAdapter(
    private val items: MutableList<AdminTaxiRequestModel>,
    private val token: String,
    private val onAcceptClick: (AdminTaxiRequestModel) -> Unit,
    private val onRejectClick: (AdminTaxiRequestModel) -> Unit,
    private val onMapClick: (TaxiRequestPointModel) -> Unit
) : RecyclerView.Adapter<AdminTaxiRequestHolder>() {

    private var layoutInflater: LayoutInflater? = null


    //---------------------------------------------------------------------------------------------- onCreateViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdminTaxiRequestHolder {
        if (layoutInflater == null)
            layoutInflater = LayoutInflater.from(parent.context)
        return AdminTaxiRequestHolder(
            binding = ItemAdminTaxiRequestBinding.inflate(layoutInflater!!, parent, false),
            onAcceptClick = onAcceptClick,
            onRejectClick = onRejectClick,
            onMapClick = onMapClick
        )
    }
    //---------------------------------------------------------------------------------------------- onCreateViewHolder


    //---------------------------------------------------------------------------------------------- onBindViewHolder
    override fun onBindViewHolder(holder: AdminTaxiRequestHolder, position: Int) {
        holder.bind(items[position], token)
    }
    //---------------------------------------------------------------------------------------------- onBindViewHolder


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun getItemCount() = items.size
    //---------------------------------------------------------------------------------------------- getItemCount


    //---------------------------------------------------------------------------------------------- addRequest
    fun addRequest(list: List<AdminTaxiRequestModel>) {
        val oldSize = items.size
        items.addAll(list)
        notifyItemRangeChanged(oldSize-1, items.size)
    }
    //---------------------------------------------------------------------------------------------- addRequest


}