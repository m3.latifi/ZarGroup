package com.zarholding.zar.view.fragment.user.insurance.signup

import android.Manifest
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.zar.core.enums.EnumApiError
import com.zar.core.tools.extensions.isShabaWithoutIR
import com.zar.core.tools.manager.PermissionManager
import com.zarholding.zar.R
import com.zarholding.zar.ZarFragment
import com.zarholding.zar.databinding.FragmentInsuranceSignupBinding
import com.zarholding.zar.ext.config
import com.zarholding.zar.ext.setTitleAndValue
import com.zarholding.zar.ext.startLoading
import com.zarholding.zar.ext.stopLoading
import com.zarholding.zar.model.data.enum.EnumEntityType
import com.zarholding.zar.model.data.response.insurance.SupplementalInsuranceLevelModel
import com.zarholding.zar.model.data.response.insurance.SupplementalInsuranceModel
import com.zarholding.zar.model.data.response.insurance.SupplementalInsurancePointModel
import com.zarholding.zar.tools.CompanionValues
import com.zarholding.zar.tools.manager.MemoryManager
import com.zarholding.zar.tools.manager.getShimmerBuild
import com.zarholding.zar.view.activity.MainActivity
import com.zarholding.zar.view.adapter.recycler.insurance.InsuranceLevelAdapter
import com.zarholding.zar.view.adapter.recycler.insurance.SpecificInsurancePointAdapter
import com.zarholding.zar.view.dialog.InsuranceRegisterDialog
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * Created by m-latifi on 10/11/2023.
 */

@AndroidEntryPoint
class InsuranceSignupFragment(
    override var layout: Int = R.layout.fragment_insurance_signup
) : ZarFragment<FragmentInsuranceSignupBinding>() {

    @Inject
    lateinit var permissionManager: PermissionManager

    @Inject
    lateinit var memoryManager: MemoryManager

    private val viewModel: InsuranceSignupViewModel by viewModels()
    private var adapterLevel: InsuranceLevelAdapter? = null
    private var insuranceRegisterDialog: InsuranceRegisterDialog? = null


    //---------------------------------------------------------------------------------------------- storagePermissionLauncher
    private val storagePermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions()
        ) { results ->
            permissionManager.checkPermissionResult(results) {
                if (it)
                    downloadPdfFile()
            }
        }
    //---------------------------------------------------------------------------------------------- storagePermissionLauncher



    //---------------------------------------------------------------------------------------------- onViewCreated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }
    //---------------------------------------------------------------------------------------------- onViewCreated


    //---------------------------------------------------------------------------------------------- initView
    private fun initView() {
        binding.shimmerViewContainer.config(getShimmerBuild())
        setOnListener()
        observeLiveDate()
        getSupplementalInsurance()
    }
    //---------------------------------------------------------------------------------------------- initView


    //---------------------------------------------------------------------------------------------- setOnListener
    private fun setOnListener() {

        binding.buttonHistory.setOnClickListener {
            gotoFragment(R.id.action_goto_insuranceHistoryFragment)
        }

        binding.buttonShowSpecific.setOnClickListener {
            viewModel.insuranceListLiveData.value?.points?.let {
                showSpecificInsurance(it)
            }
        }

        binding.checkBoxRead.setOnClickListener {
            viewModel.insuranceListLiveData.value?.let { insurance ->
                if (binding.checkBoxRead.isChecked) {
                    if (viewModel.insuranceLevelLiveData.value.isNullOrEmpty())
                        requestGetSupplementalLevels(insurance.id)
                    else
                        setInsuranceLevelAdapter(viewModel.insuranceLevelLiveData.value!!)
                } else
                    showAndHideLevel(false)
            }
        }

        binding.buttonSelectLevel.setOnClickListener {
            chooseInsuranceLevel()
        }

        binding.textViewDownloadFile.setOnClickListener { storagePermission() }
    }
    //---------------------------------------------------------------------------------------------- setOnListener


    //---------------------------------------------------------------------------------------------- setOnListener
    private fun observeLiveDate() {
        viewModel.errorLiveDate.observe(viewLifecycleOwner) {
            binding.constraintLayoutParent.visibility = View.GONE
            binding.shimmerViewContainer.stopLoading()
            insuranceRegisterDialog?.dismiss()
            showMessage(it.message, it.type)
            when (it.type) {
                EnumApiError.UnAuthorization ->
                    (activity as MainActivity?)?.gotoFirstFragmentWithDeleteUser()

                else -> {}
            }
        }

        viewModel.insuranceListLiveData.observe(viewLifecycleOwner) {
            binding.shimmerViewContainer.stopLoading()
            showNewSupplementalInsurance(newInsurance = it)
        }

        viewModel.insuranceLevelLiveData.observe(viewLifecycleOwner) {
            binding.shimmerViewContainer.stopLoading()
            setInsuranceLevelAdapter(it)
        }

    }
    //---------------------------------------------------------------------------------------------- setOnListener


    //---------------------------------------------------------------------------------------------- getSupplementalInsurance
    private fun getSupplementalInsurance() {
        binding.shimmerViewContainer.startLoading()
        viewModel.requestGetListOfSupplementalInsurance()
    }
    //---------------------------------------------------------------------------------------------- getSupplementalInsurance


    //---------------------------------------------------------------------------------------------- requestGetSupplementalLevels
    private fun requestGetSupplementalLevels(insuranceId: Long) {
        binding.shimmerViewContainer.startLoading()
        viewModel.requestGetSupplementalLevels(insuranceId)
    }
    //---------------------------------------------------------------------------------------------- requestGetSupplementalLevels


    //---------------------------------------------------------------------------------------------- showNewSupplementalInsurance
    private fun showNewSupplementalInsurance(newInsurance: SupplementalInsuranceModel) {

        binding.textViewTitle.setTitleAndValue(
            title = getString(R.string.title),
            splitter = getString(R.string.colon),
            value = newInsurance.title
        )

        binding.textViewCompanyName.setTitleAndValue(
            title = getString(R.string.companyName),
            splitter = getString(R.string.colon),
            value = newInsurance.insurerCompany
        )

        binding.textViewDownloadFile.text =
            getString(R.string.downloadInsuranceFile)

        binding.textViewStartDate.setTitleAndValue(
            title = getString(R.string.startDate),
            splitter = getString(R.string.colon),
            value = newInsurance.startDate
        )

        binding.textViewEndDate.setTitleAndValue(
            title = getString(R.string.endDate),
            splitter = getString(R.string.colon),
            value = newInsurance.endDate
        )

        binding.buttonShowSpecific.visibility = View.VISIBLE
        binding.constraintLayoutParent.visibility = View.VISIBLE

    }
    //---------------------------------------------------------------------------------------------- showNewSupplementalInsurance


    //---------------------------------------------------------------------------------------------- showSpecificInsurance
    private fun showSpecificInsurance(items: List<SupplementalInsurancePointModel>) {
        if (context == null || items.isEmpty())
            return
        val adapter = SpecificInsurancePointAdapter(items)
        val manager = LinearLayoutManager(
            requireContext(), LinearLayoutManager.VERTICAL, false
        )
        binding.recyclerSpecific.adapter = adapter
        binding.recyclerSpecific.layoutManager = manager
        binding.constraintLayoutSpecific.visibility = View.VISIBLE
        binding.buttonSelectLevel.visibility = View.GONE
        binding.textViewChooseLevel.visibility = View.GONE
        binding.textInputLayoutAccountNumber.visibility = View.GONE
        binding.textViewIr.visibility = View.GONE
    }
    //---------------------------------------------------------------------------------------------- showSpecificInsurance


    //---------------------------------------------------------------------------------------------- setInsuranceLevelAdapter
    private fun setInsuranceLevelAdapter(items: List<SupplementalInsuranceLevelModel>) {
        if (context == null || items.isEmpty())
            return
        InsuranceLevelAdapter.selectedPosition = -1
        adapterLevel = InsuranceLevelAdapter(items) {
            InsuranceLevelAdapter.selectedPosition = it
            adapterLevel?.notifyItemRangeChanged(0, items.size)
            binding.buttonSelectLevel.visibility = View.VISIBLE
            binding.textViewChooseLevel.visibility = View.VISIBLE
            binding.textInputLayoutAccountNumber.visibility = View.VISIBLE
            binding.textViewIr.visibility = View.VISIBLE
        }
        val manager = LinearLayoutManager(
            requireContext(), LinearLayoutManager.HORIZONTAL, true
        )
        binding.recyclerLevel.adapter = adapterLevel
        binding.recyclerLevel.layoutManager = manager
        showAndHideLevel(true)
    }
    //---------------------------------------------------------------------------------------------- setInsuranceLevelAdapter


    //---------------------------------------------------------------------------------------------- showAndHideLevel
    private fun showAndHideLevel(isShowing: Boolean) {
        binding.buttonSelectLevel.visibility = View.GONE
        binding.textViewChooseLevel.visibility = View.GONE
        binding.textInputLayoutAccountNumber.visibility = View.GONE
        binding.textViewIr.visibility = View.GONE
        if (isShowing) {
            binding.recyclerLevel.visibility = View.VISIBLE
            binding.textViewChooseLevel.visibility = View.VISIBLE
        } else {
            binding.recyclerLevel.visibility = View.GONE
            binding.textViewChooseLevel.visibility = View.GONE
        }
    }
    //---------------------------------------------------------------------------------------------- showAndHideLevel


    //---------------------------------------------------------------------------------------------- chooseInsuranceLevel
    private fun chooseInsuranceLevel() {

        if (InsuranceLevelAdapter.selectedPosition == -1) {
            showMessage(getString(R.string.chooseInsuranceLevelPlease))
            return
        }
        if (!binding.textInputEditAccountNumber.text.toString().isShabaWithoutIR()) {
            binding.textInputEditAccountNumber.error = getString(R.string.shabaNumberWithoutIr)
            return
        }
        viewModel.shabaNumber = binding.textInputEditAccountNumber.text.toString()
        viewModel.chooseInsuranceLevelPosition = InsuranceLevelAdapter.selectedPosition
        insuranceRegisterDialog =
            InsuranceRegisterDialog(viewModel) {
                activity?.onBackPressedDispatcher?.onBackPressed()
            }
        insuranceRegisterDialog?.show(childFragmentManager, "InsuranceRegisterDialog")

    }
    //---------------------------------------------------------------------------------------------- chooseInsuranceLevel



    //---------------------------------------------------------------------------------------------- cameraPermission
    private fun storagePermission() {
        if (!memoryManager.checkFreeSpaceForDownloading()) {
            showMessage(getString(R.string.internalMemoryIsFull))
            return
        }
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.Q) {
            val permissions = listOf(
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            val check = permissionManager.isPermissionGranted(
                permissions = permissions,
                launcher = storagePermissionLauncher
            )
            if (check)
                downloadPdfFile()
        } else
            downloadPdfFile()
    }
    //---------------------------------------------------------------------------------------------- cameraPermission



    //---------------------------------------------------------------------------------------------- downloadPdfFile
    private fun downloadPdfFile() {
        viewModel.insuranceListLiveData.value?.let { insurance ->
            val bundle = Bundle()
            bundle.putString(CompanionValues.DOWNLOAD_URL, insurance.fileName)
            bundle.putString(CompanionValues.DOWNLOAD_TYPE, EnumEntityType.insurance.name)
            gotoFragment(R.id.action_goto_DownloadUpdateFragment, bundle)
        }
    }
    //---------------------------------------------------------------------------------------------- downloadPdfFile

}