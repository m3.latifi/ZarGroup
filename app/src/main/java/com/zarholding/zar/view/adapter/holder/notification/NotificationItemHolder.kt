package com.zarholding.zar.view.adapter.holder.notification
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.R
import com.zarholding.zar.databinding.ItemNotificationBinding
import com.zarholding.zar.ext.setElapseTime
import com.zarholding.zar.model.data.response.notification.NotificationModel

/**
 * Created by m-latifi on 11/17/2022.
 */

class NotificationItemHolder(
    private val binding: ItemNotificationBinding,
    private var categoryPosition: Int,
    private val onClick: (categoryPosition: Int, notificationPosition: Int) -> Unit
) :
    RecyclerView.ViewHolder(binding.root) {


    //---------------------------------------------------------------------------------------------- bind
    fun bind(item: NotificationModel, position: Int) {
        setValueToXml(item)
        setListener(item, position)
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind


    //---------------------------------------------------------------------------------------------- setValueToXml
    private fun setValueToXml(item: NotificationModel) {
        if (item.isRead)
            binding.constraintLayoutParent.setBackgroundColor(
                binding.constraintLayoutParent.context
                .resources
                .getColor(
                    R.color.n_notificationRead,
                    binding.constraintLayoutParent.context.theme
                ))
        else
            binding.constraintLayoutParent.setBackgroundColor(
                binding.constraintLayoutParent.context
                    .resources
                    .getColor(
                        R.color.n_notificationUnRead,
                        binding.constraintLayoutParent.context.theme
                    )
            )

        binding.checkboxRead.setElapseTime(dateTime = item.createDate)
        binding.textViewSenderName.text = item.senderName
        binding.textViewMessageSubject.text = item.subject
    }
    //---------------------------------------------------------------------------------------------- setValueToXml


    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener(item: NotificationModel, position: Int) {
        binding.root.setOnClickListener { onClick(categoryPosition, position) }
        binding.checkboxRead.setOnCheckedChangeListener { _, b ->
            item.select = b
        }
    }
    //---------------------------------------------------------------------------------------------- setListener
}