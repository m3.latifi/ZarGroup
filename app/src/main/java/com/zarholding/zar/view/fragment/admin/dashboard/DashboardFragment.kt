package com.zarholding.zar.view.fragment.admin.dashboard

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.zarholding.zar.R
import com.zarholding.zar.view.adapter.recycler.DashboardAppAdapter
import com.zarholding.zar.ZarFragment
import com.zarholding.zar.databinding.FragmentDashboardBinding
import dagger.hilt.android.AndroidEntryPoint


/**
 * Created by m-latifi on 10/9/2022.
 */

@AndroidEntryPoint
class DashboardFragment(override var layout: Int = R.layout.fragment_dashboard) :
    ZarFragment<FragmentDashboardBinding>(){

    private val dashboardViewModel : DashboardViewModel by viewModels()

    //---------------------------------------------------------------------------------------------- onViewCreated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }
    //---------------------------------------------------------------------------------------------- onViewCreated



    //---------------------------------------------------------------------------------------------- initView
    private fun initView() {
        if (context == null)
            return
        val adapter = DashboardAppAdapter(dashboardViewModel.setApp()){
            if (it.link != 0)
                gotoFragment(it.link, it.bundle)
        }
        val gridLayoutManager = GridLayoutManager(
            requireContext(),
            3,
            GridLayoutManager.VERTICAL,
            false
        )

        binding.recyclerViewApps.layoutManager = gridLayoutManager
        binding.recyclerViewApps.layoutDirection = View.LAYOUT_DIRECTION_RTL
        binding.recyclerViewApps.adapter = adapter

    }
    //---------------------------------------------------------------------------------------------- initView

}