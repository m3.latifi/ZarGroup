package com.zarholding.zar.view.adapter.holder

import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.R
import com.zarholding.zar.databinding.ItemPersonnelSelectBinding
import com.zarholding.zar.ext.loadImageByToken
import com.zarholding.zar.model.data.database.entity.UserInfoEntity

/**
 * Created by m-latifi on 11/17/2022.
 */

class PersonnelSelectHolder(
    private val binding: ItemPersonnelSelectBinding,
    private val onClick: (UserInfoEntity) -> Unit
) : RecyclerView.ViewHolder(binding.root) {


    //---------------------------------------------------------------------------------------------- bind
    fun bind(item : UserInfoEntity, token: String) {
        setValueToXml(item, token)
        setListener(item)
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind



    //---------------------------------------------------------------------------------------------- setValueToXml
    private fun setValueToXml(item : UserInfoEntity, token: String) {
/*        var title = item.fullName
        item.personnelJobKeyText?.let {
            title += System.getProperty("line.separator")
            title += binding.textViewName.context.getString(R.string.jobTitleDot)
            title += " ${item.personnelJobKeyText}"
        }
        item.organizationUnit?.let {
            title += System.getProperty("line.separator")
            title += binding.textViewName.context.getString(R.string.organizationUnitDot)
            title += " ${item.organizationUnit}"
        }*/
        binding.textViewName.text = item.fullName
        binding.textViewPersonnelCode.text = item.personnelNumber
        binding.imageViewIcon.loadImageByToken(
            url = item.userName,
            token = token
        )
    }
    //---------------------------------------------------------------------------------------------- setValueToXml


    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener(item : UserInfoEntity) {
        binding.root.setOnClickListener {
            onClick(item)
        }
    }
    //---------------------------------------------------------------------------------------------- setListener
}