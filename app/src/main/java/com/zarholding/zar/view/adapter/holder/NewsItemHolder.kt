package com.zarholding.zar.view.adapter.holder

import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemNewsBinding
import com.zarholding.zar.ext.loadImage
import com.zarholding.zar.model.data.database.entity.ArticleEntity
import com.zarholding.zar.model.data.enum.EnumEntityType

/**
 * Created by m-latifi on 11/15/2022.
 */

class NewsItemHolder(
    private val binding: ItemNewsBinding,
    private val onClick: (ArticleEntity) -> Unit
) : RecyclerView.ViewHolder(binding.root) {


    //---------------------------------------------------------------------------------------------- bind
    fun bind(item: ArticleEntity) {
        setValueToXml(item)
        setListener(item)
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind


    //---------------------------------------------------------------------------------------------- setValueToXml
    private fun setValueToXml(item: ArticleEntity) {
        binding.textViewTitle.text = item.title
        binding.textViewSmallContent.text = item.summary
        binding.imageViewLogo.loadImage(
            url = item.imageName,
            entityType = EnumEntityType.articles.name,
            setPlaceholder = false
        )
    }
    //---------------------------------------------------------------------------------------------- setValueToXml


    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener(item: ArticleEntity) {
        binding.root.setOnClickListener { onClick(item) }
    }
    //---------------------------------------------------------------------------------------------- setListener
}