package com.zarholding.zar.view.fragment.user.insurance.signup

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.zarholding.zar.R
import com.zarholding.zar.ZarViewModel
import com.zarholding.zar.model.data.request.insurance.InsuranceRelationShipModel
import com.zarholding.zar.model.data.request.insurance.RegisterSupplementalInsuranceModel
import com.zarholding.zar.model.data.response.dropdown.DropDownModel
import com.zarholding.zar.model.data.response.insurance.SupplementalInsuranceLevelModel
import com.zarholding.zar.model.data.response.insurance.SupplementalInsuranceModel
import com.zarholding.zar.model.repository.DropdownRepository
import com.zarholding.zar.model.repository.SupplementalInsuranceRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by m-latifi on 10/11/2023.
 */

@HiltViewModel
class InsuranceSignupViewModel @Inject constructor(
    private val repository: SupplementalInsuranceRepository,
    private val dropdownRepository: DropdownRepository
) : ZarViewModel() {

    var chooseInsuranceLevelPosition = -1
    var shabaNumber = ""

    val insuranceRelationShips = mutableListOf<InsuranceRelationShipModel>()

    val insuranceListLiveData: MutableLiveData<SupplementalInsuranceModel> by lazy {
        MutableLiveData<SupplementalInsuranceModel>()
    }

    val insuranceLevelLiveData: MutableLiveData<List<SupplementalInsuranceLevelModel>> by lazy {
        MutableLiveData<List<SupplementalInsuranceLevelModel>>()
    }

    val relationShipLiveData: MutableLiveData<List<DropDownModel>> by lazy {
        MutableLiveData<List<DropDownModel>>()
    }

    val registerLiveData: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>()
    }

    //---------------------------------------------------------------------------------------------- requestGetListOfSupplementalInsurance
    fun requestGetListOfSupplementalInsurance() {
        viewModelScope.launch(IO + exceptionHandler()) {
            callApi(
                request = repository.requestGetListOfSupplementalInsurance(),
                showMessageAfterSuccessResponse = false
            ) {
                if (it.items?.isNotEmpty() == true) {
                    insuranceListLiveData.postValue(it.items.first())
                } else
                    setMessage(resourcesProvider.getString(R.string.insuranceIsEmpty))
            }
        }
    }
    //---------------------------------------------------------------------------------------------- requestGetListOfSupplementalInsurance


    //---------------------------------------------------------------------------------------------- requestGetSupplementalLevels
    fun requestGetSupplementalLevels(insuranceId: Long) {
        viewModelScope.launch(IO + exceptionHandler()) {
            callApi(
                request = repository.requestGetSupplementalLevels(insuranceId),
                showMessageAfterSuccessResponse = false
            ) {
                requestGetRelationShip(it)
            }
        }
    }
    //---------------------------------------------------------------------------------------------- requestGetSupplementalLevels


    //---------------------------------------------------------------------------------------------- requestGetRelationShip
    private fun requestGetRelationShip(insuranceLevelList: List<SupplementalInsuranceLevelModel>) {
        viewModelScope.launch(IO + exceptionHandler()) {

            callApi(
                request = dropdownRepository.requestGetRelationShip(),
                showMessageAfterSuccessResponse = false
            ) {
                if (!it.items.isNullOrEmpty())
                    relationShipLiveData.postValue(it.items)
                insuranceLevelLiveData.postValue(insuranceLevelList)
            }
        }
    }
    //---------------------------------------------------------------------------------------------- requestGetRelationShip


    //---------------------------------------------------------------------------------------------- requestRegisterSupplementalInsurance
    fun requestRegisterSupplementalInsurance() {
        if (shabaNumber.isEmpty() ||
            chooseInsuranceLevelPosition == -1 ||
            insuranceLevelLiveData.value.isNullOrEmpty()
        )
            return
        viewModelScope.launch(IO + exceptionHandler()) {
            val request = RegisterSupplementalInsuranceModel(
                accountNumber = shabaNumber,
                supplementalInsuranceLevelId = insuranceLevelLiveData.value!![chooseInsuranceLevelPosition].id,
                dependentList = insuranceRelationShips
            )
            callApi(
                request = repository.requestRegisterSupplementalInsurance(request),
                showMessageAfterSuccessResponse = true
            ) {
                registerLiveData.postValue(it)
            }
        }
    }
    //---------------------------------------------------------------------------------------------- requestRegisterSupplementalInsurance

}