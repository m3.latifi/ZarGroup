package com.zarholding.zar.view.custom

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.res.ResourcesCompat
import com.zarholding.zar.R

/**
 * Created by m-latifi on 5/29/2023.
 */

class CustomMenu @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {


    private lateinit var cardView: CardView
    private lateinit var imageViewIcon: ImageView
    private lateinit var textViewTitle: TextView
    private var unSelectColor: Int = Color.WHITE
    private var selectedColor: Int = Color.BLACK
    private var selectedMenu = false
    private var duration: Long = 300
    private var fontFamilyId: Int = 0


    //---------------------------------------------------------------------------------------------- init
    init {
        init(attrs)
    }
    //---------------------------------------------------------------------------------------------- init


    //---------------------------------------------------------------------------------------------- init
    private fun init(attrs: AttributeSet?) {
        View.inflate(context, R.layout.layout_menu, this)

        imageViewIcon = findViewById(R.id.imageViewIcon)
        textViewTitle = findViewById(R.id.textViewTitle)
        cardView = findViewById(R.id.cardView)

        val ta = context.obtainStyledAttributes(attrs, R.styleable.CustomMenu)
        try {
            duration = ta.getInt(R.styleable.CustomMenu_menu_animationDuration, 300).toLong()
            unSelectColor = ta.getColor(R.styleable.CustomMenu_menu_unSelectColor, Color.WHITE)
            selectedColor = ta.getColor(R.styleable.CustomMenu_menu_selectColor, Color.BLACK)
            fontFamilyId = ta.getResourceId(R.styleable.CustomMenu_android_fontFamily, 0)
            val title = ta.getString(R.styleable.CustomMenu_menu_title)
            val textSize = ta.getDimensionPixelSize(R.styleable.CustomMenu_menu_textSize, 11) /
                    resources.displayMetrics.density
            textViewTitle.setTextColor(selectedColor)
            textViewTitle.text = title
            textViewTitle.textSize = textSize
            textViewTitle.typeface = ResourcesCompat.getFont(context, fontFamilyId)
            val drawableId = ta.getResourceId(R.styleable.CustomMenu_menu_icon, 0)
            if (drawableId != 0) {
                val drawable = AppCompatResources.getDrawable(context, drawableId)
                imageViewIcon.setImageDrawable(drawable)
            }
            cardView.setCardBackgroundColor(selectedColor)

        } finally {
            ta.recycle()
        }

        unSelect()

        setOnClickListener {
            selected()
        }
    }
    //---------------------------------------------------------------------------------------------- init




    //---------------------------------------------------------------------------------------------- unSelect
    fun unSelect() {
        selectedMenu = false
        cardView.animate().translationY(-100f).setDuration(duration).start()
        textViewTitle.animate().translationY(100f).setDuration(duration).start()
        imageViewIcon.animate().translationY(0f).setDuration(duration).start()
        imageViewIcon.setColorFilter(
            unSelectColor,
            android.graphics.PorterDuff.Mode.SRC_IN
        )
    }
    //---------------------------------------------------------------------------------------------- unSelect


    //---------------------------------------------------------------------------------------------- selected
    fun selected() {
        if (!selectedMenu) {
            selectedMenu = true
            cardView.animate().translationY(0f).setDuration(duration).start()
            textViewTitle.animate().translationY(0f).setDuration(duration).start()
            imageViewIcon.animate().translationY(100f).setDuration(duration).start()
            imageViewIcon.setColorFilter(
                selectedColor,
                android.graphics.PorterDuff.Mode.SRC_IN
            )
        }
    }
    //---------------------------------------------------------------------------------------------- selected


}