package com.zarholding.zar.view.fragment.user.hse

import android.Manifest
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.LinearLayoutManager
import com.zar.core.tools.manager.PermissionManager
import com.zarholding.zar.R
import com.zarholding.zar.ZarFragment
import com.zarholding.zar.databinding.FragmentHseExaminationBinding
import com.zarholding.zar.ext.config
import com.zarholding.zar.ext.startLoading
import com.zarholding.zar.ext.stopLoading
import com.zarholding.zar.model.data.enum.EnumEntityType
import com.zarholding.zar.model.data.request.hse.HseExaminationModel
import com.zarholding.zar.model.data.response.ResponseResult
import com.zarholding.zar.tools.CompanionValues
import com.zarholding.zar.tools.manager.MemoryManager
import com.zarholding.zar.tools.manager.getShimmerBuild
import com.zarholding.zar.view.adapter.recycler.hse.HseExaminationAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by m-latifi on 11/9/2022.
 */

@AndroidEntryPoint
class HseExaminationFragment(override var layout: Int = R.layout.fragment_hse_examination) :
    ZarFragment<FragmentHseExaminationBinding>() {

    private val viewModel: HseViewModel by viewModels()

    @Inject
    lateinit var permissionManager: PermissionManager

    @Inject
    lateinit var memoryManager: MemoryManager

    private var selectedFileName = ""
    //---------------------------------------------------------------------------------------------- storagePermissionLauncher
    private val storagePermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions()
        ) { results ->
            permissionManager.checkPermissionResult(results) {
                if (it)
                    downloadPdfFile()
            }
        }
    //---------------------------------------------------------------------------------------------- storagePermissionLauncher



    //---------------------------------------------------------------------------------------------- onViewCreated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()

    }
    //---------------------------------------------------------------------------------------------- onViewCreated


    //---------------------------------------------------------------------------------------------- initView
    private fun initView() {
        binding.shimmerViewContainer.config(getShimmerBuild())
        observeLiveDate()
        getHseExaminations()
    }
    //---------------------------------------------------------------------------------------------- initView



    //---------------------------------------------------------------------------------------------- getHseExaminations
    private fun getHseExaminations() {
        binding.shimmerViewContainer.startLoading()
        viewModel.requestGetExaminations(arguments = arguments)
    }
    //---------------------------------------------------------------------------------------------- getHseExaminations


    //---------------------------------------------------------------------------------------------- observeLiveDate
    private fun observeLiveDate() {
        viewModel.errorLiveDate.observe(viewLifecycleOwner) {
            binding.shimmerViewContainer.stopLoading()
            showMessage(it.message, it.type)
        }

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.hseExaminations.collect {
                    when(it) {
                        is ResponseResult.Loading -> {}
                        is ResponseResult.Success -> {
                            setExaminationsAdapter(it.data)
                        }
                    }
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- observeLiveDate



    //---------------------------------------------------------------------------------------------- setExaminationsAdapter
    private fun setExaminationsAdapter(items: List<HseExaminationModel>) {
        binding.shimmerViewContainer.stopLoading()
        if (context == null)
            return
        val adapter = HseExaminationAdapter(items) {
            selectedFileName = it
            storagePermission()
        }
        val manager =  LinearLayoutManager(
            context,
            LinearLayoutManager.VERTICAL,
            false
        )
        binding.recyclerHse.layoutManager = manager
        binding.recyclerHse.layoutDirection = View.LAYOUT_DIRECTION_RTL
        binding.recyclerHse.adapter = adapter
    }
    //---------------------------------------------------------------------------------------------- setExaminationsAdapter


    //---------------------------------------------------------------------------------------------- cameraPermission
    private fun storagePermission() {
        if (!memoryManager.checkFreeSpaceForDownloading()) {
            showMessage(getString(R.string.internalMemoryIsFull))
            return
        }
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.Q) {
            val permissions = listOf(
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            val check = permissionManager.isPermissionGranted(
                permissions = permissions,
                launcher = storagePermissionLauncher
            )
            if (check)
                downloadPdfFile()
        } else
            downloadPdfFile()
    }
    //---------------------------------------------------------------------------------------------- cameraPermission



    //---------------------------------------------------------------------------------------------- downloadPdfFile
    private fun downloadPdfFile() {
        val bundle = Bundle()
        bundle.putString(CompanionValues.DOWNLOAD_URL, selectedFileName)
        bundle.putString(CompanionValues.DOWNLOAD_TYPE, EnumEntityType.HSE.name)
        gotoFragment(R.id.action_goto_DownloadUpdateFragment, bundle)
    }
    //---------------------------------------------------------------------------------------------- downloadPdfFile

}