package com.zarholding.zar.view.fragment.user.quota.history

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.zarholding.zar.R
import com.zarholding.zar.ZarViewModel
import com.zarholding.zar.model.data.response.quota.QuotaHistoryModel
import com.zarholding.zar.model.repository.QuotaRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by m-latifi on 9/2/2023.
 */

@HiltViewModel
class PersonnelQuotaHistoryViewModel @Inject constructor(
    private val quotaRepository: QuotaRepository
): ZarViewModel() {

    private val quotaHistory = mutableListOf<QuotaHistoryModel>()

    val quotaLiveData: MutableLiveData<List<QuotaHistoryModel>> by lazy {
        MutableLiveData<List<QuotaHistoryModel>>()
    }


    //---------------------------------------------------------------------------------------------- getMyQuotaHistory
    fun getMyQuotaHistory(){
        viewModelScope.launch(IO + exceptionHandler()) {
            if (quotaHistory.isEmpty())
                getMoreMyQuotaHistory()
            else
                quotaLiveData.postValue(quotaHistory)
        }
    }
    //---------------------------------------------------------------------------------------------- getMyQuotaHistory



    //---------------------------------------------------------------------------------------------- getMoreMyQuotaHistory
    private fun getMoreMyQuotaHistory() {
        viewModelScope.launch(IO + exceptionHandler()) {
            callApi(
                request = quotaRepository.getMyQuotaHistory(),
                onReceiveData = {
                    if (!it.items.isNullOrEmpty()) {
                        quotaHistory.addAll(it.items)
                        quotaLiveData.postValue(quotaHistory)
                    } else
                        setMessage(resourcesProvider.getString(R.string.quotaIsEmpty))
                }
            )
        }
    }
    //---------------------------------------------------------------------------------------------- getMoreMyQuotaHistory

}