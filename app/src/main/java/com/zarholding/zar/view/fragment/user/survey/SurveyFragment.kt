package com.zarholding.zar.view.fragment.user.survey

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SnapHelper
import com.zar.core.enums.EnumApiError
import com.zarholding.zar.R
import com.zarholding.zar.ZarFragment
import com.zarholding.zar.databinding.FragmentSurveyBinding
import com.zarholding.zar.ext.config
import com.zarholding.zar.ext.hideKeyboard
import com.zarholding.zar.ext.startLoading
import com.zarholding.zar.ext.stopLoading
import com.zarholding.zar.model.data.response.survey.SurveyModel
import com.zarholding.zar.model.data.response.survey.SurveyQuestionModel
import com.zarholding.zar.tools.manager.getShimmerBuild
import com.zarholding.zar.view.activity.MainActivity
import com.zarholding.zar.view.adapter.recycler.survey.SurveyAdapter
import com.zarholding.zar.view.adapter.recycler.survey.SurveyQuestionAdapter
import com.zarholding.zar.view.dialog.ConfirmDialog
import dagger.hilt.android.AndroidEntryPoint

/**
 * Create by Mehrdad on 1/21/2023
 */

@AndroidEntryPoint
class SurveyFragment(override var layout: Int = R.layout.fragment_survey) :
    ZarFragment<FragmentSurveyBinding>() {

    private val surveyViewModel: SurveyViewModel by viewModels()
    private var surveyAdapter: SurveyAdapter? = null
    private var currentPosition: Int = 0
    var scroll = false

    //---------------------------------------------------------------------------------------------- onViewCreated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        setListener()
    }
    //---------------------------------------------------------------------------------------------- onViewCreated


    //---------------------------------------------------------------------------------------------- initView
    private fun initView() {
        binding.shimmerViewContainer.config(getShimmerBuild())
        binding.expandableContent.expand()
        binding.expandableQuestions.collapse()
        observeLiveDate()
        getUserSurvey()
    }
    //---------------------------------------------------------------------------------------------- initView


    //---------------------------------------------------------------------------------------------- observeLiveDate
    private fun observeLiveDate() {
        surveyViewModel.errorLiveDate.observe(viewLifecycleOwner) {
            binding.shimmerViewContainer.stopLoading()
            binding.materialButtonSave.stopLoading()
            showMessage(it.message, it.type)
            surveyAdapter?.notifyItemRangeChanged(0, surveyAdapter?.itemCount ?: 0)
            when (it.type) {
                EnumApiError.UnAuthorization ->
                    (activity as MainActivity?)?.gotoFirstFragmentWithDeleteUser()

                else -> {}
            }
        }
        surveyViewModel.surveysLiveData.observe(viewLifecycleOwner) {
            binding.shimmerViewContainer.stopLoading()
            setSurveyList(it)
        }
        surveyViewModel.surveyDetailLiveData.observe(viewLifecycleOwner) {
            setQuestionsSlider(it)
        }

        surveyViewModel.surveySubmit.observe(viewLifecycleOwner) {
            if (it)
                activity?.onBackPressedDispatcher?.onBackPressed()
        }
    }
    //---------------------------------------------------------------------------------------------- observeLiveDate


    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener() {
        binding.materialButtonNext.setOnClickListener {
            val current = (binding.recyclerViewQuestion.layoutManager as LinearLayoutManager)
                .findFirstVisibleItemPosition()
            if (surveyViewModel.checkValidationAnswerByIndex(current)) {
                scroll = true
                binding.recyclerViewQuestion.smoothScrollToPosition(current + 1)
            } else
                showMessage(message = getString(R.string.answerTheSurveyQuestion))
            hideKeyboard()
        }

        binding.materialButtonPrevious.setOnClickListener {
            val current = (binding.recyclerViewQuestion.layoutManager as LinearLayoutManager)
                .findFirstVisibleItemPosition()
            if (current > 0) {
                scroll = true
                binding.recyclerViewQuestion.smoothScrollToPosition(current - 1)
            }
            hideKeyboard()
        }

        binding.materialButtonSave.setOnClickListener {
            hideKeyboard()
            val current = (binding.recyclerViewQuestion.layoutManager as LinearLayoutManager)
                .findFirstVisibleItemPosition()
            if (!surveyViewModel.checkValidationAnswerByIndex(current)) {
                showMessage(message = getString(R.string.answerTheSurveyQuestion))
                return@setOnClickListener
            }

            if (binding.materialButtonSave.isLoading)
                return@setOnClickListener

            if (context == null)
                return@setOnClickListener
            ConfirmDialog(
                context = requireContext(),
                type = ConfirmDialog.ConfirmType.WARNING,
                title = getString(R.string.doYouWantToSubmitSurvey),
                onYesClick = {
                    binding.materialButtonSave
                        .startLoading(getString(R.string.bePatient))
                    surveyViewModel.submitSurvey()
                },
                onShowDialog = { (activity as MainActivity?)?.enableBlurView() },
                onDismissDialog = { (activity as MainActivity?)?.disableBlurView() }
            ).show()
        }
    }
    //---------------------------------------------------------------------------------------------- setListener


    //---------------------------------------------------------------------------------------------- getUserSurvey
    private fun getUserSurvey() {
        binding.shimmerViewContainer.startLoading()
        surveyViewModel.getSurveys()
    }
    //---------------------------------------------------------------------------------------------- getUserSurvey


    //---------------------------------------------------------------------------------------------- setSurveyList
    private fun setSurveyList(surveys: List<SurveyModel>) {
        if (context == null)
            return
        surveyAdapter = SurveyAdapter(surveys) {
            surveyViewModel.requestGetSurveyDetail(it)
        }
        val manager = LinearLayoutManager(
            requireContext(), LinearLayoutManager.VERTICAL, false
        )
        binding.recyclerViewSurvey.layoutManager = manager
        binding.recyclerViewSurvey.adapter = surveyAdapter
    }
    //---------------------------------------------------------------------------------------------- setSurveyList


    //---------------------------------------------------------------------------------------------- setQuestionsSlider
    private fun setQuestionsSlider(questions: List<SurveyQuestionModel>) {
        if (context == null)
            return
        startSurvey()
        currentPosition = 0
        val adapter = SurveyQuestionAdapter(questions)
        val manager = object : LinearLayoutManager(
            requireContext(), HORIZONTAL, false
        ){ override fun canScrollHorizontally() = scroll }
        manager.reverseLayout = true
        binding.recyclerViewQuestion.layoutManager = manager
        val snapHelper: SnapHelper = PagerSnapHelper()
        binding.recyclerViewQuestion.adapter = adapter
        snapHelper.attachToRecyclerView(binding.recyclerViewQuestion)
        binding.recyclerViewQuestion.addOnScrollListener(getRecyclerViewQuestionScrollListener())
/*        val listener = object : RecyclerView.OnItemTouchListener{
            override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
                return true
            }

            override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {
            }

            override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {
            }

        }
        binding.recyclerViewQuestion.addOnItemTouchListener(listener)*/
    }
    //---------------------------------------------------------------------------------------------- setQuestionsSlider


    //---------------------------------------------------------------------------------------------- startSurvey
    private fun startSurvey() {
        binding.materialButtonSave.visibility = View.GONE
        binding.materialButtonPrevious.visibility = View.GONE
        binding.expandableContent.collapse()
        binding.expandableQuestions.expand()
    }
    //---------------------------------------------------------------------------------------------- startSurvey


    //---------------------------------------------------------------------------------------------- getRecyclerViewQuestionScrollListener
    private fun getRecyclerViewQuestionScrollListener() = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            scroll = false
            val count = (binding.recyclerViewQuestion.layoutManager as LinearLayoutManager)
                .itemCount
            val current = (binding.recyclerViewQuestion.layoutManager as LinearLayoutManager)
                .findFirstVisibleItemPosition()
            when (current) {
                0 -> {
                    binding.materialButtonSave.visibility = View.GONE
                    binding.materialButtonPrevious.visibility = View.GONE
                    binding.materialButtonNext.visibility = View.VISIBLE
                }

                count - 1 -> {
                    binding.materialButtonSave.visibility = View.VISIBLE
                    binding.materialButtonPrevious.visibility = View.VISIBLE
                    binding.materialButtonNext.visibility = View.GONE
                }

                else -> {
                    binding.materialButtonSave.visibility = View.GONE
                    binding.materialButtonPrevious.visibility = View.VISIBLE
                    binding.materialButtonNext.visibility = View.VISIBLE
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- getRecyclerViewQuestionScrollListener


}