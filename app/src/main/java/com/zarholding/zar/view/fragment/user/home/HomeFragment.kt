package com.zarholding.zar.view.fragment.user.home

import android.os.Bundle
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.LinearLayoutManager
import com.zar.core.enums.EnumApiError
import com.zarholding.zar.R
import com.zarholding.zar.model.data.database.entity.ArticleEntity
import com.zarholding.zar.view.activity.MainActivity
import com.zarholding.zar.view.dialog.ArticleDetailDialog
import com.zarholding.zar.view.dialog.ConfirmDialog
import com.zarholding.zar.view.adapter.recycler.AppAdapter
import com.zarholding.zar.view.adapter.recycler.NewsAdapter
import com.zarholding.zar.view.adapter.recycler.RequestAdapter
import com.zarholding.zar.ZarFragment
import com.zarholding.zar.databinding.FragmentHomeBinding
import com.zarholding.zar.ext.config
import com.zarholding.zar.ext.startLoading
import com.zarholding.zar.ext.stopLoading
import com.zarholding.zar.model.data.response.ResponseResult
import com.zarholding.zar.tools.manager.getShimmerBuild
import com.zarholding.zar.view.dialog.feature.NewFeatureDialog
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

/**
 * Created by m-latifi on 11/13/2022.
 */

@AndroidEntryPoint
class HomeFragment(override var layout: Int = R.layout.fragment_home) :
    ZarFragment<FragmentHomeBinding>() {

    private val homeViewModel: HomeViewModel by viewModels()


    //---------------------------------------------------------------------------------------------- onViewCreated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }
    //---------------------------------------------------------------------------------------------- onViewCreated


    //---------------------------------------------------------------------------------------------- initView
    private fun initView() {
        binding.shimmerViewContainer.config(getShimmerBuild())
        observeLiveDate()
        backClickControl()
        setAppsAdapter()
        setPersonnelRequestAdapter()
        requestGetData()
        setListener()
    }
    //---------------------------------------------------------------------------------------------- initView


    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener() {
        binding.constraintLayoutSurvey.setOnClickListener {
//            showMessage(message = getString(R.string.disableFeature), type = EnumApiError.Warning)
            gotoFragment(R.id.action_goto_SurveyFragment)
        }
        binding.constraintLayoutNew.setOnClickListener {
            showMessage(message = getString(R.string.disableFeature), type = EnumApiError.Warning)
        }
    }
    //---------------------------------------------------------------------------------------------- setListener

    //---------------------------------------------------------------------------------------------- backClickControl
    private fun backClickControl() {
        activity?.onBackPressedDispatcher?.addCallback(viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    context?.let {
                        ConfirmDialog(
                            context = it,
                            type = ConfirmDialog.ConfirmType.DELETE,
                            title = getString(R.string.doYouWantToExitApp),
                            onYesClick = { activity?.finish() },
                            onShowDialog = { (activity as MainActivity?)?.enableBlurView() },
                            onDismissDialog = { (activity as MainActivity?)?.disableBlurView() }).show()
                    }
                }
            })
    }
    //---------------------------------------------------------------------------------------------- backClickControl


    //---------------------------------------------------------------------------------------------- observeLiveDate
    private fun observeLiveDate() {
        homeViewModel.errorLiveDate.observe(viewLifecycleOwner) {
            binding.shimmerViewContainer.stopLoading()
            showMessage(it.message, it.type)
            when (it.type) {
                EnumApiError.UnAuthorization -> (activity as MainActivity?)?.gotoFirstFragmentWithDeleteUser()
                else -> {}
            }
        }

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                homeViewModel.articleList.collect {
                    binding.shimmerViewContainer.stopLoading()
                    when(it) {
                        is ResponseResult.Loading -> {}
                        is ResponseResult.Success -> {
                            setBannerSlider(it.data)
                        }
                    }
                }
            }
        }

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                homeViewModel.isNewVersion.collect {
                    if (it)
                        NewFeatureDialog().show(childFragmentManager, "feature dialog")
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- observeLiveDate


    //---------------------------------------------------------------------------------------------- requestGetData
    private fun requestGetData() {
        binding.shimmerViewContainer.startLoading()
        homeViewModel.requestGetArticles()
    }
    //---------------------------------------------------------------------------------------------- requestGetData


    //---------------------------------------------------------------------------------------------- setAppsAdapter
    private fun setAppsAdapter() {
        if (context == null)
            return
        val adapter = AppAdapter(homeViewModel.getApp()) {
            if (it != 0)
                gotoFragment(it)
        }
        val linearLayoutManager = LinearLayoutManager(
            requireContext(),
            LinearLayoutManager.HORIZONTAL,
            false
        )
        linearLayoutManager.reverseLayout = true
        binding.recyclerViewApps.layoutManager = linearLayoutManager
        binding.recyclerViewApps.adapter = adapter
    }
    //---------------------------------------------------------------------------------------------- setAppsAdapter


    //---------------------------------------------------------------------------------------------- setBannerSlider
    private fun setBannerSlider(slides: List<ArticleEntity>) {
//        val adapter = BannerAdapter(slides)
        if (context == null)
            return
        val adapter = NewsAdapter(slides) {
            ArticleDetailDialog(
                requireContext(),
                it,
                onShowDialog = { (activity as MainActivity?)?.enableBlurView() },
                onDismissDialog = { (activity as MainActivity?)?.disableBlurView() }
            ).show()
        }


        binding.carouselRecyclerviewNew.adapter = adapter
        binding.carouselRecyclerviewNew.set3DItem(true)
        binding.carouselRecyclerviewNew.setInfinite(true)
        binding.carouselRecyclerviewNew.setAlpha(true)
        binding.carouselRecyclerviewNew.setIsScrollingEnabled(true)
        binding.carouselRecyclerviewNew.setIntervalRatio(0.54f)
        binding.carouselRecyclerviewNew
            .getCarouselLayoutManager()
            .scrollToPosition(slides.size - 1)
    }
    //---------------------------------------------------------------------------------------------- setBannerSlider


    //---------------------------------------------------------------------------------------------- setPersonnelRequestAdapter
    private fun setPersonnelRequestAdapter() {
        if (context == null)
            return
        val adapter = RequestAdapter(homeViewModel.getPersonnelRequest()) { action ->
            if (action != 0)
                gotoFragment(action)
        }
        val linearLayoutManager = LinearLayoutManager(
            requireContext(),
            LinearLayoutManager.HORIZONTAL,
            false
        )

        linearLayoutManager.reverseLayout = true

        binding.recyclerViewRequest.layoutManager = linearLayoutManager
        binding.recyclerViewRequest.adapter = adapter
    }
    //---------------------------------------------------------------------------------------------- setPersonnelRequestAdapter

}