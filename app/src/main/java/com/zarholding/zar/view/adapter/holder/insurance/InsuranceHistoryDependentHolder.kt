package com.zarholding.zar.view.adapter.holder.insurance

import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.R
import com.zarholding.zar.databinding.ItemInsuranceHistoryDependentBinding
import com.zarholding.zar.ext.setTitleAndValue
import com.zarholding.zar.model.data.response.insurance.history.SupplementalInsuranceDependentPersonsModel

/**
 * Created by m-latifi on 10/17/2023.
 */

class InsuranceHistoryDependentHolder(
    private val binding: ItemInsuranceHistoryDependentBinding
): RecyclerView.ViewHolder(binding.root) {

    //---------------------------------------------------------------------------------------------- bind
    fun bind(item: SupplementalInsuranceDependentPersonsModel) {
        val context = binding.root.context
        binding.textViewFullName.setTitleAndValue(
            title = context.getString(R.string.fullName),
            splitter = context.getString(R.string.colon),
            value = "${item.firstName} ${item.lastName}"
        )
        binding.textViewNationalCode.setTitleAndValue(
            title = context.getString(R.string.nationalCode),
            splitter = context.getString(R.string.colon),
            value = item.nationalCode
        )
        binding.textViewRelation.setTitleAndValue(
            title = context.getString(R.string.relation),
            splitter = context.getString(R.string.colon),
            value = item.relationShipText
        )
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind

}