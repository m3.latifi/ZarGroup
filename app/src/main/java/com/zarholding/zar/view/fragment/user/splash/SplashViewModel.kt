package com.zarholding.zar.view.fragment.user.splash

import androidx.lifecycle.viewModelScope
import com.zarholding.zar.model.repository.NotificationRepository
import com.zarholding.zar.model.repository.UserRepository
import com.zarholding.zar.ZarViewModel
import com.zarholding.zar.model.data.response.ResponseResult
import com.zarholding.zar.model.repository.AppUpdateRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

/**
 * Created by m-latifi on 11/8/2022.
 */

@HiltViewModel
class SplashViewModel @Inject constructor(
    private val userRepository: UserRepository,
    private val notificationRepository: NotificationRepository,
    private val appUpdateRepository: AppUpdateRepository
) : ZarViewModel() {

    var newVersionFileName = ""

    private val _downloadLastVersion =
        MutableStateFlow<ResponseResult<String>>(ResponseResult.Loading(false))
    val downloadLastVersion: StateFlow<ResponseResult<String>> = _downloadLastVersion

    private val _userNotificationUnRead =
        MutableStateFlow<ResponseResult<Int>>(ResponseResult.Loading(false))
    val userNotificationUnRead: StateFlow<ResponseResult<Int>> = _userNotificationUnRead


    //---------------------------------------------------------------------------------------------- requestGetAppVersion
    fun requestGetAppVersion() {
        viewModelScope.launch(IO + exceptionHandler()) {
            appUpdateRepository
                .requestGetAppVersion()
                .collect { networkResult ->
                    checkResponse(networkResult = networkResult) {
                        if (it.isNotEmpty()) {
                            newVersionFileName = it
                            _downloadLastVersion.value = ResponseResult.Success(it)
                        } else
                            isUserLogin()
                    }
                }
        }
    }
    //---------------------------------------------------------------------------------------------- requestGetAppVersion


    //---------------------------------------------------------------------------------------------- isUserLogin
    private fun isUserLogin() {
        viewModelScope.launch(IO + exceptionHandler()) {
            userRepository.isUserLogin().collect { networkResult ->
                checkResponse(networkResult = networkResult) {
                    if (it)
                        requestGetNotificationUnreadCount()
                    else
                        _userNotificationUnRead.value = ResponseResult.Success(-1)
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- isUserLogin


    //---------------------------------------------------------------------------------------------- requestGetNotificationUnreadCount
    private fun requestGetNotificationUnreadCount() {
        viewModelScope.launch(IO + exceptionHandler()) {
            notificationRepository.requestGetNotificationUnreadCount().collect { networkResult ->
                checkResponse(networkResult = networkResult) {
                    _userNotificationUnRead.value = ResponseResult.Success(it)
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- requestGetNotificationUnreadCount

}