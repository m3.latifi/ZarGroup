package com.zarholding.zar.view.dialog.address

import androidx.lifecycle.viewModelScope
import com.zarholding.zar.ZarViewModel
import com.zarholding.zar.model.data.response.ResponseResult
import com.zarholding.zar.model.data.response.address.AddressSuggestionModel
import com.zarholding.zar.model.repository.AddressRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

/**
 * Created by m-latifi on 20/8/2022.
 */

@HiltViewModel
class SearchAddressViewModel @Inject constructor(private val repo: AddressRepository) :
    ZarViewModel() {

    private val _searchAddress =
        MutableStateFlow<ResponseResult<List<AddressSuggestionModel>>>(ResponseResult.Loading(false))
    val searchAddress: StateFlow<ResponseResult<List<AddressSuggestionModel>>> = _searchAddress


    //---------------------------------------------------------------------------------------------- requestGetSuggestionAddress
    fun requestGetSuggestionAddress(address: String, suggestion: List<AddressSuggestionModel>?) {
        viewModelScope.launch(IO + exceptionHandler()) {
            repo.requestGetSuggestionAddress(address, suggestion).collect { networkResult ->
                checkResponse(networkResult = networkResult) {
                    _searchAddress.value = ResponseResult.Success(it)
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- requestGetSuggestionAddress


}