package com.zarholding.zar.view.adapter.holder.education

import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.R
import com.zarholding.zar.databinding.ItemEducationCategoryBinding
import com.zarholding.zar.model.data.response.education.EducationCategoryModel

/**
 * Created by m-latifi on 7/26/2023.
 */

class EducationCategoryHolder(
    private val binding: ItemEducationCategoryBinding,
    private val onClick: (EducationCategoryModel) -> Unit
) : RecyclerView.ViewHolder(binding.root) {


    //---------------------------------------------------------------------------------------------- bind
    fun bind(item: EducationCategoryModel, oddRow: Boolean) {
        if (oddRow)
            configOddRow()
        else
            configEvanRow()
        binding.textViewTitle.text = item.value
        binding.root.setOnClickListener {
            onClick(item)
        }
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind


    //---------------------------------------------------------------------------------------------- configEvanRow
    private fun configEvanRow(){
        binding.constraintLayoutParent.setBackgroundResource(R.drawable.a_drawable_border_blue)
        binding.imageViewIcon.setImageResource(R.drawable.a_icon_education_blue)

    }
    //---------------------------------------------------------------------------------------------- configEvanRow


    //---------------------------------------------------------------------------------------------- configOddRow
    private fun configOddRow(){
        binding.constraintLayoutParent.setBackgroundResource(R.drawable.a_drawable_border_red)
        binding.imageViewIcon.setImageResource(R.drawable.a_icon_education_red)
    }
    //---------------------------------------------------------------------------------------------- configOddRow

}