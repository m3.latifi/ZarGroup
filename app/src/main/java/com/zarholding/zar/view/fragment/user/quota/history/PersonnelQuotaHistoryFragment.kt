package com.zarholding.zar.view.fragment.user.quota.history

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.zar.core.enums.EnumApiError
import com.zarholding.zar.R
import com.zarholding.zar.ZarFragment
import com.zarholding.zar.databinding.FragmentPersonnelQuotaHistoryBinding
import com.zarholding.zar.ext.config
import com.zarholding.zar.ext.stopLoading
import com.zarholding.zar.model.data.response.quota.QuotaHistoryModel
import com.zarholding.zar.tools.manager.getShimmerBuild
import com.zarholding.zar.view.activity.MainActivity
import com.zarholding.zar.view.adapter.recycler.quota.QuotaHistoryAdapter
import com.zarholding.zar.view.custom.EndlessScrollListener
import com.zarholding.zar.view.custom.getEndlessScrollListener
import dagger.hilt.android.AndroidEntryPoint

/**
 * Created by m-latifi on 9/2/2023.
 */

@AndroidEntryPoint
class PersonnelQuotaHistoryFragment(
    override var layout: Int = R.layout.fragment_personnel_quota_history
): ZarFragment<FragmentPersonnelQuotaHistoryBinding>() {

    private val viewModel: PersonnelQuotaHistoryViewModel by viewModels()
    private var endlessScrollListener: EndlessScrollListener? = null
    private var adapter: QuotaHistoryAdapter? = null


    //---------------------------------------------------------------------------------------------- onViewCreated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }
    //---------------------------------------------------------------------------------------------- onViewCreated



    //---------------------------------------------------------------------------------------------- initView
    private fun initView() {
        binding.shimmerViewContainer.config(getShimmerBuild())
        setListener()
        observeLiveDate()
        getQuota()
    }
    //---------------------------------------------------------------------------------------------- initView




    //---------------------------------------------------------------------------------------------- observeLiveDate
    private fun observeLiveDate() {
        viewModel.errorLiveDate.observe(viewLifecycleOwner) {
            binding.shimmerViewContainer.stopLoading()
            showMessage(it.message, it.type)
            when (it.type) {
                EnumApiError.UnAuthorization -> (activity as MainActivity?)?.gotoFirstFragmentWithDeleteUser()

                else -> {}
            }
        }

        viewModel.quotaLiveData.observe(viewLifecycleOwner) {
            binding.shimmerViewContainer.stopLoading()
            setAdapter(it)
        }
    }
    //---------------------------------------------------------------------------------------------- observeLiveDate


    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener() {

    }
    //---------------------------------------------------------------------------------------------- setListener


    //---------------------------------------------------------------------------------------------- getQuota
    private fun getQuota() {
        binding.shimmerViewContainer.startShimmer()
        viewModel.getMyQuotaHistory()
    }
    //---------------------------------------------------------------------------------------------- getQuota



    //---------------------------------------------------------------------------------------------- setAdapter
    private fun setAdapter(items: List<QuotaHistoryModel>) {
        if (context == null)
            return
        adapter?.let {
            adapter?.addQuota(items)
            endlessScrollListener?.let {
                it.setLoading(false)
                if (items.isEmpty())
                    binding.recyclerViewQuota.removeOnScrollListener(it)
            }
        } ?: run {
            adapter = QuotaHistoryAdapter(items.toMutableList())
            val manager = LinearLayoutManager(
                requireContext(), LinearLayoutManager.VERTICAL, false
            )
            endlessScrollListener = getEndlessScrollListener(manager) {
                getQuota()
            }
            binding.recyclerViewQuota.adapter = adapter
            binding.recyclerViewQuota.layoutManager = manager
            if (endlessScrollListener != null)
                binding.recyclerViewQuota.addOnScrollListener(endlessScrollListener!!)
        }
    }
    //---------------------------------------------------------------------------------------------- setAdapter


    //---------------------------------------------------------------------------------------------- onDestroyView
    override fun onDestroyView() {
        super.onDestroyView()
        adapter = null
    }
    //---------------------------------------------------------------------------------------------- onDestroyView

}