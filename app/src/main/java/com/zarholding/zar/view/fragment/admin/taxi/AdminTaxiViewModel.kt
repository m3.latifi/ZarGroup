package com.zarholding.zar.view.fragment.admin.taxi

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.zarholding.zar.model.repository.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

/**
 * Created by m-latifi on 11/19/2022.
 */

@HiltViewModel
class AdminTaxiViewModel @Inject constructor(
    private val userRepository: UserRepository
) : ViewModel(){

    companion object {
        val requestTaxiLiveDate: MutableLiveData<Int> by lazy { MutableLiveData<Int>() }
        val myTaxiLiveDate: MutableLiveData<Int> by lazy { MutableLiveData<Int>() }
    }

    //---------------------------------------------------------------------------------------------- getUserType
    fun getUserType() = userRepository.getUserType()
    //---------------------------------------------------------------------------------------------- getUserType

}