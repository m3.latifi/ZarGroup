package com.zarholding.zar.view.fragment.user.personnel

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.LinearLayoutManager
import com.zar.core.enums.EnumApiError
import com.zarholding.zar.R
import com.zarholding.zar.ZarFragment
import com.zarholding.zar.databinding.FragmentPersonelBinding
import com.zarholding.zar.ext.loadImageByToken
import com.zarholding.zar.model.data.database.entity.UserInfoEntity
import com.zarholding.zar.model.data.response.ResponseResult
import com.zarholding.zar.view.activity.MainActivity
import com.zarholding.zar.view.adapter.recycler.PersonnelSelectAdapter
import com.zarholding.zar.view.custom.EndlessScrollListener
import com.zarholding.zar.view.custom.getEndlessScrollListener
import com.zarholding.zar.view.dialog.ShowImageDialog
import com.zarholding.zar.view.dialog.personnel.PersonnelDialog
import com.zarholding.zar.view.dialog.personnel.PersonnelViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

/**
 * Created by m-latifi on 11/19/2022.
 */

@AndroidEntryPoint
class PersonnelFragment(override var layout: Int = R.layout.fragment_personel) :
    ZarFragment<FragmentPersonelBinding>() {

    private val viewModel: PersonnelViewModel by viewModels()

    private var adapterPersonnel: PersonnelSelectAdapter? = null

    private var endlessScrollListener: EndlessScrollListener? = null

    //---------------------------------------------------------------------------------------------- onViewCreated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }
    //---------------------------------------------------------------------------------------------- onViewCreated


    //---------------------------------------------------------------------------------------------- initView
    private fun initView() {
        observeLiveDate()
        setOnListener()
//        getUsers()
    }
    //---------------------------------------------------------------------------------------------- initView


    //---------------------------------------------------------------------------------------------- observeLiveDate
    private fun observeLiveDate() {
        viewModel.errorLiveDate.observe(viewLifecycleOwner) {
            binding.textViewLoading.visibility = View.GONE
            showMessage(it.message, it.type)
            when (it.type) {
                EnumApiError.UnAuthorization -> (activity as MainActivity?)?.gotoFirstFragmentWithDeleteUser()

                else -> {}
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED){
                viewModel.userInfo.collect {
                    when(it) {
                        is ResponseResult.Loading -> {}
                        is ResponseResult.Success -> {
                            setUserInfo(it.data)
                        }
                    }
                }
            }
        }

        viewModel.userListMutableLiveData.observe(viewLifecycleOwner) {
            binding.textViewLoading.visibility = View.GONE
            setPersonnelAdapter(it)
        }
    }
    //---------------------------------------------------------------------------------------------- observeLiveDate


    //---------------------------------------------------------------------------------------------- setOnListener
    private fun setOnListener() {
        binding.buttonSearch.setOnClickListener { showPersonnelDialog() }
        binding.cardViewProfile.setOnClickListener { showImageProfile() }
    }
    //---------------------------------------------------------------------------------------------- setOnListener


    //---------------------------------------------------------------------------------------------- getUsers
    private fun getUsers() {
        binding.textViewLoading.visibility = View.VISIBLE
        viewModel.requestGetUser(" ")
    }
    //---------------------------------------------------------------------------------------------- getUsers


    //---------------------------------------------------------------------------------------------- setPersonnelAdapter
    private fun setPersonnelAdapter(items: List<UserInfoEntity>) {
        adapterPersonnel?.let {
            adapterPersonnel?.addPerson(items)
            endlessScrollListener?.let {
                it.setLoading(false)
                if (items.isEmpty())
                    binding.recyclerViewPersonnel.removeOnScrollListener(it)
            }
        } ?: run {
            adapterPersonnel = PersonnelSelectAdapter(
                items = items.toMutableList(),
                token = viewModel.getBearerToken()
            ) {
                getUserInfo(it)
            }
            val manager = LinearLayoutManager(
                context,
                LinearLayoutManager.VERTICAL,
                false
            )
            endlessScrollListener = getEndlessScrollListener(manager) {
                getUsers()
            }
            binding.recyclerViewPersonnel.layoutManager = manager
            binding.recyclerViewPersonnel.adapter = adapterPersonnel
            if (endlessScrollListener != null)
                binding.recyclerViewPersonnel.addOnScrollListener(endlessScrollListener!!)
        }

    }
    //---------------------------------------------------------------------------------------------- setPersonnelAdapter


    //---------------------------------------------------------------------------------------------- showPersonnelDialog
    private fun showPersonnelDialog() {
        PersonnelDialog { getUserInfo(it) }
            .show(childFragmentManager, "personnel dialog")
    }
    //---------------------------------------------------------------------------------------------- showPersonnelDialog


    //---------------------------------------------------------------------------------------------- getUserInfo
    private fun getUserInfo(item: UserInfoEntity) {
        viewModel.requestGetUserInfoById(item.id)
    }
    //---------------------------------------------------------------------------------------------- getUserInfo


    //---------------------------------------------------------------------------------------------- setUserInfo
    private fun setUserInfo(item: UserInfoEntity) {
        binding.expandableContent.collapse()
        binding.expandableInfo.expand()
        binding.textviewName.text = item.fullName
        binding.textviewCode.text = item.userName
        binding.textviewJobKeyText.text = item.personnelJobKeyText
        binding.textViewOrganizationUnit.text = item.organizationUnit
        binding.textViewPhone.text = item.phone
        binding.textViewMobile.text = item.mobile
        binding.textViewEmail.text = item.email
        binding.imageViewProfile.loadImageByToken(
            url = item.userName,
            token = viewModel.getBearerToken()
        )
    }
    //---------------------------------------------------------------------------------------------- setUserInfo


    //---------------------------------------------------------------------------------------------- showImageProfile
    private fun showImageProfile() {
        if (context == null)
            return
        ShowImageDialog(
            requireContext(),
            viewModel.getModelForShowImageProfile(),
            onShowDialog = { (activity as MainActivity?)?.enableBlurView() },
            onDismissDialog = { (activity as MainActivity?)?.disableBlurView() }
        ).show()
    }
    //---------------------------------------------------------------------------------------------- showImageProfile

}