package com.zarholding.zar.view.fragment.user.map

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Size
import android.view.View
import com.zarholding.zar.R
import com.zarholding.zar.model.data.response.trip.TripPointModel
import com.zarholding.zar.tools.CompanionValues
import com.zarholding.zar.tools.manager.OsmManager
import com.zarholding.zar.ZarFragment
import com.zarholding.zar.databinding.FragmentMapBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.overlay.Marker
import java.util.*
import kotlin.let
import kotlin.run

/**
 * Created by m-latifi on 11/19/2022.
 */

@AndroidEntryPoint
class MapFragment(override var layout: Int = R.layout.fragment_map) :
    ZarFragment<FragmentMapBinding>() {

    private lateinit var osmManager: OsmManager
    private var originMarker: Marker? = null
    private var destinationMarker: Marker? = null
    private var latPrimary: Double = 0.0
    private var lngPrimary: Double = 0.0


    //---------------------------------------------------------------------------------------------- onViewCreated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        osmManager = OsmManager(binding.mapView)
        osmManager.mapInitialize()
        binding.textViewLoading.visibility = View.VISIBLE
        CoroutineScope(IO).launch {
            showPointOnMap()
        }

        binding.materialButtonChooseNavigationApp.setOnClickListener {
            binding.materialButtonChooseNavigationApp.visibility = View.GONE
            val intent = Intent(
                Intent.ACTION_VIEW, Uri.parse(
                    String.format(
                        Locale.US,
                        "geo:%.8f,%.8f", latPrimary, lngPrimary
                    )
                )
            )
            startActivity(intent)
        }

    }
    //---------------------------------------------------------------------------------------------- onViewCreated


    //---------------------------------------------------------------------------------------------- showPointOnMap
    private suspend fun showPointOnMap() {
        arguments?.let {
            val latOrigin = it.getDouble(CompanionValues.latOrigin)
            val lngOrigin = it.getDouble(CompanionValues.lngOrigin)
            val latDestination = it.getDouble(CompanionValues.latDestination, 0.0)
            val lngDestination = it.getDouble(CompanionValues.lngDestination, 0.0)

            val pointOrigin = GeoPoint(latOrigin, lngOrigin)
            var pointDestination: GeoPoint? = null
            if (latDestination != 0.0)
                pointDestination = GeoPoint(latDestination, lngDestination)

            val points = listOf(
                TripPointModel(latOrigin.toFloat(), lngOrigin.toFloat()),
                TripPointModel(latDestination.toFloat(), lngDestination.toFloat())
            )
            osmManager.drawPolyLine(points)

            val iconOrigin = osmManager.createMarkerIconDrawable(
                Size(100, 135),
                R.drawable.a_ic_origin
            )
            originMarker = osmManager.addMarker(iconOrigin, pointOrigin, null)
            originMarker!!.setOnMarkerClickListener { marker, _ ->
                latPrimary = marker.position.latitude
                lngPrimary = marker.position.longitude
                binding.materialButtonChooseNavigationApp.visibility = View.VISIBLE
                true
            }

            pointDestination?.let { point ->
                val iconDestination = osmManager.createMarkerIconDrawable(
                    Size(100, 135),
                    R.drawable.a_ic_destination
                )
                destinationMarker = osmManager.addMarker(iconDestination, point, null)
                destinationMarker!!.setOnMarkerClickListener { marker, _ ->
                    latPrimary = marker.position.latitude
                    lngPrimary = marker.position.longitude
                    binding.materialButtonChooseNavigationApp.visibility = View.VISIBLE
                    true
                }
                withContext(Main) {
                    binding.textViewLoading.visibility = View.GONE
                }

            } ?: run {
                osmManager.moveCamera(pointOrigin, 1L)
            }

        } ?: run {
            activity?.onBackPressedDispatcher?.onBackPressed()
        }
    }
    //---------------------------------------------------------------------------------------------- showPointOnMap

}