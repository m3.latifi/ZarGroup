package com.zarholding.zar.view.fragment.user.download

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.content.FileProvider
import androidx.fragment.app.viewModels
import com.zarholding.zar.R
import com.zarholding.zar.ZarFragment
import com.zarholding.zar.databinding.FragmentDownloadUpdateBinding
import com.zarholding.zar.model.data.enum.EnumEntityType
import com.zarholding.zar.tools.CompanionValues
import com.zarholding.zar.view.activity.MainActivity
import dagger.hilt.android.AndroidEntryPoint
import java.io.File


/**
 * Create by Mehrdad on 1/16/2023
 */

@AndroidEntryPoint
class DownloadUpdateFragment(override var layout: Int = R.layout.fragment_download_update) :
    ZarFragment<FragmentDownloadUpdateBinding>() {

    private val viewModel: DownloadViewModel by viewModels()

    private var enumType = EnumEntityType.APK


    //---------------------------------------------------------------------------------------------- onViewCreated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeLiveDate()
        getFileNameFromArgument()
    }
    //---------------------------------------------------------------------------------------------- onViewCreated


    //---------------------------------------------------------------------------------------------- observeLiveDate
    private fun observeLiveDate() {
        viewModel.errorLiveDate.observe(viewLifecycleOwner){
            showMessage(it.message, it.type)
            (activity as MainActivity?)?.onBackPressedDispatcher?.onBackPressed()
        }


        viewModel.downloadSuccessLiveData.observe(viewLifecycleOwner){
            when(enumType) {
                EnumEntityType.APK -> installApp(it)
                else -> showPdf(it)
            }
        }

        viewModel.downloadProgress.observe(viewLifecycleOwner){
            binding.progressBar.setProgressPercentage(it.toDouble(), true)
        }

    }
    //---------------------------------------------------------------------------------------------- observeLiveDate



    //---------------------------------------------------------------------------------------------- getFileNameFromArgument
    private fun getFileNameFromArgument() {
        arguments?.let { bundle ->
            val file = bundle.getString(CompanionValues.DOWNLOAD_URL, null)
            val type = bundle.getString(CompanionValues.DOWNLOAD_TYPE, "")
            try {
                enumType = enumValueOf(type)
                file?.let {
                    viewModel.downloadFile(it, enumType)
                }
            } catch (e: Exception) {
                showMessage("خطا در دریافت مقادیر")
            }
        }
    }
    //---------------------------------------------------------------------------------------------- getFileNameFromArgument



    //---------------------------------------------------------------------------------------------- installApp
    private fun installApp(file: File) {
        if (context != null) {
            val fileURI = FileProvider.getUriForFile(
                requireContext(),
                requireContext().applicationContext.packageName + ".provider",
                file
            )
            val intent = Intent(Intent.ACTION_VIEW, fileURI)
            intent.putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true)
            intent.setDataAndType(fileURI, "application/vnd.android.package-archive")
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            activity?.onBackPressedDispatcher?.onBackPressed()
            requireContext().startActivity(intent)
        }
    }
    //---------------------------------------------------------------------------------------------- installApp


    //---------------------------------------------------------------------------------------------- showPdf
    private fun showPdf(file: File){
        val fileURI = FileProvider.getUriForFile(
            requireContext(),
            requireContext().applicationContext.packageName + ".provider",
            file
        )
        val intent = Intent(Intent.ACTION_VIEW)
        intent.setDataAndType(fileURI, "application/pdf")
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        activity?.onBackPressedDispatcher?.onBackPressed()
        requireContext().startActivity(intent)
    }
    //---------------------------------------------------------------------------------------------- showPdf

}