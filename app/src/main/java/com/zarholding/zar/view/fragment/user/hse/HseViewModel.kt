package com.zarholding.zar.view.fragment.user.hse

import android.os.Bundle
import androidx.lifecycle.viewModelScope
import com.zarholding.zar.ZarViewModel
import com.zarholding.zar.model.data.request.hse.HseExaminationModel
import com.zarholding.zar.model.data.response.ResponseResult
import com.zarholding.zar.model.data.response.dropdown.DropDownModel
import com.zarholding.zar.model.repository.HseRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by m-latifi on 11/9/2022.
 */

@HiltViewModel
class HseViewModel @Inject constructor(
    private val hseRepository: HseRepository
) : ZarViewModel() {

    private val _hsePeriods =
        MutableStateFlow<ResponseResult<List<DropDownModel>>>(ResponseResult.Loading(false))
    val hsePeriods: StateFlow<ResponseResult<List<DropDownModel>>> = _hsePeriods

    private val _hseExaminations =
        MutableStateFlow<ResponseResult<List<HseExaminationModel>>>(ResponseResult.Loading(false))
    val hseExaminations: StateFlow<ResponseResult<List<HseExaminationModel>>> = _hseExaminations


    //---------------------------------------------------------------------------------------------- requestGetHsePeriods
    fun requestGetHsePeriods() {
        viewModelScope.launch(IO + exceptionHandler()) {
            _hsePeriods.value = ResponseResult.Loading(true)
            hseRepository.requestGetHsePeriods().collect {networkResult ->
                _hsePeriods.value = ResponseResult.Loading(false)
                checkResponse(networkResult = networkResult) {
                    _hsePeriods.value = ResponseResult.Success(it)
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- requestGetHsePeriods


    //---------------------------------------------------------------------------------------------- requestGetExaminations
    fun requestGetExaminations(arguments: Bundle?) {
        viewModelScope.launch(IO + exceptionHandler()) {
            _hseExaminations.value = ResponseResult.Loading(true)
            hseRepository.requestGetExaminations(arguments = arguments).collect {networkResult ->
                checkResponse(networkResult = networkResult) {
                    _hseExaminations.value = ResponseResult.Success(it)
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- requestGetExaminations

}