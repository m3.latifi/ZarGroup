package com.zarholding.zar.view.fragment.user.login

import androidx.lifecycle.viewModelScope
import com.zar.core.enums.EnumApiError
import com.zar.core.models.ErrorApiModel
import com.zarholding.zar.ZarViewModel
import com.zarholding.zar.model.data.response.ResponseResult
import com.zarholding.zar.model.repository.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

/**
 * Created by m-latifi on 11/9/2022.
 */

@HiltViewModel
class LoginViewModel @Inject constructor(private val userRepo: UserRepository) : ZarViewModel() {

    private val _loginResult =
        MutableStateFlow<ResponseResult<Boolean>>(ResponseResult.Loading(false))
    val loginResult: StateFlow<ResponseResult<Boolean>> = _loginResult

    //---------------------------------------------------------------------------------------------- getSavedUserName
    fun getSavedUserName() = userRepo.getSavedUserName()
    //---------------------------------------------------------------------------------------------- getSavedUserName


    //---------------------------------------------------------------------------------------------- getSavedPassword
    fun getSavedPassword() = userRepo.getSavedPassword()
    //---------------------------------------------------------------------------------------------- getSavedPassword


    //---------------------------------------------------------------------------------------------- login
    fun login(userName: String, password: String) {
        viewModelScope.launch {
            userRepo.login(
                userName = userName,
                password = password
            ).collect { networkResult ->
                checkResponse(
                    networkResult = networkResult
                ) {message ->
                    userPermission(message)
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- login


    //---------------------------------------------------------------------------------------------- userPermission
    private fun userPermission(loginMessage: String) {
        viewModelScope.launch {
            userRepo.requestUserPermission().collect { responseState ->
                checkResponse(
                    networkResult = responseState
                ) {
                    errorLiveDate.postValue(
                        ErrorApiModel(
                            type = EnumApiError.Done,
                            message = loginMessage
                        )
                    )
                    _loginResult.value = ResponseResult.Success(it)

                }

            }
        }
    }
    //---------------------------------------------------------------------------------------------- userPermission


    //---------------------------------------------------------------------------------------------- isBiometricEnable
    fun isBiometricEnable() = userRepo.isBiometricEnable()
    //---------------------------------------------------------------------------------------------- isBiometricEnable

}