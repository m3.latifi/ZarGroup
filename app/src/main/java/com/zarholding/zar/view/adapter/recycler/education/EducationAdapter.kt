package com.zarholding.zar.view.adapter.recycler.education

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemEducationBinding
import com.zarholding.zar.model.data.response.education.EducationModel
import com.zarholding.zar.view.adapter.holder.education.EducationHolder

/**
 * Created by m-latifi on 7/26/2023.
 */

class EducationAdapter(
    private val items: MutableList<EducationModel>,
    private val onClick: (EducationModel) -> Unit
) : RecyclerView.Adapter<EducationHolder>() {

    private var inflater: LayoutInflater? = null


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EducationHolder {
        if (inflater == null)
            inflater = LayoutInflater.from(parent.context)
        return EducationHolder(
            binding = ItemEducationBinding.inflate(inflater!!, parent, false),
            onClick = onClick
        )
    }
    //---------------------------------------------------------------------------------------------- getItemCount


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun onBindViewHolder(holder: EducationHolder, position: Int) {
        holder.bind(items[position])
    }
    //---------------------------------------------------------------------------------------------- getItemCount


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun getItemCount() = items.size
    //---------------------------------------------------------------------------------------------- getItemCount



    //---------------------------------------------------------------------------------------------- addEducation
    fun addEducation(list : List<EducationModel>) {
        val olsSize = items.size
        items.clear()
        items.addAll(list)
        notifyItemRangeChanged(olsSize-1, items.size)
    }
    //---------------------------------------------------------------------------------------------- addEducation


}