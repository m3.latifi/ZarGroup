package com.zarholding.zar.view.adapter.recycler.education

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemEducationCategoryBinding
import com.zarholding.zar.model.data.response.education.EducationCategoryModel
import com.zarholding.zar.view.adapter.holder.education.EducationCategoryHolder

/**
 * Created by m-latifi on 7/26/2023.
 */

class EducationCategoryAdapter(
    private val items: List<EducationCategoryModel>,
    private val onClick: (EducationCategoryModel) -> Unit
) : RecyclerView.Adapter<EducationCategoryHolder>() {

    private var inflater: LayoutInflater? = null
    private var oddRow = false

    //---------------------------------------------------------------------------------------------- onCreateViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EducationCategoryHolder {
        if (inflater == null)
            inflater = LayoutInflater.from(parent.context)
        return EducationCategoryHolder(
            binding = ItemEducationCategoryBinding.inflate(inflater!!, parent, false),
            onClick = onClick
        )
    }
    //---------------------------------------------------------------------------------------------- onCreateViewHolder


    //---------------------------------------------------------------------------------------------- onBindViewHolder
    override fun onBindViewHolder(holder: EducationCategoryHolder, position: Int) {
        if (position % 3 == 0)
            oddRow = !oddRow
        holder.bind(items[position], oddRow)
    }
    //---------------------------------------------------------------------------------------------- onBindViewHolder


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun getItemCount() = items.size
    //---------------------------------------------------------------------------------------------- getItemCount

}