package com.zarholding.zar.view.adapter.holder.quota

import android.view.View
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.R
import com.zarholding.zar.databinding.ItemPersonnelQuotaBinding
import com.zarholding.zar.ext.setQuotaStatus
import com.zarholding.zar.model.data.enum.EnumQuotaApprove
import com.zarholding.zar.model.data.response.quota.QuotaModel

/**
 * Created by m-latifi on 7/26/2023.
 */

class QuotaHolder(
    private val binding: ItemPersonnelQuotaBinding,
    private val onClick: (QuotaModel) -> Unit,
    private val onAccessUserClick: (Long, TextView) -> Unit
) : RecyclerView.ViewHolder(binding.root) {


    //---------------------------------------------------------------------------------------------- bind
    fun bind(item: QuotaModel, userId: Int) {
        setValueToXml(item, userId)
    }
    //---------------------------------------------------------------------------------------------- bind


    //---------------------------------------------------------------------------------------------- setValueToXml
    private fun setValueToXml(item: QuotaModel, userId: Int) {
        val scale = AnimationUtils.loadAnimation(
            binding.textViewTitle.context, R.anim.scale
        )

        val title = "${item.quotasOwnerName} (${item.quotaName})"
        binding.textViewTitle.text = title

        binding.textViewStatus.setQuotaStatus(item.status)
        binding.textViewExpire.text = binding
            .textViewExpire
            .context
            .getString(R.string.fromDateToDate, item.fromDate, item.toDate)

        when(item.quotasOwnerId){
            item.receiverId -> {
                binding.textViewAccessUser.text =
                    binding.textViewAccessUser.resources.getString(R.string.giveAccessToAnotherUser)
                setListener(item)
                binding.imageViewQr.startAnimation(scale)
            }
            userId.toLong() -> {
                binding.textViewAccessUser.text =
                    binding.textViewAccessUser.resources.getString(
                        R.string.receiverUser,
                        item.receiverName
                    )
                binding.textViewAccessUser.visibility = View.VISIBLE
            }
            else -> binding.textViewAccessUser.visibility = View.INVISIBLE

        }

        if (userId.toLong() == item.receiverId) {
            binding.root.setOnClickListener { onClick(item) }
            binding.imageViewQr.startAnimation(scale)
        }

        if (item.status == EnumQuotaApprove.Delivered)
            binding.textViewAccessUser.visibility = View.INVISIBLE

    }
    //---------------------------------------------------------------------------------------------- setValueToXml


    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener(item: QuotaModel) {
        binding.root.setOnClickListener { onClick(item) }
        binding.textViewAccessUser.setOnClickListener {
            onAccessUserClick(item.id, binding.textViewAccessUser)
        }
    }
    //---------------------------------------------------------------------------------------------- setListener
}