package com.zarholding.zar.view.adapter.recycler.quota

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemPersonnelQuotaHistoryBinding
import com.zarholding.zar.model.data.response.quota.QuotaHistoryModel
import com.zarholding.zar.view.adapter.holder.quota.QuotaHistoryHolder

/**
 * Created by m-latifi on 7/26/2023.
 */

class QuotaHistoryAdapter(
    private val items: MutableList<QuotaHistoryModel>
) : RecyclerView.Adapter<QuotaHistoryHolder>() {

    private var inflater: LayoutInflater? = null

    //---------------------------------------------------------------------------------------------- onCreateViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuotaHistoryHolder {
        if (inflater == null)
            inflater = LayoutInflater.from(parent.context)
        return QuotaHistoryHolder(
            binding = ItemPersonnelQuotaHistoryBinding.inflate(inflater!!, parent, false)
        )
    }
    //---------------------------------------------------------------------------------------------- onCreateViewHolder


    //---------------------------------------------------------------------------------------------- onBindViewHolder
    override fun onBindViewHolder(holder: QuotaHistoryHolder, position: Int) {
        holder.bind(items[position])
    }
    //---------------------------------------------------------------------------------------------- onBindViewHolder


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun getItemCount() = items.size
    //---------------------------------------------------------------------------------------------- getItemCount


    //---------------------------------------------------------------------------------------------- addQuota
    fun addQuota(list : List<QuotaHistoryModel>) {
        val olsSize = items.size
        items.clear()
        items.addAll(list)
        notifyItemRangeChanged(olsSize-1, items.size)
    }
    //---------------------------------------------------------------------------------------------- addQuota

}