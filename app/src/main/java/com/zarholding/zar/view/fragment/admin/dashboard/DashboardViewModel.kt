package com.zarholding.zar.view.fragment.admin.dashboard

import android.os.Bundle
import com.zarholding.zar.R
import com.zarholding.zar.ZarViewModel
import com.zarholding.zar.model.data.enum.EnumQuotaApprove
import com.zarholding.zar.model.data.other.AppModel
import com.zarholding.zar.tools.CompanionValues
import com.zarholding.zar.tools.manager.RoleManager
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

/**
 * Created by m-latifi on 10/9/2022.
 */

@HiltViewModel
class DashboardViewModel @Inject constructor(
    private val roleManager: RoleManager
) : ZarViewModel() {

    private val apps: MutableList<AppModel> = mutableListOf()

    //---------------------------------------------------------------------------------------------- setApp
    fun setApp(): MutableList<AppModel> {
        if (apps.isEmpty()) {
            addAdminBusApp()
            addAdminTaxiApp()
            addCeremoniesApp()
            addPersonalPurchaseApp()
            addExitCompanyControlApp()
            addGiftDeliveryWarehouseApp()
        }
        return apps
    }
    //---------------------------------------------------------------------------------------------- setApp


    //---------------------------------------------------------------------------------------------- addAdminBusApp
    private fun addAdminBusApp() {
        if (roleManager.isAccessToAdminBus())
            apps.add(
                AppModel(
                    R.drawable.a_ic_bus,
                    resourcesProvider.getString(R.string.service),
                    R.id.action_goto_AdminBusFragment
                )
            )
    }
    //---------------------------------------------------------------------------------------------- addAdminBusApp


    //---------------------------------------------------------------------------------------------- addAdminTaxiApp
    private fun addAdminTaxiApp() {
        apps.add(
            AppModel(
                R.drawable.a_ic_taxi,
                resourcesProvider.getString(R.string.agency),
                R.id.action_goto_AdminTaxiFragment
            )
        )
    }
    //---------------------------------------------------------------------------------------------- addAdminTaxiApp


    //---------------------------------------------------------------------------------------------- addCeremoniesApp
    private fun addCeremoniesApp() {
//        if (roleManager.accessByRole("Mehrdad Latifi"))
        apps.add(
            AppModel(
                R.drawable.a_ic_conference_room,
                resourcesProvider.getString(R.string.ceremonies),
                0
            )
        )
    }
    //---------------------------------------------------------------------------------------------- addCeremoniesApp


    //---------------------------------------------------------------------------------------------- addPersonalPurchaseApp
    private fun addPersonalPurchaseApp() {
//        if (roleManager.accessByRole("Mehrdad Latifi"))
        apps.add(
            AppModel(
                R.drawable.a_ic_shopping_cart,
                resourcesProvider.getString(R.string.personalPurchase),
                0
            )
        )
    }
    //---------------------------------------------------------------------------------------------- addPersonalPurchaseApp



    //---------------------------------------------------------------------------------------------- addExitCompanyControlApp
    private fun addExitCompanyControlApp() {
        if (roleManager.isAccessQuotaSecurityChecker()) {
            val bundle = Bundle()
            bundle.putString(CompanionValues.TYPE_OBJECT, EnumQuotaApprove.Finished.name)
            apps.add(
                AppModel(
                    R.drawable.a_ic_panel_gift,
                    resourcesProvider.getString(R.string.exitCompanyControl),
                    R.id.action_goto_qrCodeReaderFragment,
                    bundle
                )
            )
        }
    }
    //---------------------------------------------------------------------------------------------- addExitCompanyControlApp



    //---------------------------------------------------------------------------------------------- addGiftDeliveryWarehouseApp
    private fun addGiftDeliveryWarehouseApp() {
        if (roleManager.isAccessQuotaStockChecker()) {
            val bundle = Bundle()
            bundle.putString(CompanionValues.TYPE_OBJECT, EnumQuotaApprove.Delivered.name)
            apps.add(
                AppModel(
                    R.drawable.a_ic_warehouse,
                    resourcesProvider.getString(R.string.giftDeliveryWarehouse),
                    R.id.action_goto_qrCodeReaderFragment,
                    bundle
                )
            )
        }
    }
    //---------------------------------------------------------------------------------------------- addGiftDeliveryWarehouseApp

}