package com.zarholding.zar.view.adapter.holder.survey

import com.zar.core.view.picker.date.customviews.DateRangeCalendarView
import com.zar.core.view.picker.date.dialog.DatePickerDialog
import com.zarholding.zar.R
import com.zarholding.zar.databinding.ItemSurveyDateBinding
import com.zarholding.zar.ext.setSurveyNumber
import com.zarholding.zar.model.data.response.survey.SurveyQuestionModel


/**
 * Create by Mehrdad on 1/21/2023
 */
class SurveyDateHolder(
    private val binding: ItemSurveyDateBinding
) : SurveyQuestionHolder(binding.root) {

    //---------------------------------------------------------------------------------------------- bind
    override fun bind(item: SurveyQuestionModel, position: Int) {
        setValueToXml(item, position)
        setListener(item)
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind


    //---------------------------------------------------------------------------------------------- setValueToXml
    private fun setValueToXml(item: SurveyQuestionModel, position: Int) {
        binding.textViewNumber.setSurveyNumber(position + 1)
        binding.textViewQuestion.text = item.questionTitle
        binding.buttonDate.text = item.answer
    }
    //---------------------------------------------------------------------------------------------- setValueToXml


    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener(item: SurveyQuestionModel) {
        binding.buttonDate.setOnClickListener {
            val context = binding.buttonDate.context
            val datePickerDialog = DatePickerDialog(context)
            datePickerDialog.selectionMode = DateRangeCalendarView.SelectionMode.Single
            datePickerDialog.isDisableDaysAgo = false
            datePickerDialog.acceptButtonColor =
                context.resources.getColor(R.color.n_blueBottom, context.theme)
            datePickerDialog.headerBackgroundColor =
                context.resources.getColor(R.color.n_blueBottom, context.theme)
            datePickerDialog.headerTextColor =
                context.resources.getColor(R.color.white, context.theme)
            datePickerDialog.weekColor =
                context.resources.getColor(R.color.n_textHint, context.theme)
            datePickerDialog.disableDateColor =
                context.resources.getColor(R.color.n_textHint, context.theme)
            datePickerDialog.defaultDateColor =
                context.resources.getColor(R.color.n_blueBottom, context.theme)
            datePickerDialog.selectedDateCircleColor =
                context.resources.getColor(R.color.n_blueBottom, context.theme)
            datePickerDialog.selectedDateColor =
                context.resources.getColor(R.color.textViewColor1, context.theme)
            datePickerDialog.rangeDateColor =
                context.resources.getColor(R.color.n_blueBottom, context.theme)
            datePickerDialog.rangeStripColor =
                context.resources.getColor(R.color.n_primaryColorAlpha, context.theme)
            datePickerDialog.holidayColor =
                context.resources.getColor(R.color.n_reject, context.theme)
            datePickerDialog.textSizeWeek = 12.0f
            datePickerDialog.textSizeDate = 14.0f
            datePickerDialog.textSizeTitle = 18.0f
            datePickerDialog.setCanceledOnTouchOutside(true)
            datePickerDialog.onSingleDateSelectedListener =
                DatePickerDialog.OnSingleDateSelectedListener { startDate ->
                    binding.buttonDate.text = startDate.persianShortDate
                    item.answer = startDate.persianShortDate
                }

            datePickerDialog.showDialog()
        }
    }
    //---------------------------------------------------------------------------------------------- setListener


}
