package com.zarholding.zar.view.adapter.recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemMyServiceBinding
import com.zarholding.zar.model.data.other.ShowImageModel
import com.zarholding.zar.model.data.response.trip.TripModel
import com.zarholding.zar.view.adapter.holder.MyServiceHolder

/**
 * Created by m-latifi on 11/20/2022.
 */

class MyServiceAdapter(
    private val tripList: List<TripModel>,
    private val onClick: (TripModel) -> Unit,
    private val onDeleteRegisterStation: (TripModel) -> Unit,
    private val onClickImage: (ShowImageModel) -> Unit,
    private val onClickCall: (String?) -> Unit
) : RecyclerView.Adapter<MyServiceHolder>() {

    private var layoutInflater: LayoutInflater? = null


    //---------------------------------------------------------------------------------------------- onCreateViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyServiceHolder {
        if (layoutInflater == null)
            layoutInflater = LayoutInflater.from(parent.context)
        return MyServiceHolder(
            binding = ItemMyServiceBinding.inflate(layoutInflater!!, parent, false),
            onClick = onClick,
            onDeleteRegisterStation = onDeleteRegisterStation,
            onClickImage = onClickImage,
            onClickCall = onClickCall
        )
    }
    //---------------------------------------------------------------------------------------------- onCreateViewHolder


    //---------------------------------------------------------------------------------------------- onBindViewHolder
    override fun onBindViewHolder(holder: MyServiceHolder, position: Int) {
        holder.bind(tripList[position])
    }
    //---------------------------------------------------------------------------------------------- onBindViewHolder


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun getItemCount() = tripList.size
    //---------------------------------------------------------------------------------------------- getItemCount


}