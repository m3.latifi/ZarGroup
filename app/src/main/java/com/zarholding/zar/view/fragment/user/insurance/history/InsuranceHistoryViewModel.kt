package com.zarholding.zar.view.fragment.user.insurance.history

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.zarholding.zar.R
import com.zarholding.zar.ZarViewModel
import com.zarholding.zar.model.data.response.insurance.history.SupplementalInsuranceHistoryModel
import com.zarholding.zar.model.repository.SupplementalInsuranceRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by m-latifi on 10/11/2023.
 */

@HiltViewModel
class InsuranceHistoryViewModel @Inject constructor(
    private val repository: SupplementalInsuranceRepository
) : ZarViewModel() {


    val historyLiveData: MutableLiveData<List<SupplementalInsuranceHistoryModel>> by lazy {
        MutableLiveData<List<SupplementalInsuranceHistoryModel>>()
    }


    //---------------------------------------------------------------------------------------------- requestSupplementalHistory
    fun requestSupplementalHistory() {
        viewModelScope.launch(IO + exceptionHandler()) {
            callApi(
                request = repository.requestSupplementalHistory(),
                showMessageAfterSuccessResponse = false
            ) {
                if (!it.items.isNullOrEmpty())
                    historyLiveData.postValue(it.items)
                else
                    setMessage(resourcesProvider.getString(R.string.insuranceIsEmpty))
            }
        }
    }
    //---------------------------------------------------------------------------------------------- requestSupplementalHistory

}