package com.zarholding.zar.view.fragment.admin.bus

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.zarholding.zar.model.data.request.TripRequestRegisterStatusModel
import com.zarholding.zar.model.data.response.trip.TripRequestRegisterModel
import com.zarholding.zar.model.repository.TripRepository
import com.zarholding.zar.ZarViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import javax.inject.Inject

@HiltViewModel
class AdminBusServiceViewModel @Inject constructor(
    private val tripRepository: TripRepository
) : ZarViewModel() {

    val tripModelLiveData: MutableLiveData<List<TripRequestRegisterModel>?> by lazy {
        MutableLiveData<List<TripRequestRegisterModel>?>()
    }

    //---------------------------------------------------------------------------------------------- requestGetTripForRegister
    fun requestGetTripForRegister() {
        viewModelScope.launch(IO + exceptionHandler()) {
            if (tripModelLiveData.value.isNullOrEmpty()) {
                callApi(
                    request = tripRepository.requestGetTripForRegister(),
                    onReceiveData = { tripModelLiveData.postValue(it) }
                )
            }
        }
    }
//---------------------------------------------------------------------------------------------- requestGetTripForRegister


    //---------------------------------------------------------------------------------------------- requestChangeStatusTripRegister
    fun requestChangeStatusTripRegister(request: List<TripRequestRegisterStatusModel>) {
        viewModelScope.launch(IO + exceptionHandler()) {
            callApi(
                request = tripRepository.requestChangeStatusTripRegister(request),
                onReceiveData = {
                    tripModelLiveData.postValue(null)
                    requestGetTripForRegister()
                }
            )
        }
    }
//---------------------------------------------------------------------------------------------- requestChangeStatusTripRegister

}