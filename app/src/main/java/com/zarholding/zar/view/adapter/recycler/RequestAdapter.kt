package com.zarholding.zar.view.adapter.recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemRequestBinding
import com.zarholding.zar.model.data.other.AppModel
import com.zarholding.zar.view.adapter.holder.RequestItemHolder

/**
 * Created by m-latifi on 11/14/2022.
 */

class RequestAdapter(
    private val apps: MutableList<AppModel>,
    private val onClick: (Int) -> Unit
) :
    RecyclerView.Adapter<RequestItemHolder>() {

    private var layoutInflater: LayoutInflater? = null


    //---------------------------------------------------------------------------------------------- onCreateViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RequestItemHolder {
        if (layoutInflater == null)
            layoutInflater = LayoutInflater.from(parent.context)
        return RequestItemHolder(
            binding = ItemRequestBinding.inflate(layoutInflater!!, parent, false),
            onClick = onClick
        )
    }
    //---------------------------------------------------------------------------------------------- onCreateViewHolder


    //---------------------------------------------------------------------------------------------- onBindViewHolder
    override fun onBindViewHolder(holder: RequestItemHolder, position: Int) {
        holder.bind(apps[position])
    }
    //---------------------------------------------------------------------------------------------- onBindViewHolder


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun getItemCount() = apps.size
    //---------------------------------------------------------------------------------------------- getItemCount

}