package com.zarholding.zar.view.fragment.user.education.education

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.zarholding.zar.ZarViewModel
import com.zarholding.zar.model.data.response.education.EducationModel
import com.zarholding.zar.model.repository.EducationRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by m-latifi on 7/26/2023.
 */

@HiltViewModel
class EducationViewModel @Inject constructor(
    private val educationRepository: EducationRepository
) : ZarViewModel() {


    private val educationList = mutableListOf<EducationModel>()

    val educationLiveData: MutableLiveData<List<EducationModel>?> by lazy {
        MutableLiveData<List<EducationModel>?>()
    }


    //---------------------------------------------------------------------------------------------- getEducation
    fun getVideoList(videoGroupId: Long) {
        viewModelScope.launch(IO + exceptionHandler()) {
            if (educationList.isEmpty())
                gelMoreVideoList(videoGroupId)
            else
                educationLiveData.postValue(educationList)
        }
    }
    //---------------------------------------------------------------------------------------------- getEducation


    //---------------------------------------------------------------------------------------------- gelMoreVideoList
    fun gelMoreVideoList(videoGroupId: Long) {
        viewModelScope.launch(IO + exceptionHandler()) {
            callApi(
                request = educationRepository.getVideoList(videoGroupId),
                onReceiveData = {
                    if (!it.items.isNullOrEmpty()) {
                        educationList.addAll(it.items)
                        educationLiveData.postValue(educationList)
                    } else
                        setMessage("لیست خالی است") //TODO
                }
            )
        }
    }
    //---------------------------------------------------------------------------------------------- gelMoreVideoList


}