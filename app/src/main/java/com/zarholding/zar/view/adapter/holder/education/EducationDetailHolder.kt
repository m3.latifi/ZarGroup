package com.zarholding.zar.view.adapter.holder.education

import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemEducationDetailBinding
import com.zarholding.zar.ext.setEducationTime
import com.zarholding.zar.model.data.response.education.EducationDetailModel

/**
 * Created by m-latifi on 7/26/2023.
 */

class EducationDetailHolder(
    private val binding: ItemEducationDetailBinding,
    private val onClick: (EducationDetailModel) -> Unit
): RecyclerView.ViewHolder(binding.root) {


    //---------------------------------------------------------------------------------------------- bind
    fun bind(item: EducationDetailModel) {
        setValueToXml(item)
        binding.root.setOnClickListener { onClick(item) }
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind


    //---------------------------------------------------------------------------------------------- setValueToXml
    private fun setValueToXml(item: EducationDetailModel) {
        binding.textViewVideoTitle.text = item.title
        binding.textViewVideoDetail.setEducationTime(item.sessionMin)
    }
    //---------------------------------------------------------------------------------------------- setValueToXml

}