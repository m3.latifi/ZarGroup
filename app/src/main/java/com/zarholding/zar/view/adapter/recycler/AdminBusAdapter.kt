package com.zarholding.zar.view.adapter.recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemAdminBusBinding
import com.zarholding.zar.model.data.response.trip.TripRequestRegisterModel
import com.zarholding.zar.view.adapter.holder.AdminBusItemHolder

/**
 * Created by m-latifi on 11/16/2022.
 */

class AdminBusAdapter(
    private var items: List<TripRequestRegisterModel>,
    private val onClick: () -> Unit
) : RecyclerView.Adapter<AdminBusItemHolder>() {

    private var layoutInflater : LayoutInflater? = null


    //---------------------------------------------------------------------------------------------- onCreateViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdminBusItemHolder {

        if (layoutInflater == null)
            layoutInflater = LayoutInflater.from(parent.context)

        return AdminBusItemHolder(
            binding = ItemAdminBusBinding.inflate(layoutInflater!!, parent, false),
            onClick = onClick
        )
    }
    //---------------------------------------------------------------------------------------------- onCreateViewHolder


    //---------------------------------------------------------------------------------------------- onBindViewHolder
    override fun onBindViewHolder(holder: AdminBusItemHolder, position: Int) {
        holder.bind(items[position])
    }
    //---------------------------------------------------------------------------------------------- onBindViewHolder


    //---------------------------------------------------------------------------------------------- onBindViewHolder
    override fun getItemCount() = items.size
    //---------------------------------------------------------------------------------------------- onBindViewHolder


/*
    //---------------------------------------------------------------------------------------------- getItems
    fun getItems() = items
    //---------------------------------------------------------------------------------------------- getItems
*/


}