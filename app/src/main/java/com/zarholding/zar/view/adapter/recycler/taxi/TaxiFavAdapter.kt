package com.zarholding.zar.view.adapter.recycler.taxi

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemTaxiFavBinding
import com.zarholding.zar.model.data.response.taxi.TaxiFavPlaceModel
import com.zarholding.zar.view.adapter.holder.taxi.TaxiFavHolder

/**
 * Created by m-latifi on 10/24/2023.
 */

class TaxiFavAdapter(
    private val items: List<TaxiFavPlaceModel>,
    private val onSelect: (TaxiFavPlaceModel) -> Unit
) : RecyclerView.Adapter<TaxiFavHolder>() {

    private var layoutInflater: LayoutInflater? = null

    //---------------------------------------------------------------------------------------------- onCreateViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaxiFavHolder {
        if (layoutInflater == null)
            layoutInflater = LayoutInflater.from(parent.context)
        return TaxiFavHolder(
            ItemTaxiFavBinding.inflate(layoutInflater!!, parent, false),
            onSelect
        )
    }
    //---------------------------------------------------------------------------------------------- onCreateViewHolder


    //---------------------------------------------------------------------------------------------- onBindViewHolder
    override fun onBindViewHolder(holder: TaxiFavHolder, position: Int) {
        holder.bind(items[position])
    }
    //---------------------------------------------------------------------------------------------- onBindViewHolder


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun getItemCount() = items.size
    //---------------------------------------------------------------------------------------------- getItemCount

}