package com.zarholding.zar.view.adapter.holder.survey

import android.text.Editable
import android.text.TextWatcher
import android.util.TypedValue
import android.view.View
import android.view.View.LAYOUT_DIRECTION_RTL
import android.widget.LinearLayout
import android.widget.RadioButton
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.get
import com.zarholding.zar.R
import com.zarholding.zar.databinding.ItemSurveySingleChooseBinding
import com.zarholding.zar.ext.setSurveyNumber
import com.zarholding.zar.model.data.response.survey.SurveyQuestionModel

/**
 * Create by Mehrdad on 1/21/2023
 */
class SurveySingleChooseHolder(
    private val binding: ItemSurveySingleChooseBinding
) : SurveyQuestionHolder(binding.root) {

    private var textWatcher: TextWatcher? = null

    //---------------------------------------------------------------------------------------------- bind
    override fun bind(item: SurveyQuestionModel, position: Int) {
        setValueToXml(item, position)
        setListener(item)
        binding.executePendingBindings()
    }
    //---------------------------------------------------------------------------------------------- bind


    //---------------------------------------------------------------------------------------------- setValueToXml
    private fun setValueToXml(item: SurveyQuestionModel, position: Int) {
        binding.textViewNumber.setSurveyNumber(position + 1)
        binding.textViewQuestion.text = item.questionTitle
        binding.editTextAnswer.removeTextChangedListener(textWatcher)
        binding.radioGroupAnswer.removeAllViews()
        val param = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        param.setMargins(10, 15, 10, 10)
        val typeface = ResourcesCompat.getFont(binding.root.context, R.font.kalameh_regular)
        binding.editTextAnswer.visibility = View.GONE
        var indexSelected: Int? = null
        item.options?.let { options ->
            for (i in options.indices) {
                val radio = RadioButton(binding.root.context).apply {
                    setTextSize(TypedValue.COMPLEX_UNIT_SP, 13f)
                    setTextColor(binding.root.context.getColor(R.color.n_textColorPrimary))
                    layoutParams = param
                    buttonDrawable = AppCompatResources.getDrawable(
                        binding.radioGroupAnswer.context,
                        R.drawable.a_drawable_radio
                    )
                    setPadding(0,0,15,0)
                    text = options[i].optionTitle
                    layoutDirection = LAYOUT_DIRECTION_RTL
                    if (indexSelected == null)
                        indexSelected = if (
                            item.selectedAnswerId != null &&
                            item.selectedAnswerId == options[i].questionOptionId
                        ) i else null
                    setTypeface(typeface)
                    setOnCheckedChangeListener { _, isChecked ->
                        if (!isChecked) {
                            setTextColor(binding.radioGroupAnswer.context.getColor(R.color.n_textColorPrimary))
                            return@setOnCheckedChangeListener
                        }
                        setTextColor(binding.radioGroupAnswer.context.getColor(R.color.primaryColor))
                        item.selectedAnswerId = options[i].questionOptionId
                        if (options[i].hasAdditionalText) {
                            binding.editTextAnswer.visibility = View.VISIBLE
                            binding.editTextAnswer.setText(item.answer)
                        } else {
                            binding.editTextAnswer.visibility = View.GONE
                            binding.editTextAnswer.text.clear()
                        }
                    }
                }
                binding.radioGroupAnswer.addView(radio, param)
            }
            indexSelected?.let {
                (binding.radioGroupAnswer[it] as RadioButton).isChecked = true
            }
        }
    }
    //---------------------------------------------------------------------------------------------- setValueToXml


    //---------------------------------------------------------------------------------------------- setListener
    private fun setListener(item: SurveyQuestionModel) {
        textWatcher = object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                item.answer = s.toString()
            }
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        }
        binding.editTextAnswer.addTextChangedListener(textWatcher)
    }
    //---------------------------------------------------------------------------------------------- setListener
}
