package com.zarholding.zar.view.activity

import androidx.lifecycle.viewModelScope
import com.google.firebase.messaging.FirebaseMessaging
import com.zar.core.tools.manager.ThemeManager
import com.zarholding.zar.model.repository.TokenRepository
import com.zarholding.zar.model.repository.UserRepository
import com.zarholding.zar.tools.manager.RoleManager
import com.zarholding.zar.ZarViewModel
import com.zarholding.zar.tools.manager.SharedPreferencesManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val sharedPreferencesManager: SharedPreferencesManager,
    private val userRepository: UserRepository,
    private val roleManager: RoleManager,
    private val themeManager: ThemeManager,
    private val tokenRepository: TokenRepository
) : ZarViewModel() {

    companion object {
        var notificationCount: Int = 0
    }


    //---------------------------------------------------------------------------------------------- deleteAllData
    fun deleteAllData() {
        viewModelScope.launch(IO + exceptionHandler()) {
            FirebaseMessaging.getInstance().deleteToken()
            userRepository.deleteUser()
            sharedPreferencesManager.deleteUserLogin()
        }
    }
    //---------------------------------------------------------------------------------------------- deleteAllData


    //---------------------------------------------------------------------------------------------- setLastNotificationIdToZero
    fun setLastNotificationIdToZero() {
        viewModelScope.launch(IO + exceptionHandler()) {
            delay(1000)
            sharedPreferencesManager.setLastNotificationIdToZero()
        }
    }
    //---------------------------------------------------------------------------------------------- setLastNotificationIdToZero



    //---------------------------------------------------------------------------------------------- getUserInfo
    fun getUserInfo() = userRepository.getUser()
    //---------------------------------------------------------------------------------------------- getUserInfo


    //---------------------------------------------------------------------------------------------- getAdminRole
    fun getAdminRole() = roleManager.isAdminRole()
    //---------------------------------------------------------------------------------------------- getAdminRole


    //---------------------------------------------------------------------------------------------- applicationTheme
    fun applicationTheme() = themeManager.applicationTheme()
    //---------------------------------------------------------------------------------------------- applicationTheme


    //---------------------------------------------------------------------------------------------- getBearerToken
    fun getBearerToken() = tokenRepository.getBearerToken()
    //---------------------------------------------------------------------------------------------- getBearerToken

}