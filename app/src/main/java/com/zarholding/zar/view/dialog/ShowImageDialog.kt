package com.zarholding.zar.view.dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import com.zarholding.zar.R
import com.zarholding.zar.model.data.other.ShowImageModel
import com.zarholding.zar.view.custom.TouchImageView
import com.zarholding.zar.ext.loadImage
import com.zarholding.zar.ext.loadImageByToken

/**
 * Created by m-latifi on 11/26/2022.
 */

class ShowImageDialog(
    context: Context,
    private val item : ShowImageModel,
    onShowDialog: () -> Unit,
    onDismissDialog: () -> Unit
) : ZarDialog(context, onShowDialog, onDismissDialog) {


    //---------------------------------------------------------------------------------------------- onCreate
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_show_image)
        val lp = WindowManager.LayoutParams()
        this.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        this.window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        this.window?.setGravity(Gravity.CENTER)
        lp.copyFrom(this.window?.attributes)
        lp.horizontalMargin = 50f
        this.window?.attributes = lp
    }
    //---------------------------------------------------------------------------------------------- onCreate



    //---------------------------------------------------------------------------------------------- onStart
    override fun onStart() {
        initDialog()
        super.onStart()
    }
    //---------------------------------------------------------------------------------------------- onStart



    //---------------------------------------------------------------------------------------------- initDialog
    private fun initDialog() {
        val imageViewClose = this.findViewById<ImageView>(R.id.imageViewClose)
        val touchImageView = this.findViewById<TouchImageView>(R.id.touchImageView)
        item.token?.let {
            touchImageView.loadImageByToken(item.imageName, it)
        } ?: run {
            item.entityType?.let {
                touchImageView.loadImage(item.imageName, it)
            }
        }
        imageViewClose.setOnClickListener {
            dismiss()
        }
    }
    //---------------------------------------------------------------------------------------------- initDialog

}