package com.zarholding.zar.view.adapter.recycler.hse

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zarholding.zar.databinding.ItemHsePeriodBinding
import com.zarholding.zar.model.data.response.dropdown.DropDownModel
import com.zarholding.zar.view.adapter.holder.hse.HsePeriodHolder

/**
 * Created by m-latifi on 7/26/2023.
 */

class HsePeriodAdapter(
    private val items: List<DropDownModel>,
    private val onClick: (DropDownModel) -> Unit
) : RecyclerView.Adapter<HsePeriodHolder>() {

    private var inflater: LayoutInflater? = null

    //---------------------------------------------------------------------------------------------- onCreateViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HsePeriodHolder {
        if (inflater == null)
            inflater = LayoutInflater.from(parent.context)
        return HsePeriodHolder(
            binding = ItemHsePeriodBinding.inflate(inflater!!, parent, false),
            onClick = onClick
        )
    }
    //---------------------------------------------------------------------------------------------- onCreateViewHolder


    //---------------------------------------------------------------------------------------------- onBindViewHolder
    override fun onBindViewHolder(holder: HsePeriodHolder, position: Int) {
        holder.bind(items[position])
    }
    //---------------------------------------------------------------------------------------------- onBindViewHolder


    //---------------------------------------------------------------------------------------------- getItemCount
    override fun getItemCount() = items.size
    //---------------------------------------------------------------------------------------------- getItemCount

}