package com.zarholding.zar.tools.manager

import android.content.SharedPreferences
import com.zarholding.zar.tools.CompanionValues
import javax.inject.Inject

/**
 * Created by m-latifi on 7/31/2023.
 */

class SharedPreferencesManager @Inject constructor(private val sp: SharedPreferences) {

    private val tokenKey = "tokenKey"
    private val userNameKey = "userName"
    private val passcodeKey = "passcode"
    private val biometricKey = "biometric"
    private val versionApp = "version_app"
    private val fireBaseTokenKey = "firebase_token"

    //---------------------------------------------------------------------------------------------- isBiometricEnable
    fun isBiometricEnable() = sp.getBoolean(biometricKey, false)
    //---------------------------------------------------------------------------------------------- isBiometricEnable


    //---------------------------------------------------------------------------------------------- getUserName
    fun getUserName() = sp.getString(userNameKey, "")
    //---------------------------------------------------------------------------------------------- getUserName


    //---------------------------------------------------------------------------------------------- getPassword
    fun getPassword() = sp.getString(passcodeKey, "")
    //---------------------------------------------------------------------------------------------- getPassword


    //---------------------------------------------------------------------------------------------- saveUserLogin
    fun saveUserLogin(token: String, userName: String, password: String) {
        sp.edit()
            .putString(this.tokenKey, token)
            .putString(userNameKey, userName)
            .putString(passcodeKey, password)
            .apply()
    }
    //---------------------------------------------------------------------------------------------- saveUserLogin


    //---------------------------------------------------------------------------------------------- deleteUserLogin
    fun deleteUserLogin() {
        sp.edit()
            .putString(tokenKey, null)
            .putInt(CompanionValues.notificationLast, 0)
            .putString(fireBaseTokenKey, null)
            .apply()
    }
    //---------------------------------------------------------------------------------------------- deleteUserLogin


    //---------------------------------------------------------------------------------------------- getBearerToken
    fun getBearerToken() = "Bearer ${sp.getString(tokenKey, null)}"
    //---------------------------------------------------------------------------------------------- getBearerToken


    //---------------------------------------------------------------------------------------------- getToken
    fun getToken() = sp.getString(tokenKey, null)
    //---------------------------------------------------------------------------------------------- getToken


    //---------------------------------------------------------------------------------------------- setLastNotificationIdToZero
    fun setLastNotificationIdToZero() {
        sp.edit().putInt(CompanionValues.notificationLast, 0).apply()
    }
    //---------------------------------------------------------------------------------------------- setLastNotificationIdToZero


    //---------------------------------------------------------------------------------------------- changeBiometricEnable
    fun changeBiometricEnable(biometric: Boolean) {
        sp.edit().putBoolean(biometricKey, biometric).apply()
    }
    //---------------------------------------------------------------------------------------------- changeBiometricEnable


    //---------------------------------------------------------------------------------------------- getLastNotificationId
    fun getLastNotificationId() = sp.getInt(CompanionValues.notificationLast, 0)
    //---------------------------------------------------------------------------------------------- getLastNotificationId


    //---------------------------------------------------------------------------------------------- setLastNotificationId
    fun setLastNotificationId(id: Int) {
        sp.edit().putInt(CompanionValues.notificationLast, id).apply()
    }
    //---------------------------------------------------------------------------------------------- setLastNotificationId


    //---------------------------------------------------------------------------------------------- getVersion
    fun getVersion() = sp.getLong(versionApp, 0L)
    //---------------------------------------------------------------------------------------------- getVersion


    //---------------------------------------------------------------------------------------------- setVersion
    fun setVersion(version: Long) = sp.edit().putLong(versionApp, version).apply()
    //---------------------------------------------------------------------------------------------- setVersion


    //---------------------------------------------------------------------------------------------- getFirebaseToken
    fun getFirebaseToken() = sp.getString(fireBaseTokenKey, null)
    //---------------------------------------------------------------------------------------------- getFirebaseToken


    //---------------------------------------------------------------------------------------------- getFirebaseToken
    fun setFirebaseToken(newToken: String) {
        sp.edit().putString(fireBaseTokenKey, newToken).apply()
    }
    //---------------------------------------------------------------------------------------------- getFirebaseToken


}