package com.zarholding.zar.tools

/**
 * Created by m-latifi on 11/9/2022.
 */

class CompanionValues {

    companion object {
        const val APP_NAME = "app_name"
        const val channelId = "SuperApp"
        const val adminTaxiType = "adminTaxiType"
        const val myRequest = "myRequest"
        const val notificationLast = "notificationLast"
        const val latOrigin = "latOrigin"
        const val lngOrigin = "lngOrigin"
        const val latDestination = "latDestination"
        const val lngDestination = "lngDestination"
        const val DOWNLOAD_URL = "download_url"
        const val DOWNLOAD_TYPE = "download_type"
        const val EDUCATION_CAT_ID = "education_cat_id"
        const val EDUCATION_CAT_NAME = "education_cat_name"
        const val EDUCATION_TIME = "education_time"
        const val EDUCATION_COUNT = "education_count"
        const val EDUCATION_FILE_NAME = "education_file_name"
        const val TYPE_OBJECT = "type_object"
        const val EXAMINATION_ID = "examination_id"
    }
}