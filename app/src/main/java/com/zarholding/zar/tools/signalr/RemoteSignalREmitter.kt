package com.zarholding.zar.tools.signalr

import com.zarholding.zar.model.data.response.notification.NotificationModel


/**
 * Created by m-latifi on 11/21/2022.
 */

interface RemoteSignalREmitter {

    fun onConnectToSignalR(){}
    fun onErrorConnectToSignalR(){}
    fun onReConnectToSignalR(){}
    fun onReceiveMessage(user : String, message : NotificationModel){}
    fun onGetPoint(lat : String, lng : String){}
    fun onPreviousStationReached(message : String){}
}