package com.zarholding.zar.tools.manager.speechtotext

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.speech.RecognitionListener
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.util.Log
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Inject

/**
 * Created by m-latifi on 10/1/2023.
 */

@Module
@InstallIn(SingletonComponent::class)
class SpeechToTextManager @Inject constructor(
    @ApplicationContext private val context: Context
) {

    //---------------------------------------------------------------------------------------------- showGoogleDialog
    fun showGoogleDialog(launcher: ActivityResultLauncher<Intent>) {
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        )
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "fa")
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, " یه چیزی بگو...")
        try {
            launcher.launch(intent)
        } catch (e: Exception) {
            Toast.makeText(context, "عدم نمایش  دیالوگ گوگل", Toast.LENGTH_SHORT).show()
        }
    }
    //---------------------------------------------------------------------------------------------- showGoogleDialog


    //---------------------------------------------------------------------------------------------- speechRecognizer
    fun speechRecognizer(viewInterface: SpeechToTextInterface) {
        val speechRecognizer: SpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(context)
        speechRecognizer.setRecognitionListener(object : RecognitionListener {
            override fun onReadyForSpeech(params: Bundle?) {
                // زمانی که سیستم آماده به شنود گفتار می‌شود
                viewInterface.onReadyForSpeech()
            }

            override fun onBeginningOfSpeech() {
                // زمانی که شروع به شنود گفتار می‌شود
            }

            override fun onRmsChanged(rmsdB: Float) {
                // تغییرات در سطح صدا
            }

            override fun onBufferReceived(buffer: ByteArray?) {
                // دریافت داده‌های صوتی
            }

            override fun onEndOfSpeech() {
                // پایان شنود گفتار
                viewInterface.onEndOfSpeech()
            }

            override fun onError(error: Int) {
                // در صورت بروز خطا
                viewInterface.onEndOfSpeech()
            }

            override fun onResults(results: Bundle?) {
                // نتایج تبدیل گفتار به نوشتار در اینجا قابل دسترسی هستند
                val resultList = results?.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
                if (!resultList.isNullOrEmpty()) {
                    val recognizedText = resultList[0]
                    // اینجا می‌توانید متن شنود شده را استفاده کنید
                    viewInterface.onResults(recognizedText)
                }
            }

            override fun onPartialResults(partialResults: Bundle?) {
                // نتایج جزئی
            }

            override fun onEvent(eventType: Int, params: Bundle?) {
                // رخدادهای اضافی
            }
        })
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        // تنظیم زبان به فارسی
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "fa-IR")
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
        speechRecognizer.startListening(intent)
    }
    //---------------------------------------------------------------------------------------------- speechRecognizer



    //---------------------------------------------------------------------------------------------- splitResultToList
    fun splitResultToList(result: ActivityResult): List<String> {
        val resultStringList = mutableListOf<String>()
        if (result.resultCode == Activity.RESULT_OK && result.data != null) {
            val textArray =
                result.data!!.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
            textArray?.let { arrayList ->
                for (item in arrayList) {
                    val split = item.split(" ")
                    val splitFilter = split.filter {
                        it.length > 1
                    }
                    resultStringList.addAll(splitFilter)
                }
            }
        }
        return resultStringList.toList()
    }
    //---------------------------------------------------------------------------------------------- splitResultToList


    //---------------------------------------------------------------------------------------------- splitResultToString
    fun splitResultToString(result: ActivityResult): String {
        var resultString = ""
        if (result.resultCode == Activity.RESULT_OK && result.data != null) {
            val textArray =
                result.data!!.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
            textArray?.let { arrayList ->
                resultString = arrayList.joinToString(separator = " ") { item ->
                    item.split(" ").filter {
                        it.length > 1
                    }.joinToString(separator = " ")
                }
            }
        }
        return resultString
    }
    //---------------------------------------------------------------------------------------------- splitResultToString

}