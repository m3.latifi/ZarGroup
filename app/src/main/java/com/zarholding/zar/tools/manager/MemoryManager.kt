package com.zarholding.zar.tools.manager

import android.os.Environment
import android.os.StatFs
import javax.inject.Inject

/**
 * Created by m-latifi on 11/20/2023.
 */

class MemoryManager @Inject constructor() {

    //---------------------------------------------------------------------------------------------- getInternalMemoryFreeSize
    private fun getInternalMemoryFreeSpace(): Long {
        val stat = StatFs(Environment.getExternalStorageDirectory().path)
        val bytesAvailable: Long = stat.blockSizeLong * stat.availableBlocksLong
        return bytesAvailable / (1024 * 1024)
    }
    //---------------------------------------------------------------------------------------------- getInternalMemoryFreeSize


    //---------------------------------------------------------------------------------------------- checkFreeSpaceForDownloading
    fun checkFreeSpaceForDownloading() = getInternalMemoryFreeSpace() > 500
    //---------------------------------------------------------------------------------------------- checkFreeSpaceForDownloading

}