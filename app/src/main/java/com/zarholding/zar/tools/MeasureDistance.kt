package com.zarholding.zar.tools

import android.location.Location
import org.osmdroid.util.GeoPoint

/**
 * Created by m-latifi on 8/16/2023.
 */

//---------------------------------------------------------------------------------------------- measureDistance
fun measureDistance(point1: GeoPoint, point2: GeoPoint): Float {
    val results = FloatArray(1)
    Location.distanceBetween(
        point1.latitude, point1.longitude,
        point2.latitude, point2.longitude, results
    )
    return if (results.isNotEmpty()) results[0] else 0f
}
//---------------------------------------------------------------------------------------------- measureDistance