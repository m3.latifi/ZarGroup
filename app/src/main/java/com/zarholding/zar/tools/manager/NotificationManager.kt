package com.zarholding.zar.tools.manager

import android.Manifest
import android.app.Notification
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.zarholding.zar.R
import com.zarholding.zar.tools.CompanionValues
import com.zarholding.zar.view.activity.MainActivity
import javax.inject.Inject


/**
 * Create by Mehrdad on 1/14/2023
 */

class NotificationManager @Inject constructor(private val context: Context) {


    //---------------------------------------------------------------------------------------------- showNotification
    fun showNotification(id: Int, title: String?, message: String?) {
        val intent = Intent(context, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(
            context, System.currentTimeMillis().toInt(), intent,
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
        )
        val icon = BitmapFactory.decodeResource(
            context.resources,
            R.drawable.a_ic_app
        )
        val alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val vibrate: LongArray = longArrayOf(1000L, 1000L, 1000L, 1000L, 1000L)
        val notifyManager = NotificationManagerCompat.from(context)
        val notificationBuilder = NotificationCompat
            .Builder(context, CompanionValues.channelId)
        val notification = notificationBuilder
            .setSmallIcon(R.drawable.ic_notification)
            .setLargeIcon(icon)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setCategory(NotificationCompat.CATEGORY_MESSAGE)
            .setContentTitle(title)
            .setContentText(message)
            .setVibrate(vibrate)
            .setContentIntent(pendingIntent)
            .setDefaults(Notification.DEFAULT_ALL)
            .setSound(alarmSound)
            .setAutoCancel(true)
        if (ActivityCompat.checkSelfPermission(
                context, Manifest.permission.POST_NOTIFICATIONS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        notifyManager.notify(id, notification.build())
    }
    //---------------------------------------------------------------------------------------------- showNotification


}