package com.zarholding.zar.tools.manager.speechtotext


/**
 * Created by m-latifi on 10/18/2023.
 */

interface SpeechToTextInterface {

    fun onReadyForSpeech()
    fun onEndOfSpeech()
    fun onResults(result: String)

}