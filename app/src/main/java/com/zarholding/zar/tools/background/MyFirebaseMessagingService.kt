package com.zarholding.zar.tools.background

import android.content.Intent
import android.util.Log
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.zarholding.zar.model.data.response.notification.NotificationModel
import com.zarholding.zar.tools.CompanionValues
import com.zarholding.zar.tools.manager.NotificationManager
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * Created by m-latifi on 6/19/2023.
 */

@AndroidEntryPoint
class MyFirebaseMessagingService : FirebaseMessagingService() {

    companion object {
        private const val TAG = "meri"
    }

    private val notificationNewMessageId = 0

    @Inject
    lateinit var notificationManager: NotificationManager


    //---------------------------------------------------------------------------------------------- [START receive_message]
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.d(TAG, "From: ${remoteMessage.from}")
        Log.d(TAG, "notification: ${remoteMessage.notification?.body}")
        Log.d(TAG, "data: ${remoteMessage.data}")
        handleNow(remoteMessage)
/*        if (remoteMessage.data.isNotEmpty()) {
            if (needsToBeScheduled()) {
                // For long-running tasks (10 seconds or more) use WorkManager.
                scheduleJob()
            } else {
                // Handle message within 10 seconds
                handleNow(remoteMessage)
            }
        }*/
    }
    //---------------------------------------------------------------------------------------------- [END receive_message]


    //---------------------------------------------------------------------------------------------- [START on_new_token]
    override fun onNewToken(token: String) {
        FirebaseMessaging.getInstance().subscribeToTopic(packageName)
        Log.d(TAG, "Refreshed token: $token")
        sendRegistrationToServer(token)
    }
    //---------------------------------------------------------------------------------------------- [END on_new_token]



    //---------------------------------------------------------------------------------------------- scheduleJob
    private fun handleNow(remoteMessage: RemoteMessage) {
        remoteMessage.notification?.let {
            showNotification(message = it.body, title = it.title)
            val intent = Intent("com.zarholding.zar.receive.message")
            intent.putExtra(CompanionValues.notificationLast, it.title)
            sendBroadcast(intent)
/*            val gson = Gson()
            Log.e("meri","it.body = ${it.body}")
            val message = gson.fromJson(it.body, NotificationModel::class.java)
            val item = gson.toJson(message)
            val intent = Intent("com.zarholding.zar.receive.message")
            intent.putExtra(CompanionValues.notificationLast, item)
            sendBroadcast(intent)
            showNotification(message)
            Log.d(TAG, "Message Notification Body: ${it.body}")*/
        }
    }
    //---------------------------------------------------------------------------------------------- scheduleJob


    //---------------------------------------------------------------------------------------------- scheduleJob
    private fun sendRegistrationToServer(token: String?) {
        Log.d(TAG, "sendRegistrationTokenToServer($token)")
    }
    //---------------------------------------------------------------------------------------------- scheduleJob


    //---------------------------------------------------------------------------------------------- showNotification
    private fun showNotification(message: NotificationModel) {
        if (message.message.isNullOrEmpty())
            return
        notificationManager.showNotification(
            notificationNewMessageId,
            message.senderName,
            message.message
        )
    }
    //---------------------------------------------------------------------------------------------- showNotification


    //---------------------------------------------------------------------------------------------- showNotification
    private fun showNotification(message: String?, title: String?) {
        if (message.isNullOrEmpty())
            return
        notificationManager.showNotification(
            notificationNewMessageId,
            title,
            message
        )
    }
    //---------------------------------------------------------------------------------------------- showNotification

}