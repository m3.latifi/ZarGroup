package com.zarholding.zar.tools.manager

import com.zarholding.zar.model.data.database.dao.RoleDao
import com.zarholding.zar.model.data.database.dao.UserInfoDao
import javax.inject.Inject

/**
 * Created by m-latifi on 11/26/2022.
 */

class RoleManager @Inject constructor(
    private val userInfoDao: UserInfoDao,
    private val roleDao: RoleDao
) {

    private val superAppApprove = "SuperAppApprover" // نمایش دکمه کارتابل در صفحه خانه
    private val adminBusRole = "Cmt.CommuteRegisteredPersonnel.Approve" // نمایش تایل تایید سرویس رفت و آمد
    private val busRegisterAdd = "Cmt.CommuteRegisteredPersonnel.Add" //  نمایش تایل سرویس رفت و امد در صفحه خانه
    private val taxiRequestAdd = "Cr.CarRequest.Add" //  نمایش تایل درخواست خودرو در صفحه خانه
    private val plaqueView = "Se.Parking" // نمایش تایل پارکیگ در صفحه خانه
    private val educationView = "Cmt.ManageVideos.View"// نمایش تایل مرکز آموزش در صفحه خانه
    private val quotaEdit = "Cmt.Quotas.Edit" // تغییر وضعیت بسته
    private val quotaStockChecker = "Cmt.Quotas.StockChecker" // مسئول انبار برای تحویل بسته
    private val quotaSecurityChecker = "Cmt.Quotas.SecurityChecker" // حراست برای خروج بسته از شرکت

    //---------------------------------------------------------------------------------------------- isAdminRole
    fun isAdminRole(): Boolean {
        return !userInfoDao.getUserInfo()?.roles?.find {
            it == superAppApprove
        }.isNullOrEmpty()
    }
    //---------------------------------------------------------------------------------------------- isAdminRole


    //---------------------------------------------------------------------------------------------- accessByRole
    private fun accessByRole(role: String) =
        roleDao.getPermission(role)?.let { true } ?: run { false }
    //---------------------------------------------------------------------------------------------- accessByRole


    //---------------------------------------------------------------------------------------------- isAccessToAdminBus
    fun isAccessToAdminBus() = accessByRole(adminBusRole)
    //---------------------------------------------------------------------------------------------- isAccessToAdminBus


    //---------------------------------------------------------------------------------------------- isAccessBusRegisterAdd
    fun isAccessBusRegisterAdd() = accessByRole(busRegisterAdd)
    //---------------------------------------------------------------------------------------------- isAccessBusRegisterAdd


    //---------------------------------------------------------------------------------------------- isAccessTaxiRequestAdd
    fun isAccessTaxiRequestAdd() = accessByRole(taxiRequestAdd)
    //---------------------------------------------------------------------------------------------- isAccessTaxiRequestAdd


    //---------------------------------------------------------------------------------------------- isAccessPlaqueView
    fun isAccessPlaqueView() = accessByRole(plaqueView)
    //---------------------------------------------------------------------------------------------- isAccessPlaqueView


    //---------------------------------------------------------------------------------------------- isAccessEducationView
    fun isAccessEducationView() = accessByRole(educationView)
    //---------------------------------------------------------------------------------------------- isAccessEducationView


    //---------------------------------------------------------------------------------------------- isAccessQuotaEditView
    fun isAccessQuotaEditView() = accessByRole(quotaEdit)
    //---------------------------------------------------------------------------------------------- isAccessQuotaEditView


    //---------------------------------------------------------------------------------------------- isAccessQuotaStockChecker
    fun isAccessQuotaStockChecker() = accessByRole(quotaStockChecker)
    //---------------------------------------------------------------------------------------------- isAccessQuotaStockChecker


    //---------------------------------------------------------------------------------------------- isAccessQuotaSecurityChecker
    fun isAccessQuotaSecurityChecker() = accessByRole(quotaSecurityChecker)
    //---------------------------------------------------------------------------------------------- isAccessQuotaSecurityChecker

}