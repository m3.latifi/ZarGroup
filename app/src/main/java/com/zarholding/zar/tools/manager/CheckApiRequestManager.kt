package com.zarholding.zar.tools.manager

import com.zar.core.enums.EnumApiError
import com.zar.core.models.ErrorApiModel
import com.zar.core.tools.api.checkResponseError
import com.zarholding.zar.R
import com.zarholding.zar.di.ResourcesProvider
import com.zarholding.zar.model.data.response.GeneralResponse
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by m-latifi on 8/8/2023.
 */

@Singleton
class CheckApiRequestManager @Inject constructor() {

    @Inject
    lateinit var resourcesProvider: ResourcesProvider

    //---------------------------------------------------------------------------------------------- handelApi
    fun <T : Any> handelApi(
        request: Response<GeneralResponse<T?>>?,
        showMessageAfterSuccessResponse: Boolean,
        onReceiveData: (T) -> Unit,
        onError: (ErrorApiModel) -> Unit
    ) {
        if (request?.isSuccessful == true) {
            val body = request.body()
            body?.let { generalResponse ->
                if (generalResponse.hasError || generalResponse.data == null)
                    onError(ErrorApiModel(EnumApiError.Warning, generalResponse.message))
                else {
                    if (showMessageAfterSuccessResponse)
                        onError(ErrorApiModel(EnumApiError.Done, generalResponse.message))
                    onReceiveData(generalResponse.data)
                }
            } ?: run {
                onError(
                    ErrorApiModel(
                        EnumApiError.Error,
                        resourcesProvider.getString(R.string.dataReceivedIsEmpty)
                    )
                )
            }
        } else onError(checkResponseError(request))
    }
    //---------------------------------------------------------------------------------------------- handelApi

}