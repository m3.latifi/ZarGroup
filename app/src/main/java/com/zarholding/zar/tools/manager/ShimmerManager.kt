package com.zarholding.zar.tools.manager

import android.animation.ValueAnimator
import com.facebook.shimmer.Shimmer


/**
 * create by m-latifi on 4/4/2023
 */


//-------------------------------------------------------------------------------------------------- getShimmerBuild
fun getShimmerBuild(): Shimmer = Shimmer.AlphaHighlightBuilder()
    .setDirection(Shimmer.Direction.RIGHT_TO_LEFT)
    .setDuration(2000L)
    .setRepeatMode(ValueAnimator.REVERSE)
    .setTilt(0f)
    .build()
//-------------------------------------------------------------------------------------------------- getShimmerBuild