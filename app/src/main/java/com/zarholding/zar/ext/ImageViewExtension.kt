package com.zarholding.zar.ext

import android.widget.ImageView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.zarholding.zar.R
import com.zarholding.zar.model.api.ApiSuperApp
import com.zarholding.zar.di.Providers
import com.zarholding.zar.model.data.enum.EnumStatus

/**
 * Created by m-latifi on 01/04/2023.
 */

//-------------------------------------------------------------------------------------------------- setRegisterStationStatus
fun ImageView.setRegisterStationStatus(status: EnumStatus?) {
    status?.let {
        when (status) {
            EnumStatus.Reject -> setImageResource(R.drawable.a_ic_delete)
            EnumStatus.Pending -> setImageResource(R.drawable.a_ic_pending)
            EnumStatus.Confirmed -> setImageResource(R.drawable.a_ic_check)
        }
    }
}
//-------------------------------------------------------------------------------------------------- setRegisterStationStatus


//-------------------------------------------------------------------------------------------------- ImageView.setAppIcon
fun ImageView.setAppIcon(icon: Int) {
    setImageResource(icon)
}
//-------------------------------------------------------------------------------------------------- ImageView.setAppIcon


//-------------------------------------------------------------------------------------------------- loadImage
fun ImageView.loadImage(url: String?, entityType: String, setPlaceholder: Boolean = true) {
        val circularProgressDrawable = CircularProgressDrawable(this.context)
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.start()
    val link = "${Providers.url}${ApiSuperApp.v1}/Content/file?entityType=$entityType&fileName=$url"
    if (setPlaceholder)
        Glide
            .with(this)
            .load(link)
            .placeholder(R.drawable.a_solid_white)
            .into(this)
    else
        Glide
            .with(this)
            .load(link)
            .into(this)
}
//-------------------------------------------------------------------------------------------------- loadImage


//-------------------------------------------------------------------------------------------------- loadImageByToken
fun ImageView.loadImageByToken(url: String?, token: String) {
    val circularProgressDrawable = CircularProgressDrawable(this.context)
    circularProgressDrawable.strokeWidth = 5f
    circularProgressDrawable.centerRadius = 30f
    circularProgressDrawable.start()
    val link = "${Providers.url}${ApiSuperApp.v1}/LogIn/get-user-avatar/$url"
    val glideUrl = GlideUrl(
        link,
        LazyHeaders.Builder().addHeader("Authorization", token).build()
    )
    Glide
        .with(this)
        .load(glideUrl)
        .placeholder(R.drawable.a_ic_person_avatar)
        .into(this)
}
//-------------------------------------------------------------------------------------------------- loadImageByToken



//-------------------------------------------------------------------------------------------------- loadImageByUrl
fun ImageView.loadImageByUrl(url: String?) {
    val circularProgressDrawable = CircularProgressDrawable(this.context)
    circularProgressDrawable.strokeWidth = 5f
    circularProgressDrawable.centerRadius = 30f
    circularProgressDrawable.start()
    Glide
        .with(this)
        .load(url)
        .placeholder(R.drawable.a_ic_person_avatar)
        .into(this)
}
//-------------------------------------------------------------------------------------------------- loadImageByUrl
