package com.zarholding.zar.ext

import androidx.cardview.widget.CardView
import com.zarholding.zar.R
import com.zarholding.zar.model.data.enum.EnumStatus

/**
 * Created by m-latifi on 01/04/2023.
 */

//-------------------------------------------------------------------------------------------------- setRegisterStationStatus
fun CardView.setRegisterStationStatus(status: EnumStatus?) {
    status?.let {
        when (status) {
            EnumStatus.Pending -> setCardBackgroundColor(
                context.resources.getColor(
                    R.color.n_waiting,
                    context.theme
                )
            )
            EnumStatus.Confirmed -> setCardBackgroundColor(
                context.resources.getColor(
                    R.color.n_accept,
                    context.theme
                )
            )
            EnumStatus.Reject -> setCardBackgroundColor(
                context.resources.getColor(
                    R.color.n_reject,
                    context.theme
                )
            )
        }
    }
}
//-------------------------------------------------------------------------------------------------- setRegisterStationStatus
