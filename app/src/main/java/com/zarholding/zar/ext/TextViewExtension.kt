package com.zarholding.zar.ext

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.zar.core.tools.extensions.split
import com.zar.core.tools.extensions.toSolarDate
import com.zarholding.zar.R
import com.zarholding.zar.model.data.database.entity.UserInfoEntity
import com.zarholding.zar.model.data.enum.EnumQuotaApprove
import com.zarholding.zar.model.data.enum.EnumStatus
import java.time.Duration
import java.time.LocalDateTime

/**
 * Created by m-latifi on 11/14/2022.
 */


//-------------------------------------------------------------------------------------------------- setQuotaStatus
fun TextView.setQuotaStatus(status: EnumQuotaApprove){
    setTitleAndValue(
        title = context.getString(R.string.statusQuota),
        splitter = context.getString(R.string.colon),
        value = when(status) {
            EnumQuotaApprove.Assign -> context.getString(R.string.quotaStatusAssign)
            EnumQuotaApprove.Delivered -> context.getString(R.string.quotaStatusDelivered)
            EnumQuotaApprove.Finished -> context.getString(R.string.quotaStatusFinished)
        }
    )
}
//-------------------------------------------------------------------------------------------------- setQuotaStatus


//-------------------------------------------------------------------------------------------------- setEducationTime
fun TextView.setEducationTime(minute: Int) {
    if (minute < 60)
        text = context
            .resources
            .getString(R.string.minute, minute.toString())
    else {
        val hours = minute / 60
        val min = minute % 60
        text = if (min == 0)
            context
                .resources
                .getString(R.string.hour, hours.toString())
        else
            context
                .resources
                .getString(R.string.hour_minute, hours.toString(), min.toString())
    }
}
//-------------------------------------------------------------------------------------------------- setEducationTime


//-------------------------------------------------------------------------------------------------- getTextByValue
fun getTextByValue(title: String?, value: Any?, splitter: String): String {
    return value?.let {
        when (it) {
            is String -> "$title $splitter $it"
            is Long -> "$title $splitter $it"
            is Int -> "$it $splitter $title"
            is LocalDateTime -> "$title$splitter${it.toSolarDate()?.getSolarDate()}"
            else -> ""
        }
    } ?: run { "" }
}
//-------------------------------------------------------------------------------------------------- getTextByValue


//-------------------------------------------------------------------------------------------------- setTitleAndValue
@BindingAdapter("setTitle", "setValue", "setSplitter")
fun TextView.setTitleAndValue(title: String?, value: Any?, splitter: String) {
    text = getTextByValue(title, value, splitter)
}
//-------------------------------------------------------------------------------------------------- setTitleAndValue


//-------------------------------------------------------------------------------------------------- setSurveyNumber
@BindingAdapter("setSurveyNumber")
fun TextView.setSurveyNumber(number: Int) {
    text = context.getString(R.string.numberOfQuestion, number.toString())
}
//-------------------------------------------------------------------------------------------------- setSurveyNumber


//-------------------------------------------------------------------------------------------------- setJobKeyAndUnit
@BindingAdapter("setJobKeyAndUnit")
fun TextView.setJobKeyAndUnit(user: UserInfoEntity?) {
    text = user?.let {
        getTextByValue(
            it.personnelJobKeyText,
            it.organizationUnit,
            context.getString(R.string.space)
        )
    } ?: run { "" }
}
//-------------------------------------------------------------------------------------------------- setJobKeyAndUnit


//-------------------------------------------------------------------------------------------------- setProfileValue
@BindingAdapter("setProfileValue")
fun TextView.setProfileValue(value: String?) {
    text = if (value.isNullOrEmpty())
        context.getString(R.string.addInFiori)
    else
        value
}
//-------------------------------------------------------------------------------------------------- setProfileValue



//-------------------------------------------------------------------------------------------------- setProfileCarModel
@BindingAdapter("setProfileCarModel")
fun TextView.setProfileCarModel(value: String?) {
    text = if (value.isNullOrEmpty())
        "برای اضافه کردن روی مداد بزنید"
    else
        value
}
//-------------------------------------------------------------------------------------------------- setProfileCarModel



//-------------------------------------------------------------------------------------------------- setCarPlaque
@BindingAdapter("setCarPlaque")
fun TextView.setCarPlaque(plaque: String?) {
    text = plaque.toCarPlaque(tag.toString())
}
//-------------------------------------------------------------------------------------------------- setCarPlaque


//-------------------------------------------------------------------------------------------------- setElapseTime
@BindingAdapter("setElapseTime")
fun TextView.setElapseTime(dateTime: LocalDateTime?) {
    text = dateTime?.let {
        val now = LocalDateTime.now()
//        val date = LocalDateTime.parse(dateTime)
        val minuteBetween = Duration.between(dateTime, now).toMinutes()
        val hoursBetween = Duration.between(dateTime, now).toHours()
        if (minuteBetween < 60)
            context.getString(R.string.minuteAgo, minuteBetween.toString())
        else if (hoursBetween < 24)
            context.getString(R.string.hoursAgo, hoursBetween.toString())
        else {
            val daysBetween = Duration.between(dateTime, now).toDays()
            context.getString(R.string.dayAgo, daysBetween.toString())
        }
    } ?: run { "" }
}
//-------------------------------------------------------------------------------------------------- setElapseTime


//-------------------------------------------------------------------------------------------------- setWaitingTimeToTextView
@BindingAdapter("setWaitingTimeToTextView")
fun TextView.setWaitingTimeToTextView(time: Int) {
    text = if (time < 59)
        "$time دقیقه پیش "
    else {
        val h = time / 60
        "$h ساعت پیش "
    }
}
//-------------------------------------------------------------------------------------------------- setWaitingTimeToTextView


//-------------------------------------------------------------------------------------------------- setRequester
@BindingAdapter("setDriverName", "setCommuteTripName", "setStationName")
fun TextView.setDriverAndStation(driverName: String?, tripName: String?, stationName: String?) {
    text = context.getString(R.string.setThreeString, tripName, driverName, stationName)
}
//-------------------------------------------------------------------------------------------------- setRequester


//-------------------------------------------------------------------------------------------------- setRegisterStationStatus
fun TextView.setRegisterStationStatus(status: EnumStatus?) {
    text = status?.let {
        when (status) {
            EnumStatus.Pending -> context.resources.getString(R.string.pendingForAccept)
            EnumStatus.Confirmed -> context.resources.getString(R.string.confirmedByOfficial)
            EnumStatus.Reject -> context.resources.getString(R.string.reject)
        }
    } ?: run { "" }
}
//-------------------------------------------------------------------------------------------------- setRegisterStationStatus


//-------------------------------------------------------------------------------------------------- setMyStation
@BindingAdapter("setMyStation", "setArriveTime")
fun TextView.setMyStation(myStation: String?, arriveTime: String?) {
    text = getTextByValue(
        context.getString(R.string.myStation, myStation),
        arriveTime,
        context.getString(R.string.dash)
    )
}
//-------------------------------------------------------------------------------------------------- setMyStation


//-------------------------------------------------------------------------------------------------- setStartEndStation
@BindingAdapter("setOriginName", "setDestinationName")
fun TextView.setStartEndStation(originName: String?, destinationName: String?) {
    var title = ""
    originName?.let {
        title += "${context.getString(R.string.origin)} : $originName"
    }
    destinationName.let {
        title += " / ${context.getString(R.string.destination)} : $destinationName"
    }
    text = title
}
//-------------------------------------------------------------------------------------------------- setStartEndStation


//-------------------------------------------------------------------------------------------------- setFullDateTime
@BindingAdapter("setFullDateTime")
fun TextView.setFullDateTime(localDateTime: LocalDateTime?) {
    text = localDateTime?.let { date ->
        val solarDateModel = date.toSolarDate()
        solarDateModel?.getFullDate() ?: run { "" }
    } ?: run { "" }
}
//-------------------------------------------------------------------------------------------------- setFullDateTime


fun TextView.setPrice(price: Long) {
    val temp = "${price.split()} (ریال)"
    text = temp
}



