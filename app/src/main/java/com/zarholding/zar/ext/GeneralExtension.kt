package com.zarholding.zar.ext

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment
import com.zarholding.zar.model.data.database.entity.UserInfoEntity
import com.zarholding.zar.model.data.other.ShowImageModel
import com.zarholding.zar.model.data.response.PassengerModel
import com.zarholding.zar.model.data.response.address.AddressModel
import com.zarholding.zar.model.data.response.trip.TripStationModel

/**
 * Created by m-latifi on 01/04/2023.
 */

//-------------------------------------------------------------------------------------------------- hideKeyboard
fun Fragment.hideKeyboard() {
    view?.let { activity?.hideKeyboard(it) }
}

fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}
//-------------------------------------------------------------------------------------------------- hideKeyboard


//-------------------------------------------------------------------------------------------------- AddressModel.getAddress()
fun AddressModel.getAddress(): String {
    var addressText: String

    var county = county
    county = county?.replace("شهرستان", "")
    county = county?.replace("شهر", "")
    county = county?.trimStart()
    county = county?.trimEnd()
    if (county == null)
        county = ""

    var city = city
    city = city?.replace("شهرستان", "")
    city = city?.replace("شهر", "")
    city = city?.trimStart()
    city = city?.trimEnd()
    if (city == null)
        city = ""

    var town = town
    town = town?.replace("شهرستان", "")
    town = town?.replace("شهر", "")
    town = town?.trimStart()
    town = town?.trimEnd()
    if (town == null)
        town = ""

    addressText =
        if (city in county && county.isNotEmpty())
            county
        else if (county in city && city.isNotEmpty())
            city
        else
            "$county , $city"


    if (town !in addressText && town.isNotEmpty())
        addressText += " , $town"

    if (!neighbourhood.isNullOrEmpty())
        addressText += " , $neighbourhood"

    if (!suburb.isNullOrEmpty())
        addressText += " , $suburb"

    if (!district.isNullOrEmpty())
        addressText += " , $district"

    if (!residential.isNullOrEmpty())
        addressText += " , $residential"

    if (!road.isNullOrEmpty())
        addressText += " , $road"

    if (!building.isNullOrEmpty())
        addressText += " , $building"

    return addressText
}
//-------------------------------------------------------------------------------------------------- AddressModel.getAddress()


//-------------------------------------------------------------------------------------------------- ImageView.setAppIcon
@BindingAdapter("setAppComingSoon")
fun View.setAppComingSoon(link: Int) {
    visibility = if (link == 0)
        View.VISIBLE
    else
        View.GONE
}
//-------------------------------------------------------------------------------------------------- ImageView.setAppIcon


//-------------------------------------------------------------------------------------------------- toCarPlaque
fun String?.toCarPlaque(tag: String) =
    if (this.isNullOrEmpty())
        ""
    else {
        if (this.length != 8)
            ""
        else
            when (tag) {
                "number1" -> this.substring(0, 2)
                "alphabet" -> this.substring(2, 3)
                "number2" -> this.substring(3, 6)
                "city" -> this.substring(6, 8)
                else -> ""
            }
    }
//-------------------------------------------------------------------------------------------------- toCarPlaque


//-------------------------------------------------------------------------------------------------- getPassengers
fun List<PassengerModel>?.getPassengers(): String {
    var title = ""
    this?.let {
        for (i in this.indices)
            title += if (i == this.size - 1)
                this[i].value
            else
                "${this[i].value} - "
    }
    return title
}
//-------------------------------------------------------------------------------------------------- getPassengers


//-------------------------------------------------------------------------------------------------- getStation
fun List<TripStationModel>?.getStation(): String {
    var title = ""
    this?.let {
        for (i in it.indices)
            title += if (i + 1 < it.size)
                "${it[i].stationName} - "
            else
                it[i].stationName
    }
    return title
}
//-------------------------------------------------------------------------------------------------- getStation


//---------------------------------------------------------------------------------------------- getModelForShowImageProfile
fun UserInfoEntity?.getModelForShowImageProfile(bearerToken: String): ShowImageModel {
    val model = ShowImageModel("", null, null)
    this?.userName?.let {
        model.imageName = it
    }
    model.token = bearerToken
    return model
}
//---------------------------------------------------------------------------------------------- getModelForShowImageProfile
