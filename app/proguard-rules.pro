-keep class com.zarholding.zar.model.** { *; }
-keep class com.zarholding.zar.di.** { *; }
-keepclassmembers class * extends java.lang.Enum {
    <fields>;
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

##---------------Begin: proguard configuration for Gson  ----------
-keepattributes Signature
-keepattributes *Annotation*
-dontwarn sun.misc.**
-keep class com.google.gson.examples.android.model.** { <fields>; }
-keep class * extends com.google.gson.TypeAdapter
-keep class * implements com.google.gson.TypeAdapterFactory
-keep class * implements com.google.gson.JsonSerializer
-keep class * implements com.google.gson.JsonDeserializer
-keepclassmembers,allowobfuscation class * {
  @com.google.gson.annotations.SerializedName <fields>;
}
-keep,allowobfuscation,allowshrinking class com.google.gson.reflect.TypeToken
-keep,allowobfuscation,allowshrinking class * extends com.google.gson.reflect.TypeToken
##---------------End: proguard configuration for Gson  ----------



# Retrofit ----------------------------------------------------------------------------------------- Retrofit
-keepattributes Signature, InnerClasses, EnclosingMethod, Annotation
-keepattributes RuntimeVisibleAnnotations, RuntimeVisibleParameterAnnotations
-keepattributes AnnotationDefault
-keepclassmembers,allowshrinking,allowobfuscation interface * {
    @retrofit2.http.* <methods>;
}
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
-dontwarn javax.annotation.**
-dontwarn kotlin.Unit
-dontwarn retrofit2.KotlinExtensions
-dontwarn retrofit2.KotlinExtensions$*
-if interface * { @retrofit2.http.* <methods>; }
-keep,allowobfuscation interface <1>
-if interface * { @retrofit2.http.* <methods>; }
-keep,allowobfuscation interface * extends <1>
-keep,allowobfuscation,allowshrinking class kotlin.coroutines.Continuation
-if interface * { @retrofit2.http.* public *** *(...); }
-keep,allowoptimization,allowshrinking,allowobfuscation class <3>
-keep,allowobfuscation,allowshrinking class retrofit2.Response
-dontwarn okhttp3.logging.**
-keep class okhttp3.logging.** { *; }
-keepclassmembers class okhttp3.logging.** { *; }


# Dagger - Hilt ------------------------------------------------------------------------------------
#-dontwarn javax.annotation.**
#-keep class dagger.hilt.android.** { *; }
#-keep class dagger.hilt.internal.** { *; }
#-keep class dagger.hilt.processor.internal.** { *; }
#-keep class javax.inject.** { *; }
#-keep class javax.inject.** { *; }
#-keep class dagger.hilt.android.internal.** { *; }
#-keep class androidx.hilt.** { *; }


# Room ---------------------------------------------------------------------------------------------
#-keep class androidx.room.** { *; }
#-keepclassmembers class * {
#    @androidx.room.* <fields>;
#}


# SignalR ------------------------------------------------------------------------------------------
#-keep class microsoft.aspnet.signalr.client.** { *; }


# SLF4J --------------------------------------------------------------------------------------------
#-dontwarn org.slf4j.**
#-keep class org.slf4j.** { *; }

# osmdroid -----------------------------------------------------------------------------------------
#-keep class org.osmdroid.** { *; }


# Glide --------------------------------------------------------------------------------------------
#-keep public class * implements com.bumptech.glide.module.GlideModule
#-keep class * extends com.bumptech.glide.module.AppGlideModule {
# <init>(...);
#}
#-keep public enum com.bumptech.glide.load.ImageHeaderParser$** {
#  **[] $VALUES;
#  public *;
#}
#-keep class com.bumptech.glide.load.data.ParcelFileDescriptorRewinder$InternalRewinder {
#  *** rewind();
#}


# ZarCore and latifiVideoPlayer --------------------------------------------------------------------
#-keep class ir.latifi.library.** { *; }


# PowerSpinner -------------------------------------------------------------------------------------
#-keep class com.skydoves.powerspinner.** { *; }


# Quickie ------------------------------------------------------------------------------------------
#-keep class com.g00fy2.quickie.** { *; }


# QR Generator -------------------------------------------------------------------------------------
#-keep class com.github.alexzhirkevich.qrgenerator.** { *; }


# Google Play Services Location --------------------------------------------------------------------
#-keep class com.google.android.gms.** { *; }

# RangeSeekBar -------------------------------------------------------------------------------------
#-keep class com.mohammedalaa.range.** { *; }


# RoundedProgressBar -------------------------------------------------------------------------------
#-keep class com.mackhartley.roundedprogressbar.** { *; }


# Carousel RecyclerView ----------------------------------------------------------------------------
#-keep class com.sparrow007.carouselrecyclerview.** { *; }


# AnimationSwitch ----------------------------------------------------------------------------------
#-keep class ir.latifi.library.animationSwitch.** { *; }


# BlurView -----------------------------------------------------------------------------------------
#-keep class com.wonderkiln.blurkit.** { *; }


# Firebase Messaging -------------------------------------------------------------------------------
#-dontwarn com.google.firebase.messaging.**
#-keep class com.google.firebase.messaging.** { *; }

# JUnit --------------------------------------------------------------------------------------------
#-dontwarn junit.**
#-keep class junit.** { *; }
#-keepclassmembers class junit.** { *; }


# Espresso -----------------------------------------------------------------------------------------
#-dontwarn androidx.test.**
#-keep class androidx.test.** { *; }
#-keepclassmembers class androidx.test.** { *; }


# Core KTX -----------------------------------------------------------------------------------------
#-dontwarn androidx.core.**
#-keep class androidx.core.** { *; }
#-keepclassmembers class androidx.core.** { *; }


# AppCompat ----------------------------------------------------------------------------------------
#-dontwarn androidx.appcompat.**
#-keep class androidx.appcompat.** { *; }
#-keepclassmembers class androidx.appcompat.** { *; }


# Material -----------------------------------------------------------------------------------------
#-dontwarn com.google.android.material.**
#-keep class com.google.android.material.** { *; }
#-keepclassmembers class com.google.android.material.** { *; }


# ConstraintLayout ---------------------------------------------------------------------------------
#-dontwarn androidx.constraintlayout.**
#-keep class androidx.constraintlayout.** { *; }
#-keepclassmembers class androidx.constraintlayout.** { *; }

# Lifecycle ----------------------------------------------------------------------------------------
#-dontwarn androidx.lifecycle.**
#-keep class androidx.lifecycle.** { *; }
#-keepclassmembers class androidx.lifecycle.** { *; }

# Fragment KTX -------------------------------------------------------------------------------------
#-dontwarn androidx.fragment.**
#-keep class androidx.fragment.** { *; }
#-keepclassmembers class androidx.fragment.** { *; }


# Navigation ---------------------------------------------------------------------------------------
#-dontwarn androidx.navigation.**
#-keep class androidx.navigation.** { *; }
#-keepclassmembers class androidx.navigation.** { *; }


# Shimmer ------------------------------------------------------------------------------------------
#-dontwarn com.facebook.shimmer.**
#-keep class com.facebook.shimmer.** { *; }
#-keepclassmembers class com.facebook.shimmer.** { *; }


# XLog ---------------------------------------------------------------------------------------------
#-dontwarn com.elvishew.xlog.**
#-keep class com.elvishew.xlog.** { *; }
#-keepclassmembers class com.elvishew.xlog.** { *; }


# SDP Android---------------------------------------------------------------------------------------
#-dontwarn com.intuit.sdp.**
#-keep class com.intuit.sdp.** { *; }
#-keepclassmembers class com.intuit.sdp.** { *; }


# ExpandableLayout ---------------------------------------------------------------------------------
#-dontwarn com.github.cachapa.**
#-keep class com.github.cachapa.** { *; }
#-keepclassmembers class com.github.cachapa.** { *; }


# SwipeRefreshLayout -------------------------------------------------------------------------------
#-dontwarn androidx.swiperefreshlayout.**
#-keep class androidx.swiperefreshlayout.** { *; }
#-keepclassmembers class androidx.swiperefreshlayout.** { *; }


# Biometric KTX ------------------------------------------------------------------------------------
#-dontwarn androidx.biometric.**
#-keep class androidx.biometric.** { *; }
#-keepclassmembers class androidx.biometric.** { *; }


# Suppress warnings related to Bouncy Castle and Conscrypt -----------------------------------------
-dontwarn org.bouncycastle.jsse.BCSSLParameters
-dontwarn org.bouncycastle.jsse.BCSSLSocket
-dontwarn org.bouncycastle.jsse.provider.BouncyCastleJsseProvider
-dontwarn org.conscrypt.Conscrypt$Version
-dontwarn org.conscrypt.Conscrypt
-dontwarn org.openjsse.javax.net.ssl.SSLParameters
-dontwarn org.openjsse.javax.net.ssl.SSLSocket
-dontwarn org.openjsse.net.ssl.OpenJSSE